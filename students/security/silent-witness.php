<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/silent-witness.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1"></div>
                        				
                        <h2>Anonymous Reporting</h2>
                        				
                        <p>If you see a crime/disruptive behavior occurring on campus and would like to report
                           it anonymously, please fill out the form below.
                        </p>
                        				
                        <p>Information submitted via this form is reviewed by Security. If you have information
                           that requires an urgent response, please call your campus security immediately:
                        </p>
                        				
                        <div class="table-responsive">
                           					
                           <table class="table table table-striped add_bottom_30">
                              						
                              <tbody>
                                 							
                                 <tr>
                                    								
                                    <th scope="col" width="100">
                                       									<a href="#" title="Sort on “CAMPUS”">CAMPUS</a>
                                       								
                                    </th>
                                    								
                                    <th scope="col" width="100">
                                       									<a href="#" title="Sort on “PHONENUMBER”">PHONE NUMBER</a>
                                       								
                                    </th>
                                    							
                                 </tr>
                                 						
                              </tbody>
                              						
                              <tbody>
                                 							
                                 <tr>
                                    								
                                    <td>West</td>
                                    								
                                    <td>
                                       									<a href="tel:407-582-1000">407-582-1000</a>
                                       								
                                    </td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>East</td>
                                    								
                                    <td>
                                       									<a href="tel:407-582-2000">407-582-2000</a>
                                       								
                                    </td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>District Office</td>
                                    								
                                    <td>
                                       									<a href="tel:407-582-3000">407-582-3000</a><br>
                                       									or <a href="tel:407-582-1000">407-582-1000</a>
                                       								
                                    </td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Osceola</td>
                                    								
                                    <td>
                                       									<a href="tel:407-582-4000">407-582-4000</a>
                                       								
                                    </td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Winter Park</td>
                                    								
                                    <td>
                                       									<a href="tel:407-582-5000">407-582-6000</a><br>
                                       									or <a href="tel:407-582-2000">407-582-2000</a>
                                       								
                                    </td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Lake Nona</td>
                                    								
                                    <td>
                                       									<a href="tel:407-582-6000">407-582-7000</a><br>
                                       									or <a href="tel:407-582-4000">407-582-4000</a>
                                       								
                                    </td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>School of Public Safety</td>
                                    								
                                    <td>
                                       									<a href="tel:407-582-7000">407-582-8000</a>
                                       								
                                    </td>
                                    							
                                 </tr>
                                 						
                              </tbody>
                              					
                           </table>
                           					
                           <p><strong>(Optional)</strong> If you would like to be contacted regarding this report, please provide your contact
                              information at the bottom of the form below.
                           </p>
                           					
                           <hr class="styled_2">
                           					
                           <h3>Important User Information -- Please Read</h3>
                           					
                           <p>ALL INFORMATION WILL BE KEPT CONFIDENTIAL in accordance with Florida Statute (FSS)
                              119.
                           </p>
                           					
                           <p>In an effort to prevent system misuse, the submission of this form requires verification
                              by entering the two words displayed in the text box located above the "Submit" button.
                              This verification does not lead to the identification of the individual who submitted
                              it.<br><a href="http://net4.valenciacollege.edu/forms/security/silentWitness.cfm" target="_blank">Reporting Form</a></p>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/silent-witness.pcf">©</a>
      </div>
   </body>
</html>