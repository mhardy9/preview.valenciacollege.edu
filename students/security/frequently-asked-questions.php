<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/frequently-asked-questions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        
                        
                        
                        <div>
                           
                           
                           <h3><strong>Anonymous Reporting</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    <strong>I would like to report an issue, but I don�t want to be identified. What can I do?</strong>
                                    
                                 </p>
                                 
                                 <h5>�See Something, Say Something!�</h5> <br><br>
                                 
                                 <strong>If you would like to report an issue anonymously, please click on this link �Anonymous
                                    Reporting� and complete the form. If you have information that requires an urgent
                                    response, please call your campus Security Office immediately.</strong><br><br>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">West Campus</div>
                                          
                                          <div data-old-tag="td">407-582-1000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">East Campus�</div>
                                          
                                          <div data-old-tag="td">407-582-2000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">District Office���</div>
                                          
                                          <div data-old-tag="td"> 407-582-3000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Osceola����</div>
                                          
                                          <div data-old-tag="td">407-582-4000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Winter Park����</div>
                                          
                                          <div data-old-tag="td"> 407-582-6000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Lake Nona���</div>
                                          
                                          <div data-old-tag="td">407-582-7000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">School of Public Safety</div>
                                          
                                          <div data-old-tag="td">407-582-8000</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3><strong>Parking</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    <strong>Are parking permits required?</strong><br>
                                    Yes. Parking permits are required to park on any Valencia campus.<br>
                                    
                                    <strong>How much does a parking permit cost?</strong><br>
                                    <br>Parking permits are free of charge.
                                    
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>How do I get a parking permit?</li>
                                    
                                    <li>Log in to your Atlas account. Click on the following links and follow the directions:</li>
                                    
                                    <li>Student Tab</li>
                                    
                                    <li>Student Resources</li>
                                    
                                    <li>Personal Information</li>
                                    
                                    <li>Parking Permit</li>
                                    
                                    <li>Parking Permit Application</li>
                                    
                                 </ul>
                                 
                                 <p> Once you have printed out the Parking Permit application from Atlas, bring that form,
                                    along with a valid photo ID (Driver�s License, State ID Card, Passport, etc.), and
                                    the CURRENT vehicle registration to any campus Security Office.<br></p>
                                 
                                 <strong>Can I get my permit at any Security Office?</strong><br>
                                 Yes. All campus Security Offices can issue parking permits.<br><br>
                                 
                                 <strong>Can I register more than one vehicle for a parking permit?</strong><br>
                                 Yes. You may register multiple vehicles, however, each must be registered separately.
                                 Please print a separate application for each vehicle.<br><br>
                                 
                                 <strong>Is my parking permit valid on all Valencia campuses?</strong><br>
                                 Yes. You may park on any Valencia campus.<br><br>
                                 
                                 <strong>Do I have to affix my permit on my vehicle?</strong><br>
                                 Yes. Permits must be placed on either the lower rear windshield, driver�s side, or
                                 the left rear bumper. Faculty and Staff hang tags must be displayed on the rearview
                                 mirror, with the permit information facing out.<br><br>
                                 
                                 <strong>The car I drive is registered to another family member/friend. Is this okay?</strong><br>
                                 Yes. As long as you register the vehicle through your Atlas account and have the current
                                 registration, you can obtain the parking permit.<br><br>
                                 
                                 <strong>My permit is expired. What do I need to do to update it?</strong><br>
                                 Log in to your Atlas account and renew your permit application, print out the form
                                 and bring it, along with a valid photo ID (Driver�s License, State ID Card, Passport,
                                 etc.), and the CURRENT vehicle registration, to any campus Security Office to get
                                 a current permit.<br><br>
                                 
                                 <strong>I�ll be driving a different vehicle for a day. What do I do?</strong><br>
                                 Visit any campus Security Office to request a temporary parking permit.<br><br>
                                 
                                 <strong>I need to leave my vehicle on campus overnight. What do I do?</strong><br>
                                 Visit the campus Security Office to request an Overnight Parking pass. The pass must
                                 be displayed on the vehicle dashboard while parked on campus.<br><br>
                                 
                                 <strong>Where should I park?</strong><br>
                                 Students are to park within the <strong><u>WHITE LINED</u></strong> parking spaces.
                                 Faculty and Staff are to park within the <strong><u>YELLOW LINED</u></strong> parking spaces.
                                 Visitors should park in the spaces designated<strong><u>VISITOR PARKING</u></strong>.
                                 DO NOT park in spaces marked Handicap or <strong><u>RESERVED</u></strong> parking unless you are eligible to do so.<br><br>
                                 
                                 <strong>Do Parking Regulations only apply when the College is open?</strong><br>
                                 No. Parking at all Valencia campuses is enforced 24/7.<br><br>
                                 
                                 <strong>I am a UCF student. Do I need to get a Valencia parking permit?</strong><br>
                                 Yes. Please visit any campus Security Office to complete the application form. Please
                                 bring your UCF ID and the CURRENT vehicle registration.<br><br>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3><strong>Citations</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    <strong> I received a parking citation. What should I do?</strong><br>
                                    Contact the Security Office on the campus where you received the citation. You have
                                    the right to appeal any citation. If you choose to pay the fine, you may do so at
                                    any campus Business Office or <a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskoacc.P_ViewAcct" target="_blank">Online</a>. Citations must be paid within 15 calendar days from the date of issue to avoid late
                                    charges.<br><br>
                                    
                                    <strong>What is the appeal process for citations?</strong><br>
                                    You have the right to appeal any citation. The right to appeal is forfeited ten (10)
                                    calendar days from the date of issue. The appeal may be made in person or in writing
                                    by completing an Appeal Form which can be found by logging in to your Atlas account
                                    and visiting the Parking Permit section.
                                    The completed form must be returned or submitted to the Security Office of the campus
                                    on which you received the citation. The form and parking history will be reviewed
                                    by a Security supervisor. Based on the Supervisor�s determination, the appellant will
                                    receive a letter and notification to their Atlas email.
                                    <br><br>
                                    
                                    <strong>What happens if I don�t pay/appeal a citation?</strong>
                                    Failure to pay or appeal a citation may result in an administrative hold on the student�s
                                    account preventing the release of student records, including transcripts, grades,
                                    the ability to register for classes and graduating.<br>
                                    
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3><strong>Student/Faculty/Staff ID�s and Electronic Access Cards </strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    <strong> Are Valencia Students, Faculty and Staff required to get an ID?</strong><br>
                                    Yes. Valencia ID�s are mandatory for all Students, Faculty and Staff and may be obtained
                                    at any campus Security Office.<br><br>
                                    
                                    <strong>What do I need to bring with me to get an ID?</strong><br>
                                    You will need a government issued picture ID (Driver license, State ID card, Passport,
                                    etc.) and your Valencia ID number. Students must have registered and paid for classes.
                                    If you need a replacement ID within 2 years of issue there is a $5 charge payable
                                    at any Business Office or Answer Center for Winter Park.
                                    <br><br>
                                    
                                    <strong>I am an employee and my access card is not working. Who should I contact?</strong>
                                    Contact your campus Security Office to verify your card is still valid. Damaged/lost
                                    access cards require Dean/Department Head/Supervisor email request to the Security
                                    Office for replacement.<br><br>
                                    
                                    <strong>Will my access card work college-wide?</strong><br>
                                    No. Access is determined by your Dean/Department Head/Supervisor. It is assigned only
                                    to those areas where you work or have frequent need to visit.<br><br>
                                    
                                    <strong>How do I add access to areas not currently on my card?</strong>
                                    Your Dean/Department Head/Supervisor will need to send an email to the campus Security
                                    Office to request any additional access not currently on your card.<br><br>
                                    
                                    <strong>My access card no longer works in areas where I used to be able to access.</strong><br>
                                    Access is based on your assigned work area at the beginning of each semester. Contact
                                    your Dean/Department Head/Supervisor to verify your assigned area(s).<br><br>
                                    
                                    
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3><strong>General / Services</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    <strong>Can I smoke on campus?</strong>
                                    No. All Valencia College locations are smoke-free. This includes the use of electronic
                                    cigarettes. <br><br>
                                    
                                    <strong>My access card no longer works in areas where I used to be able to access.</strong><br>
                                    Access is based on your assigned work area at the beginning of each semester. Contact
                                    your Dean/Department Head/Supervisor to verify your assigned area(s).<br><br>
                                    
                                    <strong>Is it okay to carry my pocketknife on campus?</strong>
                                    No. Weapons are no allowed on campus.<br><br>
                                    
                                    <strong>What are your office hours?</strong><br>
                                    
                                 </p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Friday</div>
                                          
                                          <div data-old-tag="td">7:00 am - 10:00 pm</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Saturday</div>
                                          
                                          <div data-old-tag="td">7:00 am - 2:00 pm </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 <br>
                                 Please visit your campus webpage to determine if there are different hours.<br><br>
                                 
                                 <strong>It is late and I am afraid to walk to my vehicle alone. What should I do?</strong><br><br>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">West Campus</div>
                                          
                                          <div data-old-tag="td">407-582-1000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">East Campus�</div>
                                          
                                          <div data-old-tag="td">407-582-2000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">District Office���</div>
                                          
                                          <div data-old-tag="td"> 407-582-3000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Osceola����</div>
                                          
                                          <div data-old-tag="td">407-582-4000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Winter Park����</div>
                                          
                                          <div data-old-tag="td"> 407-582-6000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Lake Nona���</div>
                                          
                                          <div data-old-tag="td">407-582-7000</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">School of Public Safety</div>
                                          
                                          <div data-old-tag="td">407-582-8000</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 <br><br>
                                 
                                 <strong>I locked my keys in my car/my battery is dead/I have a flat tire. Can Security help
                                    me?</strong><br>
                                 Yes. Security has the tools to assist you with unlocking your vehicle, jumping a dead
                                 battery or putting air in a flat tire.<br><br>
                                 
                                 <strong>Can we reserve parking spaces for a conference we are hosting?</strong><br>
                                 Yes. Contact the campus Security Office where your event will be held. Please send
                                 an email request in advance listing: Date, time, location and number of spaces required.<br><br>  
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <center>Safety, Security &amp; Risk Management
                           is dedicated to providing a safe, secure and welcoming environment for our Students,
                           Faculty, Staff and Visitors. We strive to provide excellent customer service while
                           implementing the latest technologies, insuring that our working and learning environment
                           is the
                           safest it can be.
                        </center>
                        <br><center>
                           <h3>"See Something, Say Something"</h3>
                        </center>
                        
                        <center>
                           
                           
                           
                        </center>             
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Criminal Justice Institute 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-8000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          District Office 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-3000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">East Campus Bldg 5 Room 220</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-7000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Room 109</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osc_security@valenciacollege.edu">osc_security@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Poinciana Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">1-103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-6500</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB Room 170</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:drodriguez225@valenciacollege.edu">drodriguez225@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-6000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/frequently-asked-questions.pcf">©</a>
      </div>
   </body>
</html>