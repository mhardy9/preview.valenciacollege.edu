<?pcf-stylesheet path="/_resources/xsl/home.xsl" title="Home Page" extension="php"?>
<!DOCTYPE HTML>
<html lang="en-US">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content=", college, school, educational">
		<meta name="description" content="Home Page | Valencia College">
		<meta name="author" content="Valencia College">
		<title>Home Page | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?>
		<script>
			var page_url="https://preview.valenciacollege.edu/static_index.php";
		</script>
		<script>
			FontAwesomeConfig = { searchPseudoElements: true };
		</script></head>
	<body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
<div id="preloader">
<div class="pulse"></div>
</div>
<!-- Pulse Preloader --> <!-- Return to Top --><a id="return-to-top" href="/javascript:" aria-label="Return to top"> </a> <!-- <header class="valencia-alert"><iframe></header> --> <!-- ChromeNav Header ================================================== -->
<div class="header header-chrome">
<div class="container"><nav>
<div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
</nav></div>
</div>
<!-- SiteMenu Header ================================================== -->
<div class="header header-site">
<div class="container">
<div class="row"><nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
<div class="site-menu"><a id="close_in" class="open_close" href="#" aria-label="close"> </a> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?></div>
</nav></div>
</div>
</div>
<div class="sub-header bg-home">
<div id="intro-txt"></div>
</div>
<div class="container-fluid">
<div class="container margin-60">
<div class="row">
<div class="col-sm-4">
<div class="trending"><img class="img-responsive" src="/_resources/img/home/trending-student-services.jpg" alt="Valencia image description" />
<div class="col-sm-6"><a class="link-underlined" href="#" target="">Advising &amp; Counseling</a></div>
<div class="col-sm-6"><a class="link-underlined" href="#" target="">Answer Center</a></div>
<div class="col-sm-6"><a class="link-underlined" href="#" target="">Assessments</a></div>
<div class="col-sm-6"><a class="link-underlined" href="#" target="">Career Center</a></div>
<div class="col-sm-6"><a class="link-underlined" href="#" target="">Financial Aid Services</a></div>
<div class="col-sm-6"><a class="link-underlined" href="#" target="">Records/Transcripts</a></div>
<div class="col-sm-12 v-red"><strong>&gt;<a href="#" target=""> See More Student Services</a></strong></div>
</div>
</div>
<div class="col-sm-4 mob_spc_1">
<div class="deadlines">
<h4 style="text-transform: uppercase; font-weight: 600;">Important DATES</h4>
<div class="row">
<div class="date_stacked col-sm-2 col-xs-2">08&nbsp;<span>MAR</span></div>
<div class="col-sm-10 col-xs-10">Bachelor's Degree in Business Information Session</div>
</div>
<hr />
<div class="row">
<div class="date_stacked col-sm-2 col-xs-2">09 <span>MAR</span></div>
<div class="col-sm-10 col-xs-10">Veteran's Affairs Deferral Deadline</div>
</div>
<hr />
<div class="row">
<div class="date_stacked col-sm-2 col-xs-2">09&nbsp;<span>MAR</span></div>
<div class="col-sm-10 col-xs-10">Spring Break</div>
</div>
<hr />
<div class="row">
<div class="date_stacked col-sm-2 col-xs-2">30&nbsp;<span>MAR</span></div>
<div class="col-sm-10 col-xs-10">Withdrawal Deadline - "W" Grade (11:59 p.m.)</div>
</div>
<hr />
<div class="row">
<div class="date_stacked col-sm-2 col-xs-2">22&nbsp;<span>APR</span></div>
<div class="col-sm-10 col-xs-10">Day and Evening Classes End</div>
</div>
</div>
</div>
<div class="col-sm-4 mobile_spacing_30"><!--BEGIN ACTION PALETTE-->
<div class="box_style_announcement_sidebar bg-grey">
<div class="action-palette">
<div>
<div><a class="button-action home" href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.p_disploginnew?in_id=&amp;cpbl=&amp;newid=">Apply Now </a></div>
<div><a class="button-action_outline home" href="http://net4.valenciacollege.edu/promos/internal/request-info.cfm">Request Information </a></div>
<div><a class="button-action_outline home" href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Visit Valencia </a></div>
</div>
</div>
<div class="wrapper_indent box_style_1 home">
<h4>Valencia College will add two new bachelor's degrees in 2018.</h4>
<div>
<p><a href="http://net1.valenciacollege.edu/future-students/degree-options/bachelors/">Business and Organizational Leadership</a> (Summer 2018)<br /><a href="http://net1.valenciacollege.edu/future-students/degree-options/bachelors/">Nursing</a>&nbsp;(Fall 2018)</p>
</div>
<div></div>
</div>
</div>
<!--END ACTION PALETTE--></div>
</div>
</div>
</div>
<div class="hidden-xs"><a href="#3"><img class="img-responsive fullwidth" src="/_resources/img/home/valencia-info-graphic.jpg" alt="Infographic" /></a></div>
<div id="infographic-detail" class="bg-sky visible-xs container margin-30 wrapper_indent">
<h3>ABOUT VALENCIA COLLEGE</h3>
<p>At Valencia College, you'll get a quality education at a price you can afford. We offer the same education as a state university, only at about half the cost. And, with smaller campuses and classes, you'll get more support along the way.</p>
<h3>96% PLACEMENT RATE</h3>
<p>With an average annual salary of about $41,000</p>
<h3>100+ PROGRAMS</h3>
<p>Including two and four-year degrees</p>
<h3>6 CAMPUS LOCATIONS</h3>
<p>Take classes day, night or weekend at any of our six campuses, or online</p>
<h3>1 BILLION DOLLARS A YEAR</h3>
<p>Valencia's economic impact on the region</p>
</div>
<div class="container-fluid container_gray_bg">
<div class="container margin-60">
<div class="row">
<div class="col-sm-3">
<h4>Virtual Tour</h4>
<p>Don't have time to visit a Valencia Campus? Take our virtual tour to see all that Valencia College has to offer.</p>
<a class="v-red" href="http://preview.valenciacollege.edu/future-students/visit-valencia/"> View All Locations </a></div>
<div class="col-sm-3"><a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank"><img class="img-responsive fullwidth" src="/_resources/img/home/virtual-tour-east.jpg" alt="Virtual Tour East Campus" /></a></div>
<div class="col-sm-3"><a href="http://www.youvisit.com/tour/valencia/osceola?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank"><img class="img-responsive fullwidth" src="/_resources/img/home/virtual-tour-osceola.jpg" alt="Virtual Tour Osceola Campus" /></a></div>
<div class="col-sm-3"><a href="http://www.youvisit.com/tour/valencia/valenciawest?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank"><img class="img-responsive fullwidth" src="/_resources/img/home/virtual-tour-west.jpg" alt="Virtual Tour West Campus" /></a></div>
</div>
</div>
</div>
<div class="container margin-60">
<div id="tabs" class="tabs"><nav>
<ul>
<li><a href="#section-1"><i class="far fa-calendar">&nbsp;</i> Events</a></li>
<li><a href="#section-2"><i class="fal fa-newspaper">&nbsp;</i> News</a></li>
<li><a href="#section-3"><i class="far fa-users">&nbsp;</i> Social</a></li>
</ul>
</nav>
<div class="content">
<section id="section-1">
<div class="row list_news_tabs">
<div class="col-sm-4 col-sm-4">
<p><a href="http://events.valenciacollege.edu/event/janet_onofrey_paintings"> <img class="img-responsive" src="/_resources/img/events/event-1.jpg" alt="Spring Dance Concert" /></a></p>
<h3><a href="http://events.valenciacollege.edu/event/janet_onofrey_paintings">Spring Dance Concert</a></h3>
<p class="event_date">January 20, 2017 12:01 am</p>
<p>Dr. Michael Nagler, writer of 'The Search for a Nonviolent Future', graced Valencia students, and faculty with guided meditation, workshop and lecture.</p>
<div class="row">
<p class="event_location">east campus</p>
<p class="event_ticket">get tickets</p>
</div>
<div class="align_article"></div>
<hr />
<div class="row list_news_tabs_more">
<div class="tags_normal col-sm-6">valencia arts</div>
<div class="col-sm-6"></div>
</div>
</div>
<div class="col-sm-4 col-sm-4">
<p><a href="http://events.valenciacollege.edu/event/solaris_art_exhibit"> <img class="img-responsive" src="/_resources/img/events/event-2.jpg" alt="2017 Commencement Ceremony" /></a></p>
<h3><a href="http://events.valenciacollege.edu/event/solaris_art_exhibit">2017 Commencement Ceremony</a></h3>
<p class="event_date">May 7, 2017 12:01 am</p>
<p>Valencia students will receive their associate degrees when Valencia College holds its 46th spring commencement at the University of Central Florida’s CFE Arena.</p>
<div class="row"><span class="event_location" style="min-height: 200p;">UCF CFE ARENA</span></div>
<div class="align_article"></div>
<hr />
<div class="row list_news_tabs_more">
<div class="tags_normal col-sm-6">student affairs</div>
<div class="col-sm-6"></div>
</div>
</div>
<div class="col-sm-4 col-sm-4">
<p><a href="http://events.valenciacollege.edu/event/advanced_manufacturing_open_house"> <img class="img-responsive" src="/_resources/img/events/event-3.jpg" alt="STEM Education Day" /></a></p>
<h3><a href="http://events.valenciacollege.edu/event/advanced_manufacturing_open_house">ZORA! STEM Education Day</a></h3>
<p class="event_date">March 23, 2017 12:01 am</p>
<p>The 2017 Zora! Festival’s education day initiative will work to promote STEM learning with students from public schools.</p>
<div class="row"><span class="event_location">lake nona campus</span></div>
<div class="align_article"></div>
<hr />
<div class="row list_news_tabs_more">
<div class="tags_normal col-sm-6">student activities</div>
<div class="col-sm-6"></div>
</div>
</div>
</div>
<!--End row --></section>
<section id="section-2">
<div class="container-fluid">
<div class="container margin-60">
<div class="row">
<div class="col-sm-4">
<div class="list_news_tabs">
<p><a title="" href="/" target=""><img class="img-responsive" src="/_resources/img/news_1_thumb.jpg" alt="News Story 1 Picture" /></a></p>
<span class="date_published">Thursday, June 29, 2017</span>
<h3><a title="" href="/" target="">Ask an Alum: Orange County Commissioner Emily Bonilla</a></h3>
<p>Last December, Orange County swore in two newly-elected commissioners to its governing board, and as it turns out, both are Valencia alumnae. We can’t say we’re surprised given the decades of exemplary work by Valencians</p>
<a class="button small" title="" href="/" target="">Read more</a></div>
</div>
<div class="col-sm-4">
<div class="list_news_tabs">
<p><a title="" href="/" target=""><img class="img-responsive" src="/_resources/img/news_2_thumb.jpg" alt="News Story 2 Picture" /></a></p>
<span class="date_published">Tuesday, June 6, 2017</span>
<h3><a title="" href="/" target="">Taking a Turn for the Better: Inmates Graduate from Valencia’s Construction Training Program</a></h3>
<p>The tears started flowing early at the recent graduation ceremony at the Orange County Jail.</p>
<a class="button small" title="" href="/" target="">Read more</a></div>
</div>
<div class="col-sm-4">
<div class="list_news_tabs">
<p><a title="" href="/" target=""><img class="img-responsive" src="/_resources/img/news_3_thumb.jpg" alt="News Story 3 Picture" /></a></p>
<span class="date_published">Thursday, June 29, 2017</span>
<h3><a title="" href="/" target="">Madelyn Young: 1973 Valencia Graduate Recalls College’s Early Days</a></h3>
<p>Madelyn Young had a four-year, full academic scholarship to Florida State University in 1971 when she graduated from Jones High School.</p>
<a class="button small" title="" href="/" target="">Read more</a></div>
</div>
</div>
</div>
</div>
</section>
<section id="section-3">
<div class="container-fluid">
<div class="container margin-60">
<div class="row">
<div class="col-sm-4">
<div class="list_news_tabs">
<p><a title="" href="/" target=""><img class="img-responsive" src="/_resources/img/event_1_thumb.jpg" alt="Social Story 1 Picture" /></a></p>
<span class="date_published">20 Agusut 2015</span>
<h3><a title="" href="/" target="">Next students meeting</a></h3>
<p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.</p>
<a class="button small" title="" href="/" target="">Read more</a></div>
</div>
<div class="col-sm-4">
<div class="list_news_tabs">
<p><a title="" href="/" target=""><img class="img-responsive" src="/_resources/img/event_2_thumb.jpg" alt="Social Story 2 Picture" /></a></p>
<span class="date_published">20 Agusut 2015</span>
<h3><a title="" href="/" target="">10 October Open day</a></h3>
<p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.</p>
<a class="button small" title="" href="/" target="">Read more</a></div>
</div>
<div class="col-sm-4">
<div class="list_news_tabs">
<p><a title="" href="/" target=""><img class="img-responsive" src="/_resources/img/event_3_thumb.jpg" alt="Social Story 3 Picture" /></a></p>
<span class="date_published">20 Agusut 2015</span>
<h3><a title="" href="/" target="">Photography workshop</a></h3>
<p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.</p>
<a class="button small" title="" href="/" target="">Read more</a></div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
<?php/* include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/homepage-featured-stories.inc"); */?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
<div id="hidden" style="display: none;"><a id="de" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/index.pcf" rel="nofollow">©</a></div><a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/static-index.php">©</a></body>
</html>
