<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/parking.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_1">
                           
                           <h2>Parking Regulations</h2> 
                           
                           
                           
                           
                           
                           <p>Regulations are enforced by a process of monetary fines and academic discipline.</p>
                           
                           <p>Repeated violations may result in loss of vehicle privileges, vehicle impoundment
                              and suspension.
                           </p>
                           
                           
                           <p>The purpose  of these  regulations  is to  maintain  a safe  and orderly flow of 
                              traffic and  college business  and to  provide  parking in  support of  these functions
                              within  the  limits  of available  space.
                           </p>
                           
                           <p>Students and employees  are  required  to observe  these regulations as  a condition
                              of  their attendance  and employment at  Valencia College.
                           </p>
                           
                           <p>The college will assume  no responsibility  for  loss  or damage  to any vehicle,
                              or its contents, while operated or  parked  on the property  of Valencia  College.
                           </p>
                           
                           <p>Parking decals  give  holders the privilege of  parking on campus, but regretfully
                              we cannot  guarantee the holder a parking space. A lack  of space is not considered
                              a valid excuse for violation of  any  parking regulation.
                           </p>
                           
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <ol>
                                    
                                    <li>All motor vehicles operated by  students and  employees in  connection  with attendance
                                       or  employment  at Valencia  College MUST  display a current parking  decal.
                                       <p>
                                          <strong>Decals must be affixed to the rear bumper or the lower rear windshield on the driver's
                                             side of the vehicle. Decals must be affixed on motorcycles and mopeds in a place where
                                             they can be observed, with the decal number clearly visible.</strong>
                                          
                                       </p>
                                       
                                    </li>
                                    
                                    <li>Parking  decals  for  students are valid  for  a one (1) year  period: January through
                                       December; faculty  and staff  are valid for a two (2) year period.
                                    </li>
                                    
                                    <li>Parking  decals  must be obtained at  the  time of  registration for classes.  Lost,
                                       stolen, or  damaged decals must  be replaced.  Decals are issued  at no  cost to 
                                       students and  employees.
                                    </li>
                                    
                                    <li>Emergency  or temporary  permits for short  term periods  may  be obtained at  the
                                       Campus  Security Office.
                                    </li>
                                    
                                    <li>The “decal” authorizes  the  person, not the vehicle.</li>
                                    
                                 </ol>
                                 <br>
                                 
                                 If F/S vehicles are driven  on campus by  family  or friends  who  are  not  faculty
                                 or  staff,  they  must park in general  student  parking.
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">Parking Tag</div>
                                          
                                          <div data-old-tag="th">Parking Decals</div>
                                          
                                       </div>
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          
                                          <div data-old-tag="th"><a href="documents/Vertical-Hallway-Sign-for-Decal.pdf" target="_blank">Download Parking Decal Flyer</a></div>
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                 </div>  
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <h3><strong>Registration of Vehicles: Parking Permits and Decals</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ol>
                                       
                                       <li>All motor vehicles operated by  students and employees in  connection  with attendance
                                          or  employment  at Valencia  College MUST  display a current parking  decal.
                                          <p>
                                             <strong>Decals must be affixed to the rear bumper or the lower rear windshield on the driver's
                                                side of the vehicle. Decals must be affixed on motorcycles and mopeds in a place where
                                                they can be observed, with the decal number clearly visible.</strong>
                                             
                                          </p>
                                          
                                       </li>
                                       
                                       <li>Parking  decals  for  students are valid for a one (1) year  period: January through
                                          December; faculty  and staff  are valid for a two (2) year period.
                                       </li>
                                       
                                       <li>Parking  decals  must be obtained at the time of registration for classes.  Lost,
                                          stolen, or  damaged decals must  be replaced.  Decals are issued  at no cost to students
                                          and  employees.
                                       </li>
                                       
                                       <li>Emergency  or temporary  permits for short term periods  may  be obtained at  the
                                          Campus  Security Office.
                                       </li>
                                       
                                       <li>The “decal” authorizes the person, not the vehicle.</li>
                                       
                                    </ol>
                                    <br>
                                    
                                    If F/S vehicles are driven  on campus by family or friends who are not faculty or
                                    staff, they must park in general  student  parking.
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="th">Parking Tag</div>
                                             
                                             <div data-old-tag="th">Parking Decals</div>
                                             
                                          </div>
                                          
                                          
                                          <div data-old-tag="tr">
                                             
                                             
                                             <div data-old-tag="td"><a href="documents/Vertical-Hallway-Sign-for-Decal.pdf" target="_blank">Download Parking Decal Flyer</a></div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>  
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <h3><strong>Parking Regulations</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ol>
                                       
                                       <li>Student holders  of parking  decals  may  park  only  in spaces designated with  white
                                          stripes.
                                       </li>
                                       
                                       <li>Disabled  students may park in  the  white  (General Parking)  spaces  or in  the
                                          Disabled  Parking  Only spaces if  the  vehicle  bears a valid State disabled parking
                                          permit. Spaces  excluded  from  student  parking are Faculty/Staff  areas,  Loading
                                          Zones, Reserved  spaces, Visitor  parking and  Disabled  when  not  authorized.
                                       </li>
                                       
                                       <li>Parking  is prohibited on  sidewalks, grass  areas and  lawns,  wheelchair ramps,
                                          along  parking lot curbs, or  in areas  temporarily restricted  under emergency conditions.
                                       </li>
                                       
                                       <li>In the event  a vehicle  must be parked illegally  to await repairs or  fuel, Campus
                                          Security  should  be notified  immediately.
                                       </li>
                                       
                                       <li>Motorcycles and  motor  scooters  (including  mopeds) may be parked  only ï¿½in spaces
                                          provided for such  vehicles.
                                       </li>
                                       
                                       <li>Trailers,  motor  homes,  and similar recreational  vehicles  will  not  be parked
                                          anywhere on  campus  without prior  approval  of the Security  Supervisor  or designee.
                                       </li>
                                       
                                       <li>Vehicles  parked  in such  a manner as to obstruct  another vehicle,  or the flow
                                          of  vehicular  traffic,  may  be moved  or impounded  at the owner’s expense.
                                       </li>
                                       
                                       <li>Permission  to leave a vehicle on  campus overnight must  be obtained from the Security
                                          Office.  Vehicles  left parked on  campus  in excess  of 48  hours will be towed as
                                          abandoned at the  owner’s  or operator’s expense.
                                       </li>
                                       
                                    </ol>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ol>
                                       
                                       <li>All motor vehicles operated by  students and  employees in  connection  with attendance
                                          or  employment  at Valencia  College MUST  display a current parking  decal.
                                          <p>
                                             <strong>Decals must be affixed to the rear bumper or the lower rear windshield on the driver's
                                                side of the vehicle. Decals must be affixed on motorcycles and mopeds in a place where
                                                they can be observed, with the decal number clearly visible.</strong>
                                             
                                          </p>
                                          
                                       </li>
                                       
                                       <li>Parking  decals  for  students are valid  for  a one (1) year  period: January through
                                          December; faculty  and staff  are valid for a two (2) year period.
                                       </li>
                                       
                                       <li>Parking  decals  must be obtained at  the  time of  registration for classes.  Lost,
                                          stolen, or  damaged decals must  be replaced.  Decals are issued  at no  cost to 
                                          students and  employees.
                                       </li>
                                       
                                       <li>Emergency  or temporary  permits for short  term periods  may  be obtained at  the
                                          Campus  Security Office.
                                       </li>
                                       
                                       <li>The “decal” authorizes  the  person, not the vehicle.</li>
                                       
                                    </ol>
                                    <br>
                                    
                                    If F/S vehicles are driven  on campus by  family  or friends  who  are  not  faculty
                                    or  staff,  they  must park in general  student  parking.
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="th">Parking Tag</div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>  
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 
                                 <h3><strong>Moving Traffic Violations</strong></h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <ol>
                                          
                                          <li>Speed Limits:</li>
                                          
                                          <ol type="a">
                                             
                                             <li> Perimeter  Roads - 25 MPH</li>
                                             
                                             <li>Parking Lots - 10 MPH</li>
                                             
                                          </ol>
                                          
                                          <li>Observance of Entrance and Exit lanes will be strictly enforced.</li>
                                          
                                       </ol>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <h3><strong>Visitor Parking</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Parking for visitors is provided in designated lots. Vehicles operated by  Valencia
                                          College  students or employees are prohibited from parking  in Visitors  Lots  at
                                          all times
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <h3><strong>Fines</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Fines imposed  for  violations  must be paid within  15 days  from  date of  issue
                                       of citation. Amounts  due after the delinquent date  are  double the original amount
                                       of  fine.
                                    </p>
                                    
                                    
                                    <p><strong>Parking Fines:</strong></p>
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Disabled Spaces, Ramps</div>
                                             
                                             <div data-old-tag="td">$250.00</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Fire Lane</div>
                                             
                                             <div data-old-tag="td">$40.00</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Roadways, Curb Areas</div>
                                             
                                             <div data-old-tag="td">$10.00</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Visitors, Special (Faculty/Staff)</div>
                                             
                                             <div data-old-tag="td">$10.00</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Grass Areas</div>
                                             
                                             <div data-old-tag="td">$10.00</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Motorcycle Areas</div>
                                             
                                             <div data-old-tag="td">$10.00</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">No Current Decal Displayed</div>
                                             
                                             <div data-old-tag="td">$10.00</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Other Violations as Listed in these Regulations</div>
                                             
                                             <div data-old-tag="td">$10.00</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Improper Display of Decal</div>
                                             
                                             <div data-old-tag="td">$10.00</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <h3><strong>Student ID Cards</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Your student ID card can be obtained in the Security office on any Campus once you
                                       have registered and paid for your classes. You will need your student ID card to access
                                       campus services such as the Library and Testing Center. The first student ID card
                                       is free. There is a $5 fee for a replacement card. Student IDs can be used for library
                                       access, testing purposes and discounts in the community.
                                       
                                    </p>
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="th">Student ID Flyer</div>
                                             
                                          </div>
                                          
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="th"><a href="documents/Vertical-Hallway-Sign-for-Student-ID.pdf" target="_blank">Download Student ID Flyer</a></div>
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div><strong>Student ID</strong></div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><strong>Employee ID</strong></div>
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div><strong>Law Enforcement ID</strong></div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><strong>Vendor/Contractor ID</strong></div>
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/parking.pcf">©</a>
      </div>
   </body>
</html>