<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Safety, Security &amp; Risk Management">
      <meta name="Keywords" content="security, risk management, run hide fight, parking, id, security campus, campus , security, valencia college security, school, active threat, parking permits, witness, accident, report, reporting an accident, 911, emergency"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_landing_1">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Security</li>
            </ul>
         </div>
      </div>
      <main role="main">
         <div class="container_gray_bg">
            <div class="container margin-60">
               
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="main-title">
                           <td class="main-title">
                              								<img alt="Security Badge" class="img-responsive" src="/students/security/images/security-logo.png" style="display: block; margin-left: auto; margin-right: auto;"> <strong><font style="color:#BF311A">See Something, Say Something"</font></strong><br>
                              								<br>
                              								
                              <h2>Safety, Security &amp; Risk Management</h2>
                              								
                              <p>We are dedicated to providing a safe, secure and welcoming environment for our Students,
                                 Faculty, Staff and Visitors.
                              </p>
                              							
                           </td>
                        </div>
                        				
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="/students/security/compliance-and-safety.php" target=""><i class="far fa-users"></i><h3>Compliance and Safety</h3>
                                 <p>Responsible for regulatory compliance at the federal, state, &amp; local levels.</p></a>
                              							
                           </div>
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="/students/security/emergency-contact-numbers.php" target=""><i class="far fa-exclamation-triangle"></i><h3>Emergency Numbers</h3>
                                 <p>A complete list of Valencia College's emergency contact numbers</p></a>
                              							
                           </div>
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="/students/security/frequently-asked-questions.php" target=""><i class="far fa-question-circle"></i><h3>Frequently Asked Questions</h3>
                                 <p>A complete guide of frequently asked questions for all of your needs.</p></a>
                              							
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="/students/security/parking.php" target=""><i class="far fa-id-badge"></i><h3>Parking &amp; ID's</h3>
                                 <p>Students &amp; employees are required to observe these regulations as a condition while
                                    at Valencia College.
                                 </p></a>
                              							
                           </div>
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="/students/security/reporting.php" target=""><i class="far fa-inbox-in"></i><h3>Reporting an Incident</h3>
                                 <p>Dissemination of information is the key to educating our college community about the
                                    occurrence of crime on campus
                                 </p></a>
                              							
                           </div>
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="/students/security/victim-assistance-agencies.php" target=""><i class="far fa-life-ring"></i><h3>Victim Assistance Agencies</h3>
                                 <p>Contact us, if you or someone you know is the victim of a crime or a violation of
                                    the Student Conduct Code.
                                 </p></a>
                              							
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="https://atlas.valenciacollege.edu/" target="_blank"><i class="far fa-exclamation"></i><h3>Emergency Notifications</h3>
                                 <p>To receive emergency notifications via text or email log into Atlas &amp; click on Valencia
                                    Alerts in the My Atlas tab.
                                 </p></a>
                              							
                           </div>
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="/students/security/annual-security-report.php" target=""><i class="far fa-file-alt"></i><h3>Annual Security Report</h3>
                                 <p>Our report in compliance with the Jeanne Clery Disclosure of Campus Security Policy
                                    &amp; Campus Crime Statistics Act.
                                 </p></a>
                              							
                           </div>
                           <div class="col-md-4 col-sm-4">
                              								<a class="box_feat" href="/students/security/personal-safety-information.php" target=""><i class="far fa-shield"></i><h3>Personal Safety Information</h3>
                                 <p>For your safety &amp; the safety of others please read &amp; become familiar with these policies.</p></a>
                              							
                           </div>
                        </div><br>
                        				
                        <p style="text-align: center;"><a class="button-outline large" href="/students/security/valencia-college-safety-mobile-app.php">Valencia College Safety App <i class="fas fa-mobile-alt"></i></a></p>
                        <!--End container -->
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <section class="grid">
                  <div class="container margin-60">
                     <div class="main-title">
                        					
                        <h2>Response to an Active Threat</h2>
                        					
                        <p>The Security Department believes that the dissemination of information is the key
                           to educating our college community about the occurrence of crime on campus. Students
                           and employees are encouraged to report all incidents of crime.
                        </p>
                        				
                     </div>
                     <div class="row">
                        					
                        <ul class="magnific-gallery">
                           <li>
                              <figure><img src="/students/security/images/security-gallery1-thumb.png" alt="Run, Hide Fight Presentation screenshot"><figcaption>
                                    <div class="caption-content"><a href="https://www.youtube.com/watch?v=5mzI_5aj4Vs" class="video_pop" title="Run, Hide Fight Presentation"><i class="far fa-film"></i><p> Run, Hide Fight Presentation 
                                             									
                                             								
                                          </p></a></div>
                                 </figcaption>
                              </figure>
                           </li>
                           <li>
                              <figure><img src="/students/security/images/security-gallery2-thumb.png" alt="RUN. HIDE. FIGHT. screenshot"><figcaption>
                                    <div class="caption-content"><a href="https://www.youtube.com/watch?v=5mzI_5aj4Vs" class="video_pop" title="RUN. HIDE. FIGHT."><i class="far fa-film"></i><p> RUN. HIDE. FIGHT. 
                                             									
                                             								
                                          </p></a></div>
                                 </figcaption>
                              </figure>
                           </li>
                           <li>
                              <figure><img src="/students/security/images/security-gallery3-thumb.png" alt="Surviving an Active Shooter screenshot"><figcaption>
                                    <div class="caption-content"><a href="https://www.youtube.com/watch?v=9Z9zkU--FLQ" class="video_pop" title="Surviving an Active Shooter"><i class="far fa-film"></i><p> Surviving an Active Shooter 
                                             									
                                             								
                                          </p></a></div>
                                 </figcaption>
                              </figure>
                           </li>
                        </ul>
                        				
                     </div>
                  </div>
               </section>
               	
            </div>
         </div>
         <div class="container margin-60"></div>
      </main><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/index.pcf">©</a>
      </div>
   </body>
</html>