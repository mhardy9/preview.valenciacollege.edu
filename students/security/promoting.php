<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/promoting.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="box_style_1">
                           
                           <h2>Personal Safety Information</h2>
                           
                           
                           
                           
                           
                           <p>College policies exist for the purpose of providing a basis for quality campus life
                              and for setting a standard of conduct for all members of the college community conducive
                              to achieving the objectives of the college. Stated below are excerpts from college
                              policies as they relate to issues of safety and security. For your safety and the
                              safety of others please read and become familiar with these policies. 
                           </p>
                           
                           
                           
                           <div>
                              
                              
                              <h3><strong>Severe Weather Policy</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>In order to carry out the mission of the Valencia College Security Department, which
                                       is to protect the public and provide for the safety, security and welfare of staff,
                                       student population and visitors, it is essential sever weather procedures to be established
                                       for the college.
                                    </p>
                                    
                                    <p> For more information, please review the <a href="documents/Valencia-College-Severe-Weather-Policy.pdf" target="_blank">Valencia College Severe Weather Policy</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <hr class="style_2">
                              
                              <h3><strong>Timely Warning Policy</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>In compliance with the Clery Act, Valencia also provides “timely warnings” called
                                       campus safety alerts to students, staff and faculty in an effort to communicate prevention
                                       strategies or basic safety information about reported crimes or activities that are
                                       considered a threat or are in the public interest. &nbsp;Timely warnings will be issued
                                       by authorized and designated Valencia employees through the college email system to
                                       all students, faculty and staff. &nbsp;Current &nbsp;campus safety alerts will be posted on
                                       the Valencia website at: &nbsp;<a href="http://www.valenciacollege.edu/security/timelywarnings.cfm">www.valenciacollege.edu/security/timelywarnings.cfm</a> 
                                    </p>
                                    
                                    <p> The warning will be issued through the college e-mail system to students, faculty
                                       and staff. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <h3><strong>Conduct Standards</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>The Student Conduct Policy and other appropriately published rules of conduct play
                                       an important role in the College's commitment to provide for the safety and security
                                       of all it's community members. Failure of students, faculty, staff or student organizations
                                       to comply with duly established laws or college regulations may subject violator(s)
                                       to appropriate action by the college or other appropriate civil authorities. Such
                                       action might include referral to college disciplinary processes and even the possibility
                                       of arrest. Valencia College does not attempt to shelter students or employees from
                                       federal, state or local laws.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <hr class="style_2">
                              
                              <h3><strong>Policy on Sexual Assault</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Valencia College values the health and safety of every individual on campus and expects
                                       its students to treat other persons with respect and dignity. Any behavior which causes
                                       the sexual assault/abuse of another person will not be tolerated, is a violation of
                                       the College's Policy, and may result in sanctions ranging from probation to expulsion.
                                       Disciplinary action on the part of the College does not precluded the possibility
                                       of criminal charges against the individual(s).
                                       
                                    </p>
                                    
                                    <p>Sexual assault may take many forms including gang rape, acquaintance rape, date rape,
                                       and stranger rape and occurs both on and off college campuses. Sexual assault can
                                       occur at any time of the day or night, at home, in the workplace, in social settings
                                       and in public places. Both men and women have been assaulted by strangers, people
                                       whom they have known and trusted, and people whom they have dated.
                                       
                                    </p>
                                    
                                    <p>In order for the College to deal more effectively with sexual assaults, it is essential
                                       that these incidents be reported. All students and others are encouraged to report
                                       incidents of sexual assault to an adult you trust, e.g., your parents, the police,
                                       the family doctor, a school official, campus security.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <hr class="style_2">
                              
                              <h3><strong>What To Do If You Are Sexually Assaulted</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Get to a place where you will be safe from further attack. For your own protection,
                                          call the police immediately, especially if the assailant is nearby. The police will
                                          help you whether or not you choose to prosecute the assailant.
                                          
                                       </li>
                                       
                                       <li>Call a friend of family member for support. A number of college personnel and campus
                                          security are willing and trained to assist you.
                                          
                                       </li>
                                       
                                       <li>Get medical attention immediately. The primary purpose of a medical examination following
                                          a rape is to check for physical injury, the presence of sexually transmitted diseases
                                          or pregnancy as a result of the rape. The secondary purpose of a medical examination
                                          is to aid the police investigation and legal proceedings. So get medical attention
                                          as soon as possible.
                                          
                                       </li>
                                       
                                       <li>Don't bathe or douche. Bathing or douching might be the first thing you want to do.
                                          But don't, even as much as you want to. You might be literally washing away valuable
                                          evidence. Wait until you have an examination.
                                          
                                       </li>
                                       
                                       <li>Save your clothing. It is all right to change clothes. But save what you were wearing.
                                          Your clothing could be used as evidence for prosecution. Place each item of clothing
                                          in a separate paper bag for the police.
                                          
                                       </li>
                                       
                                       <li>Report the incident to the Police. It is up to you, but reporting isn't the same thing
                                          as prosecuting a rape. Prosecution can be determined later. To contact police 24-Hours
                                          a day, call 911. Security personnel are willing and able to assist victims in reporting
                                          assaults to the proper authorities. 
                                       </li>
                                       
                                    </ul>
                                    
                                    <p>If you are a victim of a sexual assault and decide not to notify the police, please
                                       secure medical attention and contact any of the victim resources listed in this guide.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <hr class="style_2">
                              
                              <h3><strong>Where To Go For Help</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Many sexual assault cases go unreported because the victim fears retaliation or possible
                                       humiliation if word gets around she/he has been the victim of a sex offense. Victims
                                       tend to feel guilty, as though they did something to bring it on themselves, and often
                                       keep the incident to themselves or share some of the incident with a close friend.
                                       While this may be helpful in the immediate sense, we urge you to talk to a knowledgeable
                                       counselor about your reactions to being victimized.
                                       
                                    </p>
                                    
                                    <p>The various on and off campus services available to all victims of violent acts are
                                       designed to assist victims in overcoming the trauma of the attack and proceeding with
                                       their goals and responsibilities at the College.
                                       
                                    </p>
                                    
                                    <p>We strongly suggest that you report any instance of sexual assault to a law enforcement
                                       agency.
                                       
                                    </p>
                                    
                                    <p>Campus Security Officers have been trained to assist rape victims. Each agency urges
                                       rape victims to report the crimes. Many rapists are repeat offenders and will be again
                                       unless they are apprehended and convicted.
                                       
                                    </p>
                                    
                                    <p>What Victims of Sexual Assault Can Expect From The Campus Security Department
                                       
                                    </p>
                                    
                                    <p>It is the policy of the Campus Security Department to insure that sexual assault victims
                                       are afforded sensitivity and maximum humane consideration.
                                       
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>Officers will treat a victim with courtesy, sensitivity, dignity and understanding.
                                          
                                       </li>
                                       
                                       <li>Officers will act thoughtfully without prejudging or blaming a victim.
                                          
                                       </li>
                                       
                                       <li>A victims request to speak to an officer of the same gender will be accommodated.
                                          
                                       </li>
                                       
                                       <li>Officers will meet privately with a victim in a location of the victims choice.
                                          
                                       </li>
                                       
                                       <li>Officers will inform a victim of services and resources available.
                                          
                                       </li>
                                       
                                       <li>Officers will facilitate contacts with law enforcement officials to initiate an investigation.
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <hr class="style_2">
                              
                              <h3><strong>What You Can Do If Someone You Know Has Been Sexually Assaulted</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>If you know someone who has been sexually assaulted, you can be of help. In the aftermath
                                       of a sexual assault, the victim may be experiencing fear, insecurity and frustration
                                       and be needing care and support from others. You, as a friend (or spouse or family
                                       member) can play an important role by providing reassurance and support.
                                       
                                    </p>
                                    
                                    <p>Allow your friend to reflect upon what has happened and the feelings experienced,
                                       but do not press for details. Let her/him set the pace. Listening is one of the best
                                       things you can do at this time. In short, be a trusted friend.
                                       
                                    </p>
                                    
                                    <p>If your friend has not received medial attention, encourage her/him to do so. For
                                       additional help and support contact the local law enforcement agency or campus security.
                                       Accompany your friend to the medical facility and expedite medical attention. It is
                                       important to know that there is a possibility that the medical facility will notify
                                       the police. However, it will be up to the victim to make a final decision as to whether
                                       a formal police report will be initiated.
                                       
                                    </p>
                                    
                                    <p>You can be a valuable resource to your friend by seeking out and providing information
                                       that will assist in understanding available options. For example, you can let your
                                       friend know that reporting the rape and collecting evidence does not automatically
                                       lock her/him into pursing prosecution of the offender. What it does do is assist the
                                       police in identifying the method and possible identity of the assailant. Since rapists
                                       tend to rape more than once, any information that can provided may prevent the sexual
                                       assault of someone else.
                                       
                                    </p>
                                    
                                    <p>Making the decision to report a sexual assault to the police, and undergo the subsequent
                                       processes of evidence collection and possible judicial proceedings, will be very difficult
                                       for your friend. And although it is only natural that you want to give advice, you
                                       must avoid trying to control the situation. A sexual assault victim needs to regain
                                       control and must be allowed to make her/his own decisions.
                                       
                                    </p>
                                    
                                    <p>Whatever decisions are made, your friend needs to know that she/he will not be judged,
                                       disapproved of or rejected by you. The sexual assault victim suffers a significant
                                       degree of physical and emotional trauma both during and immediately following the
                                       rape, and can continue to remain for a long time. By being patient, supportive and
                                       non-judgmental you can provide a safe accepting climate into which your friend can
                                       release painful feelings.
                                       
                                    </p>
                                    
                                    <p>Sometimes friends or family members take the sexual assault of a loved one very personally;
                                       almost as if the assault had happened to them; losing perspective and sight of the
                                       real victim. They feel resentment or anger and unleash this anger on the victim and
                                       everyone in general. And sometimes their sense of frustration and helplessness is
                                       pitted against a powerful urge for revenge.
                                       
                                    </p>
                                    
                                    <p>Do not make the mistake of discounting or ignoring your emotional responses. It is
                                       very important to realize that you too are responding to an unwanted crisis. You are
                                       trying to understand what has happened and adjust to unfamiliar realities. Therefore,
                                       do not hesitate to take advantage of support services in your community which offer
                                       counseling for sexual assault victims and their significant others.
                                       
                                    </p>
                                    
                                    <p>You may be asked to testify in judicial proceedings regarding your friend's remarks,
                                       actions and state of mind, especially if you were one of the first people she/he approached.
                                       Jotting down a few notes may prove to be of benefit later.
                                       
                                    </p>
                                    
                                    <p>Valencia College offers prevention education programs through the Peer Advisor program.
                                       Sexual Assault Awareness Week will be held in September each year. For information
                                       contact the Peer Advisor's Office. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <hr class="style_2">
                              
                              <h3><strong>Sexual Offenders Search</strong></h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="http://offender.fdle.state.fl.us/offender/homepage.do" target="blank">FDLE Sexual Offenders and Predators</a>
                                       
                                    </p>
                                    
                                    <p><a href="http://www.fbi.gov/scams-safety/registry/registry" target="blank">FBI Sex Offender Registry</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                           
                        </div>   
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/promoting.pcf">©</a>
      </div>
   </body>
</html>