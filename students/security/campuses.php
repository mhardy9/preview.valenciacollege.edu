<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Campuses | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/campuses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campuses</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Campuses</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        	 
                        <h2>Campuses</h2>
                        
                        
                        
                        
                        <p><strong>DISTRICT OFFICE</strong><br>
                           Park Place at MetroWest<br>
                           1768 Park Center Dr<br>
                           Orlando, FL 32835
                        </p>
                        
                        <p><a href="/locations/map/district-office.php">Campus Location</a></p>
                        
                        <p>407-299-5000</p>
                        
                        
                        
                        
                        
                        
                        
                        <p><strong>EAST CAMPUS</strong><br>
                           701 N Econlockhatchee Trail<br>
                           Orlando, FL 32825
                        </p>
                        
                        <p><a href="/locations/map/east.php">Campus Location</a></p>
                        
                        <p>407-299-5000</p>
                        
                        
                        
                        
                        
                        
                        
                        <p><strong>LAKE NONA CAMPUS</strong><br>
                           12350 Narcoossee Road<br>
                           Orlando FL 32832
                        </p>
                        
                        <p><a href="/locations/map/lake-nona.php">Campus Location</a></p>
                        
                        <p>407-582-7100</p>
                        
                        
                        
                        
                        
                        
                        
                        <p><strong>OSCEOLA CAMPUS</strong><br>
                           1800 Denn John Lane<br>
                           Kissimmee, FL 34744
                        </p>
                        
                        <p><a href="/locations/map/osceola.php">Campus Location</a></p>
                        
                        <p>407-299-5000<br>
                           407-994-4191 - Local in Osceola County
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        <p><strong>SCHOOL OF PUBLIC SAFETY</strong><br>
                           8600 Valencia College Lane<br>
                           Orlando, FL 32825
                        </p>
                        
                        <p><a href="/locations/map/public-safety.php">Campus Location</a></p>
                        
                        <p>407-582-8200</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        <p><strong>WEST CAMPUS</strong><br>
                           1800 South Kirkman Road<br>
                           Orlando, FL 32811
                        </p>
                        
                        <p><a href="/locations/map/west.php">Campus Location</a></p>
                        
                        <p>407-299-5000</p>
                        
                        
                        
                        
                        
                        
                        
                        <p><strong>WINTER PARK CAMPUS</strong><br>
                           850 W Morse Blvd<br>
                           Winter Park, FL 32789
                        </p>
                        
                        <p><a href="/locations/map/winter-park.php">Campus Location</a></p>
                        
                        <p>407-299-5000</p>
                        	  
                     </div>
                     			
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/campuses.pcf">©</a>
      </div>
   </body>
</html>