<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title IX | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/title-ix.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Title IX</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			 	 
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_1"></div>
                        
                        <h2>Title IX </h2>
                        
                        <p>Valencia College is committed to  fostering an environment free from all forms of
                           discrimination and harassment  (including, but not limited to discrimination based
                           on sex).&nbsp; In  accordance with Title IX and other government regulations, the College
                           has developed  a policy 6Hx28:2-01&nbsp; Discrimination, Harassment &amp; Related Misconduct
                           (including sexual and gender-based harassment, sexual assault, sexual  exploitation,
                           interpersonal violence, stalking, complicity, and retaliation) <a href="../about/general-counsel/policy/default.cfm-policyID=277&amp;volumeID_1=2&amp;pcdure=0&amp;navst=0.html">Discrimination,  Harassment and Related Misconduct</a></p>
                        
                        <p>To report incidents of sexual  harassment, sexual assault, interpersonal violence
                           and/or stalking, members of  the college community can contact any of the following
                           individuals:
                        </p>
                        
                        <hr class="styled_2">
                        
                        <h3>Reporting Incidents</h3>
                        
                        <p></p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td width="48%">              
                                    
                                    <table class="table ">
                                       
                                       <tbody>
                                          
                                          <tr scope="row">
                                             
                                             <td style="font-size:18px;width:45%;text-align:left;padding-left:10px;height:25px;">
                                                <a href="mailto:rkane8@valenciacollege.edu" style="text-decoration:none;color:#000000;background-color:#e6e7e9">
                                                   <span class="far fa-envelope-o" aria-hidden="true"></span> Mr. Ryan Kane</a>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                          <tr scope="row">
                                             
                                             <td style="vertical-align:top;padding-left:10px">
                                                
                                                <p><br>                      
                                                   Title IX Coordinator<br>Office, Human Resources
                                                </p>
                                                
                                                <p><span class="far fa-phone" aria-hidden="true"></span> 407-582-3421
                                                </p>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                       </tbody>
                                       
                                    </table>    
                                    
                                 </td>
                                 
                                 
                                 
                                 <td width="4%">&nbsp;</td>
                                 
                                 
                                 <td width="48%" style="vertical-align:top;">
                                    
                                    <table class="table ">
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <td style="font-size:18px;width:45%;text-align:left;padding-left:10px;height:25px;">
                                                <span class="far fa-bullhorn" aria-hidden="true"></span> <a href="https://valenciacollege.edu/students/disputes/" style="text-decoration:none;color:#000000;background-color:#e6e7e9">Student Dispute Resolution Form</a>
                                                
                                             </td>                
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td style="vertical-align:top;padding-left:10px;" height="70px">
                                                <br>If you witness or are affected by abuse or harassment, Valencia College has dedicated
                                                resources available to you.
                                                <p></p>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                       </tbody>
                                       
                                    </table>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <hr class="styled_2">                   
                                 
                                 <h3>Safety &amp; Security</h3> 
                                 	 
                                 <div class="table-responsive">				  
                                    
                                    <table class="table table table-striped">
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <th scope="col" width="100"><a title="Sort on “CAMPUS”" href="#">CAMPUS</a></th>
                                             
                                             <th scope="col" width="100"><a title="Sort on “PHONENUMBER”" href="#">PHONE NUMBER</a></th>
                                             
                                          </tr>
                                          
                                       </tbody>
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <td>West</td>
                                             
                                             <td><a href="tel:407-582-1000">407-582-1000</a></td>
                                             
                                          </tr>       
                                          
                                          <tr>
                                             
                                             <td>East</td>
                                             
                                             <td><a href="tel:407-582-2000">407-582-2000</a></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>District Office</td>
                                             
                                             <td><a href="tel:407-582-3000">407-582-3000</a><br>
                                                or <a href="tel:407-582-1000">407-582-1000</a></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Osceola</td>
                                             
                                             <td><a href="tel:407-582-4000">407-582-4000</a></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Winter Park</td>
                                             
                                             <td><a href="tel:407-582-5000">407-582-6000</a>
                                                <br>or <a href="tel:407-582-2000">407-582-2000</a>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Lake Nona</td>
                                             
                                             <td><a href="tel:407-582-6000">407-582-7000</a><br>
                                                or <a href="tel:407-582-4000">407-582-4000</a></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>School of Public Safety</td> 
                                             
                                             <td><a href="tel:407-582-7000">407-582-8000</a></td>
                                             
                                          </tr>
                                          
                                          
                                       </tbody>
                                       
                                    </table>
                                    
                                 </div>
                                 
                                 <p><strong>Any member of the Title IX/EO Team (Deputy Title IX Coordinators)</strong></p>
                                 
                                 <p>
                                    
                                    
                                 </p>
                                 
                                 <hr class="styled_2">				  
                                 
                                 <h3>District Office</h3>
                                 
                                 <table class="table ">
                                    
                                    <tbody>
                                       
                                       <tr>
                                          
                                          <td>Director, Equal Opportunity &amp; Employee Relations</td>
                                          
                                          <td>407-582-8125</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Assistant Director, Equal Opportunity &amp; Employee Relations</td>
                                          
                                          <td>407-582-3422<br>
                                             or 407-582-3454
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Director, HR Policy and Compliance Programs</td>
                                          
                                          <td>407-582-8256</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Managing Director, Safety and Security</td>
                                          
                                          <td>407-582-1336</td>
                                          
                                       </tr>
                                       
                                    </tbody>
                                    
                                 </table>
                                 
                                 <p>&nbsp;</p>
                                 
                                 <h3 class="heading_divider_gray">East Campus, Winter  Park and School of Public Safety</h3>
                                 
                                 <table class="table ">
                                    
                                    <tbody>
                                       
                                       <tr>
                                          
                                          <td>Dean of Students</td>
                                          
                                          <td>407-582-2586</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Coordinator, Student Conduct and Academic Success</td>
                                          
                                          <td>407-582-2346</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td> Regional Director, ODHR</td>
                                          
                                          <td>407-582-2760</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Regional Assistant Director, ODHR</td>
                                          
                                          <td>407-582-2816</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Assistant Director, Security</td>
                                          
                                          <td>407-582-2365</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Director, Student Services, Winter Park</td>
                                          
                                          <td>407-582-6868</td>
                                          
                                       </tr>
                                       
                                    </tbody>
                                    
                                 </table>
                                 
                                 <p>&nbsp;</p>
                                 	 
                                 <hr class="styled_2">
                                 
                                 <h3>Osceola, Lake Nona, and Poinciana Campuses </h3>
                                 
                                 <table class="table ">
                                    
                                    <tbody>
                                       
                                       <tr>
                                          
                                          <td>Dean of Students</td>
                                          
                                          <td>321-682-4142</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Coordinator, Student Conduct and Academic Success</td>
                                          
                                          <td>321-682-4093</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Regional Director, ODHR</td>
                                          
                                          <td>321-682-4710</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Regional Assistant Director, ODHR</td>
                                          
                                          <td>407-582-8042</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Assistant Director, Security</td>
                                          
                                          <td>407-582-1047</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Director, Student Services, Poinciana</td>
                                          
                                          <td>407-582-6069</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Director, Student Services, Lake Nona</td>
                                          
                                          <td>407-582-7780</td>
                                          
                                       </tr>
                                       
                                    </tbody>
                                    
                                 </table>
                                 
                                 <p>&nbsp;</p>
                                 		 
                                 <hr class="styled_2">			  
                                 
                                 <h3>West Campus</h3>
                                 <strong></strong>
                                 
                                 <table class="table ">
                                    
                                    <tbody>
                                       
                                       <tr>
                                          
                                          <td>Dean of Students</td>
                                          
                                          <td>407-582-1388</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Coordinator, Student Conduct and Academic Success</td>
                                          
                                          <td>407-582-1557</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Regional Director, ODHR</td>
                                          
                                          <td>407-582-1756</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Assistant Director, Security</td>
                                          
                                          <td>407-582-1327
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                    </tbody>
                                    
                                 </table>                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <br>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Resources</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The following off campus, confidential resources are available to members of the college
                                    community.  For more information regarding these resources, please contact the College's
                                    Title IX Coordinator/Equal Opportunity Officer at 407-582-3421.
                                    
                                 </p>
                                 
                                 <p><strong>Victim Services Center of Central Florida </strong><br> 407-497-6701 <br><a href="http://www.victimservicecenter.org">www.victimservicecenter.org</a><br> <em>(Confidential 24 hour Sexual Assault Hotline)</em></p>
                                 
                                 <p><strong>Harbor House of Central Florida</strong> <br>407-886-2856<br><a href="http://www.harborhousefl.com">www.harborhousefl.com</a><br> <em>(24 hour Domestic Violence Hotline)</em></p>
                                 
                                 <p><strong> Help Now of Osceola Inc.</strong><br> 407-847-8562<br> <a href="http://www.helpnowshelter.org">www.helpnowshelter.org</a><br> <em>(24 hour crisis hotline for domestic abuse)</em></p>
                                 
                                 <p><strong>National Domestic Violence Hotline</strong><br> 800-799-7233 <br> <a href="http://www.thehotline.org">www.thehotline.org</a><br> <em></em></p>
                                 
                                 <p><strong>BayCare Student Assistance Program</strong><br>  800-878-5470 <br> <a href="http://www.baycare.org/sap">www.baycare.org/sap</a><br> <em>(available to students who are currently enrolled in Valencia College credit classes)</em></p>
                                 
                                 <p><strong>Florida Council Against Sexual Violence</strong><br> 888-956-7273<br> <a href="http://www.fcasv.org">www.fcasv.org</a><br> <em></em></p>
                                 
                                 <p><strong>RAINN (Rape, Abuse &amp; Incest National Network)</strong><br> 800-656-4673 <br> <a href="http://www.rainn.org">www.rainn.org</a><br><em> </em></p>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/title-ix.pcf">©</a>
      </div>
   </body>
</html>