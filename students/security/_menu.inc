<ul>
	<li><a href="/students/security/index.php">Safety & Security</a></li>
	<li><a href="/students/security/director.php">A Word from the Director</a></li>
	<li><a href="/students/security/locationses.php">Campuses</a></li>
	<li class="submenu"><a class="show-submenu" href="#">Emergency Information <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
		<ul>
			<li><a href="/students/security/emergency.php">Emergency Numbers</a></li>
			<li><a href="/students/security/reporting.php">Report an Incident</a></li>
			<li><a href="/students/security/active-threat.php">Response to an Active Threat</a></li>
			<li><a href="/students/security/risk-management.php">Risk Management</a></li>
			<li><a href="/students/security/run-hide-fight.php">Run, Hide, Fight Video</a></li>
		</ul>	
	</li>	
	<li class="submenu"><a class="show-submenu" href="#">Resources <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
		<ul>
			<li><a href="/students/security/compliance-and-safety.php">Compliance and Safety</a></li>	
			<li><a href="/students/security/faq.php">Frequently Asked Questions </a></li>
			<li><a href="/students/security/parking.php">Parking and ID's</a></li>
			<li><a href="/students/security/promoting.php">Personal Safety Information</a></li>
			<li><a href="/students/security/title-ix.php">Title IX</a></li>
			<li><a href="/students/security/victim.php">Victim Assistance Agencies</a></li>
		</ul>	
	</li>		
<li><a href="http://net4.valenciacollege.edu/forms/security/contact.cfm">Contact</a></li>
</ul>