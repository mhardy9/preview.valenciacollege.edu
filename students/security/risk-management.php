<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/risk-management.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>Reporting Crime &amp; Emergencies</h2>
                        
                        
                        
                        
                        
                        <h3>What is Risk Management?</h3>
                        <strong>Identifying and Managing threats that can negatively impact individuals or a whole
                           organization.</strong>
                        
                        <h3>Contact Information:</h3>
                        The Risk Management and Insurance programs are part of the <strong>Safety, Security, and Risk Management Department.</strong>
                        
                        <h3>Office Location:</h3>
                        <strong>District Office
                           <br>
                           1768 Park Center Drive
                           <br>
                           Orlando, FL 32835</strong>
                        
                        <h3>Contacts:</h3>
                        
                        <ul>
                           
                           <li>Steve Mammino, Risk Manager/Insurance Coordinator (407-582-3860)</li>
                           
                           <li>Paul Rooney, AVP Operations</li>
                           
                           <li>Elaine Skurzewski, Admin Assistant, Insurance Policies</li>
                           
                        </ul>
                        
                        <div>
                           
                           
                           <h3><strong>Valencia College Insurance Policies</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    These documents are summaries of the insurance policies that are provided to the 28
                                    state community colleges and is managed by the <strong>F</strong>lorida <strong>C</strong>ollege <strong>S</strong>ystem <strong>R</strong>isk <strong>M</strong>anagement <strong>C</strong>onsortium, or <strong>FCSRMC</strong>.  In addition, the full 37 page Plan Document (Insurance Policy) is kept in the risk
                                    management office for more detailed information about the policies.
                                    
                                 </p>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/Summary%20of%20Insurance%20Policies%205-13-15.pdf" target="_blank">Summary of Insurance Policies</a></li>
                                    
                                    <li><a href="documents/2014-2015-Property-Casualty-Brochure.pdf" target="_blank">FCSRMC Brochure</a></li>
                                    
                                 </ul>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3><strong>Use of Private Autos for Business Purposes</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    With six campuses and more on the way, many employees will be required to commute
                                    to the various campuses for official business.  If employees travel in college owned
                                    vehicles they are covered under the auto liability policy.  <strong>However</strong>, if employees use their own vehicles for official travel, they must rely on their
                                    own insurance for liability and property damages.  This form provides acknowledgment
                                    of this policy and recommended limits of liability insurance you should carry on your
                                    insurance policy if you are receiving mileage reimbursement.
                                 </p>
                                 
                                 <p>
                                    
                                    <a href="documents/Use%20of%20Private%20Autos%20for%20Business.pdf" target="_blank">Use of Private Vehicles for Official Business</a>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3><strong>Minimum Insurance Requirements</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    When using a contractor or paying a third party entity for services, it is required
                                    they provide a certificate of insurance listing Valencia Board of Trustees and Valencia
                                    Foundation as Additional Insured. This Document Specifies the Minimum Insurance requirements
                                    for Contracts.
                                    
                                 </p>
                                 
                                 <p>
                                    
                                    <a href="documents/Minimum-Insurance-Requirements.pdf" target="_blank">Minimum Insurance Requirements</a>
                                    
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3><strong>Event Insurance</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    For third party entities that wish to provide services on Valencia Campuses and do
                                    not have their own insurance, these companies provide short term liability insurance
                                    online. These sites allow users to enter the specifics of their event and get a quote
                                    prior to purchasing the coverage. 
                                    
                                 </p>
                                 
                                 <ul>
                                    
                                    <li><a href="https://tulip.ajgrms.com/" target="_blank">TULIP - Tenant User Liability Insurance Program</a></li>
                                    
                                    <ul>
                                       
                                       <li>The Florida College System Risk Management Consortium sponsors this program.</li>
                                       
                                    </ul>
                                    
                                    <li><a href="https://www.theeventhelper.com" target="_blank">The Event Helper</a></li>
                                    
                                    <li><a href="http://www.kandkinsurance.com" target="_blank">K&amp;K Insurance</a></li>
                                    
                                 </ul>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3><strong>International Travel</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    Many Faculty, Staff and Students travel to overseas destinations every year.  Travelers
                                    must insure they are properly covered for medial emergencies and other situations
                                    when in other countries.  Travelers can check with the <a href="../international/studyabroad/index.html" target="_blank">Study Abroad and Global Experiences</a> office for advice on insurance coverages when traveling abroad.  The links are companies
                                    that provide travel insurance.  TravelGuard is sponsored through the Florida College
                                    Risk Management Consortium.
                                    
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>TravelGuard (AIG), through FCSRMC</li>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/TravelGuard%20Evacuations.pdf" target="_blank">TravelGuard Evacuations</a></li>
                                       
                                       <li><a href="documents/TravelGuard%20Assist%20Services.pdf" target="_blank">TravelGuard Assist Services</a></li>
                                       
                                       <li><a href="documents/TravelGuard%20World%20Risk%20Card.pdf" target="_blank">TravelGuard World Risk Card</a></li>                                                
                                       
                                    </ul>
                                    
                                    <li><a href="http://www.cmi-insurance.com" target="_blank">CMI Insurance Worldwide</a></li>
                                    
                                    <li><a href="http://www.medexassist.com" target="_blank">MEDEX Assist</a></li>
                                    
                                 </ul>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <span>
                           
                           <h3>Steve Mammino<br>
                              Risk Manager
                           </h3>
                           </span><center>Safety, Security &amp; Risk Management
                           is dedicated to providing a safe, secure and welcoming environment for our Students,
                           Faculty, Staff and Visitors. We strive to provide excellent customer service while
                           implementing the latest technologies, insuring that our working and learning environment
                           is the
                           safest it can be.
                        </center>
                        <br><center>
                           <h3>"See Something, Say Something"</h3>
                        </center>
                        
                        <center>
                           
                           
                           
                        </center>             
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Criminal Justice Institute 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-8000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          District Office 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-3000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">East Campus Bldg 5 Room 220</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-7000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Room 109</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osc_security@valenciacollege.edu">osc_security@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Poinciana Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">1-103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-6500</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB Room 170</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:drodriguez225@valenciacollege.edu">drodriguez225@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-6000</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/risk-management.pcf">©</a>
      </div>
   </body>
</html>