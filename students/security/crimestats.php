<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/crimestats.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Campus Crime Statistics</h2>
                        
                        
                        
                        
                        
                        <p>Each security office maintains a "crime log" which provides the most current crime
                           information and all reported offenses of the preceding sixty days.&nbsp; You are invited
                           to review these statistics by visiting your campus security office and simply asking
                           to see the "crime log."
                        </p>                 
                        
                        <p><strong>CLERY CRIME STATISTICS, DRUG, ALCOHOL &amp; WEAPON VIOLATIONS </strong></p>
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="th"><strong>YEAR</strong></div>
                                    
                                    <div data-old-tag="th"><strong>2012</strong></div>
                                    
                                    <div data-old-tag="th"><strong>2013</strong></div>
                                    
                                    <div data-old-tag="th"><strong>2014</strong></div>
                                    
                                    <div data-old-tag="th"><strong>2015</strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="th"><strong>CRIMINAL OFFENSES</strong></div>
                                    
                                    <div data-old-tag="th">
                                       <div><strong>OC</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="th">
                                       <div><strong>PP</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="th"><strong>OC</strong></div>
                                    
                                    <div data-old-tag="th"><strong>PP</strong></div>
                                    
                                    <div data-old-tag="th"><strong>OC</strong></div>
                                    
                                    <div data-old-tag="th"><strong>PP</strong></div>
                                    
                                    <div data-old-tag="th"><strong>OC</strong></div>
                                    
                                    <div data-old-tag="th"><strong>PP</strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Murder/non-negligent manslaughter </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          <strong>*</strong>3
                                       </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Negligent manslaughter </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Rape</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Fondling</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Incest</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Statutory Rape </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Robbery </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Aggravated Assault </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Burglary (B &amp; E) </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Motor Vehicle Theft </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>2</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>2</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>4</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Arson </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Hate Crimes </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>VAWA OFFENSES </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="th">
                                       <div><strong>OC</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="th">
                                       <div><strong>PP</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="th"><strong>OC</strong></div>
                                    
                                    <div data-old-tag="th"><strong>PP</strong></div>
                                    
                                    <div data-old-tag="th"><strong>OC</strong></div>
                                    
                                    <div data-old-tag="th"><strong>PP</strong></div>
                                    
                                    <div data-old-tag="th"><strong>OC</strong></div>
                                    
                                    <div data-old-tag="th"><strong>PP</strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Domestic Violence </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>NR</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>NR</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>3</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Dating Violence </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>NR</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>NR</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>2</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Stalking</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>NR</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>NR</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>3</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>VIOLATIONS</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>ARR</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>REF</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>ARR</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>REF</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>ARR</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>REF</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>ARR</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>REF</strong></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Liquor Law Violation </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Drug Abuse Violation </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>1</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>4</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>4</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>3</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Weapon Possession </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>0</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>TOTALS </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>6</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>13</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>15</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>10</strong></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>*</strong><em>Related to a single incident later ruled a homicide/suicide involving a driver and
                              three passengers, none of whom were affiliated with the college.</em></p>
                        
                        <p>Notes: <strong>OC</strong>- On Campus <strong>PP</strong>- Public Property<strong> ARR</strong>-Arrest <strong>REF</strong>-Refered <strong>NR</strong>-not reported prior to 2013 
                        </p>
                        
                        <p><a href="#top">TOP</a> 
                        </p>
                        
                        <p> <strong>CLERY STATISTICS BY CAMPUS -  2015 ONLY </strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"> CAMPUS  </div>
                                 
                                 <div data-old-tag="th">  WEST </div>
                                 
                                 <div data-old-tag="th"> EAST</div>
                                 
                                 <div data-old-tag="th">  OSC </div>
                                 
                                 <div data-old-tag="th">  DTC </div>
                                 
                                 <div data-old-tag="th">  WPK </div>
                                 
                                 <div data-old-tag="th"> LNC   </div>
                                 
                                 <div data-old-tag="th">   CJI  </div>
                                 
                                 <div data-old-tag="th">  TOTAL</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OFFENSE</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Murder/non-negligent manslaughter </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>0</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Negligent manslaughter </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Rape</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Fondling</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Incest</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Statutory Rape </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Robbery </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Aggravated Assault </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Burglary (B &amp; E) </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Motor Vehicle Theft </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>2</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>2</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>4</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Arson </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Hate Crimes </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>VAWA OFFENSES</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>OC</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>PP</strong></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Domestic Violence </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Dating Violence </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Stalking</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>VIOLATIONS</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Liquor Law Violation </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Drug Abuse Violation </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>3</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Weapons Possession </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>TOTALS</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>5</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>4</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>10</div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>*</strong><em>Related to a single incident later ruled a homicide/suicide involving a driver and
                              three passengers, none of whom were affiliated with the college.</em></p>
                        
                        <p>Notes: <strong>OC</strong>- On Campus <strong>PP</strong>- Public Property<strong> ARR</strong>-Arrest <strong>REF</strong>-Refered <strong>NR</strong>-not reported prior to 2013
                        </p>
                        
                        <p><a href="#top">TOP</a>              
                           
                        </p>
                        
                        <p><strong>CLERY   OFFENSES REPORTED INCLUDING LARCENY/THEFT</strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"><strong>YEAR</strong></div>
                                 
                                 <div data-old-tag="th">2012</div>
                                 
                                 <div data-old-tag="th">2013</div>
                                 
                                 <div data-old-tag="th">2014</div>
                                 
                                 <div data-old-tag="th">2015</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">OFFENSE</div>
                                 
                                 <div data-old-tag="th">
                                    <div>OC</div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>PP</div>
                                 </div>
                                 
                                 <div data-old-tag="th">OC</div>
                                 
                                 <div data-old-tag="th">PP</div>
                                 
                                 <div data-old-tag="th">OC</div>
                                 
                                 <div data-old-tag="th">PP</div>
                                 
                                 <div data-old-tag="th">OC</div>
                                 
                                 <div data-old-tag="th">PP</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Murder/non-negligent manslaughter </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       <strong>*</strong>3
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>0</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Negligent manslaughter </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Rape</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Fondling</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Incest</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Statutory Rape </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Robbery </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Aggravated Assault </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Burglary (B &amp; E) </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Motor Vehicle Theft </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>2</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>2</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>4</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Arson </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Hate Crimes </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Larceny/Theft Offenses </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>59</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>58</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>51</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>51</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>VAWA OFFENSES </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>OC</div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>PP</div>
                                 </div>
                                 
                                 <div data-old-tag="th">OC</div>
                                 
                                 <div data-old-tag="th">PP</div>
                                 
                                 <div data-old-tag="th">OC</div>
                                 
                                 <div data-old-tag="th">PP</div>
                                 
                                 <div data-old-tag="th">OC</div>
                                 
                                 <div data-old-tag="th">PP</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Domestic Violence </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>NR</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NR</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>3</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Dating Violence </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>NR</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NR</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>2</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Stalking</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>NR</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NR</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>3</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>VIOLATIONS</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>ARR</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>REF</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Liquor Law Violation </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Drug Abuse Violation </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>4</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>4</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>3</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Weapon Possession </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>0</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>TOTALS </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>65</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>71</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>66</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>61</strong></div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>*</strong><em>Related to a single incident later ruled a homicide/suicide involving a driver and
                              three passengers, none of whom were affiliated with the college.</em></p>
                        
                        <p>Notes: <strong>OC</strong>- On Campus <strong>PP</strong>- Public Property<strong> ARR</strong>-Arrest <strong>REF</strong>-Refered <strong>NR</strong>-not reported prior to 2013
                        </p>
                        
                        <div>
                           
                           <p><a href="#top">TOP</a></p>
                           
                        </div>    
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/crimestats.pcf">©</a>
      </div>
   </body>
</html>