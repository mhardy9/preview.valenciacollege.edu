<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia College | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/parking-permits.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Valencia College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container">
                  
                  <div class="row">
                     
                     <div class="col-lg-12">
                        
                        <p></p>
                        
                        <div>
                           <center>
                              <a href="../index.html"><img src="http://valenciacollege.edu/security/images/logo2.png" alt="Valencia College" class="img-responsive" border="0"></a><br>
                              
                              <div align="center"><img src="http://valenciacollege.edu/security/images/security-logo.jpg" alt="Campus Security Serve and Protect Badge" title="Campus Security Serve and Protect Badge" class="img-responsive" border="0"></div>
                              
                              <div align="right">
                                 <input type="button" class="alert-success" name="Quit" id="Quit" value="Close This Window" onclick="return quitBox('quit');">
                                 
                              </div>
                              
                              <div class="valenciared" align="center">
                                 
                                 <p></p>
                                 
                                 <h1><strong>WELCOME TO CAMPUS SECURITY</strong></h1> 
                              </div>
                              
                           </center>
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <hr>
                  
                  
                  <div class="container">
                     
                     <div class="row">
                        
                        <div class="card card-block text-xs-center">
                           
                           <h3 align="center">Parking Decals</h3>
                           
                           <p class="text-center"><strong>Don't Waste Time Standing in Line!</strong></p>
                           
                           <p class="valenciagold text-center">TO OBTAIN A VALENCIA STUDENT ID,<br> 
                              THE FOLLOWING MUST BE COMPLETED:
                           </p>  
                           
                           <p class="valenciabody"><strong>VEHICLE REGISTRATION:</strong><br>
                              (This is required for new and renewal). EXPIRATION DATE MUST BE CURRENT.
                           </p>
                           
                           
                           <p class="valenciabody"><strong>PARKING APPLICATION:</strong> Application is done through
                              ATLAS and can be completed in the Security office, but
                              computers are limited. We suggest that you print the
                              application at home, in the ATLAS lab or in the Library.
                           </p>
                           
                           
                           <p class="valenciabody"><strong>PHOTO ID:</strong> Valid Driver's license, State ID, Passport or
                              Student ID.
                           </p> 
                           
                           
                           <div class="valenciared text-center">
                              
                              <p><strong>PLEASE NOTE: YOU WILL NOT BE ISSUED A PARKING
                                    DECAL WITHOUT THE REQUIRED ITEMS.</strong></p> <a class="btn btn-danger btn-lg center-block" href="documents/Vertical-Hallway-Sign-for-Decal.pdf" target="_blank" role="button" color="#FFFFFF">Click here to download the PDF</a>
                              
                           </div>
                           
                        </div>
                        
                        
                        <hr>
                        <br>
                        
                        
                        <div class="card card-block text-xs-center">
                           
                           <h3 align="center">Valencia Student ID's</h3>
                           
                           <p class="text-center"><strong>Don't Waste Time Standing in Line!</strong></p>
                           
                           <p class="valenciagold text-center">TO OBTAIN A VALENCIA STUDENT ID,<br> 
                              THE FOLLOWING MUST BE COMPLETED:
                           </p>  
                           
                           <p class="valenciabody text-center"><strong>YOU MUST BE REGISTERED FOR THE UPCOMING SEMESTER.</strong></p>
                           
                           
                           <p class="valenciabody text-center"><strong>YOUR CLASSES MUST BE PAID IN FULL:</strong></p>
                           
                           <p>
                              <br>
                              
                              
                           </p>
                           
                           <p class="valenciabody">If you are using FINANCIAL AID, your account must reflect that the particular aid
                              has been processed and will cover your classes. We cannot issue an ID until your
                              account reflects the above.
                           </p>
                           
                           
                           <p class="valenciabody"> If you are PAYING OUT OF POCKET (i.e. CREDIT CARD, CASH, CHECK), your
                              account balance must be at $0.00. *If you are paying out of pocket and using the
                              T.I.P.S. program, you will need to provide the confirmation that you received when
                              the first payment was made.
                           </p>
                           
                           
                           
                           
                           <p class="valenciabody">If you are using the VA DEFERMENT (GI BILL), you will need to provide the
                              deferment paper. If you do not have the deferment paper, you may be able to verify
                              through your account, the Business Office or through the VA office here at Valencia.
                           </p>
                           
                           
                           
                           <p class="valenciabody">If you are using FLORIDA PRE-PAID, there may be a balance left over after
                              Florida Pre-Paid has processed. This is due to Florida Pre-Paid not covering extra
                              class or lab fees. You will need to pay any remaining balance to bring your account
                              to $0.00.
                           </p>
                           
                           
                           
                           
                           
                           <div class="valenciared text-center">
                              <p><strong>YOU MAY CHECK WITH THE BUSINESS OFFICE IF YOU ARE STILL UNSURE ABOUT YOUR CURRENT
                                    ACCOUNT STANDING.</strong></p>
                           </div>
                           <br><br>
                           <a class="btn btn-danger btn-lg center-block" href="documents/Vertical-Hallway-Sign-for-Student-ID.pdf" target="_blank" role="button" color="#FFFFFF">Click here to download the PDF</a>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <hr class="small">
                  
                  <div class="container">
                     
                     <div class="row">
                        
                        <div class="col-lg-12">
                           
                           <div class="valenciagrey" align="center">
                              
                              <p></p>
                              
                              <h3><a href="parking.html" target="_blank">Please visit the Security Parking Relations Page for more information on our regulations.</a></h3>
                              
                           </div>
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/parking-permits.pcf">©</a>
      </div>
   </body>
</html>