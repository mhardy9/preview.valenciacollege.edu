<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Victim Assistance Agencies | Valencia College</title>
      <meta name="Description" content="Safety, Security &amp; Risk Management">
      <meta name="Keywords" content="security, risk management, run hide fight, parking, id, security campus, campus , security, valencia college security, school, active threat, parking permits, witness, accident, report, reporting an accident, 911, emergency">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/victim-assistance-agencies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Victim Assistance Agencies</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1"></div>
                        					
                        <h2>Victim Assistance Agencies</h2>
                        					
                        <p>If you or someone you know is the victim of a crime or a violation of the Student
                           Conduct Code, it is important that you contact agencies that can help you resolve
                           the matter. Listed below are some agencies that may be able to assist you:
                        </p>
                        					
                        				
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “CAMPUS”" href="#">CAMPUS</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “PHONENUMBER”" href="#">PHONE NUMBER</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>West</td>
                                    
                                    <td><a href="tel:407-582-1000">407-582-1000</a></td>
                                    
                                 </tr>       
                                 
                                 <tr>
                                    
                                    <td>East</td>
                                    
                                    <td><a href="tel:407-582-2000">407-582-2000</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>District Office</td>
                                    
                                    <td><a href="tel:407-582-3000">407-582-3000</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Osceola</td>
                                    
                                    <td><a href="tel:407-582-4000">407-582-4000</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Winter Park</td>
                                    
                                    <td><a href="tel:407-582-5000">407-582-6000</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Lake Nona</td>
                                    
                                    <td><a href="tel:407-582-6000">407-582-7000</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>School of Public Safety</td> 
                                    
                                    <td><a href="tel:407-582-7000">407-582-8000</a></td>
                                    
                                 </tr>
                                 
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        					
                        <h3>Police or Fire Emergency Dial 911</h3>
                        					
                        <p><strong>Orlando Police Department</strong><br>
                           					<a href="tel:321-235-5300">321-235-5300</a> Non-Emergency
                        </p>
                        					
                        <p><strong>Orange County Sheriff's Office</strong><br>
                           					<a href="tel:407-836-4357">407-836-4357</a> Non-Emergency
                        </p>
                        					
                        <p><strong>Kissimmee Police Department</strong><br>
                           					<a href="tel:407-846-3333">407-846-3333</a> Non-Emergency
                        </p>
                        					
                        <p><strong>Winter Park Police Department</strong><br>
                           					<a href="tel:407-644-1313">407-644-1313</a> Non-Emergency
                        </p>
                        					
                        <p><strong>Sexual Assault/Rape Crisis</strong></p>
                        					
                        <ul>
                           						
                           <li>
                              							<strong>Victim Service Center of Central Florida</strong>
                              							
                              <ul>
                                 								
                                 <li>Business <a href="tel:407-254-9415">407-254-9415</a>
                                    								
                                 </li>
                                 								
                                 <li>Hotline 24 Hour <a href="tel:407-497-6701">407-497-6701</a>
                                    								
                                 </li>
                                 							
                              </ul>
                              						
                           </li>
                           					
                        </ul>
                        					
                        <p>If you were raped or sexually assaulted in the past, you may still need to talk with
                           someone about it now. It was a traumatic experience and may still be affecting your
                           life. Talking about being sexually assaulted may help you resolve your feelings about
                           it.
                        </p>
                        					
                        <hr class="styled_2">
                        					
                        <h3>BayCare Student Assistance Program</h3>
                        				
                        <p>BayCare is a free service available 24 hours a day, seven days a week to Valencia
                           students. For support, advice, or someone to listen please contact the BayCare program
                           at <a href="tel:800-878-5470">1-800-878-5470</a>.
                        </p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/victim-assistance-agencies.pcf">©</a>
      </div>
   </body>
</html>