<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/director.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-3 col-sm-3">
                        
                        <div class="box_style_1">
                           			<img src="/students/security/images/mike-favorit.png" alt="Director - Mike Favorit" class="img-circle styled" align="center">
                           			<br> 	<br>
                           			
                           <p><strong><font style="color:#BF311A">Director - Mike Favorit</font></strong></p>
                           			
                        </div>
                        		
                     </div>
                     
                     <div class="col-md-9 col-sm-9">
                        
                        
                        <h2>A Word from the Director - Mike Favorit</h2>
                        	
                        <p>As the Managing Director of Valencia College Safety and Security, I would like to
                           welcome you to the college. The Campus Security Department is committed to providing
                           quality security and safety services to all students, faculty, staff, and visitors
                           at each campus. Customer Service is our goal and we strive to make your experience
                           here memorable with a positive and safe learning environment.
                        </p>
                        
                        	
                        <p>I was selected as the Valencia College Managing Director for Safety and Security on
                           September 7, 2015. Prior to this selection my assignment was the Valencia College
                           West Campus Security Manager from April 1, 2014 to September 7, 2015. This encompassed
                           directing security services to the West Campus and District Office. As a Navy veteran
                           and 26 year veteran with the City of Orlando Police Department (retired Captain) I
                           had the distinct privilege of serving all citizens and now the Valencia community
                           with the utmost dedication towards safety and security.
                        </p>
                        	
                        <p>While at the Orlando Police Department my assignments included: Uniform Patrol, Uniform
                           Drug Unit, In-Service Training Unit, Downtown Bike Patrol, Recruit Training Unit Coordinator,
                           Patrol Sergeant, Metropolitan Bureau of Investigation Supervisor, Internal Affairs
                           Supervisor, Patrol Watch Commander, Airport Watch Commander, Airport Deputy Division
                           Commander, Drug Enforcement Commander, Metropolitan Bureau of Investigation Commander,
                           West Patrol Division Commander and the Special Operations Division Commander. During
                           my tenure as a Police Officer, I received numerous awards and was a certified law
                           enforcement trainer in almost every area of instruction in the police realm.
                        </p>
                        	
                        <p>I am a graduate from Columbia College with a Bachelor of Arts in Criminal Justice
                           Administration. As a Police Commander, I attended the Drug Enforcement Administration
                           Commander Course and the Southern Police Institute Command Officers Development Course.
                           Having served proudly for 17 years on the Special Weapons and Tactics Team as a Sniper,
                           Assault Team Member, Team Leader and Assistant Team Commander, I know the inherent
                           dangers within the community and draw from these experiences to serve the college
                           better. I also commanded the Emergency Services Unit for 3 years providing guidance
                           to a team designed to assist any high risk circumstance presented.
                        </p>
                        	
                        <p>I am passionate on serving my community and attack each day with a great amount of
                           enthusiasm and vision for the safety and security for all the students, faculty, staff,
                           and visitors of the college.
                        </p>
                        	
                        <p>Michael Favorit, <em>Managing Director - Safety &amp; Security.</em></p>
                        
                        
                     </div>
                     
                  </div>
                  	  
               </div>
               		
               	
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/director.pcf">©</a>
      </div>
   </body>
</html>