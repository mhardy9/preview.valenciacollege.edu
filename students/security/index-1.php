<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>	Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/index-1.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div> <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                              </div>
                              <br>
                              
                              
                              <div>
                                 <a name="navigate" id="navigate"></a>
                                 
                                 
                                 <div>
                                    <a name="content" id="content"></a>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li><a alt="EMERGENCY NUMBERS" href="emergency.html">
                                                <p>EMERGENCY NUMBERS</p></a></li>
                                          
                                          <li><a alt="REPORT AN INCIDENT" href="reporting.html">
                                                <p>REPORT AN INCIDENT</p></a></li>
                                          
                                          <li><a alt="RESPONSE TO AN ACTIVE THREAT" href="active-threat.html">
                                                <p>RESPONSE TO AN ACTIVE THREAT</p></a></li>
                                          
                                          <li><a alt="CAMPUSES" href="locations/index.html">
                                                <p>CAMPUSES</p></a></li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                 </div>  
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <a alt="A Word from the Director" href="director.html">A Word from the Director</a>
                                       <a alt="Annual Security Report" href="annual-security-report.html">Annual Security Report</a>
                                       <a alt="Parking and IDs" href="parking.html">Parking and IDs</a>
                                       <a alt="Run, Hide, Fight Video" href="run-hide-fight.html">Run, Hide, Fight Video</a>
                                       <a alt="Frequently Asked Questions" href="faq.html">Frequently Asked Questions</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <center>
                                          <h3>"See Something, Say Something"</h3>
                                       </center>
                                       
                                       <center>
                                          
                                          
                                          <link href="../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                       </center>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <a alt="Risk Management" href="risk-management.html">Risk Management</a>
                                       <a alt="Compliance &amp; Safety" href="compliance-and-safety.html">Compliance &amp; Safety</a>
                                       <a alt="Title IX" href="title-ix.html">Title IX</a>
                                       <a alt="Victim Assistance Agencies" href="victim.html">Victim Assistance Agencies</a>  
                                       <a alt="Personal Safety" href="promoting.html">Personal Safety</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div> 
                           </div>
                           
                           
                           
                           
                           
                           
                        </div>          
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/index-1.pcf">©</a>
      </div>
   </body>
</html>