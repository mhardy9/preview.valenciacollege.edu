<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/reporting.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1">
                           					
                           <h2>Reporting Crime &amp; Emergencies</h2>
                           					
                           <div data-old-tag="table">
                              						
                              <div data-old-tag="tbody">
                                 							
                                 <div data-old-tag="tr">
                                    								
                                    <div data-old-tag="td">
                                       									<a href="http://net4.valenciacollege.edu/forms/security/incidentReporting.cfm" target="_blank">Report Incident</a>
                                       								
                                    </div>
                                    								
                                    <div data-old-tag="td">
                                       									<a href="silentWitness.html" target="_blank">Anonymous Reporting</a>
                                       								
                                    </div>
                                    							
                                 </div>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <p>The Security Department believes that the dissemination of information is the key
                              to educating our college community about the occurrence of crime on campus. Students
                              and employees are encouraged to report all incidents of crime.
                           </p>
                           					
                           <p>Students and employees are reminded that our multi-campus operation involves several
                              legal jurisdictions. However, when reporting all emergencies, dial 9-1-1.
                           </p>
                           					
                           <p>In addition, Campus Security needs to be notified in an effort to support and assist
                              the responding emergency personnel. Our assistance can be critical when time is important
                              and the responding emergency team is having difficulty locating the reported emergency.
                           </p>
                           					
                           <p>Students and employees may wish to contact the Campus Security Office and request
                              an emergency response, at which time this information will be relayed to the appropriate
                              emergency response team.
                           </p>
                           					
                           <hr class="more_margin">
                           					
                           <h3>Reporting Emergencies/Crime on Campus</h3>
                           					
                           <p>All emergency situations involving:</p>
                           					
                           <ul>
                              						
                              <li>A crime in progress</li>
                              						
                              <li>A medical emergency</li>
                              						
                              <li>A fire</li>
                              					
                           </ul>
                           					
                           <p>should be immediately reported to 9-1-1. All phones, both on and off campus, and including
                              pay phones may be used to dial 9-1-1 at no charge. While on campus, persons should
                              be aware that different telephone systems might require you to dial for an "outside
                              line" before dialing 9-1-1.
                           </p>
                           					
                           <p>As an added security measure, there are "Blue Light" non-dial outdoor emergency telephones
                              located at strategic points on campus. These phones are easily identified by their
                              columns topped with blue lights. When the red button is pushed the callers is in immediate
                              contact with the campus Security Department. In addition to providing voice contact
                              with security, the dispatcher is also able to pinpoint the caller's location. These
                              phones are for emergency use only.
                           </p>
                           					
                           <p><strong>"Blue Light" non-dial outdoor emergency telephones</strong></p>
                           					
                           <p>All elevators in educational buildings have emergency phones with direct contact to
                              the monitoring station.
                           </p>
                           					
                           <p>When calling for either emergency or non-emergency service, be prepared to:</p>
                           					
                           <ol>
                              						
                              <li>Clearly identify yourself.</li>
                              						
                              <li>State where you are calling from.</li>
                              						
                              <li>State briefly, the nature of your call .</li>
                              					
                           </ol>
                           					
                           <p>If possible, the caller should stay on the line unless otherwise advised by the dispatcher.
                              The dispatcher will summon the appropriate police, fire, and/or medical service.
                           </p>
                           					
                           <hr class="more_margin">
                           					
                           <h3>Confidential Reporting Procedures</h3>
                           					
                           <p>If you are the victim of a crime and do not want to pursue actions within the College
                              System, you may still want to consider making a condifential report. With your permission,
                              the Managing Director of Safety and Security or a designee of the Valencia Security
                              Department can file a report on the details of the incident without revealing your
                              identity. The purpose of a confidential report is to comply with your wish to keep
                              the matter condifential, while taking steps to ensure the future safety of yourself
                              and others. With such information, the College can keep an accurate record of the
                              number of such incidents involving students, determine whether there is a pattern
                              of crime with regard to a particular location, method, or assailant, and alert the
                              campus community to potential danger. Reports filed in this manner are counted and
                              disclosed in the annual crimes statistics for the institution.
                           </p>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/reporting.pcf">©</a>
      </div>
   </body>
</html>