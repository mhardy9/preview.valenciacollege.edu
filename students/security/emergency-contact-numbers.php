<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/emergency-contact-numbers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>Emergency Numbers </h2>
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">West Campus</div>
                                 
                                 <div data-old-tag="th">East Campus &amp; Criminal Justice Institute</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Campus Security</strong> 407-582-1000 
                                    </p>
                                    
                                    <p>(After business hours)<br>
                                       407-582-1030 or 321-689-3539 <br>
                                       <br>
                                       <strong>Orlando Police </strong><br>
                                       Non-emergency&nbsp; 321-235-5300<br>
                                       Crime Prevention 407-246-2461 
                                    </p>                  
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Campus Security </strong>407-582-2000 
                                    </p>
                                    
                                    <p> (After business hours)<br>
                                       407-277-0332 or 321-689-3541 
                                    </p>
                                    
                                    <p><strong>Orange County Sheriff’s Office</strong><br>
                                       Non-Emergency 407-836-4357<br>
                                       Crime Prevention 407-249-4508
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Osceola Campus </div>
                                 
                                 <div data-old-tag="th">Winter Park Campus </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Campus Security</strong> 407-582-4000 
                                    </p>
                                    
                                    <p> (After business hours)<br>
                                       321-689-3543 
                                    </p>                  
                                    
                                    <p><strong>Kissimmee Police Department </strong><br>
                                       Non-emergency&nbsp; 407-846-3333<br>
                                       Crime Prevention 407-847-0176 Ext. 3123 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Campus Security</strong> 407-582-6000
                                    </p>
                                    
                                    <p>(After business hours)<br>
                                       407-277-0332 or 321-689-3541 <br>
                                       <br>
                                       <strong>Winter Park Police Department </strong><br>
                                       Non-emergency&nbsp; 407-644-1313<br>
                                       Crime Prevention 407-599-3311
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">District Office </div>
                                 
                                 <div data-old-tag="th">Lake Nona</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong> Campus Security</strong> 407-582-3000<br> or 407-582-1000 
                                    </p>
                                    
                                    <p>(After business hours)<br>
                                       407-582-1030 or 321-689-3539 <br>
                                       <br>
                                       <strong>Orlando Police </strong><br>
                                       Non-emergency&nbsp; 321-235-5300<br>
                                       Crime Prevention 407-246-2461 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Campus Security</strong> 407-582-7000 
                                    </p>
                                    
                                    <p> (After business hours)<br> 
                                       321-689-3543 
                                    </p>
                                    
                                    <p><strong>Orlando Police</strong><br>
                                       Non-emergency&nbsp; 321-235-5300<br>
                                       Crime Prevention 407-246-2461 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="th">Other Important Numbers </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    <strong>Orange County Sheriff's Office </strong><br>
                                    Non-Emergency 407-836-4357<br>
                                    Crime Prevention 407-249-4508
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <center>Safety, Security &amp; Risk Management
                           is dedicated to providing a safe, secure and welcoming environment for our Students,
                           Faculty, Staff and Visitors. We strive to provide excellent customer service while
                           implementing the latest technologies, insuring that our working and learning environment
                           is the
                           safest it can be.
                        </center>
                        <br><center>
                           <h3>"See Something, Say Something"</h3>
                        </center>
                        
                        <center>
                           
                           
                           
                        </center>             
                        
                        <p>
                           <a href="documents/Annual-Security-Report-2013.pdf" target="_blank">
                              Annual Security Report
                              </a>
                           
                        </p>
                        
                        <p>  <a href="silentWitness.html">Anonymous Reporting</a><br>
                           
                        </p>
                        
                        <p>         
                           <a href="http://net4.valenciacollege.edu/forms/security/incidentReporting.cfm" target="_blank">Incident Reporting</a><br>
                           
                        </p>
                        
                        <p> <a href="run-hide-fight.html">RUN, HIDE, FIGHT Video</a><br>
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/emergency-contact-numbers.pcf">©</a>
      </div>
   </body>
</html>