<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Events  | Valencia College</title>
      <meta name="Description" content="The Career Pathways Consortium is pleased to announce that it is hosting an event to bring Orange and Osceola high school teachers and Valencia faculty together to jointly review existing Career Pathways articulations. The event takes place on Valencia College&amp;rsquo;s West Campus.">
      <meta name="Keywords" content="events, career, pathways, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Events </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <h2>Events</h2>
                     
                     <hr class="styled_2">
                     
                     &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                     
                     <div class="indent_title_in">
                        
                        <h3>Upcoming: Joint Review of Articulations</h3>
                        =======
                        
                        
                        <link href="/_resources/css/base.css" rel="stylesheet">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div id="preloader">
                           
                           <div class="pulse"></div>
                           
                        </div>
                        <a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
                        
                        
                        <div class="header-alert" id="valencia-alert"></div>
                        
                        
                        <div class="header-chrome desktop-only">
                           
                           <div class="container">
                              
                              <nav>
                                 
                                 <div class="row">
                                    
                                    <div class="chromeNav col-lg-9 col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Main Navigation">
                                       
                                       <ul>
                                          
                                          <li class="submenu">
                                             <a href="javascript:void(0);" class="show-submenu">Students <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                             
                                             <ul>
                                                
                                                <li><a href="/academics/current-students/index.html">Current Students</a></li>
                                                
                                                <li><a href="https://preview.valenciacollege.edu/future-students/">Future Students</a></li>
                                                
                                                <li><a href="https://international.valenciacollege.edu">International Students</a></li>
                                                
                                                <li><a href="/academics/military-veterans/index.html">Military &amp;
                                                      Veterans</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li><a href="/academics/continuing-education/index.html">Continuing Education</a></li>
                                          
                                          <li><a href="/EMPLOYEES/faculty-staff.html">Faculty &amp; Staff</a></li>
                                          
                                          <li><a href="/FOUNDATION/alumni/index.html">Alumni &amp; Foundation</a></li>
                                          
                                       </ul>
                                       &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; fea88b4764015604ad30f5e4d5de1ce47c588a31
                                       
                                    </div>
                                    
                                    <div class="wrapper_indent">
                                       
                                       <h4>March 8, 2017:</h4>
                                       
                                       <p>The Career Pathways Consortium is pleased to announce that it is hosting an event
                                          to bring Orange and
                                          Osceola high school teachers and Valencia faculty together to jointly review existing
                                          Career Pathways
                                          articulations. The event takes place on Valencia College’s West Campus.
                                       </p>
                                       
                                       <p>This will also be a time for consortium partners, Orange Public School District, School
                                          District of
                                          Osceola and Valencia, to engage in collaborative conversations and develop new pathways
                                          for students to
                                          earn college credit. The Consortium is excited about this event and the opportunity
                                          it offers for everyone
                                          to reconnect and strengthen commitment to Career Pathways.
                                       </p>
                                       
                                    </div>
                                    
                                    <hr class="styled_2">
                                    
                                    
                                    <div class="indent_title_in">
                                       
                                       <h3>Past Meetings</h3>
                                       
                                    </div>
                                    
                                    <div class="wrapper_indent">
                                       
                                       <h4>June 10, 2013</h4>
                                       
                                       <p>On June 10th we had a total 94 participants from Valencia College, Orange County Public
                                          Schools and The
                                          School District of Osceola at the Special Events Center on Valencia College's west
                                          campus. We met to
                                          develop an end of course assessments for programs of study that would offer high school
                                          students the means
                                          of earning college credit by passing the end of course assessment.
                                       </p>
                                       
                                       
                                       <p>Participating programs:</p>
                                       
                                       <ul class="list_style_1">
                                          
                                          <li>Accounting</li>
                                          
                                          <li>Marketing</li>
                                          
                                          <li>Building Construction</li>
                                          
                                          <li>Drafting</li>
                                          
                                          <li>Digital Design</li>
                                          
                                          <li>Business</li>
                                          
                                          <li>Culinary</li>
                                          
                                          <li>Lodging Operations &amp; Hospitality Tourism</li>
                                          
                                          <li>Criminal Justice</li>
                                          
                                          <li>Graphics</li>
                                          
                                          <li>TV</li>
                                          
                                          <li>Technology Support Services (previously PC Support)</li>
                                          
                                          <li>Network Support Services</li>
                                          
                                          <li>Web Development</li>
                                          
                                          <li>Computer Programming</li>
                                          
                                          <li>Information Technology</li>
                                          
                                          <li>Health</li>
                                          
                                       </ul>
                                       
                                       
                                       <h4>June 10, 2013</h4>
                                       
                                       <p>The drafting/building construction committee met to discuss the 2013-2014 articulation
                                          and end-of-course
                                          assessment.
                                       </p>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <aside class="col-md-3">
                                    
                                    
                                    <div class="banner">
                                       
                                       <h3>You Earned Them. Get Them.</h3>
                                       
                                       <p>Claim the Career Pathways credits you earned in high school.</p>
                                       <a href="http://net4.valenciacollege.edu/forms/careerpathways/get-my-credits.cfm" class="banner_bt">Claim
                                          Credits</a>
                                       
                                    </div>
                                    
                                    
                                 </aside>
                                 
                              </nav>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     <main role="main">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </main>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/events.pcf">©</a>
      </div>
   </body>
</html>