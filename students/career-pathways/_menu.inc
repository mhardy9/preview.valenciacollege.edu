<ul>
        <li><a href="/students/career-pathways/index.php">Career Pathways</a></li>
        <li><a href="/students/career-pathways/about-us.php">About Us</a></li>
        <li><a href="/students/career-pathways/meet-the-staff.php">Meet the Staff</a></li>



   <li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);">Career Pathways Paths<span class="fas fa-chevron-down" aria-hidden="true"></span></a>
              <div class="menu-wrapper">
                <div class="col-md-12">
                  <h3>Career Pathways Paths</h3>
                  <ul>
                           <li><a href="/students/career-pathways/programs.php">Programs of Study and Articulation</a></li>
                          <li><a href="/students/career-pathways/resources.php">Resources</a></li>
                          <li><a href="/students/career-pathways/techprepoutcome.php">Career Pathways Outcomes</a></li>
                          <li><a href="/students/career-pathways/consortium-codirectors.php">Consortium Co-directors</a></li>
                          <li><a href="/students/career-pathways/consortium-events.php">Consortium Events</a></li>
                          <li><a href="/students/career-pathways/tech-express.php">Tech Express</a></li>
                          <li><a href="/students/career-pathways/tech-express-advisors.php">Tech Express Advisors</a></li>
                 
            </ul>

                </div>
               
              </div>
              <!-- End menu-wrapper --></li>
              

 <li><a href="http://net4.valenciacollege.edu/forms/careerpathways/get-my-credits.cfm" target="_blank">Claim Career Pathways Credits</a></li>
  <li><a href="http://net4.valenciacollege.edu/forms/careerpathways/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
