<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/clusters-pathways.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Career Clusters and Pathways</h2>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>About Career Clusters and Pathways</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Career Pathways are available in the following Career Clusters for Orange and Osceola
                              District school students. These career themes help schools ensure that students acquire
                              the knowledge and skills necessary for multiple career options within each career
                              cluster. Recommended academic and technical curriculum is identified on each career
                              pathway. Students have an opportunity to earn college or technical colege or centercredits
                              for work completed in high school. A specific pathway assessment is required for all
                              areas.
                           </p>
                           
                           <p>Click on a Career Cluster to view or download information specific to that Career
                              Pathway.
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/Agriculture.cfm">Agriculture, Food and Natural Resources</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/ArchitEngineeringTech.cfm">Architectural, Engineering &amp; Technology</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/ArtsEntertainment.cfm">Arts, A/V Technology &amp; Communications</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/BusinessMgmtAdmin.cfm">Business, Management, &amp; Administration</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/Education.cfm">Education</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/EngineeringTechnologyEducation.cfm">Engineering Techonology &amp; Education</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/HealthSciences.cfm">Health Science</a></li>
                              
                              <li><a href="#musicdrama">High School Music and Drama</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/HospitalityCulinary.cfm">Hospitality and Tourism</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/InfoTech.cfm">Information Technology</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/PublicService.cfm">Law, Public Safety &amp; Security</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/MarketingSalesService.cfm">Marketing, Sales &amp; Service</a></li>
                              
                              <li><a href="http://valenciacollege.edu/careerpathways/manufacturing.cfm">Manufacturing</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div id="agriculture" class="indent_title_in">&nbsp;</div>
                        
                        <div id="transportation" class="indent_title_in">&nbsp;</div>
                        
                        <div class="wrapper_indent">&nbsp;</div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div class="banner">
                           
                           <h3>You Earned Them. Get Them.</h3>
                           
                           <p>Claim the Career Pathways credits you earned in high school.</p>
                           <a class="banner_bt" href="http://net4.valenciacollege.edu/forms/careerpathways/get-my-credits.cfm">Claim Credits</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/clusters-pathways.pcf">©</a>
      </div>
   </body>
</html>