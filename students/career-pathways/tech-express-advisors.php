<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/tech-express-advisors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Meet the Tech Express to Valencia Advisors </h2>
                        
                        <h3>Lisa Bliss, Tech Express Advisor, OrangeTechnical College - Mid Florida Campus </h3>
                        
                        <p><img alt="Lisa Bliss" height="137" hspace="5" src="headshotcolor_000.jpg" width="92"> <a href="mailto:lbliss@valenciacollege.edu">lbliss@valenciacollege.edu</a> 
                        </p>
                        
                        <p>Lisa Bliss joined Valencia College in 2016 as member of the  Tech Express to Valencia
                           team. As the Tech  Express Advisor on the Mid-Florida Campus of Orange Technical College
                           (OTC),  Lisa brings to her role, enthusiasm and the many years of experience and 
                           knowledge acquired while employed at Orange County Public Schools. 
                        </p>
                        
                        <p> Lisa’s passion for supporting students is evident through  her smiling face and open
                           door. Lisa  works with students on the Mid Florida campus to create awareness and
                           understanding of the earned credit opportunities and career options Valencia  offers
                           to completers of specific technical programs. Lisa offers OTC students career advice,
                           counseling, and assistance with applying to Valencia. 
                        </p>
                        
                        <p> Lisa holds a Bachelor of Science Degree in Education from  Indiana State University
                           and a Master of Science Degree in School Counseling  from Purdue University. She spent
                           the  majority of her career as a teacher and school counselor for Orange County  Public
                           Schools. Lisa now brings her  leadership and program development skills to the Tech
                           Express team at Valencia  College.
                        </p>
                        
                        
                        <h3>Anita Gentz, Tech Express Advisor, Orange Technical College - <strong>Orlando and Winter Park Campuses</strong>
                           
                        </h3>
                        
                        <img alt="Anita Gentz" height="142" hspace="5" src="AnitaGentz.jpg" width="94">
                        
                        <p> <a href="mailto:agentz@valenciacollege.edu">agentz@valenciacollege.edu</a></p>
                        
                        <p>Anita joined Valencia College in 2016 as one of three Tech  Express Advisors supporting
                           the partnership between Orange County Public  Schools Orange Technical College (OTC)
                           and Valencia.&nbsp; She brings to her  role as Tech Express advisor professional, instructional
                           and administrative  experience as well as a wealth of knowledge acquired during her
                           38-year tenure  at Orange County Public Schools.&nbsp;
                        </p>
                        
                        <p> Anita enjoys encouraging students to take advantage of the  earned college credit
                           opportunities available to students completing specific  technical programs. She is
                           always ready to assist Orange and Winter Park  students with applying to Valencia.
                           Anita believes that transitioning OTC students to an articulated A.S  degree program
                           at Valencia prepares them for success and prosperity in their  chosen career field.
                           
                        </p>
                        
                        <p> Anita has a Bachelor of Arts and Master of Education from  the University of Central
                           Florida. She  holds State of Florida Department of Education certifications for Business
                           Education, Vocational Office Education, Educational Leadership, Principal K-12  and
                           Local Director of Vocational Education.  Anita now uses her experience and credentials
                           to serve students at OTC Orlando  and Winter Park campuses.
                        </p>
                        
                        
                        <h3>Francisco Perez, Tech Express Advisor, Orange Technical College - <strong>Westside and Avalon Campuses and Technical Education Center Osceola  (TECO)</strong>
                           
                        </h3>
                        <img alt="Francisco Perez" height="106" hspace="5" src="francisco-perez-600w.jpg" width="124">
                        
                        <p> <a href="mailto:fperez8@valenciacollege.edu">fperez8@valenciacollege.edu</a></p>
                        
                        <p>As an alumnus who worked in student services while attending  Valencia, Francisco
                           returned to Valencia in 2016 as a member of the Tech  Express to Valencia team. With
                           over nine  years of diverse experience in various roles in higher education, Francisco
                           is  excited to assist Orange Technical College (OTC) and Technical Education Center
                           Osceola (TECO) students - desiring to further their education and careers - with 
                           transitioning to Valencia.
                        </p>
                        
                        <p> Francisco is involved in revising and creating new  articulation agreements as well
                           as increasing student awareness of the  educational opportunities available through
                           the Tech Express partnership. As the Tech Express Advisor on the Avalon and Westside
                           campuses of OTC and at TECO, Francisco particularly enjoys building new relationships
                           with students and strengthening existing relationships with faculty and  administration.
                           
                        </p>
                        
                        <p> Francisco holds his Bachelor of Arts degree in Behavioral  and Social Sciences from
                           the University of Central Florida. He also received his Master of Science in  Human
                           Services - Organizational Management and Leadership from Springfield  College in Massachusetts.
                           Francisco is  a proud Valencia alumnus who is glad to be able to share his experience
                           and  passion for serving students at OTC Avalon and Westside campuses and at the 
                           TECO campus.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="tech-express.html"><img alt="Tech Express to Valencia" src="tech-express-mark.jpg" width="245"></a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/tech-express-advisors.pcf">©</a>
      </div>
   </body>
</html>