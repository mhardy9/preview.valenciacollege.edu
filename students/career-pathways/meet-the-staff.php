<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/meet-the-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Meet the Staff</h2>
                        
                        <h3>Vicki Fine, Manager of Career Pathways and Tech Express to Valencia</h3>
                        
                        <p><a href="mailto:vfine@valenciacollege.edu"><img src="/students/career-pathways/VickiFineSmall_001.jpg" alt="Vicki Fine, Coordinator" width="100" height="142" hspace="10">vfine@valenciacollege.edu</a></p>
                        
                        <p>Vicki joined Valencia College in 2012 as a coordinator in Conferencing and College
                           Events where she was actively involved with college-wide, department, and community
                           events as well as Valencia hosted conferences. Vicki joined the Career and Workforce
                           Education team as coordinator of Career Pathways in 2015. In the coordinator role
                           and as part of the Perkins Grant, Vicki worked with the Career Pathways Consortium,
                           comprised of Valencia College and Orange and Osceola County school districts, to build
                           pathways from secondary to post-secondary education for Career Pathways students interested
                           in attending Valencia College. Vicki is actively involved with Career Pathways on
                           the state level as a Region Vice President in the Florida Career Pathways Network.
                        </p>
                        
                        <p>In 2016 Vicki’s role expanded to manager, Career Pathways.&nbsp; While Vicki continues
                           to work with the Career Pathways Consortium to build secondary pathways, she also
                           works with technical college campuses and Valencia’s Tech Express coaches to develop
                           the exciting Tech Express initiative between Valencia, Orange Technical College and&nbsp;
                           Osceola Technical College.&nbsp; This initiative will serve to create more clearly defined
                           pathways for students at technical college campuses and a better understanding of
                           options available at Valencia.&nbsp; As manager, Career Pathways, Vicki continues to work
                           on strengthening relationships in the community by giving students more opportunities
                           to develop skills, abilities and knowledge that provide life-sustaining wages and
                           meet employers’ needs.
                        </p>
                        
                        <p>Vicki brings her collaboration and organizational skills to this position along with
                           over a decade of experience serving faculty, staff, and students in higher education.
                           She holds a Master in Business Administration and Bachelor of Business Administration
                           from Georgia Southern University in Statesboro, GA. Vicki also received her certifications
                           as a Professional in Human Resources (PHR) in 2013, a SHRM Certified Professional
                           (SHRM-CP) in 2015, a Career Pathways Leader in 2015 and was a graduate from the 2014
                           PIVOT 180 Leadership Academy.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Robert Strobbe, Data Management Specialist</h3>
                        <img src="/students/career-pathways/robert-strobbe.jpg" alt="" width="100" height="139" hspace="10">
                        
                        <p><a href="mailto:rstrobbe@valenciacollege.edu">rstrobbe@valenciacollege.edu</a></p>
                        
                        <p>Rob began working at Valencia in November 2012 as a Data Management Specialist. In
                           this role, he maintains, troubleshoots, and develops the Career Pathways databases.
                           He imports new data and ensures its validity and usefulness. This, in turn, allows
                           him to analyze student assessment results and design useful reports for internal and
                           external stakeholders.
                        </p>
                        
                        <p>Rob holds a Bachelor's of Science in Computer Information Systems (with an emphasis
                           on programming) and an Executive Graduate Certificate in Accounting, both from Strayer
                           University. He's originally from Milwaukee, WI, but lived in the Washington, D.C.
                           area before moving to Florida. There, he was the Data Analyst for The Washington Post's
                           Single Copy division.
                        </p>
                        
                        <p>Rob enjoys creating found object art, most things computer, and spending time with
                           his wife and their dog.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Kailyn Williams, Career Pathways Specialist</h3>
                        <img src="/students/career-pathways/photo-Kailyn-Williams.jpg" alt="" width="100" height="140" hspace="10">
                        
                        <p><a href="mailto:kwilliams14@valenciacollege.edu">kwilliams14@valenciacollege.edu</a></p>
                        
                        <p>A native Floridian and Valencia alumnus, Kailyn has seen the many changes to Florida
                           and Valencia. Fond memories of the Winter Park Mall and Valencia Community College
                           being at the end of Lee Rd, where the new Whole Foods is located now. "Thinking how
                           fun it use to be to drive from Sanford to West campus to watch a basketball game,
                           now the thought of getting on I-4 sends me into a panic," Kailyn recalls. Because
                           of this Kailyn has become a proponent of public transportation and regularly uses
                           the Sunrail and Lynx bus system to get back and forth to Sanford.
                        </p>
                        
                        <p>Kailyn’s first taste of working with Valencia College came as a work-study in the
                           Internship Office. Later, she had the opportunity to serve as Administrative Assistant
                           to the VP of Information Technology. After many years of working behind the scenes,
                           she transitioned into the Bridges to Success department where she worked directly
                           with students, parents, and community organizers. Now as a Career Pathways Specialist,
                           Kailyn takes all her years of experience with Valencia to serve the Career Pathways
                           team and students.
                        </p>
                        
                        <p>When Kailyn is not at Valencia, you can usually find her working at one of her other
                           two jobs or spending time with her three children and dog or just waiting on the next
                           season of The Walking Dead.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/meet-the-staff.pcf">©</a>
      </div>
   </body>
</html>