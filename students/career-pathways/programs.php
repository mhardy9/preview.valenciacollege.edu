<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/programs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="bg_content magnific" style="url(/_resources/images/testimonial_1.jpg) no-repeat center center;">
                           <div>
                              <h3>Explore your career opportunities</h3>
                              <p>Resources for learning about Career Pathways courses and articulated credits by A.S.
                                 degree program and the impact these programs have on students.
                              </p>
                           </div>
                        </div>
                        <a id="content" name="content"></a>
                        
                        <h2>Student, Parent, and Faculty&nbsp;Resources</h2>
                        
                        <p>Career Pathways are available for Orange and Osceola&nbsp;County&nbsp;high school students in&nbsp;a
                           number of&nbsp;Valencia's A.S. degree and certificate programs. There are also several
                           Career Pathways available for Lake, Marion, and Seminole County high school students.&nbsp;
                           These career paths help schools ensure that students acquire the knowledge and skills&nbsp;necessary
                           for&nbsp;high-skill, high-wage career options. Recommended academic and technical curriculum
                           is identified in each career path. Students have an opportunity to earn college credits
                           for work completed in high school by successfully passing the specific pathway assessment
                           required in each career area.&nbsp; Explore your path and see where it can lead.
                        </p>
                        
                        <p>Click on the Career Pathway to view or download information specific to that Career
                           Pathway.&nbsp; Click on the A.S. degree program to get information on the degree for that
                           pathway.&nbsp;
                        </p>
                        
                     </div>
                     
                     <div class="col-md-9"><a href="http://valenciacollege.edu/careerpathways/Agriculture.cfm">Agriculture, Food &amp;&nbsp;Natural Resources</a>&nbsp;Career Pathway to A.S. degree/certificate programs in <a href="https://net1.valenciacollege.edu/future-students/programs/plant-science-and-agricultural-technology/">Plant Science and Agricultural Technology</a> (insert image for this profession)
                     </div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/ArchitEngineeringTech.html">Architecture &amp;&nbsp;Construction</a>&nbsp;Career Pathway to A.S. degree/certificate programs in <a href="https://net1.valenciacollege.edu/future-students/programs/engineering-and-technology/">Engineering and Technology</a> (insert image for profession)
                     </div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/ArtsEntertainment.html">Arts,&nbsp;AV Technology &amp;&nbsp;Communication</a>&nbsp;<span>Career Pathway to A.S. degree/certificate programs in <a href="https://net1.valenciacollege.edu/future-students/programs/arts-and-entertainment/">Arts and Entertainment </a>(image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/BusinessMgmtAdmin.html">Business,&nbsp;Management &amp;&nbsp;Administration</a>&nbsp;<span>Career Pathway to A.S. degree/certificate programs in <a href="https://net1.valenciacollege.edu/future-students/programs/business/">Business</a> (image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/Education.html">Education &amp;&nbsp;Training</a>&nbsp;<span>Career Pathway to A.A. degree/certificate in <a href="https://net1.valenciacollege.edu/future-students/programs/education/">Education</a> (Image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/EngineeringTechnologyEducation.html">Engineering Technology &amp; Education</a>&nbsp;<span>Career Pathway to A.S. degree/certificate in <a href="https://net1.valenciacollege.edu/future-students/programs/engineering-and-technology/">Engineering and Technology</a> (image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/HealthSciences.html">Health Science</a>&nbsp;<span>Career Pathway to A.S. degree/certificate programs in <a href="https://net1.valenciacollege.edu/future-students/programs/health-sciences/">Health Sciences</a> (image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/HospitalityCulinary.html">Hospitality &amp;&nbsp;Tourism</a>&nbsp;<span>Career Pathway to A.S. degree/certificate programs in <a href="https://net1.valenciacollege.edu/future-students/programs/hospitality-and-culinary/">Hospitality and Culinary </a>(image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/InfoTech.html">Information Technology</a>&nbsp;<span>Career Pathway to A.S. degree/certificate in <a href="https://net1.valenciacollege.edu/future-students/programs/information-technology/">Information Technology</a> (image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/PublicService.html">Law,&nbsp;Public Safety &amp;&nbsp;Security</a>&nbsp;<span>Career Pathway to A.S. degree/certificate in <a href="https://net1.valenciacollege.edu/future-students/programs/public-safety-and-paralegal-studies/">Public Safety and Legal </a>(image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/MarketingSalesService.html">Marketing,&nbsp;Sales &amp;&nbsp;Service</a>&nbsp;<span>Career Pathway to A.S. degree/certificate in <a href="https://net1.valenciacollege.edu/future-students/programs/business/">Business Administration </a>(image)</span></div>
                     
                     <div class="col-md-9"><a href="/students/career-pathways/manufacturing.html">Manufacturing</a>&nbsp;<span>Career Pathway to A.S. degree/certificate in <a href="https://net1.valenciacollege.edu/future-students/programs/engineering-and-technology/">Engineering and Technology </a>(image)</span></div>
                     
                     <div class="col-md-9">
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/http://valenciacollege.edu/careerpathways/techprepoutcome.cfm" target=""><i class="far fa-download"></i><h3>Reports and Data</h3>
                                 <p>See what kind of impact Career Pathways makes</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/http://www.fldoe.org/academics/career-adult-edu/career-technical-edu-agreements/industry-certification.stml" target=""><i class="far fa-certificate"></i><h3>Industry Certifications</h3>
                                 <p>List of Florida Statewide Articulation Agreements</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/http://www.fldoe.org/academics/career-adult-edu/career-tech-edu/curriculum-frameworks/" target=""><i class="far fa-folder-open"></i><h3>Curriculum Frameworks</h3>
                                 <p>List of Florida Department of Education course curriculum frameworks</p></a>
                              
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12 col-sm-12">&nbsp;</div>
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/programs.pcf">©</a>
      </div>
   </body>
</html>