<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/about-us.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="bg_content magnific" style="url(/_resources/images/testimonial_1.jpg) no-repeat center center;">
                           <div>
                              <h3>Earn college credit&nbsp;in high school</h3>
                              <p>Complete one or more Career Pathways courses to get a head start on a career path
                                 and a college degree or certificate.&nbsp; You may be in one of these course right now.
                              </p>
                           </div>
                        </div>
                        <a id="content" name="content"></a>
                        
                        <h2>About Career Pathways</h2>
                        
                        <h3>Explore career interests while earning college credit</h3>
                        
                        <p>Career Pathways can help train you for a high-skill, high-wage job.&nbsp; Explore your
                           career interests by taking one or more articulated career and technical education
                           courses in high school.&nbsp;
                        </p>
                        
                        <h3>Academic Preparation</h3>
                        
                        <p>Students may begin focusing on higher academics and technical skills through Career
                           Pathways while in high school. Career Pathways programs&nbsp;link high school studies to
                           certificate and A.S. degree programs at Valencia. Credits earned through Career Pathways
                           can lead to a two-year certificate, an associate's degree, or a bachelor's degree.
                        </p>
                        
                        <h3>Earn college credits in high school</h3>
                        
                        <p>Career Pathways enables students to take articulated Career and Technical Education
                           courses while in high school, successfully pass the assessments, and become eligible
                           to receive college credit when they enroll at Valencia College.
                        </p>
                        
                        <h3>Save time and money on a college degree</h3>
                        
                        <p>Transfer Career Pathways credits to a degree or certification program at Valencia.&nbsp;
                           By earning college credit while in high school, students get a head start on a college
                           degree--saving time and money.
                        </p>
                        
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/http://valenciacollege.edu/careerpathways/programs.cfm" target=""><i class="far fa-map-pin"></i><h3>Students and parents</h3>
                                 <p>Find career paths that interest you.</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/students/career-pathways/documents/ImportantThingstoKnow2017.pdf" target=""><i class="far fa-question-circle"></i><h3>Frequently asked questions</h3>
                                 <p>Important things to know</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/http://net4.valenciacollege.edu/forms/careerpathways/get-my-credits.cfm" target=""><i class="far fa-ticket"></i><h3>Claim your credits</h3>
                                 <p>Claim your credits when you enroll at Valencia.</p></a>
                              
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/http://valenciacollege.edu/careerpathways/programs.cfm" target=""><i class="far fa-book"></i><h3>Teachers and Faculty</h3>
                                 <p>Resources for teachers</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/http://net4.valenciacollege.edu/forms/careerpathways/contact.cfm" target=""><i class="far fa-envelope-open"></i><h3>Contact us</h3>
                                 <p>If you can't find the answer here, contact us.</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/http://valenciacollege.edu/careerpathways/meet-the-staff.cfm" target=""><i class="far fa-users"></i><h3>Meet the staff</h3>
                                 <p>We are here to serve you.</p></a>
                              
                           </div>
                        </div>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/about-us.pcf">©</a>
      </div>
   </body>
</html>