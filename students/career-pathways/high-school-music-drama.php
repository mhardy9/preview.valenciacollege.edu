<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/high-school-music-drama.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>High School Music and Drama</h2>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        
                        <h3>Programs</h3>
                        
                        <p>Browse our music and theater programs.</p>
                        
                        
                        <div data-old-tag="table">
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="http://valenciacollege.edu/asdegrees/arts/mpt.cfm">Sound and Music Technology (AS)</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/musicperformance/">Music and Performace (AA)</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="http://valenciacollege.edu/asdegrees/arts/ed.cfm">Entertainment Design Technology (AS)</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/theatredramadramaticarts/">Theater and Dramatic Arts (AA)</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                        </div>
                        
                        
                        
                        <h3>Upcoming Events</h3>
                        
                        
                        
                        <div data-id="evita">
                           
                           
                           <div>
                              Oct<br>
                              <span>20</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/evita" target="_blank">Evita at East Campus Performing Arts Center (PAC)</a><br>
                              
                              <span>7:30 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="red_black_and_ignorant_part_one_of_the_war_plays_trilogy">
                           
                           
                           <div>
                              Nov<br>
                              <span>09</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/red_black_and_ignorant_part_one_of_the_war_plays_trilogy" target="_blank">Red Black and Ignorant (part one of The War Plays Trilogy) at Valencia College East
                                 Campus</a><br>
                              
                              <span>7:30 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="choreographers_showcase_6146">
                           
                           
                           <div>
                              Nov<br>
                              <span>17</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/choreographers_showcase_6146" target="_blank">Choreographers' Showcase at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="winter_choral_concert">
                           
                           
                           <div>
                              Nov<br>
                              <span>29</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/winter_choral_concert" target="_blank">Winter Choral Concert at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>7:30 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="3_in_motion_3920">
                           
                           
                           <div>
                              Jan<br>
                              <span>26</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/3_in_motion_3920" target="_blank">3 in Motion at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="tech-express.html"><img alt="Tech Express to Valencia" src="tech-express-mark.jpg" width="245"></a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/high-school-music-drama.pcf">©</a>
      </div>
   </body>
</html>