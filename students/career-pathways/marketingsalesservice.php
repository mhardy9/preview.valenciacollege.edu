<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/marketingsalesservice.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Marketing, Sales &amp; Service Career Pathways</h2>
                        
                        
                        <blockquote>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> 
                                       <div>
                                          
                                          <h3>2016-2017 Programs of Study (Pathways) and Articulation</h3>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h4>Career and technical education Program</h4>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <h4>Programs of Study*</h4>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <h4>Articulations to Valencia </h4>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Marketing Management and Entrepreneurial Principles </strong></div>
                                    
                                    <div data-old-tag="td">
                                       
                                       <p><a href="http://valenciacollege.edu/careerpathways/documents/MarketingOrangePOS2016-17New.doc">Orange County </a></p>
                                       
                                       <p><a href="http://valenciacollege.edu/careerpathways/documents/MarketingOsceolaPOS2016-17New.doc">Osceola County </a></p>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"><a href="http://valenciacollege.edu/careerpathways/documents/MarketingArticulation2016-2017New.docx">Articulation Agreement for award of credit </a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>Sport, Recreation &amp; 
                                          Entertainment Marketing </strong></div>
                                    
                                    <div data-old-tag="td"><a href="http://valenciacollege.edu/careerpathways/documents/SportRecreationMktgOrangePOS2016-17New.doc">Orange 
                                          County</a></div>
                                    
                                    <div data-old-tag="td"><a href="http://valenciacollege.edu/careerpathways/documents/SportsMarketingArticulation2016-2017New.docx">Articulation Agreement for award of credit </a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p><em>*Please click on links in the Programs of Study documents to see what pathways are
                                 available for this Career and Technical Education program.</em> 
                           </p>
                           
                           <p><strong>Check out the Technical Articulation Agreements available to students who complete
                                 an <a href="../asdegrees/credit_octc.html">Orange Technical College </a>or a <a href="../asdegrees/credit_teco.html">Technical Education Center Osceola </a> certificate program.</strong></p>
                           
                           <p><strong>View a comprehensive list of <a href="http://www.fldoe.org/workforce/dwdframe/artic_indcert2aas.asp">Florida Statewide Articulation Agreements based on Industry Certifications</a>.</strong></p>
                           
                           <p>The Career Clusters icons are being used with permission of the: <img alt="Career Clusters Icons from www.careerclusters.org" height="49" src="clip_image002_010.jpg" width="53">&nbsp;&nbsp;States’ Career Clusters Initiative, 2011, <a href="http://www.careerclusters.org/">www.careerclusters.org </a></p>
                           
                           
                           
                        </blockquote>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/marketingsalesservice.pcf">©</a>
      </div>
   </body>
</html>