<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Outcomes  | Valencia College</title>
      <meta name="Description" content="The following links provide you with outcomes data for the consortium for the 2011-2012, 2010-2011, 2009-2010, 2008-2009 and 2007-2008 school years. Additional detail data by high school is available.">
      <meta name="Keywords" content="career, pathways, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/outcomes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Outcomes </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Career Pathways Outcomes</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <p>The following links provide you with outcomes data for the consortium for the 2011-2012,
                              2010-2011,
                              2009-2010, 2008-2009 and 2007-2008 school years.
                              Additional
                              detail data by high school is available.
                           </p>
                           
                           
                           <p>For more information, please <a href="mailto:vfine@valenciacollege.edu">contact Vicki Fine</a>.
                           </p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/students/career-pathways/annual-report-2013-2014.pdf">2013-2014
                                    Annual Report</a></li>
                              
                              <li><a href="/documents/students/career-pathways/annual-report-2012-2013.pdf">2012-2013
                                    Outcomes Book</a></li>
                              
                              <li><a href="/documents/students/career-pathways/annual-report-2011-2012.pdf">2011-2012
                                    Outcomes Book</a></li>
                              
                              <li><a href="/documents/students/career-pathways/annual-report-2010-2011.pdf">2010-2011
                                    Outcomes Book</a></li>
                              
                              <li><a href="/documents/students/career-pathways/annual-report-2009-2010.pdf">2009-2010
                                    Outcomes Book</a></li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="banner">
                        <i class="iconcustom-school"></i>
                        
                        <h3>You Earned Them. Get Them.</h3>
                        
                        <p>Claim the Career Pathways credits you earned in high school.</p>
                        <a href="http://net4.valenciacollege.edu/forms/careerpathways/get-my-credits.cfm" class="banner_bt">Claim
                           Credits</a>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/outcomes.pcf">©</a>
      </div>
   </body>
</html>