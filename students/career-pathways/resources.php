<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Resources</h2>
                        
                        
                        <div>
                           
                           
                           <h3>Curriculum Resources for Teachers</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Resources for teachers interested in integrating academic and technical curriculum.</p>
                                 
                                 <p><a href="http://www.aacc.nche.edu/Pages/default.aspx">AACC - American Association of Community Colleges</a></p>
                                 
                                 <p><a href="http://www.acteonline.org/">ACTE - American Association of Career and Technical Education</a></p>
                                 
                                 <p><a href="http://casn.berkeley.edu/">CASN - Career Academy Support Network</a></p>
                                 
                                 <p><a href="http://www.cordcommunications.com/">CORD Communications</a></p>
                                 
                                 <p><a href="http://www.fldoe.org/workforce/ced/">Educator's Tool Kit</a></p>
                                 
                                 <p><a href="http://www.edweek.org/ew/index.html">Education Week on the Web</a></p>
                                 
                                 <p><a href="http://www.eschoolnews.com/">eSchool News Online</a></p>
                                 
                                 <p><a href="http://www.facte.org/">FACTE - Florida Association for Career and Technical Education</a></p>
                                 
                                 <p><a href="http://www.fldoe.org/">FLDOE - Florida Department of Education</a></p>
                                 
                                 <p><a href="http://www.ftpn.org/">FCPN - Florida Career Pathways Network</a></p>
                                 
                                 <p><a href="http://foundationcenter.org/">The Foundation Center</a></p>
                                 
                                 <p><a href="http://www.daggett.com/">International Center for Leadership in Education</a></p>
                                 
                                 <p><a href="http://www.iste.org/">International Society for Technology Education</a></p>
                                 
                                 <p><a href="http://www.k12grants.info/">K-12 Grant Opportunities</a></p>
                                 
                                 <p><a href="http://www.league.org/">League for Innovation</a></p>
                                 
                                 <p><a href="http://www.naf-education.org/">National Academy Foundation</a></p>
                                 
                                 <p><a href="http://nces.ed.gov/nationsreportcard/">National Center for Education Statistics</a></p>
                                 
                                 <p><a href="http://www.nctm.org/">National Council of Teachers of Mathematics</a></p>
                                 
                                 <p><a href="http://www.nea.org/">National Education Association</a></p>
                                 
                                 <p><a href="https://www.nsf.gov/">National Science Foundation</a></p>
                                 
                                 <p><a href="http://www.nsta.org/">National Science Teachers Association</a></p>
                                 
                                 <p><a href="http://nawionline.org/">NAWI - National Association for Workforce Improvement</a></p>
                                 
                                 <p><a href="https://www.nsf.gov/">NCPN - National Career Pathways Network</a></p>
                                 
                                 <p><a href="http://www.nths.org/">NTHS - National Technical Honor Society</a></p>
                                 
                                 <p><a href="https://www2.ed.gov/about/offices/list/ovae/index.html">OCTAE - Office of Career, Technical, and Adult Education</a> 
                                 </p>
                                 
                                 <p><a href="http://www.sreb.org/page/1078/high_schools_that_work.html">SREB High Schools That Work</a></p>
                                 
                                 <p><a href="http://www.toshiba.com/taf/">Toshiba America Foundation</a></p>
                                 
                                 <p><a href="http://www.census.gov/">U.S. Census Bureau</a></p>
                                 
                                 <p><a href="http://www.weareteachers.com/">We Are Teachers</a> (New) 
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Orange and Osceola Schools</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Check out Orange County Public Schools or The School District of Osceola County to
                                    find information about  Career Pathways programs.
                                 </p>
                                 
                                 <ul>
                                    
                                    <li><a href="https://www2.ocps.net/cms/one.aspx?pageId=87912">Orange County Public Schools Career &amp; Technical Education</a></li>
                                    
                                    <li><a href="http://osceolaschools.net/departments/career_and_technical_education/career_pathways/">The School District of Osceola County Career &amp; Technical Education</a></li>
                                    
                                 </ul>
                                 See what degrees and certificate programs are available to Career Pathways students
                                 at Valencia.
                                 
                                 <ul>
                                    
                                    <li><a href="https://preview.valenciacollege.edu/future-students/programs/">Valencia Degree and Certificate Programs</a></li>
                                    
                                 </ul>
                                 
                                 <p>Make the move NOW toward a future that won't let you down. Make Career Pathways work
                                    for you!
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>CTE Articles and Publications</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Time Magazine article about Career and Technical Education entitled, <a href="http://www.time.com/time/magazine/article/0,9171,2113794,00.html#ixzz1u1EGKHzn%23ixzz1u1EGKHzn">"Learning That Works</a><a href="http://www.time.com/time/magazine/article/0,9171,2113794,00.html#ixzz1u1EGKHzn%23ixzz1u1EGKHzn">"</a>.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Articulation &amp; Assessment</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Check out the Technical <a href="../asdegrees/transferagreements.html">Articulation Agreements</a> 
                                    available to Postsecondary Technical College students!
                                 </p>
                                 
                                 <p>View a comprehensive list of <a href="http://www.fldoe.org/workforce/dwdframe/artic_indcert2aas.asp">Florida Statewide Articulation Agreements</a> based on Industry Certifications.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="tech-express.html"><img alt="Tech Express to Valencia" src="tech-express-mark.jpg" width="245"></a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/resources.pcf">©</a>
      </div>
   </body>
</html>