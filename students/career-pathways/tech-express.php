<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/tech-express.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>About Tech Express </h2>
                        
                        
                        <h3>Tech Express Advisor Locations</h3>
                        
                        
                        <ul>
                           
                           <li><a href="https://otc.ocps.net/locationses/mid_florida" target="_blank">Orlando Technical College - Mid Florida Campus</a></li>
                           
                           <li><a href="https://otc.ocps.net/locationses/orlando" target="_blank">Orlando Technical College - Orlando Campus</a></li>
                           
                           <li><a href="https://otc.ocps.net/locationses/Westside" target="_blank">Orlando Technical College - Westside Campus</a></li>
                           
                           <li><a href="http://teco.osceolaschools.net/home" target="_blank">Technical Education Center - Osceola Campus</a></li>
                           
                           <li><a href="https://otc.ocps.net/locationses/winter_park" target="_blank">Orlando Technical College - Winter Park Campus</a></li>
                           
                           <li><a href="https://otc.ocps.net/locationses/avalon" target="_blank">Orlando Technical College - Avalon Campus</a></li>
                           
                        </ul>
                        
                        
                        
                        <h3>What is Tech Express? A great opportunity! </h3>
                        
                        <p>A new partnership between Orange Technical College, Technical Education Center Osceola
                           and Valencia that makes it easy for students in technical programs to transition to
                           Valencia College. Students completing approved technical programs earn credits toward
                           a college degree. Tech Express to Valencia offers access to a Valencia Tech Express
                           Advisor located on each technical college campus to assist students with reviewing
                           career options and opportunities available for students to earn credit. Tech Express
                           Advisors provide information on Valencia programs that map to technical college programs
                           and assist students in applying and enrolling at Valencia—ensuring a smooth and seamless
                           transition. 
                        </p>
                        
                        <h3>Benefits of Tech Express - Earned College Credit!</h3>
                        
                        <p>Benefits include the opportunity to earn college credit by completing approved technical
                           college programs; student and instructor access to a Valencia Tech Express Advisor
                           who can provide on-site assistance with the process to submit documentation necessary
                           to receive earned credit; on-site assistance with Valencia's application and enrollment
                           process; an opportunity to explore additional career options and expand future career
                           potential; and credit that can be applied to one of  20 different A.S. degree programs
                           available at Valencia. 
                        </p>
                        
                        <h3>How does Tech Epress work? </h3>
                        
                        <p>Meet with the Tech Express advisor on your campus to go over the program requirements
                           and process for receiving credit; complete an approved technical program that is eligible
                           for award of credit; meet the articulation agreement requirements; enroll at Valencia
                           and complete one course in the program linked to your technical program; pass the
                           approved assessment; and receive anywhere from 3 to 27 credits (depending on the articulation
                           agreement). 
                        </p>
                        
                        <h3>Save Time and Money</h3>
                        
                        <p>Make a smooth transition to Valencia   from an approved technical program that saves
                           you time and money by earning credits that can be applied toward an A.S. degree.
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="tech-express.html"><img alt="Tech Express to Valencia" src="tech-express-mark.jpg" width="245"></a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/tech-express.pcf">©</a>
      </div>
   </body>
</html>