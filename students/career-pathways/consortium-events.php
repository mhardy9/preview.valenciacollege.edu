<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Pathways | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-pathways/consortium-events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-pathways/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Pathways</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-pathways/">Career Pathways</a></li>
               <li>Career Pathways</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Consortium Events</h2>
                        
                        <p><strong>MARCH 8, 2017- CAREER PATHWAYS CONSORTIUM COLLABORATION ON PATHWAYS FOR AWARD OF CREDIT
                              </strong></p>
                        
                        
                        <p>The Career Pathways Consortium hosted an event that brought Orange and Osceola high
                           school teachers and Valencia faculty together to jointly review existing Career Pathways
                           articulations. The event took place at the Special Events Center on Valencia College’s
                           West Campus and over 100 consortium partners were in attendance.. 
                        </p>
                        
                        <p>Consortium partners from Orange Public School District, School District of Osceola
                           and Valencia engaged in collaborative conversations that explored opportunities for
                           developing new pathways for students to earn college credit. The Consortium was pleased
                           and excited about the opportunity this event offered for everyone to reconnect and
                           strengthen commitment to Career Pathways.
                        </p>
                        
                        
                        <h4>June 10, 2013</h4>
                        
                        <p>On June 10th we had a total 94 participants  from Valencia College, Orange County
                           Public Schools and The School District of  Osceola at the Special Events Center on
                           Valencia College's west campus. We met to develop an end of course  assessments for
                           programs of study that would offer high school students the  means of earning college
                           credit by passing the end of course assessment.
                        </p>
                        
                        
                        
                        
                        <h4> Programs  that participated</h4>
                        
                        <ul>
                           
                           <li> Accounting</li>
                           
                           <li> Marketing</li>
                           
                           <li> Building  Construction</li>
                           
                           <li> Drafting</li>
                           
                           <li> Digital  Design</li>
                           
                           <li> Business</li>
                           
                           <li> Culinary</li>
                           
                           <li> Lodging  Operations &amp; Hospitality Tourism</li>
                           
                           <li> Criminal  Justice</li>
                           
                           <li> Graphics</li>
                           
                           <li> TV</li>
                           
                           <li> Technology  Support Services (previously PC Support)</li>
                           
                           <li> Network  Support Services</li>
                           
                           <li> Web  Development</li>
                           
                           <li> Computer  Programming</li>
                           
                           <li> Information  Technology</li>
                           
                           <li> Health</li>
                           
                        </ul>
                        
                        <h3>Drafting Articulation Meeting</h3>
                        
                        <h4>November 4th, 2013</h4>
                        
                        <p>The drafting/building construction committee meet to discuss the 2013-2014 articulation
                           and end of course assessment.
                        </p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="tech-express.html"><img alt="Tech Express to Valencia" src="tech-express-mark.jpg" width="245"></a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-pathways/consortium-events.pcf">©</a>
      </div>
   </body>
</html>