<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Sandbox Landing | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/sandbox/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_landing_1">
         <div id="intro_txt">
            <h1>Sandbox Landing</h1>
            <nav>
               <div class="page-menu" id="page_menu">
                  <ul>
                     
                     <li><a href="http://valenciacollege.edu/admissions-records/officialtranscripts.cfm">Transcript Request</a></li>
                     
                     <li><a href="http://valenciacollege.edu/admissions-records/enrollment_verification.cfm">Enrollment Verification</a></li>
                     
                     <li><a href="http://valenciacollege.edu/admissions-records/degree_verification.cfm">Degree Verification</a></li>
                     
                  </ul>
               </div>
            </nav>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li>Sandbox</li>
            </ul>
         </div>
      </div>
      <main role="main">
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-9">
                  <div class="box_style_1">
                     <div class="wrapper_indent">
                        <h2>Admission <a href="/sandbox/program-detail.php">Details</a></h2>
                        
                        <p>Thank you for visiting the Admissions website of Valencia College! We are glad you
                           are here and we hope that you find the information on this website to be helpful as
                           you transition to one of the premiere learning-centered institutions in the United
                           States.
                        </p>
                        
                        <p class="add_bottom_30">You will be admitted to Valencia College as a degree-seeking student if you have a
                           standard high school diploma or a state-issued General Educational Development (GED)
                           diploma. To be admitted as a degree-seeking student you must have an admissions application
                           and official academic transcript(s) on file in the Admissions and Records Office.
                           If you do not have a high school diploma or GED, you may be admitted as a Provisional
                           non-degree seeking student. For a complete list of requirements, visit the Summary
                           of Admissions Requirements Chart in our catalog.
                        </p>
                        
                        <p><img class="img-responsive fullwidth" src="/_resources/images/landing/IMG_0020.jpg" alt="Valencia image description"></p>
                        
                        <h3>Admissions Deadlines</h3>
                        
                        <p>Remember, it takes time to process your admissions application. The earlier you apply,
                           the better! Please visit the online Important Dates &amp; Deadlines calendar for admissions
                           Application Priority Deadlines.
                        </p>
                        
                        <h3>Application Fees</h3>
                        
                        <p>There is a non-refundable application fee of $35.00 for Associate's degree level applicants
                           and for Bachelor's degree level applicants (you must have already completed an Associate's
                           degree in order to apply for admission to a Bachelor's degree program at Valencia).
                           Dual Enrollment students cannot apply online and should visit the Dual Enrollment
                           website for application instructions. Previous Valencia students who have not taken
                           classes here in the past two (2) years will need to reapply for admission and pay
                           the non-refundable $35.00 readmission fee.
                        </p>
                     </div>
                  </div>
               </div>
               <aside class="col-md-3">
                  		
                  		
                  <div>
                     	
                     <ul class="action_palette">
                        		
                        <li><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.p_disploginnew?in_id=&amp;cpbl=&amp;newid=" class="button_action landing">Apply Now <i class="fa fa-long-arrow-right"></i></a></li>
                        		
                        <li><a href="http://net4.valenciacollege.edu/promos/internal/request-info.cfm" class="button_action_outline landing">Request Information <i class="fa fa-long-arrow-right"></i></a></li>
                        		
                        <li class="row" style="margin-left:-.5em"><a href="http://net1.valenciacollege.edu/future-students/visit-valencia/" class="button_action_outline split landing col-md-6">Visit Valencia <i class="fa fa-long-arrow-right"></i></a> <span class="col-md-1 split landing">&nbsp;</span>
                           			<a href="https://vcchat.valenciacollege.edu/CCPROChat/CPChatRequest.jsp?form.ServiceID=29&amp;form.CustName=WebCaller&amp;form.SEcureChat=1" class="button_action_outline landing split col-md-6" target="foo" onsubmit="window.open('', 'foo', 'width=450,height=500,status=yes,resizable=yes,scrollbars=yes,location=no')">Chat With Us <i class="fa fa-long-arrow-right"></i></a></li>
                        	
                     </ul>
                     
                  </div>
                  		<br>
                  		<br>
                  		
                  <div>
                     <h4 class="v-red cap"><strong>Contact us</strong></h4>
                     <p>Visit us on campus at the Answer Center.</p>
                     <div>
                        <h5 class="val">Locations</h5><i class="far fa-map-marker fa-spacer"></i>East Building 5, Room 211<br><i class="far fa-map-marker fa-spacer"></i>Lake Nona Building 1, Room 149<br><i class="far fa-map-marker fa-spacer"></i>Osceola Building 2, Room 150<br><i class="far fa-map-marker fa-spacer"></i>West SSB, Room 106<br><i class="far fa-map-marker fa-spacer"></i>Winter Park Building 1, Room 210<br><h5 class="val">Hours</h5><i class="far fa-calendar fa-spacer"></i>Mon. – Thurs. 8 a.m. – 6 p.m.<br><i class="far fa-calendar fa-spacer"></i>Friday 9 a.m. – 5 p.m.<br><br><i class="far fa-envelope fa-spacer"></i>enrollment@valenciacollege.edu<br><i class="far fa-phone fa-spacer"></i>407-582-1507<br></div>
                  </div>
                  	
               </aside>
            </div>
         </div>
         		
         <div class="container-fluid container_gray_bg">
            <div class="container margin_30">
               <div class="row wrapper_indent">
                  <div class="col-md-2 v-red">STEPS TO ENROLL</div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">1</span><span class="step_to_do_text">Apply for admission and financial aid</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">2</span><span class="step_to_do_text">Create your ATLAS account</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">3</span><span class="step_to_do_text">register for classes &amp; pay tuition and fees</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">4</span><span class="step_to_do_text">get your students identification and parking decal</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
                  <div class="col-md-2"><span class="step_to_do_number">5</span><span class="step_to_do_text">buy your books and go to class</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
               </div>
               <div class="row desktop_only">
                  <div class="col-md-2"></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
               </div>
            </div>
         </div>
         		
         		
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-6">
                  <div class="wrapper_indent">
                     						
                     <h4 class="v-red cap">Degree Verification</h4>
                     						
                     <p>DegreeVerify provides instant online verifications of college degrees or attendance
                        claimed by job applicants. The service is designed to simplify verification for employers,
                        background screening firms, executive search firms and employment agencies, who regularly
                        screen candidates.
                     </p>
                     						<strong>Degree Verification Instructions</strong>
                     						
                     <p>For Degree Verification go to the National Student Clearinghouse website and click
                        on the VERIFY button to get started.
                     </p>
                     						
                     <p>Valencia College submits degree information to the Clearinghouse once per term. There
                        is a fee for Degree Verification; please see the National Student Clearinghouse website
                        for more information.
                     </p>
                     					
                  </div>
               </div>
               <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;"><img class="img-responsive fullwidth" src="/_resources/images/landing/IMG_9913.jpg" alt="Students in Computer Lab"></div>
            </div>
         </div>
         		
         		
         <div class="container-fluid container_gray_bg">
            <div class="container margin_30">
               <div class="indent_title_in">
                  <h4 class="v-red cap">Florida residency</h4>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="wrapper_indent"><strong>Reminder</strong>
                        						
                        <p>Students who are eligible for Florida residency for tuition purposes pay a much lower
                           tuition rate. See the current rates.
                        </p>
                        						
                        <p>For students who fail to submit documentation and still register for classes, tuition
                           will be charged at the out-of-state rate.
                        </p>
                        						
                        <p>If your request for Florida Residency and the necessary documentation is not received
                           prior to the Proof of Florida Residency Deadline as listed in the Important Dates
                           and Deadlines calendar, your residency will be processed for the next available term.
                        </p>
                        						<strong>Degree Verification Instructions</strong>
                        						
                        <p>For Degree Verification go to the National Student Clearinghouse website and click
                           on the VERIFY button to get started.
                        </p>
                        						
                        <p>Valencia College submits degree information to the Clearinghouse once per term. There
                           is a fee for Degree Verification; please see the National Student Clearinghouse website
                           for more information.
                        </p>
                        					
                     </div>
                  </div>
                  <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;">
                     						
                     <p><strong>Dependent vs. Independent Students</strong></p>
                     						
                     <p>The determination of dependent or independent student status is important because
                        it is the basis for whether the student has to submit his/her own documentation for
                        residency (as an independent) or his/her parent's or guardian's documentation of residency
                        (as a dependent). Parent means one or both of the parents of a student, any guardian
                        of a student or any person in a parental relationship to a student.
                     </p>
                     						
                     <div class="row"><span class="date_stacked col-md-2">09 <span>MAR</span></span><span>Proof of Florida Residency Deadline</span><span><a class="button_outline_small" href="/" target="">submit residency</a></span></div>
                     					
                  </div>
               </div>
            </div>
         </div>
         		
         		
         <div class="container margin_60">
            <div class="main_title">
               						
               <h2>Frequently questions</h2>
               						
               <p>Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.</p>
               					
            </div>
            						
            <div class="row">
               <div class="col-md-4">
                  <div class="box_style_2">
                     										
                     <h3>Et ius tota recusabo democritum?</h3>
                     										
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     									
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="box_style_2">
                     										
                     <h3>Posse exerci volutpat has?</h3>
                     										
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     									
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="box_style_2">
                     										
                     <h3>Te pri facete latine salutandi?</h3>
                     										
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     									
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="box_style_2">
                     										
                     <h3>Et ius tota recusabo democritum?</h3>
                     										
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     									
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="box_style_2">
                     										
                     <h3>Mediocritatem sea ex, nec id agam?</h3>
                     										
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     									
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="box_style_2">
                     										
                     <h3>Te pri facete latine salutandi?</h3>
                     										
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     									
                  </div>
               </div>
            </div>
            					
         </div>
         	
      </main><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/sandbox/index.pcf">©</a>
      </div>
   </body>
</html>