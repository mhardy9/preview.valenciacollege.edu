<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Internal Page | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/sandbox/internal-page-1.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/sandbox/">Sandbox</a></li>
               <li>Internal Page</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               <div id="lipsum">
                  
                  <h2>Lorem ipsum <a href="/sandbox/_props.html">dolor</a></h2>
                  
                  <p>Sit amet, consectetur adipiscing elit. Curabitur convallis semper aliquet. Curabitur
                     gravida eros ut justo consequat tempor. Pellentesque habitant morbi tristique senectus
                     et netus et malesuada fames ac turpis egestas.
                  </p>
                  
                  <p>Vestibulum pretium odio ac arcu ultrices euismod. Fusce porta magna lectus, et fringilla
                     lacus volutpat dictum. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                     Sed quis nulla sed nibh malesuada interdum. Proin varius arcu mattis vestibulum rutrum.
                     Proin pellentesque tortor eu consequat fringilla. Morbi eu mauris at lectus porttitor
                     egestas. Cras interdum rutrum fringilla. Duis ut euismod quam. Donec consequat ultricies
                     odio a luctus. Ut pretium ex non orci pellentesque tempor. Suspendisse potenti. Phasellus
                     quis magna convallis, pellentesque felis sit amet, porta nunc.
                  </p>
                  
                  <h3>Proin et condimentum lectus.</h3>
                  
                  <p>Aliquam ex ante, interdum nec lorem in, tincidunt posuere metus. Integer tristique
                     rutrum pharetra. Vivamus auctor ornare fringilla. Pellentesque habitant morbi tristique
                     senectus et netus et malesuada fames ac turpis egestas. Nunc libero est, dictum mattis
                     neque at, interdum efficitur ipsum. Curabitur lacus ante, dignissim ac nisl vel, porttitor
                     pretium leo.
                  </p>
                  
                  <h3>In malesuada,</h3>
                  
                  <p>justo id finibus imperdiet, justo augue elementum massa, sit amet malesuada tortor
                     ante sit amet purus. Praesent maximus, nisi non fringilla fringilla, est sem scelerisque
                     nunc, et gravida purus sapien ut metus. Pellentesque tincidunt, urna ut molestie pulvinar,
                     justo lacus imperdiet risus, sed rhoncus est lorem ac felis.
                  </p>
                  
                  <p>Morbi metus leo, pulvinar at sapien non, ornare efficitur elit. Donec eget condimentum
                     diam. Cras varius laoreet eros, eu pretium mi sollicitudin vitae. Nulla at justo neque.
                     Mauris maximus aliquam ornare. Pellentesque sem quam, fermentum nec neque quis, interdum
                     rhoncus massa. Maecenas ac volutpat massa. Suspendisse orci justo, gravida a elit
                     iaculis, ultrices commodo eros. Aenean pellentesque libero blandit turpis cursus,
                     et dictum enim pulvinar. Fusce pulvinar facilisis risus ut mattis. Quisque eu tempor
                     mi, non aliquam purus. Pellentesque non maximus ligula.
                  </p>
                  
                  <ul>
                     
                     <li>Suspendisse iaculis varius augue ut tempor.</li>
                     
                     <li>Nulla convallis porttitor dui tincidunt rutrum.</li>
                     
                     <li>Nulla venenatis ante sapien.</li>
                     
                     <li>Curabitur id quam eget dui bibendum convallis fermentum id sapien.</li>
                     
                  </ul>
                  
                  <p>Cras lacinia eu nibh non hendrerit. Aenean eu enim ac ligula malesuada placerat. Nulla
                     vel ligula nec nisl blandit laoreet ac non sapien. Ut feugiat tortor id lacinia tincidunt.
                     Nulla ac libero magna. Morbi ullamcorper neque a enim placerat, a consequat mauris
                     euismod. Fusce auctor gravida consectetur. Pellentesque tincidunt ornare felis, in
                     venenatis erat mollis et. Nulla sed nunc vel sem malesuada consequat vel ac arcu.
                     Praesent eu turpis sit amet augue porta elementum.
                  </p>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/sandbox/internal-page-1.pcf">©</a>
      </div>
   </body>
</html>