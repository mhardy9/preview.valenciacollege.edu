<ul>
	<li>
		<a href="/foundation/alumni/index.php" class="show-submenu">Alumni Association<i class="fas fa-chevron-down" aria-hidden="true"></i></a>
		<ul>
			<li><a href="/foundation/alumni/about-alumni.php">About Alumni </a></li>
			<li><a href="/foundation/alumni/scholarships.php">Scholarships </a></li>
			<li><a href="/foundation/alumni/distinguished-graduate.php">Distinguished Graduate </a></li>

		</ul>
	</li>

	<li><a href="/foundation/alumni/donate.php">Donate</a></li>
	<li>
		<a href="/foundation/alumni/events.php" class="show-submenu">Events<i class="fas fa-chevron-down" aria-hidden="true"></i></a>
		<ul>
			<li><a href="/foundation/alumni/events-past.php">Past Events</a></li>
		</ul>
	</li>
	<li><a href="/foundation/alumni/photo-galleries.php">Photo Galleries</a></li>
	<li><a href="http://net4.valenciacollege.edu/forms/alumni/membership.cfm" target="_blank">Join</a></li>
	<li><a href="/foundation/alumni/vitae-magazine.php">Valencia's Alumni Magazine: Vitae</a></li>
	<li><a href="/foundation/alumni/contacts.php">Contact </a></li>
</ul>
