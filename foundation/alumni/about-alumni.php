<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>About Alumni | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/about-alumni.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li><a href="/foundation/alumni/">Alumni</a></li>
               <li>About Alumni</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>About Alumni</h2>
                        
                        <h3>Our Mission</h3>
                        
                        <p>The Valencia College  Alumni Association seeks to support the college and its mission
                           by providing  valued-added programs and services to foster and strengthen the relationships
                           between the college and its alumni. To this end, the alumni association aims to  develop
                           opportunities for lifelong personal, educational and professional  growth for alumni
                           and students of Valencia College. 
                        </p>
                        
                        <h3>Advisory Board</h3>
                        
                        <p>The association is led by an advisory board comprised of a small group of active alumni
                           leaders. The advisory board is tasked with providing guidance, leadership and active
                           support for the Valencia College Alumni Association's programs and events.
                        </p>
                        
                        <h3>Support Staff</h3>
                        
                        <p> <img src="/_resources/images/foundation/alumni/Jennifer-Mezquita.jpg" alt="Jennifer Mezquita" class="img-responsive"><br>
                           					  <strong>Dr.  Jennifer Mezquita</strong><br>
                           					Director,  Alumni Engagement and Annual Giving
                        </p>
                        
                        <p>Dr. Jennifer Mezquita is an alumna of  Valencia College where she earned her Associates
                           of Arts degree. She also holds  a bachelor’s degree from the University of Florida,
                           and both her Master’s  degree and a doctor of education degree from the University
                           of Central Florida.  Dr. Mezquita comes to the Valencia College Alumni Association
                           after serving  three years as Manager of Student Services at the West Campus Answer
                           Center.  Dr. Mezquita is the former President of the Valencia College Association
                           and  the Association of Valencia Women.
                        </p>
                        <br>
                        
                        <p><img src="/_resources/images/foundation/alumni/Karissa-Eliz-Rodriguez.jpg" alt="Karissa Eliz Rodriguez" class="img-responsive"><strong><br>Karissa Eliz Rodriguez</strong><br>
                           Coordinator,  Alumni Engagement and Annual Giving
                        </p>
                        
                        <p>Karissa  Eliz Rodriguez holds a bachelor’s degree in music from Oral Roberts University.
                           Karissa, an Orlando native, was previously a Student Services Advisor in the  West
                           Campus Answer Center. Before Joining the Valencia College family, Karissa  Participated
                           in the World Race, a service volunteer program through Adventures  in Missions where
                           she traveled and volunteered throughout 11 countries in 11  months. It was through
                           this experience she discovered her passion for building  community and empowering
                           others to do the same.
                        </p>
                        
                        
                        
                     </div>      
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/about-alumni.pcf">©</a>
      </div>
   </body>
</html>