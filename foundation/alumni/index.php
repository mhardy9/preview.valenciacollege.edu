<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Alumni | Valencia College</title>
      <meta name="Description" content="Alumni Association develops and provide opportunities for lifelong personal, educational and professional growth for alumni and students of Valencia College.">
      <meta name="Keywords" content="alumni, alumni association, volunteer, career, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni</h1>
            <p>
               
               
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li>Alumni</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               
               	
               <div class="container margin-60" role="main">
                  					
                  <div class="row">
                     					
                     <div class="col-md-12">
                        
                        <a id="whoarewe"></a>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="#whoarewe">Who Are We?</a></li>
                           
                           <li><a href="#membership">Membership and Benefits</a></li>
                           
                           <li><a href="#volunteer">Volunteer</a></li>
                           
                           <li><a href="#careercorner">Career Corner</a></li>
                           
                        </ul>
                        
                        
                        
                        <h2>Who Are We? </h2>
                        
                        
                        <p>To put it simply, the Valencia Alumni Association helps Valencia's alumni stay in
                           touch with one another,
                           students and with the college. Getting involved is the perfect way to know what's
                           going on with Valencia,
                           other alumni and the community.
                        </p> 
                        							
                        							
                        <h4>Our Mission</h4>
                        						
                        <p>Develop and provide opportunities for lifelong personal, educational and professional
                           growth for alumni and students of
                           Valencia College.
                        </p>
                        
                        <h3>Welcome!</h3>
                        
                        <p>The Valencia College Alumni Association was founded in 1979 to maintain a relationship
                           between the
                           college and its alumni by developing a sense of community among alumni and to promote
                           the lifelong
                           personal, educational, and professional growth of the alumni of the college. As a
                           graduate of Valencia
                           College, you should find great pride that you have received an education from one
                           of the finest
                           institutions in the nation and the inaugural winner of the Aspen Prize for Community
                           College Excellence.
                           You are now part of a network of over 100,000 alumni that live and work across the
                           globe. Many
                           opportunities are available for you to stay connected to Valencia. Your free membership
                           in the Alumni
                           Association will provide you with information regarding future plans for Valencia,
                           leadership and
                           networking opportunities, national discounts, cultural events, career information,
                           a subscription to Vitae
                           - Valencia's award-winning alumni magazine - and updates on your former classmates.<br> <br> Through your
                           involvement with the Alumni Association, you are not only connected to the college,
                           but you are also
                           helping to shape its future.
                        </p>
                        
                        <p>We encourage you to keep your contact information updated with us so that we can continue
                           to offer you
                           opportunities to strengthen and maintain your Valenia Alumni connections. We look
                           forward to seeing you
                           soon at an upcoming event!
                        </p>
                        
                        <h4>Need more information?</h4>
                        						
                        <p>Contact us at <a href="mailto:alumni@valenciacollege.edu?Subject=Alumni%20Association%20Web%20Site%20Contact">alumni@valenciacollege.edu</a>.
                           
                        </p>
                        
                        
                        
                        
                        <hr>
                        
                        <a id="membership"></a>
                        
                        					 
                        <h3>Membership and Benefits</h3>
                        
                        <p>It's True! Membership has its Privileges. </p>
                        
                        
                        <p>Your annual membership is absolutely free and allows you to remain connected with
                           Valencia and to get
                           involved with exciting Alumni Association benefits and events.
                        </p>
                        
                        <h4>Valencia Alumni Association Membership Categories: </h4>
                        						
                        <p><strong>Full Member </strong><br> Has recently completed a certificate program or earned a degree at Valencia.
                        </p> 
                        						
                        
                        <p><strong>Student Member</strong><br> Has started but not yet completed a certificate program or degree at
                           Valencia.
                        </p>
                        						
                        <p><strong>Apply for Valencia Alumni Association Leadership Team! </strong><br>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/foundation/alumni/documents/Alumni-Leadership-Board-Job-Description.pdf"> Alumni Leadership
                                    Job Description</a></li>
                              
                              <li><a href="/foundation/alumni/documents/Alumni-Leadership-Team-Application.pdf">Alumni Association
                                    Leadership Team Application </a></li>
                              
                              <li>
                                 							<a href="http://net4.valenciacollege.edu/forms/alumni/membership.cfm">Alumni Association Membership Signup</a></li>
                              							
                           </ul> 
                        </p>
                        
                        <hr>
                        
                        <a id="volunteer"></a>
                        
                        
                        <h3>Volunteer Opportunities for Everyone</h3>
                        
                        
                        <p>The Valencia Alumni Association offers many opportunities for alumni to stay connected
                           to Valencia and
                           build connections with fellow alumni and the community.
                        </p>
                        
                        <p>We look forward to having you join our team and help to design a great alumni association
                           experience! 
                        </p>
                        
                        <h4>Want to volunteer? </h4>
                        
                        <ul class="list_style_1">
                           
                           <li>General volunteer – Called upon to support above committee projects or other alumni
                              programs.
                           </li>
                           
                        </ul>
                        
                        <p>For more information or to let us know your interests as an association volunteer,
                           please call the Alumni
                           Relations office at 407-582-3426 or E-mail at <a href="mailto:alumni@valenciacollege.edu?Subject=Alumni%20Association%20Web%20Site%20Contact">alumni@valenciacollege.edu</a>.
                           
                        </p>
                        
                        
                        <h4>Upcoming volunteer opportunities: </h4>
                        
                        <p>Valencia College's 4th Annual Run, Walk for Heroes on Osceola Campus <strong>will be held on Friday,
                              September 9, 2016</strong>. All proceeds support Valencia Scholarships for Emergency responders on the
                           Valencia Osceola campus. 
                        </p>
                        
                        <p>If you are interested in volunteering please contact Alumni Relations at <a href="mailto:alumni@valenciacollege.edu">alumni@valenciacollege.edu</a>.
                        </p>
                        
                        
                        
                        
                        <hr>
                        
                        <a id="careercorner"></a>
                        
                        
                        
                        <h3>Career Corner</h3>
                        
                        
                        <p> Whether you have just graduated and are now looking for a job or are simply ready
                           for a career change,
                           the Career Corner is designed to provide you with resources for career assistance.
                        </p>
                        
                        
                        <h4>Career Corner Resource Links</h4>
                        
                        <p><a href="https://www.collegecentral.com/ArticleList.cfm?CatID=CAR%20" target="_blank">College Central</a><br>
                           <a href="/students/internship/" target="_blank">Internship &amp; Workforce Services
                              Office</a>
                           
                        </p>
                        
                        <h4>Degree &amp; Enrollment Verification</h4>
                        
                        <p>
                           DegreeVerify provides instant online verifications of college degrees or attendance
                           claimed by job
                           applicants. The service is designed to simplify verification for employers, background
                           screening firms,
                           executive search firms and employment agencies, who regularly screen candidates.
                        </p>
                        
                        <p>
                           All degree verifications at Valencia College will be through the<br> <a href="http://www.nslc.org/secure_area/DVEV/dvev_bridge.asp" target="_blank">National Student
                              Clearinghouse</a> only. See Details at <a href="/admissions/admissions-records/index.php">Valencia
                              Records</a>
                           
                        </p>
                        
                        <hr>
                        
                        <h3>VALENCIA Continuing Education, Training &amp; Employee Development </h3>
                        
                        <p>Ask about the 20% discount for Valencia Alumni!</p>
                        
                        <p>Reap the rewards of realizing your potential. Valencia College Continuing Education
                           offers a variety of
                           industry certifiations and short-term training programs to help you advance your career.
                           Plus, with
                           evening, weekend, workday and online classes, you can get the skills you need to get
                           ahead on your time.
                           The discount applies to all continuing education courses. Check these opportunities
                           out today! <br> <br>
                           <a href="https://preview.valenciacollege.edu/continuing-education/" target="_blank">Continuing Education</a>
                           
                        </p>
                        
                        						
                     </div>
                     
                     
                  </div>
                  
               </div>
               
               
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/index.pcf">©</a>
      </div>
   </body>
</html>