<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Scholarships and Awards for Students by Alumni | Valencia College</title>
      <meta name="Description" content="Alumni Association develops and provide opportunities for lifelong personal, educational and professional growth for alumni and students of Valencia College.">
      <meta name="Keywords" content="alumni, alumni association, volunteer, career, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/scholarships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni</h1>
            <p>
               
               
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li><a href="/foundation/alumni/">Alumni</a></li>
               <li>Scholarships and Awards for Students by Alumni</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Scholarships and Awards for Students by Alumni</h2>
                        
                        <p><strong>NOTICE</strong>: The Alumni Relations office is located in the District Office, fourth floor.
                        </p>
                        The Alumni Association administers scholarships and award programs that benefit Valencia
                        students. If you have questions regarding these scholarships, please contact the Alumni
                        Relations Office at <a href="mailto:alumni@valenciacollege.edu?Subject=Alumni%20Association%20Web%20Site%20Contact">alumni@valenciacollege.edu</a> or407-582-3219.
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/foundation/finaid/Scholarship_bulletin.html">Financial Aid Services Scholarship Bulletin Board</a></li>
                           
                           <li><a href="http://www.valencia.org/scholarships/">Valencia Foundation Scholarships</a> - Scholarships available now! APPLY TODAY!
                           </li>
                           
                           <li><a href="https://donate.valencia.org/pages/donation-page-alumni">Donate</a></li>
                           
                           <li><a href="/foundation/students/admissions-records/index.html">Request Official Transcript - Degree and Enrollment Verification </a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <p>The <strong>Valencia Alumni Association</strong> hosts the <strong>Alumni Achievers Reception</strong> each year to honor its student scholarship and award recipients for the year, as
                           well as outstanding volunteers. Our last reception was held on June 15th 2016.<br> See photos in the <span> <a href="/foundation/alumni/photo-galleries.php">Photo Gallery!</a></span></p>
                        
                        <hr>
                        
                        <p><a href="https://valencia.scholarships.ngwebsolutions.com/Scholarx_RequestLogon.aspx"><img class="img-responsive" src="/_resources/images/foundation/alumni/make-your-move-apply-today.jpg" alt="msa"></a> <strong> <br> </strong></p>
                        
                        <p><strong>The following Scholarship Applications are coming soon! </strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>2017-2018 Valencia Alumni Association General Scholarship Application</li>
                           
                           <li>2017-2018 Mayor Richard T. Crotty Valencia - UCF Alumni Association 2+2 Scholarship
                              application for Graduating Valencia students.
                           </li>
                           
                        </ul>
                        
                        <hr>
                        
                        <p><strong>Eileen Brunner Stana Scholarship:</strong> valued from $500<br> <strong>Eileen Brunner Stana Scholarship 2015-2016 Scholars:</strong> Maria Berrios-Pizarro <br> Deontta Johnson
                        </p>
                        
                        <hr>
                        
                        <p>&nbsp;</p>
                        <strong>Valencia Alumni Association Bill Castellano Civic Leadership Scholarship -</strong> valued from $500.<strong> <br> <strong>Valencia Alumni Association Bill Castellano Civic Leadership Scholarship Scholar(s):</strong><br> Maria Serrano <br></strong><hr>
                        
                        <p><strong>Ron Brandolini Honors Memorial Scholarship:</strong> valued from $500.
                        </p>
                        <strong>Ron Brandolini Honors Memorial Scholarship 2016 Scholar(s):</strong>
                        
                        <ul class="list_style_1">
                           
                           <li>Sierra Westfall</li>
                           
                           <li>Bridget Frost</li>
                           
                           <li>Elizabeth Lachman</li>
                           
                           <li>Andrew Uyeda</li>
                           
                        </ul>
                        
                        <hr><strong>Tina Collyer Memorial Emt Scholarship: Tina's Heart</strong> valued from $500. <br> <strong><a href="https://valencia.scholarships.ngwebsolutions.com/Scholarx_RequestLogon.aspx">Tina's Heart 2017 Scholar Coming Soon</a></strong><hr>
                        
                        <p><strong>Dr. Homer Samuels Dental Hygiene Scholarship: </strong>valued from $500 <strong><br> </strong> <br> <strong>Dr. Homer Samuels Dental Hygiene Scholarship 2015 Scholar: <br> </strong> Ashley Bleam
                        </p>
                        
                        <hr>
                        
                        <p><strong>Lynn Capraun Respiratory Care Alumni Scholarship - </strong>valued from $500
                        </p>
                        
                        <p><strong>Lynn Capraun Respiratory Care Alumn Scholarship 2015 Scholar:</strong><br> Simon Taylor
                        </p>
                        
                        <hr>
                        
                        <p><strong>A. Merci Saintil Family Scholarship</strong><br> <strong>2015 A. Merci Saintil Family Scholarship(s):</strong><br> Straunje Nelson<br> Richard Woods
                        </p>
                        
                        <hr><strong>Howe Collazo Starfish Swam Scholarship 2015 Scholars:</strong><br>
                        
                        <p>Maria Berrios-Pizzarro<br> Alexis Hilyer<br> Jessica Meier<br> Susana Garcia Einbcke <br> <br> <strong>Starfish Service 2015 Award:</strong></p>
                        
                        <p>Waldstein Joseph<br> Jaqueline Perez
                        </p>
                        
                        <hr>
                        
                        <p><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466"><img class="img-responsive" src="/_resources/images/foundation/alumni/bird-cartoon-scholarship.jpg" alt="birds"></a></p>
                        
                        <p><strong>Valencia Alumni Association Annual Scholarships: All valued up to $500</strong><br> Alumni Annual Scholarship 2015 -16 Scholars:<br> <strong> David F. Collier Memorial Scholarship</strong><br> Shalom Gonzalez
                        </p>
                        
                        <p><strong>Charlie &amp; Ilona Edwards Memorial Scholarship</strong><br> Erika Dasilva<br> Satesh Sookhan
                        </p>
                        
                        <p><strong>Lula Crosby Keyes Scholarship</strong><br> Damon Little<br> Cheliz Pena
                        </p>
                        
                        <p><strong>Roger G. Keyes Scholarship</strong><br> Amanda Ferreira<br> Alejandra Carvajal<br> Christopher Hernandez
                        </p>
                        
                        <p><strong>Rosita N. Martinez Scholarship</strong><br> Shakira Vargas<br> Sashaley De Jesus
                        </p>
                        
                        <p><strong>Betty House Palmer Scholarship</strong><br> Susana Garcia<br> Felix Delgado<br> Erick Cruz<br> Seth Wagner
                        </p>
                        
                        <p><strong>Woodberry-Moran Family Scholarship</strong><br> Gina Garcia<br> Hayley McKown
                        </p>
                        
                        <p><strong>Florence E. &amp; Andrea JasicaScholarship</strong><br> Lynsey Pink <br> Zachary Greene-Rettig<br> Brenda Castillo
                        </p>
                        
                        <p><strong>Susan Kay Notaro Award</strong> <br> Vanessa Suarez
                        </p>
                        
                        <hr>
                        
                        <p><strong>Valencia College Alumni Association &amp; Student Development Student Leadership Award:
                              </strong>valued up to $500.<br> <strong>Valencia College Alumni Association &amp; Student Development Student Leadership Award
                              Scholars:</strong><br> Angie De Costa<br> Stephanie Rocco<br> Shelby Pickar-Dennis<br> Joeily Figueroa<br> Jessica Conrad<br> Alyssia Wheeler
                        </p>
                        
                        <hr>
                        
                        <img class="img-responsive" src="/_resources/images/foundation/alumni/ucf-alumni-logo.gif" alt="ucf logo">
                        <img class="img-responsive" src="/_resources/images/foundation/alumni/valencia-alumni-logo-ucf-alumni.jpg" alt="Valencia Alumni Logo">
                        
                        
                        <p>&nbsp;</p>
                        
                        <p><strong>Mayor Richard T. Crotty - UCF Alumni Associations' 2 + 2 Scholarship for Graduating
                              Valencia Students</strong> - Awarded only to graduating Valencia students transferring to UCF
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Awards graduating student $4,000. A $2,000 check is deposited in student's account
                              at UCF each year for two years.
                           </li>
                           
                        </ul>
                        
                        <p><strong> <a href="https://valencia.scholarships.ngwebsolutions.com/ScholarX_ApplyForScholarship.aspx?applicationformid=4649&amp;AYID=444">2016-2017 APPLICATION COMING SOON! </a></strong><br> <strong>2015-2016 2 + 2 Scholar Graduates of Valencia:</strong><br> Danya Belnour<br> Bridget Frost<br> Renaldo Riggins
                        </p>
                        
                        <hr>
                        
                        <p><strong>Mayor Richard T. Crotty - UCF Alumni Associations' 2 + 2 Scholarship for Incoming
                              High School Students entering Valencia from Orange &amp; Osceola Counties</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>Awards high school incoming student a total of $4,000. ($1,000 each year for 4 years.)</li>
                           
                        </ul>
                        <br> <strong><a href="https://valencia.scholarships.ngwebsolutions.com/ScholarX_ApplyForScholarship.aspx?applicationformid=4649&amp;AYID=444">2016-2017 APPLICATION COMING SOON! </a></strong><br> <strong> 2015-2016 2 + 2 Valencia Incoming Scholars:</strong><br> Tiffany Akbay<br> Cassandra Hernandez<br> Jacqueline Santiago
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/scholarships.pcf">©</a>
      </div>
   </body>
</html>