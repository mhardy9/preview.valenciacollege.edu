<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Volunteer Opportunities for Everyone | Valencia College</title>
      <meta name="Description" content="Alumni Association develops and provide opportunities for lifelong personal, educational and professional growth for alumni and students of Valencia College.">
      <meta name="Keywords" content="alumni, alumni association, volunteer, career, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/volunteer.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni</h1>
            <p>
               
               
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li><a href="/foundation/alumni/">Alumni</a></li>
               <li>Volunteer Opportunities for Everyone</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               
               
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        					
                        <h2>Volunteer Opportunities for Everyone</h2>
                        
                        
                        <p>The Valencia Alumni Association offers many opportunities for alumni to stay connected
                           to Valencia and build connections with fellow alumni through our online community:
                           <a href="https://valenciaalumniconnect.com" target="_blank">Valencia Nation</a>.
                        </p>
                        
                        
                        <p>We look forward to having you join our team and help to design a great alumni association
                           experience! 
                        </p>
                        
                        
                        <p>Want to volunteer? </p>
                        
                        <ul class="list_style_1">
                           
                           <li>General volunteer – Called upon to support above committee projects or other alumni
                              programs. 
                           </li>
                           
                        </ul>
                        
                        
                        <p>For more information or to let us know your interests as an association&nbsp;volunteer,
                           please call the Alumni Relations office at 407-582-3254 or E-mail at <a href="mailto:alumni@valenciacollege.edu?Subject=Alumni%20Association%20Web%20Site%20Contact">alumni@valenciacollege.edu</a>.
                        </p>
                        
                        <p>Upcoming volunteer opportunities:</p>
                        
                        <p>Valencia  College will be starting their fall semester in August and we would love
                           your  help welcoming those students back to campus!
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Lake Nona  Campus - August 28,  2017, 11 am-  2pm</li>
                           
                           <li>West Campus -  August 29, 2017, 11am-2pmÂ&nbsp;Â&nbsp; </li>
                           
                           <li>East Campus - Sept 5, 2017, 11am-2pm</li>
                           
                           <li>Winter Park Campus - Sept. 6, 2017, 10am-12:30pm</li>
                           
                           <li>Osceola Campus - Sept. 6 2017, 11am-2pm<br>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p>We are looking for alumni to participate in our upcoming event Alumni Connections
                           with Career Services. alumni volunteers will be asked to join in a Career Panel Discussion
                           where students can learn more about our successful alumni. We are looking for volunteers
                           who are comfortable and excited to share their stories and give tips and encouragement
                           to fellow alumni and students as they pursue their career and academic goals.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>West Campus,October 5,  2017, 11am-4pm </li>
                           
                        </ul>
                        
                        <p>If you are interested in volunteering please contact Alumni Relations at <a href="mailto:alumni@valenciacollege.edu">alumni@valenciacollege.edu</a>.
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               
               
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/volunteer.pcf">©</a>
      </div>
   </body>
</html>