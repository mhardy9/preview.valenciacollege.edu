<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Donate | Valencia College</title>
      <meta name="Description" content="Alumni Association develops and provide opportunities for lifelong personal, educational and professional growth for alumni and students of Valencia College.">
      <meta name="Keywords" content="alumni, alumni association, volunteer, career, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/donate.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni</h1>
            <p>
               
               
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li><a href="/foundation/alumni/">Alumni</a></li>
               <li>Donate</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               
               	
               <div class="container margin-60" role="main">
                  					
                  <div class="row">
                     					
                     <div class="col-md-12">
                        
                        
                        <h2>Donate</h2>
                        
                        <p>LEGACY CLASS GIFT - The Tradition That Keeps on Giving</p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/EmmyTorres.jpg" alt="Emmy Torres" class="img-responsive"><br>
                           <strong>Mary Smedley Collier Distinguished Graduate 2016 Emmy Torres</strong></p>
                        
                        <p>TAKE THE CHALLENGE AND LEAVE A LEGACY -- YOURS! </p>
                        
                        <p>Valencia's graduating class and friends continue the tradition of supporting Valencia
                           students through
                           the Legacy Class Gift. EVERY penny donated will support students and is tax deductible.
                           
                        </p>
                        
                        <p>Donate to the Legacy Class Gift 2016: </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Drop by ANY of Valencia College's Business Office locations, just mention "LEGACY"
                              Class Gift.
                           </li>
                           
                           <li> Make an online credit card donation below.</li>
                           
                        </ul>
                        <a id="Credit"></a>
                        
                        <p>LEGACY CLASS GIFT - Pay by Credit Card</p>
                        
                        <p><a href="https://donate.valencia.org/alumni" class="button" target="_blank">Donate</a>  
                        </p>
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               
               
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/donate.pcf">©</a>
      </div>
   </body>
</html>