<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia's Alumni Magazine: Vitae | Valencia College</title>
      <meta name="Description" content="Alumni Association develops and provide opportunities for lifelong personal, educational and professional growth for alumni and students of Valencia College.">
      <meta name="Keywords" content="alumni, alumni association, volunteer, career, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/vitae-magazine.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni</h1>
            <p>
               
               
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li><a href="/foundation/alumni/">Alumni</a></li>
               <li>Valencia's Alumni Magazine: Vitae</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               
               	
               <div class="container margin-60" role="main">
                  					
                  <div class="row">
                     					
                     <div class="col-md-12">
                        
                        
                        <h2>Valencia's Alumni Magazine: Vitae</h2>
                        
                        
                        <p><em>Vitae</em> is Valencia Alumni Association's bi-annual magazine created for our extended family
                           of
                           alumni and friends. Rooted in the Latin word for "life," vitae means: the course of
                           one's life or career,
                           a short account of a person's life, a résumé. The purpose of an alumni magazine is
                           to keep you connected
                           to Valencia throughout your life, and the title <em>Vitae</em> reflects the collective résumé of alumni,
                           faculty and staff who have proudly walked through our doors.
                        </p>
                        
                        <a href="http://vitaeonline.com" target="_blank"><img src="/_resources/images/FOUNDATION/alumni/vitae-banner.jpg" alt="Vitae Magazine - Vitae Online" height="409" width="582" class="img-responsive"></a>
                        
                        
                        
                        <h4>Download <em>Vitae</em> Magazine
                        </h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://www.vitaeonline.com/?issue=winter-2016" target="_blank">Issue 16, 2016 Winter </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.vitaeonline.com/?issue=summer-2016" target="_blank">Issue 15, 2016 Summer </a>
                              
                           </li>
                           
                           <li><a href="http://www.vitaeonline.com/?issue=winter-2015" target="_blank">Issue 14, 2015 Winter</a></li>
                           
                           <li>
                              <a href="http://www.vitaeonline.com/?issue=summer-2015" target="_blank">Issue 13, 2015 Summer </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.vitaeonline.com/?issue=vitae-fall-2014" target="_blank">Issue 12, 2014 Fall </a>
                              
                           </li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/13alu003-vitae13-issuu" target="_blank">Issue 11, 2014
                                 Summer </a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/13alu002-vitae12-winter-issuu" target="_blank">Issue
                                 10, 2013 Winter</a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae11-2013-spring-summer?e=1857924/3780710" target="_blank">Issue 9, 2013 Summer </a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae-10?e=1857924/3780586" target="_blank">Issue 8,
                                 2012 Fall </a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae9?e=1857924/6034975" target="_blank">Issue 7,
                                 2012 Spring </a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae-8?e=1857924/3352896" target="_blank">Issue 6,
                                 2011 Fall </a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae__7?e=1857924/3780750" target="_blank">Issue 5,
                                 2011 Spring </a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae__6?e=1857924/3780903" target="_blank">Issue 4,
                                 2010 Fall </a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae_spring_10" target="_blank">Issue 3, 2010
                                 Spring</a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae_summer-fall09" target="_blank">Issue 2, 2009
                                 Fall </a></li>
                           
                           <li><a href="https://issuu.com/valenciacollege/docs/vitae___3?e=1857924/3780779" target="_blank">Issue 1,
                                 2009 Spring</a></li>
                           
                        </ul>
                        
                        
                        <hr>
                           
                           
                           <h4>Download Previous Issues <em>Alumni Voice &amp; Valencia Matters </em>
                              
                           </h4>
                           
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/foundation/alumni/documents/Alumni-Voice-Winter-2006.pdf" target="_blank">Alumni Voice
                                    Winter 2006</a></li>
                              
                              <li><a href="/foundation/alumni/documents/Alumni-Voice-Fall-2005.pdf" target="_blank">Alumni Voice Fall
                                    2005 </a></li>
                              
                              <li><a href="/foundation/alumni/documents/Valencia-Matters-Fall-2004.pdf" target="_blank">Valencia Matters
                                    Fall 2004 </a></li>
                              
                              <li><a href="/foundation/alumni/documents/Valencia-Matters-Spring-2004.pdf" target="_blank">Valencia
                                    Matters Spring 2004</a></li>
                              
                           </ul>
                           
                           <p>We are always looking for content for <em>Vitae,</em> Valencia Alumni Association's bi-annual magazine.
                              <br> Please submit article ideas or alumni updates to <a href="mailto:alumni@valenciacollege.edu">alumni@valenciacollege.edu</a>.
                              
                           </p>
                           				
                           						
                        
                        						
                     </div>
                     						
                  </div>
                  			
                  			
               </div>
               
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/vitae-magazine.pcf">©</a>
      </div>
   </body>
</html>