<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Distinguished Graduate | Valencia College</title>
      <meta name="Description" content="Alumni Association develops and provide opportunities for lifelong personal, educational and professional growth for alumni and students of Valencia College.">
      <meta name="Keywords" content="alumni, alumni association, volunteer, career, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/distinguished-graduate.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni</h1>
            <p>
               
               			
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li><a href="/foundation/alumni/">Alumni</a></li>
               <li>Distinguished Graduate</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               			
               			
               <div class="container margin-60" role="main">
                  				
                  <div class="row">
                     					
                     <div class="col-md-12">
                        
                        						
                        <h2>Distinguished Graduate</h2>
                        						
                        <p><strong>Valencia College Alumni Association Mary Smedley Collier Distinguished Graduate Award</strong></p>
                        						<img src="/_resources/images/foundation/alumni/photo-Rula-Khalaf-2017-04-06.jpg" alt="Rula Khalaf" class="img-responsive img-circle">
                        						
                        <p><strong> 2016-2017 DISTINGUISHED GRADUATE: Rula Khalaf</strong></p>
                        							
                        <p>Distinguished Grad Rula  Khalaf, 20, officially became a resident of the United States
                           in August 2013. Brought to America when her family of six moved from the Middle East,
                           she  studied at Timber Creek High, followed by an eventful two years at Valencia.
                           This spring she graduates with an Associate in Arts degree and, in the Fall, will
                           transfer to UCF with a 4.0 GPA. In her junior year of  high school, Khalaf had not
                           given much thought to college, as it was not common for young women to go to school
                           in her homeland. In her Senior year, her counselors pushed the idea a bit more, prompting
                           her to apply to Valencia and UCF. Accepted to both, she opted for Valencia's  smaller
                           class sizes and more intimate campus setting.
                        </p>
                        
                        						
                        <p> "UCF sounded a bit overwhelming," Khalaf says. "They would tell me  there are up
                           to 1,000 students in a class. I thought, ‘Why not start at a place  that has smaller
                           classes, and get the same quality education at a better  price?'" Khalaf quickly immersed
                           herself in course work and extracurricular  activities. "The first week, my mind was
                           bent on studying, focusing on school,  getting A's," Khalaf said. "Then Welcome Back
                           Week happened at the East Campus  mall area; I became familiarized with what Valencia
                           had to offer and wanted to  take part in everything, including clubs." The clubs proved
                           important to  Khalaf's success, allaying initial fears that she might be ostracized
                           because  of her background. Khalaf joined Phi Theta Kappa honor society and Student
                           Government Association, later becoming the chief of staff at East Campus SGA.  In
                           the classroom, she credits the New Student Experience class with giving her  direction.
                           "It's a mandatory class, and a lot of people think it's busy work,  because maybe
                           they already know what they want to do. But where I'm from,  nobody ever asked me
                           what I wanted to be." Khalaf took the course seriously  delving into the career research,
                           attending all of the recommended Skillshops.  She ultimately decided on dentistry,
                           and even took on the role of president for  East Campus's Future Medical Professionals
                           club. This fall, she will pursue a  major in biomedical sciences at University of
                           Central Florida, with aspirations  of enrolling in the University of Florida's College
                           of Dentistry. Although she has  accomplished a lot in four years, Khalaf says gaining
                           a feeling of acceptance  was the biggest hurdle to overcome. "Coming to America, I
                           was afraid," Khalaf  says. "We came here thinking people would hate us. If they saw
                           my mom in the  hijab, I thought they might not accept my family because of the way
                           the few  instances of extremism in the Muslim world are portrayed in the media." 
                           Inspired by the welcoming, familial atmosphere at Valencia, Khalaf now hopes to  make
                           the college proud as a graduate. "Life's all about how you perceive it.  Playing the
                           victim will only slow things down," Khalaf says. "Don't let people  pity you, let
                           them be inspired by you."
                        </p>
                        
                        					
                     </div>
                     
                     				
                  </div>
                  			
               </div>
               
               
               		
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/distinguished-graduate.pcf">©</a>
      </div>
   </body>
</html>