<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Past Events | Valencia College</title>
      <meta name="Description" content="Alumni Association develops and provide opportunities for lifelong personal, educational and professional growth for alumni and students of Valencia College.">
      <meta name="Keywords" content="alumni, alumni association, volunteer, career, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/events-past.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni</h1>
            <p>
               
               
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li><a href="/foundation/alumni/">Alumni</a></li>
               <li>Past Events</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               
               	
               <div class="container margin-60" role="main">
                  					
                  <div class="row">
                     					
                     <div class="col-md-12">
                        
                        <h2>Past Events</h2>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="#2015pastevents">2015 Past Events</a></li>
                           
                           <li><a href="#2014pastevents">2014 Past Events</a></li>
                           
                           <li><a href="#2013pastevents">2013 Past Events</a></li>
                           
                           <li><a href="#2012pastevents">2012 Past Events</a></li>
                           
                           <li><a href="#2011pastevents">2011 Past Events</a></li>
                           
                        </ul>
                        
                        
                        <h3>2016 Past Events</h3>
                        
                        
                        <p><a href="https://www.facebook.com/events/1070498066304017/"><img src="/_resources/images/FOUNDATION/alumni/5knew.JPG" alt="5K Run, Walk and Roll" height="166" width="229"></a></p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/5K-photos.JPG" alt="5K Run, Walk and Roll Photos" height="81" width="355" class="img-responsive"></p>
                        
                        
                        Valencia College 11th Annual <br>
                        5K Run, Walk &amp; Roll<br>
                        <em>in memory of alumni leader Justin Harvey</em><br>
                        <br> For athletes of all abilities!<br> Funds raised will support Valencia students through criminal justice, fire safety,
                        EMS and veteran scholarships.<br>
                        Saturday, April 2, 2016, 6:00 p.m.<br>
                        Valencia West Campus<br>
                        1800 South Kirkman Road<br>
                        Orlando, FL 32811<br> 
                        
                        
                        <hr class="styled_2">
                        
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" class="img-responsive"></p>
                        
                        <p>Wednesday, March 16, 2016<br>
                           Valencia College West Campus, Special Events Center (Bldg.8) <br>
                           6:30 p.m. - 9:00 p.m.
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Orlando-Magic-vs-Chicago-Bulls.JPG" alt="Orlando Magic vs Chicago Bulls" class="img-responsive"></p>
                        
                        <p>VALENCIA COLLEGE NIGHT <br>
                           Wednesday, March 2, 2016 at 7pm <br>
                           Amway Center
                        </p>
                        
                        <a id="2015pastevents"></a>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>2015 Past Events</h3>
                        
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Awards-A-Night-of-Celebration.jpg" alt="Awards A Night of Celebration" class="img-responsive"></p>
                        
                        <p>Friday, December 18, 2015<br>
                           7:00 p.m. - 9:00 p.m.<br>
                           Award Ceremony: 7:30-8:00 pm <br>
                           Valencia West Campus, Special Events Center (Bldg 8)
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" class="img-responsive"></p>
                        
                        <p>Thursday, November 19, 2015<br>
                           Valencia College West Campus, Bldg 8 <br>
                           6:30 p.m. - 9:00 p.m.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/5K-Photo-Collage.jpg" alt="5K Photo Collage" class="img-responsive"> 
                        </p>
                        
                        <p>Valencia College 3rd Annual Walk/Run for Heroes(Osceola Campus 5K run)<br>
                           brought to you by the Valencia Alumni Association<br>
                           All proceeds go toward the Rotary Club of Lake Nona's Sept. 11 Memorial Fund to support
                           Valencia scholarships for emergency responders.<br>
                           Date: Friday, September 11, 2015 6:30 p.m. <br>
                           Location: Valencia Osceola Campus
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><a href="https://www.networkafterwork.com/events/orlando"><img src="/_resources/images/FOUNDATION/alumni/Network-After-Work-Logo.jpg" alt="Network After Work Logo" class="img-responsive"></a></p>
                        
                        <p>Join Network After Work on Tuesday August 25th at Tin Roof (8371 International Dr)
                           from 6-9pm. Network After Work is America's premier face-to-face business networking
                           company with monthly mixer events in over 50 cities catering to nearly one million
                           members. Attendees make valuable connections with other members of their business
                           community while enjoying different exciting venues including clubs, restaurants, hotels
                           and special event locations. Events attract a diverse mix of up to several hundred
                           professionals from all industries and career levels. It all takes place on a weekday
                           right after work in a fun and relaxed atmosphere conducive to making connections.
                           Name-tags color coded by industries help sort through the many opportunities while
                           a free drink and light bites before 7pm helps get the ball rolling right from the
                           start. 
                        </p>
                        
                        <p>Admission Includes:<br>
                           
                           <ol class="list_style_1">
                              
                              <li>Access to 150 Professionals</li>
                              
                              <li>Color-Coded Name Badge</li>
                              
                              <li>First Featured Cocktail before 7pm</li>
                              
                              <li>Light Appetizers from 6-7pm</li>
                           </ol><br>
                           <a href="https://www.networkafterwork.com/events/orlando">https://www.networkafterwork.com/events/orlando/</a></p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" height="79" width="257"></p>
                        
                        <p>Wednesday, August 19, 2015<br>
                           6:30 p.m. – 9:00 p.m.<br>
                           Valencia West Campus, <br>
                           Bldg.8, Special Events Center 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Alumni-Night-Orlando-City-Soccer-Flyer.jpg" alt="Alumni Night Orlando City Soccer Flyer" class="img-responsive"></p>
                        Valencia College Alumni and Friends with the Orlando City Soccer Club<br>
                        ALUMNI NIGHT!<br>
                        Date: Saturday, August 8, 2015 at 7:30 pm<br>
                        							Location: Citrus Bowl
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-Alumni-Wine-Cheese.png" alt="Reception Alumni Wine Cheese" class="img-responsive"></p>
                        
                        <p>Valencia Alumni &amp; Friends Wine &amp; Cheese Reception </p>
                        
                        <p>Enjoy light bites, beverages and after-hours networking with other professionals,
                           alumni and friends.
                        </p>
                        
                        <p>Friday, July 24, 2015<br>
                           Drop in between 6:00 - 8:00 p.m.<br>
                           Location: Valencia's West Campus<br>
                           Special Events Center (Bldg. 8) 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" class="img-responsive"></p>
                        
                        <p>This will be our first event at Valencia College Lake Nona and it will coincide with
                           the Food Truck Bazaar! The Bazaar will be on campus from 5-8 pm and you can bring
                           food with you to TEDxOrlandoSalon. Light refreshments will also be served. We invite
                           you to share this with friends, family and co-workers. 
                        </p>
                        
                        <p>Thursday, June 25, 2015<br>
                           6:30 p.m. – 9:00 p.m.<br>
                           Valencia Lake Nona Campus, Room 148 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-Alumni-Achievers.JPG" alt="Alumni Achievers Reception" class="img-responsive"></p>
                        
                        <p>Alumni Achievers Reception <br>
                           <em>by invitation only</em></p>
                        
                        <p>The Alumni Association hosts this reception to honor Association student scholarship
                           and award recipients, and outstanding alumni and volunteers. 
                        </p>
                        
                        <p>Date: Wednesday, June 3, 2015<br>
                           6:30 p.m. - Reception<br> 7:00 p.m. - 8:30 p.m. - Program<br>
                           Location: Valencia's West Campus, Special Events Center, Building 8
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/UnityExecSuiteflyer5-21-15FR-Copy.jpg" alt="unity flyer" class="img-responsive"></p>
                        
                        <p>Unity Networking Mixer </p>
                        
                        <p>Bridging the Next Generation <br>
                           <em>three-part series</em><br>
                           PART II: MILLENIALS RISING<br></p>
                        <img src="/_resources/images/FOUNDATION/alumni/UnityExecSuiteflyer5-21-15BK.jpg" alt="unity flyer" class="img-responsive">
                        
                        <p>Date: Thursday, May 21, 2015<br>
                           6:00-9:00 p.m. <br> Location: The Abbey <br>
                           100 South Eola Dr , Orlando
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-Alumni-Wine-Cheese.png" alt="Reception Alumni Wine Cheese" class="img-responsive"></p>
                        
                        <p> Valencia Alumni &amp; Friends Wine &amp; Cheese Reception</p>
                        
                        <p>Enjoy light bites, beverages and after-hours networking with other professionals,
                           alumni and friends. 
                        </p>
                        
                        <p>Thursday, April 30, 2015 <br>
                           Drop in between 6:00 - 8:00 p.m.<br>
                           <a href="http://valenciacollege.edu/map/district-office.cfm">Valencia's District Office</a>- Terrace (5th Floor) 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" class="img-responsive"></p>
                        
                        <p>Wednesday, April 15, 2015&gt;<br>
                           6:30 p.m. – 9:00 p.m.<br>
                           Valencia West Campus, <br> Bldg.8, Special Events Center 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/5knew.JPG" alt="5K logo" class="img-responsive"></p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/5K-photos.JPG" alt="5K photos" class="img-responsive"></p>
                        
                        <p><a href="http://racetimesports.com/assets/race_logos/VCC5K-2015.htm">The RACE RESULTS ARE IN!!!!!</a></p>
                        Valencia College 10th Annual<br>
                        5K Run, Walk &amp; Roll<br>
                        brought to you by the Valencia Alumni Association <em>in memory of alumni leader Justin Harvey</em><br> <br> For athletes of
                        all abilities!<br> Funds raised will support Valencia<br> students through criminal justice,
                        firefighter<br> and EMS/EMT scholarships.<br> <br> Date: Saturday, March 21, 2015, 6:00 p.m.<br>
                        								Location: Valencia West Campus<br>
                        								1800 South Kirkman Road<br>
                        								Orlando, FL 32811 
                        
                        <hr class="styled_2">
                        
                        <p>Valencia College Alumni and Friends with the Orlando City Soccer Club </p>
                        Date: Sunday, March 8, 2015, 5:00 p.m. <br>
                        Location: Citrus Bowl 
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Orlando-Magic-vs-Sacremento-Kings.JPG" alt="Orlando Magic vs Sacremento Kings" class="img-responsive"><br>
                           <img src="/_resources/images/FOUNDATION/alumni/College-Night-Pure-Magic-Event.JPG" alt="College Night Pure Magic Event" class="img-responsive"></p>
                        VALENCIA - ORLANDO MAGIC NIGHT <br>
                        Orlando MAGIC vs. Sacramento KINGS <br>
                        Friday, March 6, 2015, 7:00 p.m. <br>
                        Amway Center, Downtown Orlando 
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Alumni-Night-Solar-Bears.jpg" alt="Alumni Night Solar Bears" class="img-responsive"></p>
                        
                        <p>Valencia College Alumni and Friends with the ORLANDO SOLAR BEARS <br>
                           Date: Friday, February 27, 2015 , 7:30 p.m. <br>
                           Location: Amway Center, Downtown Orlando
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Unity-Networking-Mixer-Flyer.JPG" alt="Unity Networking Mixer Flyer" class="img-responsive"></p>
                        
                        <p>Unity Networking Mixer Orlando Then &amp; Now</p>
                        
                        <p><em>TRANSITION | COMMUNITY | PROGRESSION</em> 
                        </p>
                        
                        <p>Join us for an inspirational Black History Month celebration with our esteemed panelists
                           to learn about their journeys and why their story matters to you. 
                        </p>
                        Learn how adversity combined with perseverance can elevate your career to the next
                        level.
                        
                        <p>Date: Thursday, February 26, 2015<br>
                           6:00-9:00 p.m. <br>
                           Location:The Abbey <br> 100 South Eola Dr ,Orlando 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" class="img-responsive"></p>
                        
                        <p>Wednesday, February 11, 2015<br>
                           6:30 p.m. – 9:00 p.m.<br>
                           Valencia Osceola Campus, Bldg.4, Rm 105 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>Join Network After Work on Thursday, January 29th at Kasa (183 S Orange Ave) from
                           6-9pm. Enjoy light bites and a free drink before 7pm while networking with local business
                           professionals and entrepreneurs from a range of industries. Color-coded name tags
                           help you connect with local members of the industries that matter most to you.&nbsp; America's
                           premier face-to-face business networking organization holds monthly mixer events like
                           this in over 40 other cities and caters to nearly one million members. Each month's
                           get-together takes place in a new and exciting venue, making it fun to continue building
                           your professional network with face-to-face interactions.
                        </p>
                        
                        <p>Admission Includes:<br>
                           
                           <ol class="list_style_1">
                              
                              <li>Access to 150 Professionals</li>
                              
                              <li>Color-Coded Name Badge</li>
                              
                              <li>First Featured Cocktail before 7pm</li>
                              
                              <li>Light Appetizers from 6-7pm</li>
                           </ol>
                        </p>
                        
                        
                        <a id="2014pastevents"></a>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>2014 Past Events</h4>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Awards-A-Night-of-Celebration-Flyer.jpg" alt="Awards A Night of Celebration Flyer" class="img-responsive"></p>
                        
                        <p>A Night of Celebration<br>
                           Friday, December 12, 2014<br> 
                           7:00 p.m. – 10:00 p.m.<br>
                           Award Ceremony: 7:30-8:00 pm <br>
                           Valencia West Campus, Special Events Center (Bldg 8) 
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando-Salon.jpg" alt="TEDx Orlando Salon" class="img-responsive"></p>
                        
                        <p>Wednesday, December 10, 2014<br>
                           6:30 p.m. – 9:00 p.m.<br>
                           Valencia East Campus, Bldg 8, Room 101
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><a href="https://www.networkafterwork.com/events/orlando"><img src="/_resources/images/FOUNDATION/alumni/Network-After-Work-Don-Jefes.JPG" alt="Network After Work Don Jefes" class="img-responsive"></a></p>
                        
                        <p>Join Network After Work on Thursday, November 20th at Don Jefe's from 6-9pm. Network
                           After Work is comprised of nearly one million like-minded professionals from around
                           the country. Network with attendees from all industries and career levels interested
                           in expanding their professional network and creating new business opportunities. Events
                           take place monthly which allows guests a chance to foster new professional connections
                           in a relaxed atmosphere conducive to business and social networking. Upon entering,
                           guests will receive a name tag color-coded by industry which allows for easy navigation.
                           Network After Work events give guests a chance to get their name and brand in front
                           of top local business professionals while visiting the city's best After Work destinations.
                           
                        </p>
                        
                        <p>To RSVP please visit: <br>
                           <a href="https://www.networkafterwork.com/events/orlando">https://www.networkafterwork.com/events/orlando</a>
                           
                        </p>
                        
                        <p>Admission Includes:<br>
                           
                           <ol class="list_style_1">
                              
                              <li>Access to 200+ Professionals</li>
                              
                              <li>Color-Coded Name Badge</li>
                              
                              <li>First Featured Cocktail before 7pm</li>
                              
                              <li>Light Appetizers from 6-7pm</li>
                           </ol><br>
                           Admission: $12 in advance / $15 with RSVP at the door / $20 without RSVP
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><br> <img src="/_resources/images/FOUNDATION/alumni/Blue-Martini-After-Hours-Mixer.jpg" alt="Blue Martini After Hours Mixer" class="img-responsive"></p>
                        
                        <p>Valencia College Alumni Association After-Hours Mixer </p>
                        
                        <p>Date: Friday, November 14, 2014<br> 6:00-8:00 p.m.<br>
                           Location: <a href="http://orlando.bluemartinilounge.com/">Blue Martini Lounge</a> <br>
                           4200 Conroy Rd ,Orlando, FL 32839 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Unity-Networking-Mixer-flyers.JPG" alt="Unity Networking Mixer flyers" class="img-responsive"></p>
                        
                        <p>Unity Networking Mixer </p>
                        
                        <p>Date: Wednesday, November 12, 2014<br>
                           6:00-9:00 p.m. <br>
                           Location:  DoubleTree by Hilton Orlando at Sea World<br>
                           10100 International Dr , Orlando, FL 32819
                        </p>
                        
                        <p>JOIN THE CONVERSATION at our quarterly professional networking eventdesigned toCONNECT
                           diverse professionals, DEVELOP new skills and CELEBRATE our successes.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/College-Night-Pure-Magic-Fall-Offer.JPG" alt="College Night Pure Magic Fall Offer" class="img-responsive"></p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Orlando-Magic-vs-Washington-Wizards.JPG" alt="Orlando Magic vs Washington Wizards" class="img-responsive"></p>
                        
                        <p>VALENCIA - ORLANDO MAGIC NIGHT <br>
                           Orlando MAGIC vs. Washington WIZARDS <br>
                           Thursday, October 30, 2014, 7:00 p.m. <br>
                           OPENING NIGHT <br> Amway Center, Downtown Orlando 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Blue-Martini-After-Hours-Mixer-Flyer.JPG" alt="Blue Martini After Hours Mixer Flyer" class="img-responsive"></p>
                        
                        <p>Valencia College Alumni Association After-Hours Mixer </p>
                        
                        <p>Date: Friday, October 24, 2014<br> 6:00-8:00 p.m.<br>
                           Location: <a href="http://orlando.bluemartinilounge.com/">Blue Martini Lounge</a> <br> 4200 Conroy Rd ,Orlando, FL 32839 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/NetworkAfterWork_000.JPG" alt="Network After Work" class="img-responsive"></p>
                        
                        <p>Join Network After Work on Wednesday October 22nd at Vyce (112 Orange Ave) from 6-9pm.
                           Network After Work is comprised of nearly one million like-minded professionals from
                           around the country. Network with attendees from all industries and career levels interested
                           in expanding their professional network and creating new business opportunities. Events
                           take place monthly which allows guests a chance to foster new professional connections
                           in a relaxed atmosphere conducive to business and social networking. Upon entering,
                           guests will receive a name tag color-coded by industry which allows for easy navigation.
                           Network After Work events give guests a chance to get their name and brand in front
                           of top local business professionals while visiting the city's best After Work destinations.
                        </p>
                        
                        <p>  To RSVP please visit:  <a href="https://www.networkafterwork.com/events/orlando">https://www.networkafterwork.com/events/orlando</a></p>
                        
                        <p>Admission Includes:<br>
                           
                           <ol class="list_style_1">
                              
                              <li>Access to 200+ Professionals</li>
                              
                              <li>Color-Coded Name Badge</li>
                              
                              <li>First Featured Cocktail before 7pm</li>
                              
                              <li>Light Appetizers from 6-7pm</li>
                           </ol>
                        </p>
                        
                        <p>Admission: $12 in advance / $15 with RSVP at the door / $20 without RSVP </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" class="img-responsive"></p>
                        
                        <p>Wednesday, October 8, 2014<br>
                           6:30 p.m. – 9:00 p.m.<br>
                           Valencia West Campus, <br>
                           Bldg.8, Special Events Center 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Blue-Martini-After-Hours-Mixer-Flyer-2014.JPG" alt="Blue Martini After Hours Mixer Flyer" class="img-responsive"></p>
                        
                        <p>Valencia College Alumni Association After-Hours Mixer </p>
                        
                        <p>Date: Friday, September 26, 2014<br>
                           6:00-8:00 p.m.<br>
                           Location: <a href="http://orlando.bluemartinilounge.com/">Blue Martini Lounge</a> <br>
                           4200 Conroy Rd, Orlando, FL 32839 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Network-After-Work-Latitudes.JPG" alt="Network After Work Latitudes" class="img-responsive"></p>
                        
                        <p></p>
                        
                        <p> You are invited to the next Network After Work event on Wednesday, September 24th
                           at Latitude's (33 W Church St.) from 6-9pm. Network After Work hosts events in over
                           40 locations and has over
                           600,000 members nationwide. Events provide you with a great opportunity to expand
                           your network while creating new business opportunities and are open to all industries
                           and career levels. Upon your arrival
                           you will receive a name badge color coded by industry which allows for easy navigation.
                           
                        </p>
                        
                        
                        <p>To RSVP please visit: <br> <a href="https://www.networkafterwork.com/events/orlando">https://www.networkafterwork.com/events/orlando</a></p>
                        
                        <p>Admission Includes:<br>
                           
                           <ol class="list_style_1">
                              
                              <li>Access to 200+ Professionals</li>
                              
                              <li>Color-Coded Name Badge</li>
                              
                              <li>First Featured Cocktail before 7pm</li>
                              
                              <li>Light Appetizers from 6-7pm</li>
                           </ol>
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/5K-Photo-Collage.jpg" alt="5K Photo Collage" class="img-responsive">
                           
                        </p>
                        
                        <p>Valencia College 2nd Annual Family Walk/Run for Heroes (Osceola Campus 5K run &amp; 2.5K
                           fun run) 
                        </p>
                        
                        <p>brought to you by the Valencia Alumni Association <br> All proceeds go toward the Rotary Club of Lake Nona's Sept. 11 Memorial Fund to support
                           Valencia scholarships for emergency
                           responders.<br> <br>
                           Date: Friday, September 5, 2014<br> 6:30 p.m. <br> <br>
                           Location: Valencia Osceola Campus<br>
                           1800 Denn John Lane<br> Kissimmee, FL 34744 <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-Association-of-Honors-Alumni-Wine-Cheese.jpg" alt="Association of Honors Alumni Wine Cheese Reception" class="img-responsive"></p>
                        
                        <p>Association of Honors Alumni <br> Wine &amp; Cheese Reception<br> Honors alumni and
                           friends networking fun complete with food and beverages, music, games!<br> Casual dress recommended!<br>
                           <br>  Friday, August 8, 2014 <br> Drop in between 6:00 - 8:00 p.m.<br>
                           Valencia's West Campus<br> Special Events Center, Building 8 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando-Salon.jpg" alt="TEDx Orlando Salon" height="135" width="242"></p>
                        
                        <p>Thursday, August 6, 2014<br> 6:30 p.m. – 9:00 p.m.<br> Valencia Osceola Campus, <br> Bldg. 4, Rm 105
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Network-After-Work-Latitudes-Blue-Martini.JPG" alt="Network After Work Latitudes Blue Martini" class="img-responsive"></p>
                        
                        <p>Orlando at Blue Martini </p>
                        
                        <p>Date: Tuesday, July 29, 2014<br> 6:00-9:00 p.m.<br> Location: <a href="http://orlando.bluemartinilounge.com/">Blue Martini Lounge</a> <br> 4200 Conroy Rd , Orlando, FL
                           32839 <br> <br> Network After Work is comprised of 500,000 like minded professionals from around
                           the
                           country and are for all industries and career levels interested in expanding their
                           professional network
                           and creating new business opportunities. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando-Salon-Flyer-2014.JPG" alt="TEDx Orlando Salon Flyer" class="img-responsive"></p>
                        
                        <p>Thursday, June 12, 2014<br> 6:30 p.m. – 9:00 p.m.<br> Valencia West Campus, <br> Bldg. 8, Special Events
                           Center 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-Alumni-Achievers.JPG" alt="Alumni Achievers Reception" class="img-responsive"></p>
                        
                        <p></p>
                        
                        <p>Alumni Achievers Reception <br> <em>by invitation only</em></p>
                        
                        <p>The Alumni Association hosts this reception to honor Association student scholarship
                           and award
                           recipients, and outstanding alumni and volunteers.<br> <br> Date: Tuesday, June 3, 2014<br> 6:30
                           p.m. - Reception<br> 7:00 p.m. - 8:30 p.m. - Program <br> Location: Valencia's West
                           Campus, Special Events Center, Building 8<br> <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><a href="http://www.buffalowildwings.com/en/"><img src="/_resources/images/FOUNDATION/alumni/buffalo-wild-wings-mixer.jpg" alt="buffalo wild wings mixer" class="img-responsive"></a></p>
                        
                        <p>Valencia Osceola Alumni Mixer <br>  Join us for some networking and fun <br> with some
                           amazing people! <br> <a href="http://www.buffalowildwings.com/en/"><br> Happy Hour at Buffalo Wild
                              Wings</a></p>
                        
                        <p>Date: Thursday, May 15, 2014<br> 5:00-7:00 p.m.<br> Location: <a href="http://www.buffalowildwings.com/en/">Buffalo Wild Wings </a><br> 1307 E. Osceola Pkwy,Kissimmee, FL
                           34744
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" class="img-responsive"></p>
                        
                        <p>TEDxOrlando Salon</p>
                        
                        <p>Wednesday, April 16, 2014<br> 6:30 p.m. – 9:00 p.m.<br> Valencia College's <br>
                           Collaborative Design Center, Bldg. 10
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/job-fair-2014.JPG" alt="job fair 2014" class="img-responsive"></p>
                        
                        <p>2014 Job Fair<br> <br> Date: Monday, March 31, 2014<br> 10:00 AM to 2:00 PM&nbsp; 
                        </p>
                        
                        <p>Location: Valencia West Campus, Bldg. 8, Special Events Center <br> 1800 South Kirkman Road<br>
                           Orlando, FL 32811<br>
                           
                           Learn about local career opportunities,<br> network with employers, &amp; possibly interview on the spot.
                           Check out the dates for free resume writing and interview workshops<br> leading up to the fair. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><a href="http://racetimesports.com/assets/race_logos/Valencia-032914.htm">RACE RESULTS ARE IN! </a></p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/5K-Flyer.jpg" alt="5K Flyer" height="195" width="255"></p>
                        
                        <p>Valencia College 9th Annual <br> 5K Run, Walk &amp; Roll<br> brought to you by the<br>
                           Valencia Alumni Association<br> <em>in memory of alumni leader Justin Harvey</em><br> <br> For athletes of
                           all abilities!<br> Funds raised will support Valencia<br> students through criminal justice, firefighter,
                           <br> nursing and EMS scholarships.<br> <br> Date: Saturday, March 29, 2014, 6:00 p.m.<br>
                           Location: Valencia West Campus<br> 1800 South Kirkman Road<br> Orlando, FL 32811
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Blue-Martini-After-Hours-Mixer-Flyer-2014-03.JPG" alt="Blue Martini After Hours Mixer Flyer 2014" class="img-responsive"></p>
                        
                        <p>Valencia College Alumni After-Hours Mixer </p>
                        
                        <p>Date: Thursday, March 13, 2014<br> 6:00-8:00 p.m.<br> Location: <a href="http://orlando.bluemartinilounge.com/">Blue Martini Lounge</a> <br> 4200 Conroy Rd ,
                           Orlando, FL 32839 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>Pre-Game</p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/meetup-logo.jpg" alt="meetup logo" class="img-responsive"></p>
                        
                        <p> Join us for discounted beverages and networking&nbsp;before the Orlando Magic Game this
                           Sunday from 4-5:30pm!<br>
                           <br> We will be at <a href="https://www.emberorlando.com/">Ember in downtown Orlando.</a></p>
                        
                        <hr class="styled_2">
                        
                        <p>VALENCIA - ORLANDO MAGIC NIGHT <br> Spring Break Offer!<br>
                           
                        </p>
                        
                        <p>Orlando MAGIC vs. Philadelphia 76ers <br> <br> Date: Sunday, March 2, 2014, 6:00
                           p.m. <br> Location: Amway Center, Downtown Orlando <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><a href="http://tedxorlando.com/salon/"><img src="/_resources/images/FOUNDATION/alumni/TEDx-Orlando.jpg" alt="TEDx Orlando" class="img-responsive"></a></p>
                        
                        <p>TEDxOrlando Salon</p>
                        
                        <p>Thursday, February 20, 2014<br> 6:30 p.m. – 9:00 p.m.<br> Valencia West Campus, <br>
                           Bldg. 8, Special Events Center 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>
                           <img src="/_resources/images/FOUNDATION/alumni/Reception-for-business-professionals-Get-Connected.jpg" alt="Reception for business professionals Get Connected" class="img-responsive"></p>
                        
                        <p>Valencia Professional Networking Reception <br> for business professionals, Valencia
                           students and alumni<br> <br> Join Valencia alumni and friends for an evening of<br> organized,
                           professional networking.<br> <br> Tuesday, February 18, 2014<br> 6:00 p.m. – 8:00
                           p.m.<br> Valencia East Campus, Bldg.5, Rm 112 <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Alumni-Night-Solar-Bears-Flyer-II.jpg" alt="Alumni Night Solar Bears Flyer II" class="img-responsive"></p>
                        
                        <p>Valencia College Alumni and Friends<br> with the <br> ORLANDO SOLAR BEARS <br> <br>
                           Date: Sunday, January 26, 2014, 3:00 p.m. <br> Location: Amway Center, Downtown
                           Orlando
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Blue-Martini-After-Hours-Mixer-Flyer-2014-01.jpg" alt="Blue Martini After Hours Mixer Flyer" class="img-responsive"></p>
                        
                        <p><br>
                           
                        </p>
                        
                        <p>Valencia College Alumni After-Hours Mixer </p>
                        
                        <p>Date: Thursday, January 16, 2014<br> 5:30-7:00 p.m.<br> Location: <a href="http://orlando.bluemartinilounge.com/">Blue Martini Lounge</a> <br> 4200 Conroy Rd ,
                           Orlando, FL 32839 
                        </p>
                        
                        <a id="2013pastevents"></a>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>2013 Past Events</h4>
                        
                        <p>&nbsp;</p>
                        
                        <p> 2013 HOMECOMING MONTH! </p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/banner-alumni-homecoming.jpg" alt="alumni homecoming" class="img-responsive"></p>
                        
                        <p>This October, join Valencia for our homecoming celebration! With a variety of activities
                           and events on
                           multiple campuses, and off campus, you'll have plenty of opportunities to revisit
                           the college and connect
                           with fellow alumni, retirees and friends. 
                        </p>
                        
                        <p>For more information contact <a href="mailto:alumni@valenciacollege.edu">Community &amp; Alumni
                              Relations</a></p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/homecoming-offer.jpg" alt="homecoming offer" class="img-responsive">
                           
                        </p>
                        
                        <p>VALENCIA - ORLANDO MAGIC NIGHT <br> <br> Orlando MAGIC vs. New Orleans PELICANS <br>
                           Friday, November 1, 2013<br> 7:00 p.m. <br> Amway Center, Downtown Orlando 
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-Paralegal.jpg" alt="Paralegal Reception" class="img-responsive"></p>
                        
                        <p>Paralegal Alumni Reception<br> Thursday, October 24, 2013<br> 6:00 p.m. – 8:00
                           p.m.<br> Valencia's Criminal Justice Institute, auditorium
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Dental-Reunion-Flyer.jpg" alt="Dental Reunion Flyer" class="img-responsive"></p>
                        
                        <p>Join us during Dental Hygiene Month in October, and as part of Valencia Homecoming
                           Month, for
                           the <br> <br> 35th Anniversary Valencia Dental Hygiene Program Reunion<br>
                           Saturday, October 12, 2013 <br> 2:00 p.m.-5:00 p.m.<br> West Campus,
                           Special Events Center (Bldg. 8) <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-For-Business-Professionals-Talking-People.jpg" alt="Reception For Business Professionals Talking People" class="img-responsive"></p>
                        
                        <p>Osceola Alumni Networking Reception <br> For Business Professionals, Valencia Students
                           and Alumni.<br> <br> Join Valencia alumni and friends for an evening of organized, professional
                           networking.<br> <br> Thursday, October 10, 2013<br> 6:00 p.m. – 8:00 p.m.<br> Valencia
                           Osceola Campus, Bldg. 4, Rm 105 <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><u><a href="http://racetimesports.com/assets/VALENCIA-OSC-AGEGRP13.htm">Race Results are in! </a></u></p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/5Kcover.jpg" alt="5K Photos" class="img-responsive"></p>
                        
                        <p><u><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157635462457087/">Check out the
                                 photos!</a></u></p>
                        
                        <p>Valencia College Family Walk/Run for Heroes<br> (Osceola Campus 5K run &amp; 2.5K fun run) <br> Saturday,
                           September 7, 2013<br> 6:30 p.m. <br> <br> All proceeds go toward the Rotary Club of Lake Nona's Sept. 11
                           Memorial Fund to support Valencia scholarships for emergency responders. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Annual-Luau-Hibiscus-Flowers.jpg" alt="Annual Luau Hibiscus Flowers" class="img-responsive"></p>
                        
                        <p>Association of Honors Alumni – AHA! <br> 4th Annual Luau<br> Honors alumni and friends
                           networking fun complete with tropical food and beverages, music, games! Casual or
                           tropical dress
                           recommended!<br>  Friday, August 9, 2013 <br> 7:00 p.m.- 9:00 p.m. <br>
                           Valencia's West Campus, Special Events Center, Building 8
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Young-Professionals-of-Kissimmee-Chambers-Flyer.jpg" alt="Young Professionals of Kissimmee Chambers Flyer" class="img-responsive"></p>
                        
                        <p>Young Professionals of Kissimmee Chambers<br> Meet &amp; Greet <br> Thursday, June 27, 2013 <br> 5:30
                           p.m.- 7:00 p.m. <br> <a href="http://www.3sistersspeakeasy.com/">3 Sisters Speakeasy </a></p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-Alumni-Achievers-Trophy.jpg" alt="Reception Alumni Achievers Trophy" height="152" width="141"></p>
                        
                        <p>Event: Alumni Achievers Reception <br> The Alumni Association hosts
                           this reception to honor Association scholarship and award recipients, and outstanding
                           alumni and
                           volunteers.<br> Date: Wednesday, June 5, 2013<br> 6:30 p.m. - Reception<br> 7:00 p.m. -
                           8:30 p.m. - Program <br> Location: Valencia's West Campus, Special Events Center,
                           Building 8 <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Brazilian-Film-Festival-Flyer.jpg" alt="Brazilian Film Festival Flyer" class="img-responsive"></p>
                        
                        <p>Event: Valencia's 6th Brazilian Film Festival <br> Locations: <br> West
                           Campus: April 5, 9,11,12 (7 p.m. Room 3-111)<br> Osceola Campus: April 8 (7 p.m. Bldg. 1 Auditorium) 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/5K-Flyer.jpg" alt="5K Flyer" height="195" width="255"><br>
                           
                        </p>
                        
                        <p>Valencia's 8th Annual 5K Run, Walk &amp; Roll<br> <em>in memory of alumni leader Justin
                              Harvey </em><br> March 30, 2013<br> For athletes of all abilities!
                        </p>
                        
                        <p>Funds raised will support Valencia students through<br> criminal justice, firefighter, <br> nursing and
                           EMS scholarships.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Humanities-Speakers-Series-Connected.png" alt="Humanities Speakers Series Connected" class="img-responsive"></p>
                        
                        <p>Event: The Humanities Speakers Series <br> Screening of Tiffany Shlain's
                           award-winning film,<em>Connected </em><br> Tuesday, March 26, 2013 <br> 7:00
                           p.m.<br> Location:East Campus, Performing Arts Center 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Alumni-Night-Orlando-Magic-Flyer-Spring-Break-Offer.jpg" alt="Alumni Night Orlando Magic Flyer Spring Break Offer" class="img-responsive"></p>
                        
                        <p>VALENCIA - ORLANDO MAGIC NIGHT <br> Spring Break Offer!
                        </p>
                        
                        <p>Orlando MAGIC vs. Memphis GRIZZLIES <br> Sunday, March 3, 2013<br> 6:00 p.m. <br> Amway
                           Arena
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Reception-for-business-professionals-Get-Connected-Flyer.jpg" alt="Reception for business professionals Get Connected Flyer" class="img-responsive"></p>
                        
                        <p><br> GET CONNECTED! <br> <br> Professional Networking Reception <br> for Business
                           Professionals, Valencia<br> Students and Alumni<br> <br> Friday, February 22nd, 2013 <br>
                           6:00 p.m. – 8:00 p.m.<br> Location: Valencia West Campus,<br> 1800 South Kirkman Road<br>
                           Special Events Center -Bldg. 8
                        </p>
                        
                        <a id="2012pastevents"></a>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>2012 Past Events</h4>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Dia-Latino-en-ICE-2012.jpg" alt="Ice flyer" class="img-responsive">
                           
                        </p>
                        
                        <p>Event: “Dia Latino en ICE!” <br> Thursday, November 15th,
                           2012 <br> 10:00 a.m. – 10:00 p.m.<br> Location: Gaylord Palms 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/International-Education-Week-2012.jpg" alt="International Education Week 2012" class="img-responsive"></p>
                        
                        <p>Movie: Amelie (Sponsored by Valencia Volunteers) <br> Thursday, November 15th,
                           2012 <br> 5:00 p.m. – 7:00 p.m.<br> Special Events Center (Bldg. 8), West Campus 
                        </p>
                        
                        <p>Description: Amelie tells the story of a shy waitress, played by Audrey Tautou, who
                           decides to change the lives of those around her for the better, while struggling with
                           her own isolation.
                           The film was an International co-production between companies in France and Germany.
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/International-Education-Week-2012.jpg" alt="Connected" class="img-responsive"></p>
                        
                        <p>Mexican Art Display (Courtesy of Mexican Consulate)<br> Wednesday, November 14th, 2012
                           <br> 9:00 a.m. – 3:00 p.m. <br> HSB 105, West Campus
                        </p>
                        
                        <p>Description: A diverse representation of the traditional art of Mexico. <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/KiteRunner_001.jpg" alt="kite runner movie" class="img-responsive">
                           
                        </p>
                        
                        <p>Movie: Kite Runner (Sponsored by Valencia Volunteers and the Peer Educators) <br>
                           Tuesday,November 13th, 2012 <br> 5:00 p.m. – 7:00 p.m.<br> HSB 211, West
                           Campus 
                        </p>
                        
                        <p>Description: Kite Runnertells the story of Amir, a young boy from the Wazir Akbar
                           Khan
                           district of Kabul, whose closest friend is Hassan, his father's young Hazara servant.
                           The story is set
                           against a backdrop of tumultuous events, from the fall of Afghanistan's monarchy through
                           the Soviet
                           invasion, the exodus of refugees to Pakistan and the United States, and the rise of
                           theTaliban regime.
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/International-Education-Week-2012.jpg" alt="International Education Week 2012" class="img-responsive"></p>
                        
                        <p>Movie: Connected (Selected as part of the 2012 American Film Showcase) <br> Monday,
                           November 12th, 2012 <br> 5:00 p.m. – 7:00 p.m. <br> Special Events Center (Bldg. 6), West Campus
                           
                        </p>
                        
                        <p>Description: Connected: Have you ever faked a restroom trip to check your email? Slept
                           with your laptop? Or become so overwhelmed that you just unplugged from it all? In
                           this funny,
                           eye-opening, and inspiring film, Director Tiffany Shlain takes audiences on an exhilarating
                           rollercoaster
                           ride to discover what it means to be connected in the 21stcentury. Connected is being
                           shown in
                           collaboration with Nichole Jackson and the East Campus Humanities Department. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Network-After-Work-Kasa.jpg" alt="Network After Work Kasa" class="img-responsive"></p>
                        
                        <p>History Book Club With Eliot Kleinberg<br> "War in Paradise: Stories of World War II in
                           Florida"<br> Saturday, November 10th, 2012<br> 2:00 p.m. – 3:30 p.m. <br> History
                           Center<br> 65 East Central Boulevard, Downtown Orlando 
                        </p>
                        
                        <p>Admission: FREE<br> <br> Description: Learn about how WWII impacted
                           Florida, not only its soldiers, but also its citizens and its landscape. Meet author
                           Eliot Kleinberg, a
                           Florida native and graduate of the University of Florida, who writes for The Palm
                           Beach Post in West Palm
                           Beach and has written 10 books about Florida. The event will be immediately followed
                           by a book signing.
                           Refreshments will be provided. For more information, visit thehistorycenter.org or
                           call 407.836.7010.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>2012 HOMECOMING WEEK! </p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/banner-alumni-homecoming.jpg" alt="Homecoming 2012" class="img-responsive"></p>
                        
                        <p>This October, join Valencia for our homecoming celebration! With a variety of activities
                           and events on
                           multiple campuses, and off campus, you'll have plenty of opportunities to revisit
                           the college and connect
                           with fellow alumni, retirees and friends. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><a href="http://fiestainthepark.com/"><img src="/_resources/images/FOUNDATION/alumni/fiesta_park_logo_m.jpg" alt="Fiesta In The Park at Lake Eola" class="img-responsive"></a><br>
                           
                        </p>
                        
                        <p>42nd Annual Fall Fiesta in the Park at Lake Eola<br> Saturday, November 3, 2012, 10:00 a.m.- 5:00
                           p.m. <br> Sunday, November 4, 2012, 12:00 p.m.- 5:00 p.m. <br>
                           
                        </p>
                        
                        <p>The event features more than 300 artist and crafter booths, a kids' area, festival
                           fair, live
                           entertainment, food and beverages. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Homecoming-UCF-Game.jpg" alt="Homecoming UCF Game" class="img-responsive"><br> UCF HOMECOMING GAME<br> $20 Tickets for UCF vs SMU Homecoming<br>
                           
                        </p>
                        UCF Knights vs. Southern Methodist University Mustangs!<br> Saturday, November 3, 2012 <br> 7:00 p.m. <br>
                        Bright House Networks Stadium
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Orlando-Magic-Night.jpg" alt="Orlando Magic Night" class="img-responsive"></p>
                        
                        <p>Valencia- Orlando Magic Night!<br> Season's Opening Game <br> Orlando MAGIC vs. Denver NUGGETS
                           <br> Friday, November 2, 2012 <br> 7:00 p.m. <br> Amway Arena
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Popcom-Flicks-Chicken-Run.jpg" alt="Popcom Flicks Chicken Run" class="img-responsive"><br>
                           
                        </p>
                        
                        <p>Popcom Flicks!<br> Chicken Run <br> Thursday, November 1, 2012 <br>
                           Admission: FREE <br> Time: 8:00 p.m. <br> Location: Central Park<br> 251 Park Avenue
                           South <br> Winter Park, FL
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Trick-or-Treat-on-Matador-Street-Flyer.jpg" alt="Trick or Treat on Matador Street Flyer" class="img-responsive"></p>
                        
                        <p>Trick or Treat on Matador Street </p>
                        
                        <p> There will be games, prizes, entertainment and food! </p>
                        
                        <p><em>Wednesday, October 31, 2012<br>  Time: 5:00 p.m. - 8:00 p.m. <br> Valencia College,
                              West Campus HSB 105 </em></p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Barry-Estabrook-Tomatoland.jpg" alt="Barry Estabrook Tomatoland" class="img-responsive"></p>
                        
                        <p>Barry Estabrook and "Tomatoland"</p>
                        
                        <p> Barry Estabrook is an investigative food journalist who writes for the New York Times,
                           the Washington
                           Post and The Atlantic. He will be discussing his recent book Tomatoland, which details
                           the human and
                           environmental costs of Florida's tomato industry as well as local and national efforts
                           to grow a tastier
                           and more sustainable tomato. For more information about Estabrook, click <a href="http://politicsoftheplate.com/?page_id=2">here</a>. 
                        </p>
                        
                        <p><em>Tuesday, October 30, 2012 </em> <br> Time: 7:00-9:30 p.m.<br> Admission: FREE
                           <br> Location:<a href="http://www.rollins.edu/visit-rollins/locations-map/"> <u>Rollins College - Bush
                                 Executive Center / Crummer Hall</u></a><br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Lake-Nona-Rendering.jpg" alt="Lake Nona Rendering" class="img-responsive"></p>
                        
                        <p>Lake Nona Campus Reception &amp; Tours<br> <em>Drop by after work!</em><br> Learn
                           more about Valencia's newest campus!<br> Monday, October 29, 2012<br> 6:00 p.m. - Networking
                           reception/refreshments 6:30 p.m.- 7:00 p.m. - Student-led tours<br> Location: Lake Nona Campus<br>
                           FREE<br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><a href="https://www.orlandofarmersmarket.com/"><img src="/_resources/images/FOUNDATION/alumni/FarmersMarket.JPG" alt="Farmers Market" class="img-responsive"></a>
                           
                        </p>
                        
                        <p> Weekly Orlando Farmers Market</p>
                        
                        <p> Come experience Central Florida's largest weekly market. </p>
                        
                        <p><em>Sunday, October 28, 2012<br>  10:00 a.m. – 4:00 p.m.</em><br> <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Urinetown-The-Musical.jpg" alt="Urinetown The Musical" class="img-responsive"></p>
                        
                        <p>Urinetown: The Musical <br> 
                        </p>
                        <br> Synopsis: In an attempt to regulate water consumption, Urinetown has outlawed the
                        use of private
                        toilets. The citizenry must use public, pay-per-use amenities owned and operated by
                        Urine Good Company, a
                        malevolent corporation run by the corrupt Caldwell B. Cladwell. It satirizes the legal
                        system, capitalism,
                        social irresponsibility, populism, bureaucracy, corporate mismanagement, and municipal
                        politics.<em> <br>
                           <br> Sunday, October 28, 2012 </em><em> <br> Time: 2:00 p.m. <br> Location: East Campus
                           Performing Arts Center (PAC)</em>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Bach-Festival-Society-Logo.JPG" alt="Bach Festival Society Logo" class="img-responsive"></p>
                        
                        <p>Modern Masterworks</p>
                        
                        <p>The 150-member Bach Festival Choir and the Bach Orchestra perform works by living
                           artists Eric Whitacre,
                           Steven Paulus, Paul Moravec, Morten Lauridsen, and more.
                        </p>
                        
                        <p><em>Saturday, October 27, 2012, 7:30 – 9:30 p.m. <br> Sunday, October 28, 2012,
                              3:00 – 5:00 p.m. <br> Knowles Memorial Chapel, Rollins College</em><br>
                           <br> 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Walk-N-Roll-for-Spina-Bifida.jpg" alt="Walk-N-Roll for Spina Bifida" class="img-responsive"><br> Join the Valencia College Team at the <br> Walk-N-Roll Spina
                           Bifida<br> Saturday, October 27, 2012<br> 10:00 a.m.<br> Blue Jacket Park in Orlando, Florida
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Latin-Night-Flyer.jpg" alt="Latin Night Flyer" height="173" width="128">
                           
                        </p>
                        
                        <p>HOMECOMING WEEK KICKOFF!<br> Event is CANCELLED <br> Latin Night 2012 <br>
                           <em>hosted by the Latin American Student Organization - Osceola Campus </em><br> Live DJ,
                           Food &amp; Non-Alcoholic Drinks Included <br> Friday, November 2, 2012<br> 7:00 - 11:00 p.m.<br>
                           Osceola Campus (in front of cafeteria)
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>Tuesday, October 9, 2012 <br> 11:30 a.m.- 2:00 p.m. <br> Valencia's West Campus, HSB 105<br>
                           <em>By Invitation Only</em></p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Retiree-Connection-Luncheon-Happy-Retirement.jpg" alt="Retiree Connection Luncheon Happy Retirement" class="img-responsive"></p>
                        
                        <p>Valencia Retiree Connection Luncheon </p>
                        
                        <hr class="styled_2">
                        
                        <p>Sunday, September 23, 2012 <br> 2:00 p.m. - 5:00 p.m.<br> Valencia's West Campus Special
                           Events Center, Building 8 <br> <em>By Invitation Only</em></p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Alumni-Leadership-Board-Retreat.jpg" alt="Alumni Leadership Board Retreat" class="img-responsive"></p>
                        
                        <p>Alumni Leadership Board Retreat</p>
                        
                        <hr class="styled_2">
                        
                        <p>Saturday, September 15, 2012 <br> 7:00 p.m. - 10:00 p.m.<br> Rosen Shingle Creek 
                        </p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/TASTE-postcard-2012.jpg" alt="TASTE postcard 2012" class="img-responsive"></p>
                        
                        <p>A Taste for Learning </p>
                        
                        <p>An international wine sampling and silent auction to benefit Valencia Foundation and
                           Orlando Health
                           Foundation 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>Saturday, September 15, 2012 <br> 8:30 a.m. - 11:30 a.m.<br> Lake Eola Forum 
                        </p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Tinas-Turn-Out-Walk-Run.jpg" alt="Tinas Turn Out Walk Run" class="img-responsive"></p>
                        
                        <p>Tina's Turn Out Walk &amp; Run </p>
                        
                        <p>Enjoy a walk or run around Lake Eola in memory &amp; celebration of Tina Collyer's life!
                           All proceeds
                           from this event will go to the Tina Collyer Memorial 'Tina's Heart' EMT Scholarship<br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Pathways-to-Peace-A-Week-of-Remembrance-Flyer.jpg" alt="Pathways to Peace A Week of Remembrance Flyer" class="img-responsive">
                           
                        </p>
                        
                        <p>Pathways to Peace: A Week of Remembrance</p>
                        
                        <p>“United We Stand for Peace” Interfaith Panel Discussion<br> <em>Monday,
                              September 10, 6:00-8:00 p.m.</em><br> Rabbi Steven Engel, Reverend Bryan Fulwider, and Imam Muhammad Musri
                           will lead a discussion on monotheistic religions and violence. This will be followed
                           with a question and
                           answer period, moderated by Adriene Tribble, professor of humanities. Auditorium,
                           Building 1.
                        </p>
                        
                        <p>Memorial Service to Honor First Responders <br> Tuesday, September 11, 1:30-2:30
                           p.m.<br> This county-wide memorial service will honor our local first responders and remember
                           those who were lost on September 11th. Clock Tower, behind Building 2.
                        </p>
                        
                        <p>“Joyeux Noël” Film and Focus Series<br> <em>Wednesday, September 12, 6:00-8:50
                              p.m.</em><br> Documentary film showing and discussion lead by Paul Chapman, professor of
                           humanities. Auditorium, Building 1.
                        </p>
                        
                        <p>“Religious Understanding” Presentation, Sikh Society of Central Florida<br> <em>Thursday,
                              September 13, 1:30-2:30 p.m. </em><br> Members of our local Sikh community will present on
                           Sikhism, the recent shooting of a temple in Wisconsin, and what the Sikh Society of
                           Central Florida is
                           doing in the community. Auditorium, Building 1. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>Tuesday, August 21 , 2012 <br> 8:30 p.m.<br> Valencia's West Campus, HSB 105 
                        </p>
                        
                        <p>Valencia Alumni Leadership Team Meeting <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>Tuesday, August 21 , 2012 <br> 6:30 p.m. - 8:30 p.m.<br> Valencia's West Campus, HSB 105
                           
                        </p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Orlando-Magic-Night.jpg" alt="Orlando Magic Night" height="173" width="148"></p>
                        
                        <p>Valencia Alumni Idea Exchange &amp; Volunteer Appreciation Reception <br> <br> <em>For
                              all Valencia alumni, friends and volunteers. Network and share ideas for future alumni
                              projects while
                              celebrating past accomplishments and the volunteers that made them possible! </em><br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>Friday, August 10, 2012<br> 7:30 - 10:00 p.m. <br> Valencia's West Campus Special Events
                           Center, Building 8 <br>
                           
                        </p>
                        
                        <p><img src="/_resources/images/FOUNDATION/alumni/Annual-Luau-Hibiscus-Flowers-2012.jpg" alt="Aloha AHA!" height="125" width="150"></p>
                        
                        <p><em>Association of Honors Alumni – AHA! <br> 3rd Annual Luau<br> <br> </em> <em>Honors
                              alumni and friends networking fun complete with tropical food and beverages, music,
                              games! Casual or
                              tropical dress recommended! </em></p>
                        
                        <p><em>Donations accepted to support the Association of Honors Alumni Transfer Scholarship:
                              <a href="https://donate.valencia.org/alumni">Click Here to Donate</a></em></p>
                        
                        <hr class="styled_2">
                        
                        <p>June 5, 2012 <br>
                           
                        </p>
                        
                        <p>Valencia's West Campus Special Events Center Bldg 8-111</p>
                        
                        <p>8:15 p.m. following Alumni Achievers Reception</p>
                        
                        <p>Valencia Alumni Association Leadership Team Business Meeting</p>
                        
                        <hr class="styled_2">
                        
                        <p>June 5, 2012<br> 6:30 p.m. - Reception<br> 7:00 p.m. - 8:15 p.m. - Program <br>
                           Valencia's West Campus, Special Events Center, Building 8 <br>
                           
                        </p>
                        
                        <p>Alumni Achievers Reception <br> The Alumni Association hosts this reception to honor
                           Association scholarship and award recipients, and outstanding alumni and volunteers.
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>May 5, 2012<br> 12:00 p.m. - 3:00 p.m. <br> Following Commencement Ceremony<br>
                           Valencia's Osceola Campus<br>
                           
                        </p>
                        
                        <p>Association of Honors Alumni - Honors Program Graduate Reception -- private reception
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>May 5, 2012</p>
                        
                        <p>Heritage Park, Silver Spurs Arena, Kissimme, Florida</p>
                        
                        <p>Commencement Ceremony</p>
                        
                        <hr class="styled_2">
                        
                        <p>April 10, 2012<br> <em>For alumni jobseekers &amp; employers!<br> *3:00-7:00 p.m. <br>
                              </em>*Valencia's West Campus, Special Events Center, Building 8 
                        </p>
                        
                        <p>2012 Valencia Job Fair <br> Learn about local career opportunities, network with
                           employers, &amp; possibly interview on the spot. Check out the dates for free resume writing
                           and interview
                           workshops leading up to the fair. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>April , 2012 <br> 6:30 p.m. - 7:30 p.m.<br> Valencia's West Campus Special Events
                           Center, Bldg 8-111 A,B 
                        </p>
                        
                        <p>Mayor Richard T. Crotty Valencia- UCF Alumni 2+2 High School Incoming Scholarship
                           Interviews
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>April 25, 2012<br> Valencia's West Campus Special Events Center Bldg 8-111 A/B<br> 6:00
                           p.m. to 7:30 p.m. 
                        </p>
                        
                        <p>Networking Reception &amp; Meeting </p>
                        
                        <p>Valencia Alumni Association Idea Exchange</p>
                        
                        <hr class="styled_2">
                        
                        <p>March 31, 2012<br> <em>For athletes of all abilities! </em><br> *5:00 p.m. - Registration <br> *6:00 p.m. - 5K race begins!<br> *Free crafts for kids and a Fun Run <br> *Race
                           Headquarters: Valencia's West Campus, University Center, Building 11, Room 106 
                        </p>
                        
                        <p>Valencia's 7th Annual 5K Run, Walk &amp; Roll <em>in memory of Justin Harvey</em> <br>
                           <em>Funds raised will support criminal justice, firefighter and EMS scholarships </em><br> <br> <a href="http://racetimesports.com/valencia-college-5k/">5K Registration &amp; Details</a></p>
                        
                        <hr class="styled_2">
                        
                        <p>March 12, 2012<br>
                           
                        </p>
                        
                        <p>Valencia's West Campus, Special Events Center</p>
                        
                        <p>Mayor Richard T. Crotty Valencia- UCF Alumni 2+2<br> Valencia Graduating Scholarship Committee
                           Interviews
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>March 12, 2012<br> Valencia's West Campus, Special Events Center 
                        </p>
                        
                        <p>Mary S. Collier Distinguished Graduate Award Committee Interviews</p>
                        
                        <hr class="styled_2">
                        
                        <p>March 3, 2012<br> Orlando Magic vs. Milwaukee Bucks <br> 7:00 p.m. <br> Amway Arena,
                           Downtown Orlando<br> <em>*Discounted tickets $20 each for VALENCIA in Section 222.</em><br> Call Jake at
                           407-916-2642 to purchase your tickets. Seating limited!<br>
                           
                        </p>
                        
                        <p>Valencia Alumni &amp; Friends Spring Break Valencia-Orlando Magic Night  <br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>January 25, 2012<br> Valencia's Criminal Justice Institute Campus, 8600 Valencia College
                           Lane <br> 6:00 p.m. - Meet &amp; Eat Networking <br> 6:30 p.m. - CJI Tour <br> 7:00 p.m - Idea Exchange
                           
                        </p>
                        
                        <p>Valencia Alumni Association &amp; Friends <br> Idea Exchange
                        </p>
                        
                        <a id="2011pastevents"></a>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>2011 Past Events</h4>
                        
                        <p>December 2, 2011<br> 
                           						   11 a.m.- 12:30 p.m. with networking until 1:30 p.m.<br>
                           						   West Campus, Bldg. 9, Rm. 106<br>
                           
                        </p>
                        
                        <p>Culinary Arts Grande Buffet &amp; Culinary Alumni Reunion <br> Admission: $20 per
                           person. Proceeds to support learning opportunities for Culinary Program students.<br> <br> LIMITED
                           SEATING! <br> <em>Pay at the door or reserve a seat in advance by sending a check made out to
                              "Culinary Arts Student Assoc." to Valencia Grand Buffet, 11109 Lemay Drive, Clermont
                              FL 34711. </em></p>
                        
                        <hr class="styled_2">
                        
                        <p> October 2011<br> Reconnect both on &amp; off the Valencia campuses! 
                           
                        </p>
                        
                        <p> VALENCIA HOMECOMING MONTH<br> This October, join Valencia for our homecoming
                           celebration! With a variety of activities and events on multiple campuses, and off
                           campus, you'll have
                           plenty of opportunities to revisit the college and connect with fellow alumni, retirees
                           and friends.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>October 22, 2011 <br> 8 a.m. – 9:00 a.m. Team Photos<br> 9 a.m. Program<br> 9:20
                           Walk begins from Lake Eola through scenic Thornton Park (approx. 2 miles) <br> <a href="http://2011walktoendalz.kintera.org/faf/search/searchTeamPart.asp?ievent=456704&amp;team=4310845">The
                              Alzheimer's Association Walk to End Alzheimer's™</a> is the nation's largest event to raise
                           awareness and funds for Alzheimer care, support and research. Join TEAM VALENCIA at
                           Lake Eola to support
                           this worthwhile cause! 
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>October 12, 2011 <br> Valencia's West Campus Special Events Center Bldg 8-111 A/B<br>
                           6:00 p.m. to 7:30 p.m. Networking Reception &amp; Meeting <br> Valencia Alumni
                           Association &amp; Friends <br> Idea Exchange
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>September 24, 2011 <br> Lake Eola Park, Forum <br> Walk/Run begins at 9:30 a.m.<br>
                           T-Shirt pickup &amp; day of event registrations starts at 8:30 a.m. 
                        </p>
                        
                        <p>Tina's Turn Out Scholarship Fundraiser <br> Enjoy a walk or run around Lake Eola in
                           memory &amp; celebration of Tina Collyer's life! All proceeds from this event will go
                           to the Tina Collyer
                           Memorial EMT Scholarship (also known as <em>‘Tina's Heart'</em>),
                        </p>
                        
                        <hr class="styled_2">
                        
                        <p>September 14, 2011 <br> Valencia's West Campus Special Events Center Bldg 8-111 A/B<br>
                           6:00 p.m. to 7:30 p.m. Networking Reception &amp; Meeting
                        </p>
                        
                        <p>Valencia Alumni Association Leadership Team Business Meeting </p>
                        
                        <hr class="styled_2">
                        
                        <p>September 9, 2011 <br> Valencia's East Campus Performing Arts Center 7:00
                           p.m.<br> 
                        </p>
                        
                        <p>Back 2 School Comedy Jam <br> 
                           							Starring Kevin Bozeman and Buzz Sutherland 
                        </p>
                        						
                        						
                        						
                        						
                        						
                        
                        			
                     </div>
                     						
                  </div>
                  		
               </div>
               				
               							
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/events-past.pcf">©</a>
      </div>
   </body>
</html>