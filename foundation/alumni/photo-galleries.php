<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Photo Galleries | Valencia College</title>
      <meta name="Description" content="Alumni Association develops and provide opportunities for lifelong personal, educational and professional growth for alumni and students of Valencia College.">
      <meta name="Keywords" content="alumni, alumni association, volunteer, career, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/foundation/alumni/photo-galleries.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/foundation/alumni/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Alumni</h1>
            <p>
               
               
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/foundation/">Foundation</a></li>
               <li><a href="/foundation/alumni/">Alumni</a></li>
               <li>Photo Galleries</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               
               	
               <div class="container margin-60" role="main">
                  					
                  <div class="row">
                     					
                     <div class="col-md-12">
                        
                        <h2>Photo Galleries </h2>
                        
                        
                        <p></p> 
                        							 
                        <table class="table table table-striped cart-list add_bottom_30">
                           
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th scope="col">Date</th>
                                 
                                 <th scope="col">Title</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>September 9, 2016</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157673185341732">Valencia College,
                                       Osceola Campus 4th Annual 5K Run/Walk for Heroes </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>June 15, 2016</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/albums/72157667347026654">Alumni
                                       Achiever's Reception 2016 </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>April 2, 2016</td>
                                 
                                 <td>
                                    <a href="https://www.facebook.com/media/set/?set=a.10156803694910608.1073741862.105255620607&amp;type=3">Valencia
                                       College 11th Annual 5K Run, Walk &amp; Roll</a>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>December 18, 2015</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/albums/72157663098647481">A Night of
                                       Celebration 2015</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>September 11, 2015</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/albums/72157658323480539/page1">Valencia
                                       College 3rd Annual Walk/Run for Heroes 2015 (Osceola Campus 5K run)</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>July 31, 2015</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/albums/72157658411977525">Alumni Planning
                                       Retreat </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>July 30, 2015</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/albums/72157658411544605">Alumni &amp;
                                       Friends Bowling Night</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>July 24, 2015</td>
                                 
                                 <td>
                                    <a href="https://www.flickr.com/photos/valenciafoundation/sets/72157654550644543">Alumni and Friends
                                       Wine and Cheese Reception</a> &amp; <a href="https://youtu.be/VyEENnV0VRE">Video</a>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>June 3, 2015</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157654297004521">Alumni Achievers
                                       Reception 2015</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>April 30, 2015</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157652627591289">Alumni and Friends
                                       Wine &amp; Cheese Reception </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 21, 2015</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157651580977762/">2015 Valencia
                                       10th Annual 5K </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>February 26, 2015</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157651158299641/">Association of
                                       Bridges Alumni Leadership Networking Reception </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>December 12, 2014</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157649817023131/">A Night of
                                       Celebration 2014</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>September 5, 2014</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157647453836665/">Valencia College
                                       Family Walk/Run for Heroes 2014 (Osceola Campus 5K run &amp; 2.5K fun run) </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>August 8, 2014</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157646648334141/">2014 Association
                                       of Honors Alumni Wine and Cheese Reception </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>June 3, 2014</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157645112982142/">2014 Alumni
                                       Achievers Reception</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>May 3 , 2014</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157644587481636/">2014 Honors
                                       Reception </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 29, 2014</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157643407312404/">2014 Valencia 9th
                                       Annual 5K </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>February 20, 2014</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157643407044314/">TEDxOrlando
                                       Salon </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>February 18, 2014</td>
                                 
                                 <td><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157643406400704/">2014 Professional
                                       Networking Reception -East Campus </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>November 1, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157637423357826/">2013 Valencia
                                       Orlando Magic Night </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>October 30,2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157637448035575/">2013 Retiree
                                       Connection Luncheon </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>October 24, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157637058913075/">Paralegal Alumni
                                       Reception 2013</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>October 12, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157637423511125/">35th Anniversary
                                       Valencia Dental Hygiene Program Reunion 2013 </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>October 10, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157636552802084/">Osceola Alumni
                                       Networking Reception 2013 </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>September 7, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157635462457087/">Valencia College
                                       Family Walk/Run for Heroes 2013 (Osceola Campus 5K run &amp; 2.5K fun run)</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>June 5, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/myvalencia/sets/72157633987334286/">2013 Alumni Achievers
                                       Reception</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>May 4, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157633571363407/">2013 Honors
                                       Reception </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 30, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157633155270739/">2013 Valencia 8th
                                       Annual 5K </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>February 22, 2013</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157632880409897/">2013 Professional
                                       Networking Reception </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>November 2, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157631953331408/">2012 Valencia
                                       Orlando Magic Night </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>October 27, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157631953388450/">2012 Walk-N-Roll
                                       Spina Bifida</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>September 23, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157631666451982/">2012 Alumni
                                       Leadership Board Retreat</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>September 15, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157631666820933/">Tina's Turnout
                                       2012</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>August 21, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157631667005739/">2012 Valencia
                                       Alumni Idea Exchange &amp; Volunteer Appreciation Reception </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>August 10, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157631667086350/">2012 Association
                                       of Honors Alumni-AHA! 3rd Annual Luau</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>June 5, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/myvalencia/sets/72157630004259195/">2012 Alumni Achievers
                                       Reception </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>May 5, 2012</td>
                                 
                                 <td>
                                    <a href="http://www.flickr.com/photos/myvalencia/sets/72157629952828548/">2012 Honors Reception </a>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>April 10, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/myvalencia/sets/72157629794750859/">Job Fair 2012</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 31, 2012</td>
                                 
                                 <td><a href="http://www.flickr.com/photos/myvalencia/sets/72157629731452311/">2012 Valencia 7th Annual
                                       5K </a></td>
                                 
                              </tr>
                              
                              
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               
               
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/foundation/alumni/photo-galleries.pcf">©</a>
      </div>
   </body>
</html>