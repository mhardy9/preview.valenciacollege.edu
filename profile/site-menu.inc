<!-- Site Header================================================== -->
<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo"><a href="/index.php"><img src="/img/logo-vc.png" alt="Valencia College Logo" width="265" height="40" data-retina="true" /></a></div>
</div>
<nav class="col-md-9 col-sm-9 col-xs-9" aria-label="Subsite Navigation" role="navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
<div class="site-menu">
<div id="site_menu"><img src="/img/logo-vc.png" alt="Valencia College" width="265" height="40" data-retina="true" /></div>
<a id="close_in" class="open_close" href="#"> </a>
<ul class="mobile-only">
<li><a href="https://preview.valenciacollege.edu/search">Search</a></li>
<li class="submenu"><a class="show-submenu" href="javascript:void(0);">Login</a>
<ul>
<li><a href="http://atlas.valenciacollege.edu/">Atlas</a></li>
<li><a href="http://atlas.valenciacollege.edu/">Alumni Connect</a></li>
<li><a href="http://login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3">Office 365</a></li>
<li><a href="http://learn.valenciacollege.edu">Valencia &amp; Online</a></li>
<li><a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu">Webmail</a></li>
</ul>
</li>
<li><a class="show-submenu" href="javascript:void(0);">Top Menu</a>
<ul>
<li><a href="/STUDENTS/academics/current-students/index.php">Current Students</a></li>
<li><a href="/STUDENTS/academics/future-students/index.php">Future Students</a></li>
<li><a href="https://international.valenciacollege.edu">International Students</a></li>
<li><a href="/STUDENTS/academics/military-veterans/index.php">Military &amp; Veterans</a></li>
<li><a href="/STUDENTS/academics/continuing-education/index.php">Continuing Education</a></li>
<li><a href="/EMPLOYEES/faculty-staff.php">Faculty &amp; Staff</a></li>
<li><a href="/FOUNDATION/alumni/index.php">Alumni &amp; Foundation</a></li>
</ul>
</li>
</ul>
<ul>
<li><a href="/staff/">Valencia Employees</a></li>
<li class="submenu"><a class="show-submenu" href="javascript:void(0);">Departments</a>
<ul>
<li><a href="/staff/allied-health">Allied Health</a></li>
<li><a href="/staff/office-of-information-technology">Office of Information Technology</a></li>
</ul>
</li>
</ul>
</div>
<!-- End site-menu --></nav></div>
</div>
<!-- container --></div>
<!-- End Header Sub-nav  -->