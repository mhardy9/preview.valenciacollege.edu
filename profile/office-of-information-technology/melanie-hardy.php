<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Melanie Hardy | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/profile/office-of-information-technology/melanie-hardy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/staff/site-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_1">
         <div id="intro_txt">
            <h1>Melanie Hardy</h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/profile/">Staff</a></li>
               <li><a href="/profile/office-of-information-technology/">Office of Information Technology Department</a></li>
               <li>Melanie Hardy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container margin_60">
            <div class="row">
               <aside class="col-md-3">
                  <div class="profile">
                     <p class="text-center"><img src="/profile/office-of-information-technology/img/2012-09-06%2017.20.13.jpg" alt="Melanie Hardy" class="img-circle styled_2"></p>
                     <ul class="social_teacher">
                        <li><a href="https://www.facebook.com/melanie.e.hardy"><i class="fa fa-facebook-f"></i></a></li>
                        <li><a href="https://twitter.com/melanieehardy"><i class="fa fa-twitter"></i></a></li>
                     </ul>
                     <ul>
                        <li>Name <strong class="pull-right">Melanie Hardy</strong></li>
                        <li>Email <strong class="pull-right"><a href="mailto:mhardy9@valenciacollege.edu">mhardy9@valenciacollege.edu</a></strong></li>
                        <li>Telephone <strong class="pull-right">(407) 582-5579</strong></li>
                        <li>Type <strong class="pull-right">Web/Portal Developer</strong></li>
                        <li>Department <strong class="pull-right">IT Office</strong></li>
                        <li>Location <strong class="pull-right">West Campus 4-12 10-208</strong></li>
                     </ul>
                  </div>
               </aside>
               <div class="col-md-9">
                  <div class="box_style_1">
                     <div class="indent_title_in">
                        <h3>Profile</h3>
                     </div>
                     <div class="wrapper_indent">
                        <ul>
                           
                           <li>Web Developer with four years experience with the design and daily maintenance of
                              multiple eCommerce portals. Nine years experience in programming for Higher Education.
                           </li>
                           
                           <li>&nbsp;Programming background with experience in iHTML, PHP, ColdFusion, MS SQL, mySQL,
                              FTP &amp; XML in relation to creation, development and maintenance of eCommerce Portals,
                              Customer Reporting Portals, Internal Company Portals, WordPress Sites &amp; College Websites/Web
                              Applications.
                           </li>
                           
                           <li>Administrator for Elucian's Luminis Student Platform.</li>
                           
                           <li>Administrator for College's WordPress installations</li>
                           
                           <li>Administrator for College's Windows Web Servers.</li>
                           
                           <li>Integration/SSO expert for College's 3rd Party installations.</li>
                           
                           <li>Team leader and member on multiple Portal Design and Development Projects.</li>
                           
                           <li>Trained other programmers on development standards.</li>
                           
                           <li>System Support Technician with seventeen years experience in the daily maintenance
                              and monitoring of multiple company networks with a focus on Web based systems.
                           </li>
                           
                           <li>Management experience in directing system maintenance as well as user support using
                              a team of trained network technicians and help desk technicians
                           </li>
                           
                           <li>Oversaw the setup and maintenance of both server systems and workstations. (MS NT
                              Server – MS Server 2003 - Server 2012, MS Win 3.1 – MS Windows10)
                           </li>
                           
                           <li>Excellent communication, technical and interpersonal skills with both technical and
                              non-technical personnel and clients
                           </li>
                           
                           <li>Certified Scrum Master<br><br>Specialties: Development Tools : Java, Javascript, iHTML, PHP, ColdFusion, HTML5,
                              JQuery<br>Databases : Oracle, mySQL 4.x/5.x, MS SQL 2000/2005/2014<br>Web Tools : HTML, XML/XSLT, MS IIS, Apache, MS FTP, Cerberus FTP, <br>OS Expertise:&nbsp; MS Server 2008 - Server 2012, MS Win 3.1 – MS Windows10, Redhat Linux<br>Graphic Design Software: Adobe Photoshop, Adobe Go Live, Adobe Image Ready<br>Networking Experience: Firewall, Load Balancing, Database Clustering, RAID Array,
                              Switch/Router
                           </li>
                           
                        </ul>
                        <div class="row">
                           <div class="col-md-6">
                              <ul class="list_3">
                                 <li><strong>BS Computer &amp; Electrical Engineering</strong><p>
                                       								University of Florida
                                       							
                                    </p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <hr class="styled_2">
                        <div class="wrapper_indent">
                           <div class="table-responsive">
                              <h3>Relevant Links</h3>
                              
                              <ul>
                                 
                                 <li><a href="https://familywelch.com">Family Welch</a></li>
                                 
                                 <li><a href="https://melanie-hardy.com">Personal Website</a></li>
                                 
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/profile/office-of-information-technology/melanie-hardy.pcf">©</a>
      </div>
   </body>
</html>