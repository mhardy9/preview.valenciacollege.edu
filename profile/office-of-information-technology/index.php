<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content="OIT employs technology to create, communicate, and apply knowledge that facilitates learning and supports the mission and goals of Valencia College. | Valencia College">
      <title>Office of Information Technology Department | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/profile/office-of-information-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/staff/site-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-3" style="background-image: url('http://preview.valenciacollege.edu/_resources/images/header_bg-1_1600x800_east-library.jpg');">
         <div id="intro-txt">
            <h1>Office of Information Technology Department</h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/profile/">Staff</a></li>
               <li>Office of Information Technology Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container margin-60">
            <div class="main-title">
               <h2>OIT Employees</h2>
               <p></p>
            </div>
            <div>
               <div class="dataTables_wrapper">
                  <div class="dataTables_filter"><label>
                        								Filter by Type:&nbsp;
                        								<select id="employee-type">
                           <option value="">- Select -</option>
                           <option value="Professional">Professional</option></select></label></div>
               </div>
               <table id="profiles" class="table table-striped">
                  <thead>
                     <tr>
                        <th><span>NAME</span></th>
                        <th><span>DEPARTMENT</span></th>
                        <th><span>TITLE</span></th>
                        <th><span>TELEPHONE</span></th>
                        <th><span>LOCATION</span></th>
                        <th><span>MC</span></th>
                        <th><span>Type</span></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td><a href="/profile/office-of-information-technology/melanie-hardy.php">Melanie&nbsp;Hardy</a></td>
                        <td>IT Office</td>
                        <td>Web/Portal Developer</td>
                        <td><a href="tel:(407) 582-5579">(407) 582-5579</a></td>
                        <td>West Campus 4-12 10-208</td>
                        <td>4-12</td>
                        <td>Professional</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/profile/office-of-information-technology/index.pcf">©</a>
      </div>
      <link href="/_resources/css/jquery.dataTables.css" rel="stylesheet"><script src="/_resources/js/jquery.dataTables.js"></script><script>
				
				$.fn.dataTable.ext.search.push(
					function( settings, data, dataIndex ) {
						var employeeTypeFilter = $('#employee-type').val();
						var employeeType = data[6]; // use data for the employee-type column

						if (employeeTypeFilter == '' || employeeTypeFilter == employeeType){
							return true;
						}
						return false;
					}
				);
				
				$(document).ready(function() {
					/* Initialise datatables */
					var oTable = $('#profiles').dataTable();

					/* Add event listener to the dropdown input */
					$('select#employee-type').on('change', function(){
						oTable.fnDraw();
					});
				}); 
			</script></body>
</html>