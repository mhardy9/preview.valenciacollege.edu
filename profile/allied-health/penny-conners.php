<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Penny Conners | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/profile/allied-health/penny-conners.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/staff/site-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_1">
         <div id="intro_txt">
            <h1>Penny Conners</h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/profile/">Staff</a></li>
               <li><a href="/profile/allied-health/">Division of Allied Health</a></li>
               <li>Penny Conners</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container margin_60">
            <div class="row">
               <aside class="col-md-3">
                  <div class="profile">
                     <p class="text-center"><img src="/profile/allied-health/img/penny-headshot.jpg" alt="Penny Conners" class="img-circle styled_2"></p>
                     <ul class="social_teacher"></ul>
                     <ul>
                        <li>Name <strong class="pull-right">Penny Conners</strong></li>
                        <li>Email <strong class="pull-right"><a href="mailto:pconners1@valenciacollege.edu">pconners1@valenciacollege.edu</a></strong></li>
                        <li>Telephone <strong class="pull-right">(407) 582-5687</strong></li>
                        <li>Type <strong class="pull-right">Dean, Allied Health</strong></li>
                        <li>Department <strong class="pull-right">Allied Health Office</strong></li>
                        <li>Location <strong class="pull-right">West Campus 4-44 2-208</strong></li>
                     </ul>
                  </div>
               </aside>
               <div class="col-md-9">
                  <div class="box_style_1">
                     <div class="indent_title_in">
                        <h3>Profile</h3>
                     </div>
                     <div class="wrapper_indent">
                        <p>I started my career at Valencia in August 2010 as the Dean of Allied Health.&nbsp; I bring
                           over 30 years of experience as an educator and administrator in health sciences with
                           a specific background based in the field of radiology.&nbsp; Prior to my employment at
                           Valencia, I served as the Assistant Director of the Joint Review Committee on Education
                           in Radiologic Technology (JRCERT) in Chicago, IL.&nbsp; Prior to my career at the JRCERT,
                           I spent 25 years at Kaskaskia Community College in Centralia, IL in a variety of positions
                           including radiology professor/clinical coordinator, radiology program director, and
                           Dean of Career and Technical Education.
                        </p>
                        
                        <p style="margin-bottom: 0;">My Master’s Degree is in Educational Administration from Southern Illinois University
                           at Carbondale, IL with a specialization in Workforce, Education, and Training.
                        </p>
                        
                        <p style="margin-top: 0; margin-bottom: 0;">&nbsp;</p>
                        
                        <p style="margin-top: 0;">The Allied Health Division encompasses the programs of Cardiovascular Technology,
                           Dental Hygiene, Diagnostic Medical Sonography, Emergency Medical Technology, Paramedic
                           Technology, Radiography – including the Associate of Science and the Bachelor of Radiologic
                           and Imaging Sciences, Computed Tomography, Magnetic Resonance Imaging, Respiratory
                           Care Technology, and Polysomnography Technology.&nbsp; Allied health professionals consist
                           of a group of health care professionals with formal education and clinical training
                           who must go through the process of national certification and/or state licensure.&nbsp;
                           Allied health care professionals work closely with physicians and other members of
                           the health care team to deliver high quality patient care.&nbsp;
                        </p>
                        
                        <p>This Allied Health Division is comprised of 16 full-time faculty members, over 80
                           adjunct faculty members, several career staff professionals, and 2 administrative
                           assistants.&nbsp; This division is committed to serving the needs of the local service
                           community and providing quality educational services.&nbsp;
                        </p>
                        <div class="row">
                           <div class="col-md-6">
                              <ul class="list_3">
                                 <li><strong>M Educational Administration</strong><p>
                                       								Southern Illinois University at Carbondale
                                       							
                                    </p>
                                 </li>
                                 <li><strong>&nbsp;</strong><p>
                                       								&nbsp;
                                       							
                                    </p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <hr class="styled_2">
                        <div class="wrapper_indent">
                           <div class="table-responsive"></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/profile/allied-health/penny-conners.pcf">©</a>
      </div>
   </body>
</html>