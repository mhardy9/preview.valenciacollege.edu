<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sarah Powers | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.min.css">
      <link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.validation.min.css"><script>
					var page_url="https://preview.valenciacollege.edu/profile/allied-health/sarah-powers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/staff/site-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_1">
         <div id="intro_txt">
            <h1>Sarah Powers</h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/profile/">Staff</a></li>
               <li><a href="/profile/allied-health/">Division of Allied Health</a></li>
               <li>Sarah Powers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container margin_60">
            <div class="row">
               <aside class="col-md-3">
                  <div class="profile">
                     <p class="text-center"><img src="/_resources/images/no-photo-female-thumb.png" alt="Sarah Powers" class="img-circle styled_2"></p>
                     <ul class="social_teacher"></ul>
                     <ul>
                        <li>Name <strong class="pull-right">Sarah Powers</strong></li>
                        <li>Email <strong class="pull-right"><a href="mailto:spowers2@valenciacollege.edu">spowers2@valenciacollege.edu</a></strong></li>
                        <li>Telephone <strong class="pull-right">(407) 582--1550</strong></li>
                        <li>Type <strong class="pull-right">Professor, Cardio Technology</strong></li>
                        <li>Department <strong class="pull-right">Cardiovascular Tech WEC</strong></li>
                        <li>Location <strong class="pull-right">West Campus 4-44 AHS 116</strong></li>
                        <li>Office Hours <strong class="pull-right">Monday: AHS 117 8am-9am Tuesday: Virtual: 9am-12pm  Wednesday:AHS 117 8am-9am Thursday:
                              AHS 117 12pm-2pm Friday: AHS 117 12pm-2pm </strong></li>
                     </ul>
                  </div>
               </aside>
               <div class="col-md-9">
                  <div class="box_style_1">
                     <div class="indent_title_in">
                        <h3>Profile</h3>
                     </div>
                     <div class="wrapper_indent">
                        <p>I am a graduate of Valencia college's Cardiovascular Technology program from the class
                           of 2005. I have both my AA and my AS degree from Valencia College. I taught as an
                           adjunct for the CVT program for 2 years following graduation. I am both certified
                           as an RCIS (Registered Cardiovascular Invasive Specialist) and an RCES (Registered
                           Cardiovascular Electrophysiology Specialist). I was one of the first chosen in the
                           country to sit for the RCES pilot examination and one of the first 50 to obtain the
                           certification. I graduated from the University of Central Florida with a Bachelors
                           of Science in Technical Education and Industrial Training. I am also a member of the
                           SICP (Society of Invasive Cardiovascular Professionals) and HRS (The Heart Rhythm
                           Society) <br> I have over 8 years of experience in the field of Cardiovascular Technology. I worked
                           for the Cardiac Electrophysiology department at Orlando Regional Medical center for
                           my first 5 years in the field. I also worked for 3 years as a field clinical engineer
                           for St. Jude Medical, an international corporation specializing in cardiac devices.
                           In this position I was fortunate to gain a wealth of experience working closely with
                           EP labs and physicians throughout the state of Florida. <br> I am very excited to be returning to the college that essentially helped me find
                           my focus in life and being able to work with and guide the future technologists in
                           our program.
                        </p>
                        		
                        		
                        <div class="row">
                           <div class="col-md-6">
                              <ul class="list_3">
                                 <li><strong>D E D Higher Ed Administration</strong><p>
                                       								University of Florida
                                       							
                                    </p>
                                 </li>
                                 <li><strong>M Ed Mathematics</strong><p>
                                       								Campbell University
                                       							
                                    </p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        	
                        <hr class="styled_2">
                        <div class="wrapper_indent">
                           <div class="table-responsive">
                              		
                              <h3 class="enlarge">Summer 2017 Courses</h3>
                              		
                              		
                              <table class="table table-striped add_bottom_30">
                                 <thead>
                                    <tr>
                                       <th>Discipline</th>
                                       <th>Course Name</th>
                                       <th>Course Number</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>Mathematics</td>
                                       <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201730&amp;subj_in=MAC&amp;crse_in=1114&amp;crn_in=30376">College Trigonometry</a></td>
                                       <td>MAC1114</td>
                                    </tr>
                                    <tr>
                                       <td>Mathematics</td>
                                       <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201730&amp;subj_in=STA&amp;crse_in=2023&amp;crn_in=32256">Statistical Methods</a></td>
                                       <td>STA2023</td>
                                    </tr>
                                 </tbody>
                              </table>
                              		
                              		
                              <h3>Syllabi</h3>
                              		
                              <ul>
                                 			
                                 <li><a href="/documents/PROFILES/dmatthews13/syllabus-MAC1114.docx">MAC1114</a></li>
                                 			
                                 <li><a href="/documents/PROFILES/dmatthews13/syllabus-STA2023.docx">STA2023</a></li>
                                 		
                              </ul>
                              		
                              <h3>Course Material</h3>
                              		
                              <ul>
                                 			
                                 <li><a href="/documents/PROFILES/dmatthews13/All-Matrix-Operations.pdf">All Matrix Operations</a></li>
                                 			
                                 <li><a href="/documents/PROFILES/dmatthews13/Determinants-3x3.pdf">Determinants 3x3</a></li>
                                 			
                                 <li><a href="/documents/PROFILES/dmatthews13/gaussian-elimination.pdf">Gaussian Elimination</a></li>
                                 		
                              </ul>
                              		
                              <h3>Relevant Links</h3>
                              		
                              <ul>
                                 			
                                 <li><a href="#">Math Tutoring</a></li>
                                 			
                                 <li><a href="#">Video on Gaussian Elimination</a></li>
                                 			
                                 <li><a href="#">Library</a></li>
                                 		
                              </ul>
                              	
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="ou-form container_gray_bg" id="newsletter_container">
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-8 col-md-offset-2 text-center">
                  <h3>Subscribe to our Newsletter for latest news.</h3>
                  <div id="message-newsletter">
                     <div id="status_74643119-339c-4781-be67-88d76c494736"></div>
                  </div>
                  <form id="form_74643119-339c-4781-be67-88d76c494736" name="newsletter" method="post" class="form-inline" autocomplete="off"><span class="hp74643119-339c-4781-be67-88d76c494736" style="display:none; margin-left:-1000px;"><label for="hp74643119-339c-4781-be67-88d76c494736" class="hp74643119-339c-4781-be67-88d76c494736">If you see this don't fill out this input box.</label><input type="text" id="hp74643119-339c-4781-be67-88d76c494736"></span><input type="hidden" name="form_uuid" value="74643119-339c-4781-be67-88d76c494736"><input type="hidden" name="site_name" value="www"><input type="hidden" name="pageurl" value="https://preview.valenciacollege.edu/profile/allied-health/sarah-powers.php"><input name="email" id="email" type="email" value="" placeholder="Your Email" class="form-control"><button type="submit" id="btn_74643119-339c-4781-be67-88d76c494736" class="button"> Subscribe</button></form>
               </div>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><script type="text/javascript" src="/_resources/ldp/forms/js/ou-forms.js"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/profile/allied-health/sarah-powers.pcf">©</a>
      </div>
   </body>
</html>