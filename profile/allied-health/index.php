<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Allied Health Department | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/profile/allied-health/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/staff/site-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-3" style="background-image: url('http://preview.valenciacollege.edu/_resources/images/header_bg-1_1600x800_osceola-3.jpg');">
         <div id="intro-txt">
            <h1>Allied Health Department</h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/profile/">Staff</a></li>
               <li>Division of Allied Health</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container margin-60">
            <div class="main-title">
               <h2>Allied Health Employees</h2>
               <p></p>
            </div>
            <div>
               <div class="dataTables_wrapper">
                  <div class="dataTables_filter"><label>
                        								Filter by Type:&nbsp;
                        								<select id="employee-type">
                           <option value="">- Select -</option>
                           <option value="Administrative">Administrative</option>
                           <option value="Faculty">Faculty</option></select></label></div>
               </div>
               <table id="profiles" class="table table-striped">
                  <thead>
                     <tr>
                        <th><span>NAME</span></th>
                        <th><span>DEPARTMENT</span></th>
                        <th><span>TITLE</span></th>
                        <th><span>TELEPHONE</span></th>
                        <th><span>LOCATION</span></th>
                        <th><span>MC</span></th>
                        <th><span>Type</span></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td><a href="/profile/allied-health/penny-conners.php">Penny&nbsp;Conners</a></td>
                        <td>Allied Health Office</td>
                        <td>Dean, Allied Health</td>
                        <td><a href="tel:(407) 582-5687">(407) 582-5687</a></td>
                        <td>West Campus 4-44 2-208</td>
                        <td>4-44</td>
                        <td>Administrative</td>
                     </tr>
                     <tr>
                        <td><a href="/profile/allied-health/sarah-powers.php">Sarah&nbsp;Powers</a></td>
                        <td>Cardiovascular Tech WEC</td>
                        <td>Professor, Cardio Technology</td>
                        <td><a href="tel:(407) 582--1550">(407) 582--1550</a></td>
                        <td>West Campus 4-44 AHS 116</td>
                        <td>4-44</td>
                        <td>Faculty</td>
                     </tr>
                     <tr>
                        <td><a href="/profile/allied-health/jamy-chulak.php">Jamy&nbsp;Chulak</a></td>
                        <td>Respiratory Therapy WEC</td>
                        <td>Professor, Respiratory Care</td>
                        <td><a href="tel:(407) 582-5477">(407) 582-5477</a></td>
                        <td>West Campus 4-44 AHS-235</td>
                        <td>4-44</td>
                        <td>Faculty</td>
                     </tr>
                     <tr>
                        <td><a href="/profile/allied-health/sharon-shenton.php">Sharon&nbsp;Shenton</a></td>
                        <td>Cardiopulmonary Sciences WEC</td>
                        <td>Professor, Cardiopulmonary Sci</td>
                        <td><a href="tel:(407) 582-5816">(407) 582-5816</a></td>
                        <td>West Campus 4-44 4-211</td>
                        <td>4-44</td>
                        <td>Faculty</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/profile/allied-health/index.pcf">©</a>
      </div>
      <link href="/_resources/css/jquery.dataTables.css" rel="stylesheet"><script src="/_resources/js/jquery.dataTables.js"></script><script>
				
				$.fn.dataTable.ext.search.push(
					function( settings, data, dataIndex ) {
						var employeeTypeFilter = $('#employee-type').val();
						var employeeType = data[6]; // use data for the employee-type column

						if (employeeTypeFilter == '' || employeeTypeFilter == employeeType){
							return true;
						}
						return false;
					}
				);
				
				$(document).ready(function() {
					/* Initialise datatables */
					var oTable = $('#profiles').dataTable();

					/* Add event listener to the dropdown input */
					$('select#employee-type').on('change', function(){
						oTable.fnDraw();
					});
				}); 
			</script></body>
</html>