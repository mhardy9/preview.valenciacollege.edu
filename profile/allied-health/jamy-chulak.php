<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Jamy Chulak | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.min.css">
      <link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.validation.min.css"><script>
					var page_url="https://preview.valenciacollege.edu/profile/allied-health/jamy-chulak.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/staff/site-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_1">
         <div id="intro_txt">
            <h1>Jamy Chulak</h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/profile/">Staff</a></li>
               <li><a href="/profile/allied-health/">Division of Allied Health</a></li>
               <li>Jamy Chulak</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container margin_60">
            <div class="row">
               <aside class="col-md-3">
                  <div class="profile">
                     <p class="text-center"><img src="/profile/allied-health/img/Jamy-chulak.jpg" alt="Jamy Chulak" class="img-circle styled_2"></p>
                     <ul class="social_teacher"></ul>
                     <ul>
                        <li>Name <strong class="pull-right">Jamy Chulak</strong></li>
                        <li>Email <strong class="pull-right"><a href="mailto:jchulak@valenciacollege.edu">jchulak@valenciacollege.edu</a></strong></li>
                        <li>Telephone <strong class="pull-right">(407) 582-5477</strong></li>
                        <li>Type <strong class="pull-right">Professor, Respiratory Care</strong></li>
                        <li>Department <strong class="pull-right">Respiratory Therapy WEC</strong></li>
                        <li>Location <strong class="pull-right">West Campus 4-44 AHS-235</strong></li>
                     </ul>
                  </div>
               </aside>
               <div class="col-md-9">
                  <div class="box_style_1">
                     <div class="indent_title_in">
                        <h3>Profile</h3>
                     </div>
                     <div class="wrapper_indent">
                        <p>I completed my AS degree in Respiratory Care at Tallahassee Community College and
                           later transferred to UCF where I obtained a BS in Cardiopulmonary Sciences. I obtained
                           an MS Degree in Respiratory Care Leadership at Northeastern University in Boston,
                           MA. <br> <br> I enjoyed working at a pediatric home health care company in central Florida specializing
                           in the care of premature infants and education. I continued my profession at Orlando
                           Health’s level I trauma center. Orlando Regional Medical Center offered training in
                           all areas of critical care including floor care, intensive care, surgical care, and
                           emergency airway management in a dynamic setting. <br> <br> I began teaching at Seminole State College as an adjunct professor and joined Valencia
                           College in January 2008 as the Director of Clinical Education (Clinical Coordinator).
                           In August of 2009 I assumed the role as Program Director (Program Chair) and have
                           enjoyed working with the students, faculty, and leadership here at Valencia College.
                           It’s truly “a better place to learn”.
                        </p>
                        <div class="row">
                           <div class="col-md-6">
                              <ul class="list_3">
                                 <li><strong>M S Respiratory Care Leadership</strong><p>
                                       								Northeastern University
                                       							
                                    </p>
                                 </li>
                                 <li><strong>B S Cardiopulmonary Science</strong><p>
                                       								University of Central Florida
                                       							
                                    </p>
                                 </li>
                                 <li><strong>A A General Studies</strong><p>
                                       								Tallahassee Community College
                                       							
                                    </p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <hr class="styled_2">
                        <div class="wrapper_indent">
                           <div class="table-responsive">
                              <h3 class="enlarge">Fall 2017 Courses</h3>
                              
                              <table class="table table-striped add_bottom_30">
                                 <thead>
                                    <tr>
                                       <th>Discipline</th>
                                       <th>Course Name</th>
                                       <th>Course Number</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>Respiratory Care</td>
                                       <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=RET&amp;crse_in=1264C&amp;crn_in=17288">Principles of Mech Ventilation </a></td>
                                       <td>RET1264C</td>
                                    </tr>
                                 </tbody>
                              </table>
                              
                              <h3>Syllabi</h3>
                              
                              <ul>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/RET%201295C%20Syllabus-2016.pdf">RET 1295C Chest Medicine</a></li>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/RET%201450C%20Syllabus-2016.pdf">RET 1450C Basic Physiologic Monitoring</a></li>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/RET%201264C%20Syllabus-2016.pdf">RET 1264C Principles of Mechanical Ventilation</a></li>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/Leadership%20in%20Healthcare.pdf">HSA 4184 Leadership in Healthcare</a></li>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/RTE%203205%20Administration%20and%20Supervision.pdf">RTE 3205 Administration and Supervision</a></li>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/Principles%20of%20Respiratory%20Care-%20Syllabus%202017.pdf">RET 1025C Principles of Respiratory Care</a></li>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/Cardiopulmonary%20Physiology-%20Syllabus%202017.pdf">RET 1485C Cardiopulmonary Anatomy &amp; Physiology</a></li>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/Medical%20Terminology%20Syllabus.pdf">HSC 1531 Medical Terminology</a></li>
                                 
                                 <li class="materialsList"><a href="/profile/allied-health/documents/jchulak/RET%202350C%20Syllabus-2016.pdf">Respiratory Pharmacology</a></li>
                                 
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="ou-form container_gray_bg" id="newsletter_container">
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-8 col-md-offset-2 text-center">
                  <h3>Subscribe to our Newsletter for latest news.</h3>
                  <div id="message-newsletter">
                     <div id="status_74643119-339c-4781-be67-88d76c494736"></div>
                  </div>
                  <form id="form_74643119-339c-4781-be67-88d76c494736" name="newsletter" method="post" class="form-inline" autocomplete="off"><span class="hp74643119-339c-4781-be67-88d76c494736" style="display:none; margin-left:-1000px;"><label for="hp74643119-339c-4781-be67-88d76c494736" class="hp74643119-339c-4781-be67-88d76c494736">If you see this don't fill out this input box.</label><input type="text" id="hp74643119-339c-4781-be67-88d76c494736"></span><input type="hidden" name="form_uuid" value="74643119-339c-4781-be67-88d76c494736"><input type="hidden" name="site_name" value="www"><input type="hidden" name="pageurl" value="https://preview.valenciacollege.edu/profile/allied-health/jamy-chulak.php"><input name="email" id="email" type="email" value="" placeholder="Your Email" class="form-control"><button type="submit" id="btn_74643119-339c-4781-be67-88d76c494736" class="button"> Subscribe</button></form>
               </div>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><script type="text/javascript" src="/_resources/ldp/forms/js/ou-forms.js"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/profile/allied-health/jamy-chulak.pcf">©</a>
      </div>
   </body>
</html>