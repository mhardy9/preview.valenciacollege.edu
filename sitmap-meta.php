<?php
$urls = array();  
set_time_limit(1200);
$DomDocument = new DOMDocument();
$DomDocument->preserveWhiteSpace = false;
$DomDocument->load('sitemap.xml');
$DomNodeList = $DomDocument->getElementsByTagName('loc');

foreach($DomNodeList as $url) if (strpos($url->nodeValue, 'valenciacollege.edu/students/') == true && strpos($url->nodeValue, '.php') == true && strpos($url->nodeValue, '/blog/') == false && strpos($url->nodeValue, '/profile/') == false && strpos($url->nodeValue, '/_resources/') == false && strpos($url->nodeValue, '/blog/') == false && strpos($url->nodeValue, '/_staging/') == false && strpos($url->nodeValue, '/secure/') == false) 
{
    $urls[] = $url->nodeValue;
}

//display it
   $map = '';
   $section = '';

sort($urls);  
$map .= '<table class="table">';
//<tr><th>URL</th><th>Page Title</th><th>Description</th><th>Keywords</th><th>H1</th><th>H2</th></tr>
foreach ($urls as $url)
{
	if ( $url ) {
		//echo $url."<br>";
	$map .='<tr>';
   $contents = file_get_contents($url);

   

   //find the Page title
    preg_match('/(?<=\<[Tt][Ii][Tt][Ll][Ee]\>)\s*?(.*?)\s*?(?=\<\/[Tt][Ii][Tt][Ll][Ee]\>)/U', $contents, $title);
    $title = $title[0];
	
	   // find description
   preg_match('/(?<=\<[Mm][Ee][Tt][Aa]\s[Nn][Aa][Mm][Ee]\=\"Description\" content\=\")(.*?)(?="\s*?\/?\>)/U', $contents, $description);
   $description = $description[0];
   
   	   // find keywords
   preg_match('/(?<=\<[Mm][Ee][Tt][Aa]\s[Nn][Aa][Mm][Ee]\=\"Keywords\" content\=\")(.*?)(?="\s*?\/?\>)/U', $contents, $keywords);
   $keywords = $keywords[0];
	
	// find the first h1 tag
   $header = array();
   preg_match('/(?<=\<[Hh]1\>)(.*?)(?=\<\/[Hh]1\>)/U', $contents, $header);
   $header = strip_tags($header[0]);
   
   // find the first h2 tag
   $header2 = array();
   preg_match('/(?<=\<[Hh]2\>)(.*?)(?=\<\/[Hh]2\>)/U', $contents, $header2);
   $header2 = strip_tags($header2[0]);
   
   
   //td for URL
   if ( strlen($url) > 0 ) {
	  $map .= '<td><a href="'.$url.'">'.str_replace("https://preview.valenciacollege.edu","",$url).'</a></td>'."\n"; 
   }
   else {$map .= '<td>&nbsp;</td>';}
   //td for Page Title
	if ( strlen($title) > 0 ) {
	  $map .= '<td>'.$title.'</td>'."\n"; 
   }
   else {$map .= '<td>&nbsp;</td>';}
      //td for Description
   	if ( strlen($description) > 0 ) {
	  $map .= '<td>'.$description.'</td>'."\n"; 
   }
   else {$map .= '<td>&nbsp;</td>';}
   //td for Keywords
    if ( strlen($keywords) > 0 ) {
	  $map .= '<td>'.$keywords.'</td>'."\n"; 
   }
   else {$map .= '<td>&nbsp;</td>';}
   //td for H1
       if ( strlen($header) > 0 ) {
	  $map .= '<td>'.$header.'</td>'."\n"; 
   }
   else {$map .= '<td>&nbsp;</td>';}
   //td for H2
       if ( strlen($header2) > 0 ) {
	  $map .= '<td>'.$header2.'</td>'."\n"; 
   }
   else {$map .= '<td>&nbsp;</td>';}
 }
}
$map .="</tr></table>";
	 
// write output to a file
// $fp = fopen('sitemap.html', "w+");
// fwrite($fp,$map);
// fclose($fp);
 
// print output
echo $map;

?>