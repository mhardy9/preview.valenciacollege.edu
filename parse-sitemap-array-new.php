<?php
$urls = array();  

$DomDocument = new DOMDocument();
$DomDocument->preserveWhiteSpace = false;
$DomDocument->load('sitemap-small.xml');
$DomNodeList = $DomDocument->getElementsByTagName('loc');

//Setup variables
	   $map = '';
	   $pagetitle ='';
	   $urlin='';
   $students = array();
   
//Create Section Info Array
	$sections = array(
		array ('About','/_staging/_resources/img/prestige/bg-aboutus.jpg','About Valencia','about'),
		array ('Academics','/_resources/img/header_bg_1_1600x800_osceola-2.jpg','Academics at Valencia','academics'),
		array ('Admissions','/_resources/img/prestige/bg-academics-osceola.jpg','Admissions at Valencia','admissions'),
		array ('Employees','/_resources/img/header_bg_1_1600x800_osceola-2.jpg','Valencia Employees','employees'),
		array ('Faculty','/_resources/img/header_bg_1_1600x800_osceola-2.jpg','Valencia Faculty','faculty'),
		array ('Finaid', '/_resources/img/header_bg_1_1600x800_osceola-2.jpg','Finanical Aid at Valencia','finaid'),
		array ('Foundation','/_resources/img/header_bg_1_1600x800_osceola-2.jpg','Valencia Foundation','foundation'),
		array ('Locations','/_resources/img/header_bg_1_1600x800_osceola-2.jpg','Valencia Locations','locations'),
		array ('Students','/_resources/img/header_bg_1_1600x800_osceola-2.jpg','Valencia Students','students')
	); 

//Declare Arrays for each major section

	foreach($sections as $key => $section) {
	$sectiontitle = $sections[$key][1];
	echo $sectiontitle.'<br/>';
	${$sectiontitle} = array();		
	if (isset($about)){echo "About Set <br>";}
	if (isset($academics)){echo "Academics Set <br>";}
	if (isset($admissions)){echo "Admissions Set <br>";}
	if (isset($employees)){echo "Employees Set <br>";}
	if (isset($faculty)){echo "Faculty Set <br>";}
	if (isset($finaid)){echo "Finaid Set <br>";}
	if (isset($foundation)){echo "Foundation Set <br>";}
	if (isset($locations)){echo "Locations Set <br>";}
	if (isset($students)){echo "Students Set <br>";}
	}

   // $about = array();
   // $academics = array();
   // $admissions = array();
   // $employees = array();
   // $faculty = array();
   // $finaid = array();
   // $foundation = array();
  // $locations = array();
  
  
exit;
//Create array for URLs from SiteMap that are needed   
foreach($DomNodeList as $url) if (strpos($url->nodeValue, 'index.php') == true & strpos($url->nodeValue, 'valenciacollege.edu/static-index.php') == false && strpos($url->nodeValue, '/blog/') == false && strpos($url->nodeValue, '/profile/') == false && strpos($url->nodeValue, '/_resources/') == false && strpos($url->nodeValue, '/blog/') == false && strpos($url->nodeValue, '/_staging/') == false && strpos($url->nodeValue, '/secure/') == false) 
{
    $urls[] = $url->nodeValue;
}
   
   
//Functions
function get_http_response_code($urlin) {
			  $headers = get_headers($urlin);
			  return substr($headers[0], 9, 3);
			}			
function get_page_headings($urlin) 
	{
	$contents = file_get_contents($urlin);	
	// find the first h1 tag
		$heading1 = array();
		preg_match('/(?<=\<[Hh]1\>)(.*?)(?=\<\/[Hh]1\>)/U', $contents, $heading1);
		if (count($heading1) > 0){$heading1 = strip_tags($heading1[0]);}
		else {$heading1 = '';}
	// find the first h2 tag
		$heading2 = array();
		preg_match('/(?<=\<[Hh]2\>)(.*?)(?=\<\/[Hh]2\>)/U', $contents, $heading2);
		if (count($heading2) > 0){$heading2 = strip_tags($heading2[0]);}
		else {$heading2 = '';}
		return $pagetitle = trim($heading1." - ".$heading2);
		
	}	
//Parse XML SiteMap
	sort($urls);

foreach ($urls as $url)
{
	if ( $url ) 
	{
		//Analyze URL to get major section
		
		$urlParts = explode('/', str_ireplace(array('http://', 'https://'), '', $url));
		$subfolder = $urlParts[1];
				
		//Create array with [url][H1 - H2] for each major folder	
		switch ($subfolder)
		{
			case "about":	
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) 
				{
					$get_page_headings = get_page_headings($url);			
					//Insert into Array
					$aboutdata = array('pagetitle' => $get_page_headings, 'url' => $url);
					array_push($about, $aboutdata);
					sort($about);
					break;					
				}
			case "academics":
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) {$get_page_headings = get_page_headings($url);}
				//Insert into Array
				$academicsdata = array('pagetitle' => $get_page_headings, 'url' => $url);
				array_push($academics, $academicsdata);
				sort($academics);
				break;					
			
			case "admissions";
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) {$get_page_headings = get_page_headings($url);}
				//Insert into Array
				$admissionsdata = array('pagetitle' => $get_page_headings, 'url' => $url);
				array_push($admissions, $admissionsdata);
				sort($admissions);
				break;
			
			case "employees";
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) {$get_page_headings = get_page_headings($url);}
				//Insert into Array
				$employeesdata = array('pagetitle' => $get_page_headings, 'url' => $url);
				array_push($employees, $employeesdata);
				sort($employees);
				break;				
			
			case "faculty";
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) {$get_page_headings = get_page_headings($url);}
				//Insert into Array
				$facultydata = array('pagetitle' => $get_page_headings, 'url' => $url);
				array_push($faculty, $facultydata);
				sort($faculty);
				break;
				
			case "finaid";
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) {$get_page_headings = get_page_headings($url);}
				//Insert into Array
				$finaiddata = array('pagetitle' => $get_page_headings, 'url' => $url);
				array_push($finaid, $finaiddata);
				sort($finaid);
				break;
			
			case "foundation";
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) {$get_page_headings = get_page_headings($url);}
				//Insert into Array
				$foundationdata = array('pagetitle' => $get_page_headings, 'url' => $url);
				array_push($foundation, $foundationdata);
				sort($foundation);
				break;
			
			case "locations";
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) {$get_page_headings = get_page_headings($url);}
				//Insert into Array
				$locationsdata = array('pagetitle' => $get_page_headings, 'url' => $url);
				array_push($locations, $locationsdata);
				sort($locations);
				break;
			
			case "students";		
				//Check to see if file is returned
				$get_http_response_code = get_http_response_code($url);
				//Get Page Headings
				if ( $get_http_response_code == 200 ) {$get_page_headings = get_page_headings($url);}
				//Insert into Array
				$studentsdata = array('pagetitle' => $get_page_headings, 'url' => $url);
				array_push($students, $studentsdata);
				sort($students);
				break;
		}
	}
	
}	

		
//Create Cards for each Section
	//About
	$map .= '
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_staging/_resources/img/prestige/bg-aboutus.jpg" alt="About Valencia">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>About</h3>
					</div>
					<div class="list-group">';	
				
		//Loop thru About Array & set up List Group
	foreach ($about as $aboutrow)
		{
			if ( strlen($aboutrow['pagetitle']) > 0 ) {
			 $map .= '<a  class="list-group-item list-group-item-action" href="'.$aboutrow['url'].'" title="'.$aboutrow['pagetitle'].'">'.$aboutrow['pagetitle'].'</a>'."\n";
			} 
			else{
			 $map .= '<div  class="list-group-item list-group-item-danger">Page Not Found - '.$aboutrow['url'].'</div>'."\n";
			};			
		}
	$map .= '</div>
				</span>
					</div>			
						</div>';
					 
//write output to a file
$fp = fopen('sitemap.html', "w+");
fwrite($fp,$map);
fclose($fp);
 
//print output
echo $map;
echo "<pre>";
print_r($academics);
echo "</pre>";
?>