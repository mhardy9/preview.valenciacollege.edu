<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Locations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/map/public-safety.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/map/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Locations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/map/">Map</a></li>
               <li>Locations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="https://preview.valenciacollege.edu/lynx/?utm_source=LOCATION&amp;utm_medium=BANNER&amp;utm_content=LYNX-Bus-Pass&amp;utm_term=17COM004&amp;utm_campaign=LYNX"><img alt="Valencia College and LYNX parner for free bus fare." height="260" src="../../locations/map/17COM004-LYNX-free-fares-770x260.jpg" width="770"></a>
                        
                        
                        
                        
                        
                        <div>
                           
                           <p><a href="../../locations/public-safety/index.html">Website</a></p>
                           
                           <p><a href="../../locations/map/map-public-safety.html" target="_blank"><strong>Interactive Map</strong></a></p>
                           
                           <p><a href="http://www.myatlascms.com/map/print/?id=614&amp;lat=28.550750999999998&amp;lng=-81.26933300000007&amp;zoom=17&amp;catIds=" target="_blank">Print Map</a></p>
                           
                        </div>
                        
                        
                        <h2>School of Public Safety</h2>
                        
                        <p>8600 Valencia College Lane, Orlando, FL 32825</p>
                        
                        <p>407-582-8200 </p>
                        
                        <p><strong>Fire Training Facility</strong></p>
                        
                        <p> 2966 West Oak Ridge Road, Orlando, FL 32809</p>
                        
                        <p><a href="https://www.google.com/maps/dir//2966+W+Oak+Ridge+Rd,+Orlando,+FL+32809/%4028.471642,-81.4182878,17z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x88e77c17f97b31df%3A0x7329bc5104934351!2m2!1d-81.4160991!2d28.471642" target="_blank">Driving Directions</a></p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="600" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14018.474244501698!2d-81.269009!3d28.5511827!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x92bf39fec32e7daa!2sValencia+College%2C+School+of+Public+Safety!5e0!3m2!1sen!2sus!4v1477513770908" width="772"></iframe>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/map/public-safety.pcf">©</a>
      </div>
   </body>
</html>