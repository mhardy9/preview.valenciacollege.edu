<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Interactive Map and Locations - Valencia College - Orlando, Florida | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/map/map-winter-park.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/map/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/map/">Map</a></li>
               <li>Interactive Map and Locations - Valencia College - Orlando, Florida</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="header" style="background-color:#fff;">
                  
                  
                  
                  <div class="container-fluid" style="background-color:#f3f3f3;">
                     
                     <div id="AtlasLoginMasterContainer" style="float:right;">
                        
                        <div id="HeaderTextLinks">
                           <a href="http://valenciacollege.edu/news/">News</a> &nbsp;&nbsp; 
                           <a href="http://valenciacollege.edu/events/">Events</a> &nbsp;&nbsp; 
                           <a href="http://valencia.org/">Giving</a> &nbsp;&nbsp; 
                           <a href="../../locations/map/index.html">Locations</a> &nbsp;&nbsp; 
                           <a href="http://valenciacollege.edu/visit/">Visit</a> &nbsp;&nbsp; 
                           <a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.p_disploginnew?in_id=&amp;cpbl=&amp;newid=">APPLY NOW</a> &nbsp;&nbsp; | &nbsp;&nbsp; 
                           <a href="https://atlas.valenciacollege.edu/" title="Atlas Login" style="text-decoration:none;"><span class="AtlasIcon"></span> <span class="AtlasText"><span class="kern-2">A</span>T<span class="kern-1">LAS</span>&nbsp;<span class="kern-2">L</span><span class="kern-1">OGIN</span></span></a>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  
                  <div class="container-fluid">
                     
                     <div class="col-xs-3">
                        
                        <div id="logo">    
                           
                           
                           <a href="../../locations/map/index.html"><img src="http://valenciacollege.edu/images/logos/valencia-college-logo.svg" alt="Valencia College" width="275" height="42" aria-label="Valencia College"></a>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               <h1 style="position: absolute; top: 30px; left:380px;">Winter Park Campus Interactive Map</h1>
               
               
               <div id="wrapper">
                  <iframe id="frame" src="http://myatlascms.com/map/?id=614#!mc/28.59687800000002,-81.36031300000002?z/19" width="100%" frameborder="0" scrolling="no" border="0" style="border:0px solid #fff; margin:0; padding:0;">
                     <p>Your browser does not support iframes.</p></iframe>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/map/map-winter-park.pcf">©</a>
      </div>
   </body>
</html>