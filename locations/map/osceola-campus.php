<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Locations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/map/osceola-campus.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/map/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Locations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/map/">Map</a></li>
               <li>Locations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="https://preview.valenciacollege.edu/lynx/?utm_source=LOCATION&amp;utm_medium=BANNER&amp;utm_content=LYNX-Bus-Pass&amp;utm_term=17COM004&amp;utm_campaign=LYNX"><img alt="Valencia College and LYNX parner for free bus fare." height="260" src="../../locations/map/17COM004-LYNX-free-fares-770x260.jpg" width="770"></a>
                        
                        
                        
                        
                        <div>
                           
                           <p><a href="../../locations/osceola/index.html">Campus Website</a></p>
                           
                           <p><a href="../../locations/map/map-osceola.html" target="_blank"><strong>Interactive Map</strong></a></p>
                           
                           <p><a href="../../locations/map/documents/Valencia-College-Campus-Map-Osceola.pdf" target="_blank">Print Version of Campus Map</a></p>
                           
                           <p><a href="http://www.myatlascms.com/map/print/?id=614&amp;lat=28.306625000000007&amp;lng=-81.38220999999999&amp;zoom=17&amp;catIds=" target="_blank">Print Map</a></p>
                           
                           <p><a href="http://www.youvisit.com/tour/valencia/osceola" target="_blank"><strong>Virtual Tour</strong></a></p>
                           
                        </div>
                        
                        
                        <h2>Osceola Campus</h2>
                        
                        <p>1800 Denn John Lane, Kissimmee, FL 34744</p>
                        
                        <p><br>
                           407-299-5000 
                        </p>
                        
                        
                        <iframe allowfullscreen="" frameborder="0" height="600" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14050.82363332247!2d-81.3813716!3d28.3072316!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xaeae935fd3c6a4f5!2sOsceola+Campus+-+Valencia+College!5e0!3m2!1sen!2sus!4v1477513627525" width="772"></iframe>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/map/osceola-campus.pcf">©</a>
      </div>
   </body>
</html>