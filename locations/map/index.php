<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Locations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/map/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/map/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Locations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li>Map</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="https://preview.valenciacollege.edu/lynx/?utm_source=LOCATION&amp;utm_medium=BANNER&amp;utm_content=LYNX-Bus-Pass&amp;utm_term=17COM004&amp;utm_campaign=LYNX"><img alt="Valencia College and LYNX parner for free bus fare." height="260" src="../../locations/map/17COM004-LYNX-free-fares-770x260.jpg" width="770"></a>
                        
                        
                        <h2>All Campus Locations</h2>
                        
                        
                        <h3>Mailing Address</h3>
                        
                        <p><strong>Valencia College</strong><br>
                           P.O. Box 3028, Orlando, FL 32802-3028
                        </p>
                        
                        <p>407-299-5000</p>
                        
                        
                        <h3>District Office</h3>
                        
                        <p><img alt="District Office Building" height="150" src="../../locations/map/district-office.jpg" title="District Office" width="148">Park Place at MetroWest<br>
                           1768 Park Center Drive, Orlando, FL 32835
                        </p>
                        
                        <p><a href="../../locations/map/district-office.html">Driving Directions</a></p>
                        
                        <p>407-299-5000</p>
                        
                        
                        <h3>Advanced Manufacturing Training Center</h3>
                        
                        <p><img alt="Advanced Manufacturing Training Center Building" height="150" src="../../locations/map/advanced-manufacturing-training-center.jpg" title="Advanced Manufacturing Training Center" width="150">1099 Shady Lane, Kissimmee, FL 34744
                        </p>
                        
                        <p><a href="https://www.google.com/maps/dir//1099+Shady+Ln,+Kissimmee,+FL+34744/%4028.279814,-81.3583997,17z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x88dd85937ee44893%3A0x1e1621063a802e35!2m2!1d-81.356211!2d28.279814" target="_blank">Driving Directions</a></p>
                        
                        <p> 407-582-8227</p>
                        
                        <ul>
                           
                           <li><a href="https://manufacturing.valenciacollege.edu/about/locations/our-training-center/" target="_blank">Training Center Website</a></li>
                           
                           <li><a href="../../locations/map/map-manufacturing.html" target="_blank">Interactive Map</a></li>
                           
                           <li><a href="http://myatlascms.com/map/print/?id=614&amp;lat=28.280169000000004&amp;lng=-81.35927600000008&amp;zoom=15&amp;catIds=13882" target="_blank">Print Map</a></li>
                           
                        </ul>
                        
                        
                        <h3>Downtown Campus</h3>
                        
                        <p><img alt="downtown campus rendering" height="150" src="../../locations/map/downtown-campus-rendering-150x150.jpg" title="downtown campus" width="150">500 W Livingston St, Orlando, FL 32801
                        </p>
                        
                        <p><a href="http://maps.google.com/maps?daddr=500+W+Livingston+St,Orlando,Florida" target="_blank">Driving Directions</a></p>
                        
                        <p>407-299-5000</p>
                        
                        <ul>
                           
                           <li>
                              <a href="https://preview.valenciacollege.edu/downtown-campus/">Campus Website</a><br>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Downtown Campus</strong> Opening for classes
                           Fall 2019
                        </p>
                        
                        
                        <h3>East Campus</h3>
                        
                        <p> <a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank"><img alt="East Campus Virtual Tour" border="0" height="150" src="../../locations/map/youvisit-east-245px.jpg" title="East Campus" width="150"></a>701 N Econlockhatchee Trail, Orlando, FL 32825
                        </p>
                        
                        <p><a href="../../locations/map/east.html">Driving Directions</a></p>
                        
                        <p>407-299-5000</p>
                        
                        <ul>
                           
                           <li><a href="../../locations/east/index.html">Campus Website</a></li>
                           
                           <li><a href="../../locations/map/map-east.html" target="_blank">Interactive Map</a></li>
                           
                           <li><a href="../../locations/map/documents/valencia-college-campus-map-east.pdf" target="_blank">Print Version of Campus Map</a></li>
                           
                           <li><a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank">Virtual Tour</a></li>
                           
                        </ul>
                        
                        
                        <h3>Lake Nona Campus</h3>
                        
                        <p><img alt="Lake Nona Campus Building" height="150" src="../../locations/map/locations_lakeNona.jpg" title="Lake Nona Campus" width="150">12350 Narcoossee Road, Orlando, FL 32832
                        </p>
                        
                        <p><a href="../../locations/map/lake-nona.html">Driving Directions</a></p>
                        
                        <p>407-582-7100</p>
                        
                        <ul>
                           
                           <li><a href="../../locations/lake-nona/index.html">Campus Website</a></li>
                           
                           <li><a href="../../locations/map/map-lake-nona.html" target="_blank">Interactive Map</a></li>
                           
                           <li><a href="http://www.myatlascms.com/map/print/?id=614&amp;lat=28.384766000000006&amp;lng=-81.24618499999997&amp;zoom=18&amp;catIds=" target="_blank">Print Map</a></li>
                           
                        </ul>
                        
                        
                        <h3><strong>Osceola Campus</strong></h3>
                        
                        <p><a href="http://www.youvisit.com/tour/valencia/osceola?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank"><img alt="Osceola Campus Virtual Tour" border="0" height="150" src="../../locations/map/youvisit-osceola-245px.jpg" title="Osceola Campus" width="150"></a>1800 Denn John Lane, Kissimmee, FL 34744
                        </p>
                        
                        <p><a href="../../locations/map/osceola.html">Driving Directions</a></p>
                        
                        <p>407-299-5000<br>
                           407-994-4191 - Local in Osceola County
                        </p>
                        
                        <ul>
                           
                           <li><a href="../../locations/osceola/index.html">Campus Website</a></li>
                           
                           <li><a href="../../locations/map/map-osceola.html" target="_blank">Interactive Map</a></li>
                           
                           <li><a href="../../locations/map/documents/valencia-college-campus-map-osceola.pdf" target="_blank">Print Version of Campus Map</a></li>
                           
                           <li><a href="http://www.youvisit.com/tour/valencia/osceola?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank">Virtual Tour</a></li>
                           
                        </ul>
                        
                        
                        <h3>Poinciana Campus</h3>
                        
                        <p> <a href="../../locations/map/map-poinciana.html" target="_blank"><img alt="Poinciana Campus Rendering" height="150" src="../../locations/map/locations_Poinciana.png" title="Poinciana Campus" width="150"></a>3255 Pleasant Hill Road, 
                           Kissimmee, Florida 34746
                        </p>
                        
                        <p> <a href="https://goo.gl/maps/kBMpzS5iyXN2">Driving Directions</a></p>
                        
                        <ul>
                           
                           <li><a href="../../locations/poinciana/index.html">Campus Website</a></li>
                           
                           <li><a href="../../locations/map/map-poinciana.html" target="_blank">Interactive Map </a></li>
                           
                           <li><a href="../../locations/map/documents/valencia-college-campus-map-poinciana.pdf" target="_blank">Print Version of Campus Map</a></li>
                           
                           
                        </ul>
                        
                        <p>Poinciana Campus Opening for classes
                           Fall 2017
                        </p>
                        
                        
                        <h3>School of Public Safety</h3>
                        
                        <p><img alt="SPS Campus Building" height="150" src="../../locations/map/locations_cji.jpg" title="SPS Campus" width="148">8600 Valencia College Lane, Orlando, FL 32825
                        </p>
                        
                        <p><a href="../../locations/map/public-safety.html">Driving Directions</a></p>
                        
                        <p>407-582-8200</p>
                        
                        <ul>
                           
                           <li><a href="../../locations/public-safety/index.html">Campus Website</a></li>
                           
                           <li><a href="../../locations/map/map-public-safety.html" target="_blank">Interactive Map</a></li>
                           
                           <li><a href="http://www.myatlascms.com/map/print/?id=614&amp;lat=28.550750999999998&amp;lng=-81.26933300000007&amp;zoom=17&amp;catIds=" target="_blank">Print Map</a></li>
                           
                        </ul>
                        
                        
                        <h3>Fire Rescue Training Facility</h3>
                        
                        <p>2966 West Oak Ridge Road, Orlando, FL 32809</p>
                        
                        <p><a href="https://www.google.com/maps/dir//2966+W+Oak+Ridge+Rd,+Orlando,+FL+32809/%4028.471642,-81.4182878,17z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x88e77c17f97b31df%3A0x7329bc5104934351!2m2!1d-81.4160991!2d28.471642" target="_blank">Driving Directions</a></p>
                        
                        
                        
                        <h3>West Campus</h3>
                        
                        <p><a href="http://www.youvisit.com/tour/valencia/valenciawest?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank"><img alt="West Campus Virtual Tour" border="0" height="150" src="../../locations/map/youvisit-west-245px.jpg" title="West Campus Campus" width="150"></a>1800 South Kirkman Road, Orlando, FL 32811
                        </p>
                        
                        <p><a href="../../locations/map/west.html">Driving Directions</a></p>
                        
                        <p>407-299-5000</p>
                        
                        <ul>
                           
                           <li><a href="../../locations/west/index.html">Campus Website</a></li>
                           
                           <li><a href="../../locations/map/map-west.html" target="_blank">Interactive Map</a></li>
                           
                           <li><a href="../../locations/map/documents/valencia_campus_map_west.pdf" target="_blank">Print Version of Campus Map</a></li>
                           
                           <li><a href="http://www.youvisit.com/tour/valencia/valenciawest?tourid=tour1&amp;pl=v&amp;m_prompt=1" target="_blank">Virtual Tour</a></li>
                           
                        </ul>
                        
                        
                        <h3>Winter Park Campus</h3>
                        
                        <p><img alt="Winter Park Campus Building" height="150" src="../../locations/map/locations_winterPark.jpg" title="Winter Park Campus" width="148">850 W Morse Blvd, Winter Park, FL 32789
                        </p>
                        
                        <p><a href="../../locations/map/winter-park.html">Driving Directions</a></p>
                        
                        <p>407-299-5000</p>
                        
                        <ul>
                           
                           <li><a href="../../locations/wp/index.html">Campus Website</a></li>
                           
                           <li><a href="../../locations/map/map-winter-park.html" target="_blank">Interactive Map</a></li>
                           
                           <li><a href="../../locations/map/documents/Valencia-Campus-Map-Winter-Park.pdf" target="_blank">Print Version of Campus Map</a></li>
                           
                        </ul>
                        
                        
                        
                        
                        <p><a href="../../locations/map/index.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/map/index.pcf">©</a>
      </div>
   </body>
</html>