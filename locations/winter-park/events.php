<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Events  | Valencia College</title>
      <meta name="Description" content="Events | Winter Park Campus | Valencia College">
      <meta name="Keywords" content="college, school, educational, winter, park, events">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/winter-park/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Winter Park Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/winter-park/">Winter Park</a></li>
               <li>Events </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../../locations/winter-park/index.html"></a>
                        
                        
                        <h3>Campus Events</h3>
                        
                        
                        
                        <div class="event_item">
                           
                           <a href="http://events.valenciacollege.edu/event/sustainability_around_the_world_603" target="_blank" class="event_tooltip" title="The World is not waiting for America to lead on quality of life issues.  This presentation will provide examples of sustainable leadership from several other countries in the...">
                              
                              
                              <div class="event_image">
                                 <img src="http://images-cf.localist.com/photos/575868/small/ec81621a45e2b1ede002a6d14b3f13ecb27f9792.jpg">
                                 
                              </div>
                              
                              
                              <div class="event_content content_style">
                                 
                                 <span class="event_link  big">Sustainability Around the World at Winter Park Campus</span> <br>
                                 
                                 
                                 <span class="event_sub content_date">
                                    <strong>November 13, 2017</strong>
                                    
                                    at 12:30 PM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        <div class="event_item">
                           
                           <a href="http://events.valenciacollege.edu/event/sustainability_around_the_world_360" target="_blank" class="event_tooltip" title="The World is not waiting for America to lead on quality of life issues.  This presentation will provide examples of sustainable leadership from several other countries in the...">
                              
                              
                              <div class="event_image">
                                 <img src="http://images-cf.localist.com/photos/572301/small/9be46ab5ac41dd6b84792ec5edb35f5755fb1881.jpg">
                                 
                              </div>
                              
                              
                              <div class="event_content content_style">
                                 
                                 <span class="event_link  big">Sustainability Around the World at Winter Park Campus</span> <br>
                                 
                                 
                                 <span class="event_sub content_date">
                                    <strong>November 13, 2017</strong>
                                    
                                    at 12:30 PM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        <div class="event_item">
                           
                           <a href="http://events.valenciacollege.edu/event/multicultural_thanksgiving_3690" target="_blank" class="event_tooltip" title="Join us for our annual Multicultural Thanksgiving Luncheon. Donate 3 non-perishable items or make a $3.00 cash donation to enjoy a smorgasbord of delicious homemade dishes and...">
                              
                              
                              <div class="event_image">
                                 <img src="http://images-cf.localist.com/photos/568585/small/922961159f70301f28515bd7b61dd1d310d62e24.jpg">
                                 
                              </div>
                              
                              
                              <div class="event_content content_style">
                                 
                                 <span class="event_link  big">MultiCultural Thanksgiving at Winter Park Campus</span> <br>
                                 
                                 
                                 <span class="event_sub content_date">
                                    <strong>November 14, 2017</strong>
                                    
                                    at 11:00 AM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        <div class="event_item">
                           
                           <a href="http://events.valenciacollege.edu/event/multicultural_thanksgiving_6217" target="_blank" class="event_tooltip" title="Please join us for one of Winter Park Campus' oldest and most iconic events of the year! This year we continue the tradition, and celebrate Thanksgiving by inviting you to...">
                              
                              
                              <div class="event_image">
                                 <img src="http://images-cf.localist.com/photos/572283/small/5c1fb1c90bb8ec895bf3a2a104f6e75a106caf9a.jpg">
                                 
                              </div>
                              
                              
                              <div class="event_content content_style">
                                 
                                 <span class="event_link  big">Multicultural Thanksgiving  at Winter Park Campus</span> <br>
                                 
                                 
                                 <span class="event_sub content_date">
                                    <strong>November 14, 2017</strong>
                                    
                                    at 11:00 AM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        <div class="event_item">
                           
                           <a href="http://events.valenciacollege.edu/event/coffee_careers_connections" target="_blank" class="event_tooltip" title="International Education Week presents:   Coffee, Careers and Connections  Wednesday, Nov. 15 Student Lounge-Winter Park 10a-12:30p  Meet the Career Center and Counseling...">
                              
                              
                              <div class="event_image">
                                 <img src="http://images-cf.localist.com/photos/563209/small/a9e1e2afe88b7ea4cde180b2e644fb315cfa5472.jpg">
                                 
                              </div>
                              
                              
                              <div class="event_content content_style">
                                 
                                 <span class="event_link  big">Coffee, Careers &amp;Connections at Winter Park Campus</span> <br>
                                 
                                 
                                 <span class="event_sub content_date">
                                    <strong>November 15, 2017</strong>
                                    
                                    at 10:00 AM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        <a href="http://events.valenciacollege.edu/WinterParkCampus#.US4PFTBwea8">View All</a>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/winter-park/events.pcf">©</a>
      </div>
   </body>
</html>