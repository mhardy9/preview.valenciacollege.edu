<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Winter Park Campus  | Valencia College</title>
      <meta name="Description" content="Winter Park Campus | Valencia College">
      <meta name="Keywords" content="college, school, educational, winter, park, campus">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/winter-park/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Winter Park Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li>Winter Park</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a href="../../locations/winter-park/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Winter Park Campus</h2>
                        
                        
                        
                        <p>The Winter Park Campus is located in the heart of Winter Park, at the   corner of
                           Morse Blvd and Denning Dr, steps from the popular Park Avenue shopping district and
                           the new SunRail station. This campus is home to the Jeffersonian Honors Program, which
                           is focused on producing active, global citizens. The faculty, staff, and students
                           of the Winter Park Campus routinely engage with the community through service-based
                           projects at multiple facilities, including the Winter Park Community Center and the
                           DePugh Nursing Home.
                        </p>
                        
                        <a class="button color1" href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=2.172561703.507279844.1510575333-1684407954.1471273916">Schedule a Tour</a>
                        
                        <a class="button color2" href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        <h3>Academic Resources</h3>
                        
                        <ul>
                           
                           <li><a href="http://net5.valenciacollege.edu/schedule/wp.cfm">Classes 
                                 Offered</a></li>
                           
                           <li><a href="../../locations/winter-park/student-support-winter-park/index.html">Communication Support Center</a></li>
                           
                           <li><a href="../../locations/honors/index.html">Honors</a></li>
                           
                           <li><a href="../../locations/library/index.html">Library</a></li>
                           
                           <li><a href="../../locations/winter-park/testing.html">Testing Center</a></li>
                           
                           <li><a href="../../locations/winter-park/math-support-center/index.html">Math Support Center</a></li>
                           
                        </ul>
                        
                        
                        <h3>Student Resources</h3>
                        
                        <ul>
                           
                           <li><a href="../../locations/students/answer-center/index.html">Answer Center</a></li>
                           
                           <li><a href="../../locations/students/locations-store/index.html">Bookstore</a></li>
                           
                           <li><a href="../../locations/career-center/index.html">Career Services</a></li>
                           
                           <li><a href="../../locations/internship/index.html">Internships &amp; Workforce Services</a></li>
                           
                        </ul>
                        
                        
                        <h3>Faculty &amp; Staff Resources</h3>
                        
                        <ul>
                           
                           <li><a href="../../locations/security/index.html">Campus Safety and Security</a></li>
                           
                           <li><a href="../../locations/winter-park/FacultyResources.html">Faculty Resources</a></li>
                           
                           <li><a href="../../locations/winter-park/credit/index.html">Credit Course Department</a></li>
                           
                           <li> <a href="../../locations/winter-park/ProfessionalDevelopment.html">Professional Development</a>
                              
                           </li>
                           
                           <li><a href="../../locations/winter-park/documents/2-3-2017.pdf" target="_blank">2-3-2017 WP Campus Newsletter</a></li>
                           
                        </ul>
                        
                        
                        <h3>Contact Information</h3>
                        
                        407-299-5000<br>
                        850 W. Morse Blvd.<br>Winter Park, FL 32789<br>
                        <a href="http://maps.google.com/maps/ms?vps=2&amp;ie=UTF8&amp;hl=en&amp;oe=UTF8&amp;msa=0&amp;msid=213232051841984513042.0004ac7720c6e77da6b53" target="_blank">
                           Directions</a><br>
                        <a href="../../locations/map/winter-park.html" target="_blank">
                           Campus Map</a><br><br>
                        
                        
                        
                        <img alt="Welcome back week, SGA event 2013" height="157" src="/_resources/img/locations/winter-park/thm_photo_01.jpg" width="240">
                        <img alt="Chris Borglum €™s literature class 2013" height="157" src="/_resources/img/locations/winter-park/thm_photo_02.jpg" width="240">
                        
                        <img alt="TEDx Womenâ€™s Conference event 2013" height="157" src="/_resources/img/locations/winter-park/thm_photo_03.jpg" width="240">
                        <img alt="Faculty and staff Development 2013" height="157" src="/_resources/img/locations/winter-park/thm_photo_04.jpg" width="240">
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/winter-park/index.pcf">©</a>
      </div>
   </body>
</html>