<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty &amp; Staff  | Valencia College</title>
      <meta name="Description" content="Faculty &amp;amp; Staff | Winter Park Campus">
      <meta name="Keywords" content="college, school, educational, winter, park, faculty, staff">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/winter-park/facultystaff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Winter Park Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/winter-park/">Winter Park</a></li>
               <li>Faculty &amp; Staff </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a href="../../locations/winter-park/index.html"></a>
                        
                        
                        <h2>Faculty &amp; Staff</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><img alt="Chris Borglum" border="2" height="77" src="/_resources/img/locations/winter-park/CBorglum.jpg" width="60"></div>
                                 
                                 <div>
                                    
                                    <p><strong><font face="Arial">Chris Borglum</font></strong></p>
                                    
                                    <ul>
                                       
                                       <li>English Professor</li>
                                       
                                       <li>cborglum@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Julie Corderman" border="2" height="77" src="/_resources/img/locations/winter-park/NoImage_000.jpg" width="60"></div>
                                 
                                 <div>
                                    
                                    <p><strong><font face="Arial">Julie Corderman</font></strong></p>
                                    
                                    <ul>
                                       
                                       <li>Student Services Mgr</li>
                                       
                                       <li>jcorderman@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Suzette Dohany" border="2" height="77" src="/_resources/img/locations/winter-park/SDohaney.jpg" width="60"> 
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong><font face="Arial">Suzette Dohany</font> </strong></p>
                                    
                                    <ul>
                                       
                                       <li>Speech Professor</li>
                                       
                                       <li>sdohany@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Jean Marie Fuhrman" border="2" height="77" src="/_resources/img/locations/winter-park/JMFuhrman.jpg" width="60"> 
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong><font face="Arial">Jean Marie Fuhrman</font></strong></p>
                                    
                                    <ul>
                                       
                                       <li>Reading Professor </li>
                                       
                                       <li>jfuhrman@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Damion Hammock" border="2" height="77" hspace="0" src="/_resources/img/locations/winter-park/DHammock.jpg" vspace="0" width="60"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Damion Hammock</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Mathematics Professor </li>
                                       
                                       <li>dhammock@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img alt="John Niss" height="77" src="/_resources/img/locations/winter-park/NoImage_001.jpg" width="60"> 
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong><font face="Arial">John Niss</font></strong></p>
                                    
                                    <ul>
                                       
                                       <li>Mathematics Professor</li>
                                       
                                       <li>jniss@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Susan MacPeek" border="2" height="77" src="/_resources/img/locations/winter-park/SDunn.jpg" width="60"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Susan Dunn</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Manager Credit Programs </li>
                                       
                                       <li>sdunn18@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Julie Corderman" border="2" height="77" src="/_resources/img/locations/winter-park/NoImage_000.jpg" width="60"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Cate McGowan</strong></p>
                                    
                                    <ul>
                                       
                                       <li>English Professor </li>
                                       
                                       <li>mmcgowan@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Jamie Prusak" border="2" height="77" src="/_resources/img/locations/winter-park/NoImage_000.jpg" width="60"> 
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong><font face="Arial">Jamie Prusak </font> </strong></p>
                                    
                                    <ul>
                                       
                                       <li>Biology Professor </li>
                                       
                                       <li>jprusak@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Julie Corderman" border="2" height="77" src="/_resources/img/locations/winter-park/NoImage_000.jpg" width="60"> 
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Cynthia Massaad</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Biology Professor </li>
                                       
                                       <li>cmassaad@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Katherine Moore" border="2" height="77" src="/_resources/img/locations/winter-park/MichaelLergier.jpg" width="60"> 
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Michael Lergier</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Operations Manager</li>
                                       
                                       <li>mlergierhernandez@valenciacollege.edu</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>                  
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Julie Corderman" border="2" height="77" src="/_resources/img/locations/winter-park/NoImage_000.jpg" width="60"> 
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong><font face="Arial">Sunni Favali-Prevatt</font></strong></p>
                                    
                                    <ul>
                                       
                                       <li> Student Development Coordinator </li>
                                       
                                       <li> sfavaliprevatt@valenciacollege.edu </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Dr. Upsana Santra" border="2" height="77" hspace="0" src="/_resources/img/locations/winter-park/USantra.jpg" vspace="0" width="60"> 
                                 </div>
                                 
                                 <div>
                                    
                                    <p><span><strong>Upasana Santra</strong></span></p>
                                    
                                    <ul>
                                       
                                       <li>Mathematics Professor</li>
                                       
                                       <li><span>usantra@valenciacollege.edu</span></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Leesa Sward" height="77" src="/_resources/img/locations/winter-park/NoImage_000_000.jpg" width="60"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Terri Daniels</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Executive Dean, Winter Park Campus </li>
                                       
                                       <li><span>tdaniels31@valenciacollege.edu</span></li>
                                       
                                    </ul>                      
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Marva Pryor" height="77" src="/_resources/img/locations/winter-park/NoImage_000_001.jpg" width="60"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Marva Pryor </strong></p>
                                    
                                    <ul>
                                       
                                       <li>Business Professor</li>
                                       
                                       <li><span>mpryor@valenciacollege.edu</span></li>
                                       
                                    </ul>                      
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/winter-park/facultystaff.pcf">©</a>
      </div>
   </body>
</html>