<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Winter Park Campus Credit Course Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/winter-park/credit/dean.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/winter-park/credit/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Winter Park Campus Credit Course Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/winter-park/">Winter Park</a></li>
               <li><a href="/locations/winter-park/credit/">Credit</a></li>
               <li>Winter Park Campus Credit Course Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="../../../locations/winter-park/credit/index.html"></a>
                        
                        <h2>Dean's Office Staff </h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><img alt="Michele McArdle, Dean of Winter Park Campus" border="1" height="137" hspace="10" src="../../../locations/winter-park/credit/michelle.jpg" width="100"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Michele McArdle, Executive Dean of Winter Park Campus</strong></p>
                                    
                                    <p>Michele McArdle has been with Valencia College since 1990 and currently is the Executive
                                       Dean of the Winter Park Campus. She is a member of the Leadership Team which was selected
                                       for the Outstanding Leader Award by the Academy for Leadership Training and Development.
                                       Dr.&nbsp; McArdle has a Bachelor of Arts from Seton Hill University and a Master of Arts
                                       from Duquesne University. She received her Doctorate in Education with an emphasis
                                       on educational leadership from the University of Central Florida.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Susan MacPeek, Program Manager, Credit Programs" border="1" height="137" hspace="10" src="../../../locations/winter-park/credit/susan-macpeek_000.jpg" vspace="0" width="100"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Susan Dunn, Manager, Credit Programs</strong> 
                                    </p>
                                    
                                    <p>Susan Dunn joined the Valencia team in 2008 as Manager, Credit Programs at the Winter
                                       Park Campus. She earned a Bachelor of Arts Degree in Anthropology from the University
                                       of Central Florida and a Master of Arts Degree in Anthropology from the University
                                       of South Carolina in Columbia, SC. Prior to coming to Valencia, Mrs. Dunn served as
                                       Coordinator of Testing and Evaluation at the University of Florida where she also
                                       worked extensively with the state-wide CLAST program.&nbsp; 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Peggy Johnson, Staff Assistant" border="1" height="137" hspace="10" src="../../../locations/winter-park/credit/Peggy-Color_000.jpg" vspace="0" width="100"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Peggy Johnson, Office Systems Manager</strong> 
                                    </p>
                                    
                                    <p>Peggy Johnson has been employed at Valencia since 1997. Ms. Johnson served as the
                                       Career Staff Association chair (formally known as the Staff Council) for Winter Park
                                       campus in 2003-2005 and past president of the African American Cultural Society Club.
                                       She received her A.A. degree on May 8,2010.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Michael Lergier, Operations Manager" border="1" height="137" hspace="10" src="../../../locations/winter-park/credit/MichaelLergier2.jpg" vspace="5" width="100"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Michael Lergier, Operations Manager </strong></p>
                                    
                                    <p>Michael Lergier joined Valencia College as a work study employee in 2000. He became
                                       a part-time employee in 2001 as a Computer Lab Assistant and Audio/Visual Technician.
                                       He earned a Bachelor of Science degree from the University of Central Florida in 2008,
                                       with a minor in Communications and Marketing. In 2009 he became a full-time employee
                                       with the college as the Operations Manager for Continuing Education. He is currently
                                       the Operations Manager of the Winter Park Campus, a position which he started in 2014.
                                       <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> 
                                    <div><img alt="Katy Miller's Picture" height="137" src="../../../locations/winter-park/credit/KatyMiller_000.jpg" width="100"></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Katy Miller, Campus Library Director </strong></p>
                                    
                                    <p>Katy Miller joined Valencia in 2010 and is the Campus Library Director of the Winter
                                       Park Campus, overseeing the Library and Testing Centers. She has a Bachelor of Arts
                                       from the University of Memphis and a Master's degree in Library and Information Science
                                       from the University of North Texas. Before joining Valencia, Mrs. Miller headed the
                                       Editorial Research Department of the Orlando Sentinel newspaper.
                                    </p>                  
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <p><a href="../../../locations/winter-park/credit/dean.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/winter-park/credit/dean.pcf">©</a>
      </div>
   </body>
</html>