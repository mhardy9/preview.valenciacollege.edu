<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Resources  | Valencia College</title>
      <meta name="Description" content="Faculty Resources | Winter Park Campus | Valencia College">
      <meta name="Keywords" content="college, school, educational, winter, park, faculty, resources">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/winter-park/facultyresources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Winter Park Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/winter-park/">Winter Park</a></li>
               <li>Faculty Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a href="../../locations/winter-park/index.html"></a>
                        
                        
                        
                        
                        <h2>Faculty Resources</h2>
                        
                        <h3>Syllabus Resources</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/locations/winter-park/Course-Syllabus-Resources-FALL-201810.pdf" target="_blank">Fall Syllabus Checklist</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/locations/winter-park/CourseSyllabusResourcesSpring2017_201720.doc" target="_blank">Spring Syllabus Checklist </a>  <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/locations/winter-park/CourseSyllabusResourceSummer201630.doc" target="_blank">Summer Syllabus Checklist </a> <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Forms</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/locations/winter-park/REQUESTFORCLASSTOMEETOFFCAMPUS.doc">Class Request to Meet Off Campus</a> <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/locations/winter-park/RequestForGuestSpeaker.doc">Request for Guest Speaker</a> <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Adjunct Faculty Resources</h3>
                        
                        <ul>
                           
                           <li><a href="../../locations/faculty/development/programs/adjunct/index.html">Adjunct Faculty</a></li>
                           
                        </ul>
                        
                        <p>Faculty Meeting Summaries </p>
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/locations/winter-park/FacultyMeetingSummary8-26-15.docx">August 26, 2015</a> <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/locations/winter-park/FacultyMeetingSummary9-29-15.docx">September 29, 2015</a> <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/locations/winter-park/FacultyMeetingSummary10-30-15.docx"> October 30, 2015</a> <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/locations/winter-park/FacultyMeetingSummary1-7-16.docx">January 7, 2016</a> <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/locations/winter-park/FacultyMeetingSummary1-21-16.docx">January 21, 2016 </a> <i class="far fa-file-text-o" aria-hidden="true"></i>
                              
                           </li>
                           
                        </ul>  
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/winter-park/facultyresources.pcf">©</a>
      </div>
   </body>
</html>