<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Services  | Valencia College</title>
      <meta name="Description" content="Student Services | Winter Park Campus | Valencia College">
      <meta name="Keywords" content="college, school, educational, winter, park, student, services">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/winter-park/services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Winter Park Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/winter-park/">Winter Park</a></li>
               <li>Student Services </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a href="../../locations/winter-park/index.html"></a>
                        
                        
                        
                        
                        <h2>Student Services </h2>
                        
                        <p><strong>Answer Center</strong><br>
                           The Answer Center is where all student services activities begin 
                           (Winter Park Room 210). This is the first stop for students to submit 
                           an application, discuss financial aid, inquire about assessment 
                           and orientation, and receive basic educational advising.
                        </p>
                        
                        <p>A professional staff member, known as a Student Services 
                           Specialist, will meet with a student visiting the Answer Center 
                           to assist in the following areas:
                        </p>
                        
                        <div> 
                           
                           <ul>
                              
                              <li> Admissions: Application, residency, and deadlines</li>
                              
                              <li> Financial Aid: Award information, proper documentation, and 
                                 deadlines
                              </li>
                              
                              <li> Transcripts: Accept and submit transcripts to Home Office; 
                                 advisement from unofficial transcripts
                              </li>
                              
                              <li> Graduation: Proper documentation, commencement information, 
                                 and deadlines; review Degree Audits with students to verify 
                                 requirements for graduation 
                              </li>
                              
                              <li> Referrals: Referrals are needed to access other departments; 
                                 these are received from Answer Center Personnel (as necessary)
                              </li>
                              
                           </ul>
                           <strong> Student Development </strong><br>
                           Student Development cultivates dynamic environments that build community 
                           and enhance learning. This department is your contact for Student 
                           Government Association, photo IDs, community service and involvement, 
                           leadership development activities, student clubs and organizations, 
                           campus information and programming &amp; events. For information 
                           on how to get involved on campus or in the community, stop by Student 
                           Development in Room 200.
                        </div>
                        
                        <p><strong>Office for Students with Disabilities</strong><br>
                           If you have a documented physical, emotional, or learning disability 
                           you are entitled to reasonable accommodations if you have registered 
                           with the Office for Students with Disabilities (OSD). The OSD will 
                           work with faculty and staff as needed to facilitate a smooth delivery 
                           of services. The OSD Advisor is located in Room 203.
                        </p>
                        
                        <p><strong>Personal Counseling</strong><br>
                           If you need help with personal problems to be more successful in 
                           classes and to reach your educational and career goals, personal 
                           counseling and referral services are available. Please visit the 
                           Answer Center (Room 210) for a referral to a Counselor.
                        </p>
                        
                        <p><strong>Honors Advising</strong><br>
                           The college-wide director of The Honors Program holds office hours 
                           each semester at the Winter Park Campus (for specific dates and 
                           times call extension 6887). The honors counselor is available Monday 
                           through Friday to advise honor students regarding their honors track 
                           and to answer any questions from students interested in the program. 
                           Pre-registration for college-wide honors classes is also available 
                           each semester. The Honors Program Advisor's office is located in 
                           Room 203.
                        </p>
                        
                        <p><strong>Career Development Services</strong><br>
                           The Career Center offers students and community residents assistance 
                           with developing a career plan. The Career Center is located in room 
                           214 and can be accessed through the Student Services Department. 
                           There are books, videos and computers for visitors to use in researching 
                           career opportunities and job trends. Use of the Career Center is 
                           by appointment only. For more information visit the Career Center 
                           website at <a href="../../locations/career-center/index.html">valenciacollege.edu/careercenter/</a>.
                        </p>
                        
                        <p><strong>Scholarships</strong><br>
                           There are many scholarships available to students. There are those 
                           from the Valencia foundation and others given by various organizations 
                           and schools. A list of Websites that can be helpful in finding additional 
                           information is also available. Various information and applications 
                           can be picked up in Student Services. Additional information may 
                           be found at <a href="../../locations/finaid/programs/scholarships.html">http://valenciacollege.edu/finaid/programs/scholarships.cfm</a><br>
                           <br>
                           <strong>Financial Aid</strong><br>
                           Many people need assistance in paying for college, and there are 
                           many ways of getting that extra help. Sources such as loans, grants, 
                           and scholarships are available to students. The application for 
                           federal grants and loans can be found at <a href="http://www.fafsa.ed.gov/">http://www.fafsa.ed.gov</a>/. 
                           Visit <a href="../../locations/finaid/index.html">http://valenciacollege.edu/finaid/</a> for 
                           forms, FAQs, and a FA user guide.If you have additional finacial 
                           aid questions, please visit the Answer Center (room 210) for assistance.<br>
                           <br>
                           <strong>Tutoring</strong><br>
                           This service is available for students who feel they need extra 
                           help in certain courses. If you want a tutor, please visit the Math 
                           or Communications Support Center.
                        </p>
                        
                        <p>Contact your Winter Park Answer Center for more details 
                           at: <a href="mailto:%20AnswerCenterWP@valenciacollege.edu">AnswerCenterWP@valenciacollege.edu</a>.
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/winter-park/services.pcf">©</a>
      </div>
   </body>
</html>