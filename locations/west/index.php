<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus | Valencia College</title>
      <meta name="Description" content="West Campus">
      <meta name="Keywords" content="college, school, educational, west">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/west/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li>West</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../locations/west/index.html"></a>
                        
                        
                        <p><strong>Valencia College</strong>, a premier institution within the state's  college system, serves Orange and Osceola
                           counties in Central Florida. The <strong>West Campus</strong> 
                           is located on a 180 acre site in West Orange County. 
                        </p>
                        
                        <p>Opportunities for personal growth and development abound. You will be informed, guided
                           and inspired by an outstanding faculty. You will be nurtured and assisted by a skilled
                           and dedicated staff. Enhance your perspectives by reviewing sites provided by our
                           academic divisions, library, student development, and advising and counseling services.
                        </p>
                        
                        <h3>Faculty and Staff Services and Resources</h3>
                        
                        <ul>
                           
                           <li><a href="../../employees/human-resources/index.php">Human Resources</a></li>
                           
                           <li><a href="http://site.valenciacollege.edu/faculty-association/west/">Faculty Senate</a></li>
                           
                           <li><a href="../../about/facilities/index.php">Facilities</a></li>
                           
                           <li><a href="../../employees/office-of-information-technology/index.php">Office of Information Technology</a></li>
                           
                           <li><a href="../../about/general-counsel/index.php">Valencia Policy Manual</a></li>
                           
                           <li><a href="../../locations/west/travel-funds.php">Academic Travel Fund Request</a></li>
                           
                           <li><a href="http://valenciacollege.edu/divisionplans/">Division Action Plans</a></li>
                           
                           <li><a href="http://net5.valenciacollege.edu/facultyroster/">Roster of Instructional Staff</a></li>
                           
                           <li><a href="http://valenciacollege.edu/faculty/forms/credentials/">Credentials Manual</a></li>
                           
                           <li><a href="../../faculty/resources/index.php">Faculty Resources</a></li>
                           
                           <li><a href="../../employees/word-processing/index.php">Word Processing</a></li>
                           
                        </ul>
                        
                        
                        <h3><a href="../../students/learning-support/index.php">Learning Support Services</a></h3>
                        
                        <ul>
                           
                           <li><a href="../../students/learning-support/west/communications/index.php">Communications Center</a></li>
                           
                           <li><a href="../../students/learning-support/math/index.php">Math Center</a></li>
                           
                           <li><a href="../../students/testing/index.php">Testing Center</a></li>
                           
                           <li><a href="../../students/learning-support/tutoring/index.php">Tutoring Center</a></li>
                           
                        </ul>
                        
                        
                        <h3>Library and Other Learning Resources </h3>
                        
                        <ul>
                           
                           <li><a href="../../students/library/locations/west-campus-library.php">West Campus Library</a></li>
                           
                           <li><a href="../../employees/office-of-information-technology/learning-technology-services/faculty-resources/index.php">Learning Technology and Alternative Delivery</a></li>
                           
                           <li><a href="../../students/labs/west.php">Campus Lab Hours</a></li>
                           
                        </ul>
                        
                        
                        <h3>Student Services and Resources</h3>
                        
                        <ul>
                           
                           <li><a href="../../students/locations-store/index.php">Valencia Bookstore</a></li>
                           
                           <li><a href="../../students/career-center/aboutus.php">Career Development Services</a></li>
                           
                           <li><a href="../../students/student-development/default.php">Student Development</a></li>
                           
                           <li><a href="../../students/internship/index.php">Internship and Workforce Services</a></li>
                           
                           <li><a href="http://net5.valenciacollege.edu/schedule/">Class Schedule</a></li>
                           
                           <li><a href="../../students/catalog/index.php">College Catalog</a></li>
                           
                           <li><a href="../../locations/west/fitness/index.php">Fitness Center</a></li>
                           
                        </ul>
                        	
                        <h3>Campus Resources</h3>
                        	<a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a><br>
                        	<a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a><p></p>
                        	
                        <h3>Contact</h3>
                        	407-299-5000<br>
                        	1800 South Kirkman Road<br>Orlando, FL 32811
                        <p></p>
                        
                        	<a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac771a01f96ba8126&amp;msa=0&amp;ll=28.522512,-81.464428&amp;spn=0.0092,0.017027" target="_blank">
                           		Directions</a><br>
                        	<a href="../../locations/map/west.php" target="_blank">
                           		Campus Map</a><br> 
                        	<a href="http://www.youvisit.com/tour/valencia/valenciawest?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="west campus virtual tour" height="245" src="images/youvisit-west-245px.jpg" width="245"></a>
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/west/index.pcf">©</a>
      </div>
   </body>
</html>