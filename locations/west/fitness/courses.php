<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Courses Offered | Fitness Center | West Campus | Valencia College</title>
      <meta name="Description" content="Courses Offered | Fitness Center | West Campus">
      <meta name="Keywords" content="college, school, educational, west, fitness, center, courses">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/west/fitness/courses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/west/fitness/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus</h1>
            <p>Fitness Center</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/west/">West</a></li>
               <li><a href="/locations/west/fitness/">Fitness</a></li>
               <li>Courses Offered</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h2>Health &amp; Fitness Courses</h2>
                                    
                                    <ul>
                                       
                                       <li>Fitness &amp; Wellness
                                          for Life -<strong> HLP 1081</strong> 
                                       </li>
                                       
                                       <li>Personal Health (online) -<strong> HSC 1100</strong> 
                                       </li>
                                       
                                       <li>Concepts in Personal Training - <strong>HLP 2550C</strong> 
                                       </li>
                                       
                                       <li>Weight Traning I &amp; II - <strong>PEM 1131 &amp; PEM 1132</strong> 
                                       </li>
                                       
                                       <li>Yoga -<strong> PEM 1121</strong>
                                          
                                       </li>
                                       
                                       <li>Golf I &amp; II -<strong> PEL 1121 &amp; PEL 2122</strong> 
                                       </li>
                                       
                                       <li>Karate-<strong>PEM 1441</strong>
                                          
                                       </li>
                                       
                                       <li>Kickboxing -<strong> PEM 1176</strong> 
                                       </li>
                                       
                                       <li>Belly Dancing Aerobics-<strong>PEM 1171</strong>
                                          
                                       </li>
                                       
                                       <li>Self Defense for Women -<strong> PEM 1405</strong> 
                                       </li>
                                       
                                    </ul>
                                    
                                    <p>View the <a href="http://net5.valenciacollege.edu/schedule/">credit course schedule</a> online.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="" height="147" src="../../../locations/west/fitness/HipHopClass33.jpg" width="215"></p>
                                    
                                    <p><img alt="" height="147" src="../../../locations/west/fitness/Weight%20Training.jpg" width="215"></p>
                                    
                                    <p><img alt="" height="147" src="../../../locations/west/fitness/YogaClass107.jpg" width="215"></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h2>&nbsp;</h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/west/fitness/courses.pcf">©</a>
      </div>
   </body>
</html>