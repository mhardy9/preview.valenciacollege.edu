<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/west/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/west/">West</a></li>
               <li>West Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../locations/west/index.html"></a>
                        
                        
                        <h2>Campus Events</h2>
                        
                        	<img src="/locations/west/images/723f8ebd25799fcd8458cfac501190675ad883c2.jpg" align="left" hspace="15">        
                        	<a href="http://events.valenciacollege.edu/event/on-campus_recruitment_-_loews_hotelsuniversal_orlando" target="_blank" title="Recruiting for a variety of positions">
                           		<span>On-Campus Recruitment - Loews Hotels@universal Orlando at West Campus</span> <br>
                           		<span><strong>September 21, 2017</strong> at 9:00 AM</span></a>
                        	
                        	
                        <p></p>
                        	
                        	
                        <img src="../../locations/west/images/d83e4660a20bfd087d61d9192e3bcebbadfa8583.jpg" align="left" hspace="15">
                        <a href="http://events.valenciacollege.edu/event/bts_collection_drive_918_-928_to_benefit_orlando_union_rescue_mission" target="_blank" title="The Orlando Union Rescue Mission a renowned homeless shelter since 1948, has reached out to Valencia’s Bridges to Success due to the shelter's dire need of some basic...">
                           			<span>BTS Collection Drive (9/18 -9/28) to Benefit Orlando Union Rescue Mission at West
                              Campus</span> <br>
                           			<span><strong>September 21, 2017</strong> at 1:00 PM</span></a>
                        
                        	
                        <p></p>
                        	
                        <img src="/locations/west/images/723f8ebd25799fcd8458cfac501190675ad883c2.jpg" align="left" hspace="15">
                        <a href="http://events.valenciacollege.edu/event/miracle_of_love_592" target="_blank" title="Free HIV testing for Students">
                           			<span>Miracle of Love at West Campus</span> <br>
                           			<span><strong>October 03, 2017</strong> at 10:00 AM</span></a>
                        
                        	
                        <p></p>
                        
                        <img src="../../locations/west/images/a5ec65008e190c9d7d54f02c57b7d739c8a4c39a.jpg" align="left" hspace="15">    
                        	<a href="http://events.valenciacollege.edu/event/miracle_of_love_9524" target="_blank" title="Free HIV Testing for students.">
                           		<span>Miracle of Love at West Campus</span> <br>        
                           <span><strong>November 07, 2017</strong> at 10:00 AM</span></a>
                        
                        	
                        <p></p>
                        	
                        	<img src="../../locations/west/images/723f8ebd25799fcd8458cfac501190675ad883c2.jpg" align="left" hspace="15">
                        	<a href="http://events.valenciacollege.edu/event/miracle_of_love_5742" target="_blank" title="Free HIV Testing for students.">
                           		<span>Miracle of Love at West Campus</span> <br>       
                           <span><strong>December 05, 2017</strong> at 10:00 PM</span></a>
                        	
                        	
                        <p></p>
                        
                        <a href="http://events.valenciacollege.edu/westcampus#.US4O9jBwea8">View All</a>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/west/events.pcf">©</a>
      </div>
   </body>
</html>