<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Travel Funds | West Campus | Valencia College</title>
      <meta name="Description" content="Travel Funds | West Campus">
      <meta name="Keywords" content="college, school, educational, west, travel, funds">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/west/travel-funds.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/west/">West</a></li>
               <li>Travel Funds</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Principles and Guidelines</h2>
                        
                        <p><strong>PRINCIPLES</strong></p>
                        
                        <ul>
                           
                           <li>West Campus Academic Travel Funds are meant to promote employee performance and skill
                              enhancement and to provide funding for West Campus full-time faculty, and for West
                              Campus staff--career or professional--who work in an academic division that reports
                              to the president of West Campus.
                           </li>
                           
                           <li>Each Campus receives a specific amount of money each year to be used by the campus<strong><span>&nbsp;</span>to cover travel costs and related expenses for conference, workshops and related professional
                                 development.</strong><span>&nbsp;</span>On West Campus, the campus president disperses this money through a committee of representatives
                              from administration, faculty, and professional staff.
                           </li>
                           
                        </ul>
                        
                        <p><strong>GUIDELINES</strong></p>
                        
                        <p>Listed below are guidelines the travel committee uses to award travel money.</p>
                        
                        <ul>
                           
                           <li>The convention, conference, workshop, etc., must relate directly to your assigned
                              duties at Valencia and you must have the approval of your supervisor.
                           </li>
                           
                           <li>New requests are considered ahead of requests from applicants who received funding
                              during the previous two cycles.
                           </li>
                           
                           <li>Preference is given to higher levels of participation (speakers or conference chairs).</li>
                           
                           <li>Preference is given to requests that the travel committee considers to have high impact
                              on personal, division, campus, and college goals.
                           </li>
                           
                           <li>Preference is given to requests for in-state travel.</li>
                           
                           <li>In cases where multiple applicants wish to attend the same event, the travel committee
                              will also consider:
                              
                              <ul>
                                 
                                 <li>The additional impact gained by multiple people attending the event.</li>
                                 
                                 <li>The description of how what is learned at the event will be shared with others at
                                    the college.
                                 </li>
                                 
                                 <li>Recommendations by division administration.</li>
                                 
                                 <li>Which applications to the same event were recieved first.</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>AWARD AMOUNTS</strong></p>
                        
                        <p>The annual cap is $1,500. The goal of the cap is to balance adequate funding for individual
                           trips and an equitable distribution of the travel money.
                        </p>
                        
                        <p>The travel committee monitors and reacts to the amount of money that comes available
                           each year for travel. It also debates on a regular basis the merits of capping awards
                           versus fully funding trips, taking into account feedback from the campus president
                           and other groups as warranted.
                        </p>
                        
                        <p><strong>NOTIFICATION</strong></p>
                        
                        <p>Award emails are typically sent out to the employee receiving the award and their
                           dean or immediate supervisor within a week of application deadlines.
                        </p>
                        
                        <p><strong>RETURNING / REALLOCATING YOUR AWARD</strong></p>
                        
                        <p>Because the travel committee must sometimes reject worthy applications due to limited
                           budgets, it expects awarded money to be used.
                        </p>
                        
                        <p>If you decide not to use your funding, you should inform your dean and the travel
                           committee chairs as soon as possible.
                        </p>
                        
                        <p><strong>SHARING YOUR INFORMATION</strong></p>
                        
                        <p>Upon returning from your trip, you are encouraged to share information as appropriate
                           with students and colleagues to increase the impact of your experience.
                        </p>
                        
                        <h2>Cycles/Application Deadlines</h2>
                        
                        <h3>2017 - 2018</h3>
                        
                        <p><strong>Note:<span>&nbsp;</span></strong>After filling out the online form, print it, sign it, and send it to your dean or
                           supervisor for an approval signature. Complete applications are due to the campus
                           president's office by the end of the business day on the application deadline.
                        </p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr class="tableHead_1" valign="top">
                                 
                                 <th scope="col" width="93">Cycle</th>
                                 
                                 <th scope="col" width="138">
                                    
                                    <p>If You Plan To<br>Travel During
                                    </p>
                                    
                                 </th>
                                 
                                 <th class="notice" scope="col" width="106">Application Deadline</th>
                                 
                                 <th scope="col" width="111">Committee Meeting</th>
                                 
                                 <th scope="col" width="94">% Budget Awarded</th>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td>
                                    
                                    <div align="center">Cycle 1</div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">7/1/17 - 10/31/17</div>
                                    
                                 </td>
                                 
                                 <td class="notice">
                                    
                                    <div class="notice" align="center"><strong>5/26/17</strong></div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">Before 6/9/17</div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">30%</div>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td>
                                    
                                    <div align="center">Cycle 2</div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">11/1/17 - 2/28/18</div>
                                    
                                 </td>
                                 
                                 <td class="notice">
                                    
                                    <div class="notice" align="center"><strong>9/18/17</strong></div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">Before 10/6/17</div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">35%</div>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td>
                                    
                                    <div align="center">Cycle 3</div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">3/1/18 - 6/30/18</div>
                                    
                                 </td>
                                 
                                 <td class="notice">
                                    
                                    <div class="notice" align="center"><strong>1/16/18</strong></div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">Before 1/26/18</div>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <div align="center">35%</div>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h2>Apply for Travel Funds</h2>
                        
                        <ol>
                           
                           <li>Fill out the form.</li>
                           
                           <li>Print Form</li>
                           
                           <li>Obtain Signature from your Dean</li>
                           
                           <li>Sign Form</li>
                           
                           <li>Send electronically to Nathaly Munoz Iguaran nmunoz8@valenciacollege.edu</li>
                           
                           <li>Make sure to name your electronic file with your name before sending it.&nbsp;</li>
                           
                        </ol>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/west/travel-request/travel-request.cfm" target="_blank">Click here to apply!</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/west/travel-funds.pcf">©</a>
      </div>
   </body>
</html>