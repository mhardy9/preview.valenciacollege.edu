<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus President | West Campus | Valencia College</title>
      <meta name="Description" content="Campus President | West Campus">
      <meta name="Keywords" content="college, school, educational, west, campus, president">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/west/campus-president/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/west/campus-president/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus</h1>
            <p>Campus President</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/west/">West</a></li>
               <li>Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/west/index.html"></a>
                        
                        
                        
                        
                        <div>
                           <img alt="Photo of Campus President - Falecia Williams" height="252" src="images/falecia-williams-180x252website.jpg" width="180">   
                           <h3>Dr. Falecia Williams</h3>
                           
                           
                           Dr. Williams has worked at Valencia for 16 years, first as an adjunct professor, then
                           in leadership roles in conjunction with dual enrollment, high school outreach programs
                           and workforce development. Most recently, she led a team charged with the creation
                           of the college's first bachelor degrees.
                           <br>
                           <br>
                           
                           Prior to Valencia, she was a leader in gifted and honors education at the secondary
                           level. She also served as the director of a community-based grant program to increase
                           enrollment and college completion among first-generation college students.
                           <br><br>
                           
                           Williams holds a Doctor of Education with a specialization in Community College Leadership
                           from the University of Central Florida. She currently resides in Windermere.
                           <br><br>
                           
                           In August, the college decentralized some academic operations, changing the title
                           of its most senior campus leaders from "provost" to "president" and giving them more
                           authority to innovate based on the needs of their particular campus.
                           <br><br>
                           
                           The West Campus, with a fall enrollment of more than 19,000 students, is located on
                           South Kirkman Road in Orlando, one of Valencia's four Central Florida campuses.
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/west/campus-president/index.pcf">©</a>
      </div>
   </body>
</html>