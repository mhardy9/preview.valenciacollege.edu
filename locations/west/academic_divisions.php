<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/west/academic_divisions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/west/">West</a></li>
               <li>West Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12"><a id="content" name="content"></a> <br>
                        				
                        <div>
                           					
                           <div>
                              						
                              <div>
                                 							
                                 <div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Allied Health</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div>
                                                   													
                                                   <ul>
                                                      														
                                                      <li><a href="/locations/west/health/cehealth/index.html">Continuing Education</a></li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/cardiovascular-technology/index.php">Cardiovascular Technology</a></li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/computed-tomography/index.php">Computed Tomography</a></li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/diagnostic-medical-sonography/index.php">Diagnostic Medical Sonography</a></li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/dental-hygiene/index.php">Dental Hygiene</a></li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/emergency-medical-services/index.php">Emergency Medical Services</a></li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/health-information-technology/index.php">Health Information Technology</a></li>
                                                      														
                                                      <li>Health-Related</li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/magnetic-resonance-imaging/index.php">Magnetic Resonance Imaging</a></li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/radiography/index.php">Radiography</a></li>
                                                      														
                                                      <li><a href="/academics/programs/health-sciences/respiratory-care/index.php">Respiratory Care</a></li>
                                                      														
                                                      <li>Sonography</li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Bldg 2, Rm 208
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/academics/departments/health-sciences/allied-health/index.php">Allied Health <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Arts and Humanities</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <ul>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/architecture/index.php">Architecture</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/fine-arts/index.php">Art</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/fine-arts/index.php">Film</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/graphic-interactive-design/index.php">Graphics Technology</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/humanities/index.php">Humanities</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/interdisciplinary-studies/index.php">Interdisciplinary Studies</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/foreign-languages/index.php">Foreign Languages</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/fine-arts/index.php">Music</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/philosophy/index.php">Philosophy</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/religion/index.php">Religion</a></li>
                                                      														
                                                      <li><a href="/academics/departments/arts-humanities-west/programs/fine-arts/index.php">Theater</a></li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Bldg 5, Rm 146
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-5730 or 407-582-1300 <i class="fas fa-fax"></i> 407-582-5436
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-clock"></i> Monday - Friday: 8:00am to 5pm
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/academics/departments/arts-humanities-west/index.php">Arts and Humanities<img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Engineering, Computer Programing and Technology</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <ul>
                                                      														
                                                      <li><a href="/academics/departments/engineering/programs/building-construction-technology/index.php">Building Construction Technology</a></li>
                                                      														
                                                      <li><a href="/academics/departments/engineering/programs/network-engineering-technology/index.php">Computer Engineering Technology (Networking)</a></li>
                                                      														
                                                      <li><a href="/academics/departments/engineering/programs/civil-surveying-engineering-technology/index.php">Civil/Surveying Engineering Technology</a></li>
                                                      														
                                                      <li><a href="/academics/departments/engineering/programs/computer-information-technology/index.php">Computer Information Technology</a></li>
                                                      														
                                                      <li><a href="/academics/departments/engineering/programs/computer-programming-and-analysis/index.php">Computer Programming &amp; Analysis, &amp; Data Base Technology</a></li>
                                                      														
                                                      <li><a href="/academics/departments/engineering/programs/drafting-design-technology/index.php">Drafting and Design Technology</a></li>
                                                      														
                                                      <li><a href="/academics/departments/engineering/programs/electronics-engineering-technology/index.php">Electronics Engineering Technology</a></li>
                                                      														
                                                      <li><a href="/academics/programs/engineering-technology/index.php">Engineering</a></li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Bldg 9, Rm 140
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1904 <i class="fas fa-fax"></i> 407-582-1900
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Bldg. 11-312
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1039
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/academics/departments/engineering/index.php">Engineering, Computer Programming and Technology <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Behavioral and Social Sciences</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <ul>
                                                      														
                                                      <li><a href="/academics/departments/business-hospitality/programs/accounting/">Accounting</a></li>
                                                      														
                                                      <li><a href="/academics/departments/behavioral-social-sciences/anthropology/index.php">Anthropology</a></li>
                                                      														
                                                      <li><a href="/academics/departments/business-hospitality/programs/business-administration-management/">Business Administration &amp; Management</a></li>
                                                      														
                                                      <li><a href="/academics/departments/behavioral-social-sciences/criminal-justice-technology/index.php">Criminal Justice Technology</a></li>
                                                      														
                                                      <li><a href="/academics/departments/behavioral-social-sciences/economics/index.php">Economics</a></li>
                                                      														
                                                      <li><a href="/academics/departments/behavioral-social-sciences/education/index.php">Education</a></li>
                                                      														
                                                      <li>Educator Preparation Institute (EPI)</li>
                                                      														
                                                      <li>Finance</li>
                                                      														
                                                      <li>Geography</li>
                                                      														
                                                      <li><a href="/academics/departments/behavioral-social-sciences/history/index.php">History</a></li>
                                                      														
                                                      <li><a href="/academics/departments/behavioral-social-sciences/physical-education/index.php">Physical Education</a></li>
                                                      														
                                                      <li>Political Science</li>
                                                      														
                                                      <li>Psychology</li>
                                                      														
                                                      <li><a href="/academics/departments/behavioral-social-sciences/sociology/index.php">Sociology</a></li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Bldg 11, Rm 103
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1203 <i class="fas fa-fax"></i> 407-582-1675
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-clock"></i> Monday - Friday 8am - 5pm
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/academics/departrments/behavioral-and-social-sciences/index.php">Behavioral and Social Sciences <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Library</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Bldg 6
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1574
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-clock"></i> Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/locations/library/index.html">Library <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								<br> 
                                 </div>
                                 							
                                 <div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Business and Hospitality</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <ul>
                                                      														
                                                      <li>Accounting</li>
                                                      														
                                                      <li>Business Administration &amp; Management</li>
                                                      														
                                                      <li>Finance</li>
                                                      														
                                                      <li>Hospitality and Tourism, Baking and <br> Pastry Management, Culinary and Restaurant Management
                                                      </li>
                                                      														
                                                      <li>Office Systems Technology</li>
                                                      														
                                                      <li>Real Estate</li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Building 7, Room 107
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1069
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-clock"></i> Monday - Friday: 8am to 5pm
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/locations/west/business-and-hospitality/index.html">Business and Hospitality <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Communications</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <ul>
                                                      														
                                                      <li>English</li>
                                                      														
                                                      <li>English for Academic Purposes</li>
                                                      														
                                                      <li>Prep English</li>
                                                      														
                                                      <li>Journalism</li>
                                                      														
                                                      <li>Reading</li>
                                                      														
                                                      <li>Prep Reading</li>
                                                      														
                                                      <li>Speech</li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Bldg 5, Rm 231
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1616 <i class="fas fa-fax"></i> 407-582-1676
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/locations/departments/west/communications/index.html">Communications <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Mathematics</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <ul>
                                                      														
                                                      <li>Mathematics</li>
                                                      														
                                                      <li>Prep Math</li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> Bldg 7, Rm 108
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1625 or 407-582-1848 <i class="fas fa-fax"></i> 407-582-1856
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/locations/departments/west/math/index.html">Mathematics <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Nursing</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <ul>
                                                      														
                                                      <li>Continuing Education for Health Professions</li>
                                                      														
                                                      <li>Nursing</li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> HSB, Rm 200
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1118 or 407-582-1566 <i class="fas fa-fax"></i> 407-582-1278
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-clock"></i> M-F 8:00am - 5:00 pm
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/locations/west/nursing/index.html">Nursing <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Science</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <ul>
                                                      														
                                                      <li>Astronomy</li>
                                                      														
                                                      <li>Biology</li>
                                                      														
                                                      <li>Chemistry</li>
                                                      														
                                                      <li>Environmental Science Technology</li>
                                                      														
                                                      <li>Geology</li>
                                                      														
                                                      <li>Landscape &amp; Horticulture Technology</li>
                                                      														
                                                      <li>Meteorology</li>
                                                      														
                                                      <li>Oceanography</li>
                                                      														
                                                      <li>Physical Science</li>
                                                      														
                                                      <li>Physics</li>
                                                      													
                                                   </ul>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>&nbsp;</div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-map-marker"></i> AHS, Rm 231
                                                            </div>
                                                            															
                                                         </div>
                                                         															
                                                         <div>
                                                            																
                                                            <div><i class="fas fa-phone"></i> 407-582-1407
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/locations/departments/west/science/index.html">Science <img src="/locations/west/smore.png" alt="" width="10" height="10" border="0"></a></div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <h3>Learning Support Services</h3>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>
                                                												
                                                <div><br>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>
                                                            																
                                                            <div>
                                                               																	
                                                               <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/locations/learning-support/testing/index.html"> Testing Center </a></div>
                                                               																
                                                            </div>
                                                            																
                                                            <div>
                                                               																	
                                                               <div><i class="fas fa-map-marker"></i> Bldg 11, Rm 142
                                                               </div>
                                                               																
                                                            </div>
                                                            																
                                                            <div>
                                                               																	
                                                               <div><i class="fas fa-phone"></i> 407-582-1323
                                                               </div>
                                                               																
                                                            </div>
                                                            																
                                                            <div>
                                                               																	
                                                               <div><i class="fas fa-clock"></i> Monday-Thursday 8 am-9 pm<br>Friday: 8 am-12 pm<br>Saturday: 9 am-2 pm<br>Sunday: Closed
                                                               </div>
                                                               																
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   													
                                                   <div>
                                                      														
                                                      <div>
                                                         															
                                                         <div>
                                                            																
                                                            <div>
                                                               																	
                                                               <div><i class="fas fa-arrow-alt-circle-right"></i> <a href="/locations/learning-support/tutoring/index.html"> Tutoring </a></div>
                                                               																
                                                            </div>
                                                            																
                                                            <div>
                                                               																	
                                                               <div><i class="fas fa-map-marker"></i> Bldg 7, Rm 240
                                                               </div>
                                                               																
                                                            </div>
                                                            																
                                                            <div>
                                                               																	
                                                               <div><i class="fas fa-phone"></i> 407-582-1633
                                                               </div>
                                                               																
                                                            </div>
                                                            																
                                                            <div>
                                                               																	
                                                               <div><i class="fas fa-clock"></i> Monday - Thursday: 8 am to 8 pm<br>Friday: 8 am to 7 pm<br>Saturday: 10 am to 3 pm<br>Sunday: Closed
                                                               </div>
                                                               																
                                                            </div>
                                                            															
                                                         </div>
                                                         														
                                                      </div>
                                                      													
                                                   </div>
                                                   												
                                                </div>
                                                											
                                             </div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    							
                                 </div>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/west/academic_divisions.pcf">©</a>
      </div>
   </body>
</html>