<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/about-administration.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Administration</h2>
                        
                        <h3 dir="auto">Jennifer Robertson</h3>
                        
                        <p><img alt="Jennifer Robertson" height="175" src="../../locations/poinciana/2017-04-24-Jennifer-Robertson-Portrait-041.jpg" width="130">Dr. Robertson currently serves as Valencia College’s executive dean of the Poinciana
                           Campus. Prior to this position, she served as the director of the Study Abroad and
                           Global Experiences office at Valencia and executive director of the Florida Consortium
                           for International Education since 2011. Dr. Robertson has a master’s degree in business
                           administration and just completed a doctoral program at the University of Central
                           Florida in educational leadership. She has lived and worked throughout Latin America
                           and owned a language school in Puerto Rico prior to coming to Florida. Dr. Robertson
                           is fluent in Spanish and is currently studying Portuguese to better serve our Central
                           Florida communities.
                        </p>
                        
                        
                        
                        
                        <h3 dir="auto">Mary McGowan - Director of Student Services</h3>
                        
                        <p><img alt="Mary McGowan" height="168" src="../../locations/poinciana/photo-Mary-McGowan.jpg" width="130">Mary earned her Associate in Arts degree at  Valencia College. She transferred to
                           University of Central Florida to complete  her bachelor’s degree in Psychology, as
                           well as a Master of Arts in Educational  Leadership. She has worked at Valencia since
                           2007, starting as a Student  Leader. Her other previous roles at Valencia include
                           Administrative Support  Specialist, Academic Advisor, Answer Center Advisor, and New
                           Student  Orientation Coordinator.
                        </p>
                        
                        
                        
                        
                        <h3 dir="auto">Amaris Guzman - Community Outreach Manager</h3>
                        
                        <p><img alt="Amaris Guzman" height="188" src="../../locations/poinciana/photo-Guzman_Amaris.jpg" width="130">Dr. Guzman currently serves as the Community Outreach Manager for Valencia's new Poinciana
                           Campus. Prior to Valencia, she served as an Hispanic/Latino Outreach and Retention
                           Advisor and Professor of Strategies for Success in Georgia, Professor of Latin American
                           Studies in New York, and Northeast Affiliate Coordinator for the National Council
                           of La Raza in Washington, D.C. Dr. Guzman is a New Jersey native of Dominican descent
                           and has traveled the U.S., Latin America, and the Caribbean to best address Hispanic/Latino
                           community formation and its role in U.S. higher education. Dr. Guzman has a bachelor's
                           degree in criminal justice from St. John's University, a master's degree in Latin
                           American and Border Studies from the University of Texas at El Paso, and a master's
                           degree in Latin American, Caribbean, and U.S. Latino Studies from the University at
                           Albany-SUNY. In 2016, she earned her Ph.D. in Educational Leadership, Research, and
                           Counseling at Louisiana State University.
                        </p>
                        
                        
                        
                        
                        <h3 dir="auto">Josh Austin</h3>
                        
                        <p><img alt="Josh Austin" height="172" src="../../locations/poinciana/photo-josh_austin.png" width="130">Josh Austin graduated from Polk State College with his AA in Liberal Arts in 2012.
                           After completing his associate's degree, Josh transferred to the University of Central
                           Florida where he completed a bachelor's degree in Sociology and a master's degree
                           in Educational Leadership. He is currently in the Organizational Leadership doctoral
                           program at Nova Southeastern University. Prior to this position, Josh served as a
                           Student Development Program Advisor at the Osceola Campus and adjunct faculty member
                           for the college's New Student Experience course.
                        </p>
                        
                        <h3 class="heading_divider_red" dir="auto">Brenda Baralt</h3>
                        
                        <p><img src="/_resources/img/locations/poinciana/Brenda-Baralt-2017.jpg" width="130" height="157" alt="Brenda Baralt" class="pull-right">Brenda is an alumna from Valencia College. She began her role as Executive Assistant
                           with Valencia in January 2017. She has a rich background in the Hospitality and Human
                           Resources field of over 10 years. Brenda is fluent in the English and Spanish language.
                           
                        </p>
                        
                        <p dir="auto"></p>
                        
                        <p>&nbsp;</p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" title="Apply Now"></a>
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-6054</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:poincianacampus@valenciacollege.edu">poincianacampus@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>3255 Pleasant Hill Road<br>Kissimmee, FL 34746
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        <a href="https://www.google.com/maps/place/Valencia+College,+Poinciana+Campus/@28.200584,-81.439852,17z/data=!4m5!3m4!1s0x0:0xd660c4e1fe2b010d!8m2!3d28.2005841!4d-81.439852?hl=en-US" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/poinciana.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/about-administration.pcf">©</a>
      </div>
   </body>
</html>