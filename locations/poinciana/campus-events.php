<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/campus-events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Campus Events</h2>
                        
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/next_electronic_board_assembly_eba_start_dates" target="_blank">
                              
                              
                              <div>
                                 <img src="../../locations/poinciana/43a21bd661946a2cda617063d1166558f58009b2.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>Warehouse Packaging Specialist Start Date (Poinciana Campus) at Poinciana Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>October 02</strong>
                                    
                                    
                                    <p>Interested in a career in warehousing?  If so then this accelerated, one month course
                                       may be just what you are looking for.  For more information, please contact Chris...
                                    </p>
                                    
                                    
                                    </span>
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/international_education_week_2017" target="_blank">
                              
                              
                              <div>
                                 <img src="../../locations/poinciana/c213ce5be4b50dcbc33179d68be44ea398c7cf79.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>International Education Week 2017</span> <br>
                                 
                                 
                                 <span>
                                    <strong>November 13</strong>
                                    
                                    
                                    <p>Join Valencia’s efforts in recognizing international education by hosting an event
                                       at this year’s International Education Week, which will be held college wide Monday,...
                                    </p>
                                    
                                    
                                    </span>
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/spring_2018_classes_begin" target="_blank">
                              
                              
                              <div>
                                 <img src="../../locations/poinciana/3c5d1827d51bcda796deb6c0c5d71a62ff1314c2.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>Spring 2018 Classes Begin</span> <br>
                                 
                                 
                                 <span>
                                    <strong>January 08</strong>
                                    
                                    
                                    <p>Spring 2018 begin     Good luck!</p>
                                    
                                    
                                    </span>
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/drop_deadline_1159pm" target="_blank">
                              
                              
                              <div>
                                 <img src="../../locations/poinciana/3c5d1827d51bcda796deb6c0c5d71a62ff1314c2.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>Drop Deadline (11:59pm)</span> <br>
                                 
                                 
                                 <span>
                                    <strong>January 16</strong>
                                    
                                    
                                    <p>Today is the deadline to drop courses from your current schedule.      Please contact
                                       your Dual Enrollment Advisor for more...
                                    </p>
                                    
                                    
                                    </span>
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/campus-events.pcf">©</a>
      </div>
   </body>
</html>