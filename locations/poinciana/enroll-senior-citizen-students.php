<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/enroll-senior-citizen-students.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Enroll at Poinciana - Senior Citizen Students</h2>
                        
                        <p dir="auto">Students who are classified as a Florida resident for tuition purposes age 60 years
                           or older are eligible for Valencia's tuition waiver up to a maximum of 6 credit hours
                           per term. The waiver includes tuition and standard course fees. The Application for
                           Admission fee and any special fee associated with a course will be the responsibility
                           of the individual requesting the waiver. Registration using this waiver is limited
                           to "audit" only; classes may not be taken for grades or academic credit. Registration
                           for any class is limited to "space availability," and will only be allowed after the
                           date listed in the <a href="http://catalog.valenciacollege.edu/importantdatesdeadlines/" target="_blank">Important Dates and Deadlines</a> Calendar in the online official catalog. Internships and independent study classes
                           are excluded from the waiver. To use the waiver, the senior citizen must register
                           and, on the day of registration, visit a campus Business Office with documentation
                           of age, such as a driver's license, and request the Senior Citizen Waiver. Any special
                           fees must be paid at that time.
                        </p>
                        
                        <p dir="auto">Note: If you register for the course prior to the allowed registration period, or
                           are given a capacity override, you will not be eligible to use the waiver for the
                           course, even if the course is dropped from your record.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" title="Apply Now"></a>
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-6054</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:poincianacampus@valenciacollege.edu">poincianacampus@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>3255 Pleasant Hill Road<br>Kissimmee, FL 34746
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        <a href="https://www.google.com/maps/place/Valencia+College,+Poinciana+Campus/@28.200584,-81.439852,17z/data=!4m5!3m4!1s0x0:0xd660c4e1fe2b010d!8m2!3d28.2005841!4d-81.439852?hl=en-US" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/poinciana.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/enroll-senior-citizen-students.pcf">©</a>
      </div>
   </body>
</html>