<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/campus-directory.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Campus Directory</h2>
                        
                        <p>FIRST FLOOR </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Name</div>
                                 
                                 <div>Room</div>
                                 
                                 <div>Phone</div>
                                 
                                 <div>Hours</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><span>Atlas Lab </span></div>
                                 
                                 <div> 102</div>
                                 
                                 <div>6068</div>
                                 
                                 <div>MTWR 8am-6pm / F 9am-5pm <br>
                                    SAT closed during non-peak registration
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Campus Bookstore</div>
                                 
                                 <div>112A</div>
                                 
                                 <div>4029</div>
                                 
                                 <div>
                                    
                                    <p>MTWR 8am-7pm<br>
                                       F                         8am-4pm<br>
                                       1pm-1:30pm closed
                                    </p>
                                    
                                    <p>Closed on 09/20/2017 for the rest of the Fall Semester. Will Return on Spring 2018
                                       <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>Continuing Education Client Service Center</div>
                                 
                                 <div> 101</div>
                                 
                                 <div>6688</div>
                                 
                                 <div>MTWR 8am-6pm / F 9am-5pm <br>
                                    SAT closed during non-peak registration
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Center for Accelerated Training</div>
                                 
                                 <div>CAT Bldg</div>
                                 
                                 <div>6201/6202</div>
                                 
                                 <div>MTWR 7am-5pm / F 7am-11am<br>
                                    SAT 8:30am-2:30pm
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Culinary Department</div>
                                 
                                 <div>112</div>
                                 
                                 <div>6083</div>
                                 
                                 <div>MTWRF 8am-5pm<br>
                                    SAT closed
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>The Pointé Cafe</div>
                                 
                                 <div>115</div>
                                 
                                 <div>6036</div>
                                 
                                 <div>MTWR 7:30am-5pm / F 7:30am-2pm<br>
                                    SAT closed
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Student Development</div>
                                 
                                 <div>116</div>
                                 
                                 <div>6044</div>
                                 
                                 <div>MTWRF 8am-6pm<br>
                                    SAT closed
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Security </div>
                                 
                                 <div>103</div>
                                 
                                 <div>6500</div>
                                 
                                 <div>MTWR 6:30am-10:30pm<br>
                                    F 6:30am-5:30pm<br>
                                    SAT 7:30am-3:30pm
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Student  Services </div>
                                 
                                 <div>101</div>
                                 
                                 <div>6504</div>
                                 
                                 <div>MTWR 8am-6pm / F 9am-5pm<br>
                                    SAT closed during non-peak registration
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Plant Operations/Custodial Department</div>
                                 
                                 <div>
                                    <p>Building 14</p>
                                 </div>
                                 
                                 <div>6146</div>
                                 
                                 <div>MTWRF 7am-8pm<br>
                                    SAT Closed
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>SECOND FLOOR</p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Name</div>
                                 
                                 <div>Room</div>
                                 
                                 <div>Phone</div>
                                 
                                 <div>Hours</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>OIT  Tech Support Hours </div>
                                 
                                 <div>231E</div>
                                 
                                 <div>6060</div>
                                 
                                 <div>MTWR 7am-9pm / F 8am-5pm<br>
                                    SAT closed
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>The Plaza (Tutoring)</div>
                                 
                                 <div>231</div>
                                 
                                 <div>6118</div>
                                 
                                 <div>MTWR 8am-7pm / F 8am-3pm<br>
                                    SAT Closed
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>THIRD FLOOR </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Name</div>
                                 
                                 <div>Room</div>
                                 
                                 <div>Phone</div>
                                 
                                 <div>Hours</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Assessment / Testing Center</div>
                                 
                                 <div>325</div>
                                 
                                 <div>6052</div>
                                 
                                 <div>MTWR 8am-7pm / F 8am-5pm / SAT closed <br>
                                    Faculty testing MTWR 8am-7pm / F 8am-5pm
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Campus Administration / Staff &amp; Faculty Mailboxes</div>
                                 
                                 <div>302</div>
                                 
                                 <div>6100</div>
                                 
                                 <div>MTWR 8am-8pm<br>
                                    F 8am-5pm <br>
                                    SAT 8am-3pm
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Library</div>
                                 
                                 <div>331</div>
                                 
                                 <div>6027</div>
                                 
                                 <div>MTWR 8am-8pm / F 8am-5pm<br>
                                    SAT 8am-12pm
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Science Department</div>
                                 
                                 <div>326</div>
                                 
                                 <div>6082</div>
                                 
                                 <div>MTWRF 8am-5pm<br>
                                    SAT closed
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Center for Teaching &amp; Learning Innovation</div>
                                 
                                 <div>305</div>
                                 
                                 <div>6131</div>
                                 
                                 <div>MTWR 9am-6pm / F virtual<br>
                                    SAT closed
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../locations/poinciana/locations-directory.html#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/campus-directory.pcf">©</a>
      </div>
   </body>
</html>