<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/programs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Programs</h2>
                        
                        <p>The Poinciana Campus will offer a number of degree and continuing education program
                           options for students. 
                        </p>
                        
                        <p><strong><u>Associate in Arts Degree</u>: </strong>Students who plan to attend a four-year institution after they complete their associate's
                           degree will have the option to choose from the following <a href="../../locations/aadegrees/premajors_list.html">program pathways</a>:
                        </p>
                        
                        <ul>
                           
                           <li>General Studies</li>
                           
                           <li>Health Sciences</li>
                           
                           <li>Biomed/Premed/Biology</li>
                           
                           <li>Math/Engineering</li>
                           
                        </ul>            
                        
                        <p><strong><u>Associate in Science Degree</u>:</strong> Students who plan to go into the workforce or continue on to a Bachelor of Science
                           degree can choose one of the following:
                        </p>
                        
                        <ul>
                           
                           <li><a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/business-administration/">Business Administration (AS to BS)</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/culinary-management/">Culinary Management</a></li>
                           
                           <li>
                              <a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/hospitality-and-tourism-management/">Hospitality &amp; Tourism Management (AS to BS)</a> 
                           </li>
                           
                           <li>
                              <a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/computer-information-technology/">Computer Information Technology</a> 
                           </li>
                           
                           <li>
                              <a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/criminal-justice/">Criminal Justice Technology (AS to BS)</a>   
                           </li>
                           
                        </ul>            
                        
                        <p><strong><u>Continuing Education Programs</u>:</strong>The Poinciana Campus will offer a variety of short-term, non-credit courses for individuals
                           looking to start a new career, change careers, improve their language skills, or simply
                           start a hobby. <a href="../../locations/poinciana/documents/Poinciana-Centers-for-Accelerated-Training-Flyer-Fall-2017.pdf" target="_blank">Current program options</a></p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/intensive-english-program/" target="_blank">Intensive English Program</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/english-as-a-second-language/" target="_blank">ESOL Program</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/languages/spanish/" target="_blank">Spanish Program</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/construction-and-architecture/" target="_blank">Construction (short-term training) </a></li>
                           
                        </ul>            
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/programs.pcf">©</a>
      </div>
   </body>
</html>