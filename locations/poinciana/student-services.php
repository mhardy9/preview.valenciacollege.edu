<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/student-services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Student Services</h2>
                        
                        <p>The following is a list of services to be offered at the Poinciana Campus. Please
                           email us at <a href="mailto:poincianacampus@valenciacollege.edu" rel="noreferrer">poincianacampus@valenciacollege.edu</a> if you have any questions.
                        </p>
                        
                        <p>GENERAL STUDENT SERVICES </p>
                        
                        <ul>
                           
                           <li>Advising</li>
                           
                           <li>Answer Center </li>
                           
                           <li>Atlas Lab </li>
                           
                           <li>Business Services</li>
                           
                           <li>Financial Aid</li>
                           
                           <li>Security (Student IDs)</li>
                           
                           <li>Textbook Purchases </li>
                           
                        </ul>            
                        
                        <p>LEARNING SUPPORT SERVICES </p>
                        
                        <ul>
                           
                           <li>Assessment &amp; Testing</li>
                           
                           <li>Computer Labs</li>
                           
                           <li>
                              <a href="../../locations/library/index.html">Library Services</a> 
                           </li>
                           
                           <li>Tutoring Services (English, EAP, Science, Math)  </li>
                           
                        </ul>            
                        
                        <p>STUDENT DEVELOPMENT </p>
                        
                        <ul>
                           
                           <li>Skillshops</li>
                           
                           <li>Student Clubs &amp; Organizations</li>
                           
                           <li>UFit</li>
                           
                           <li>Valencia Volunteers  </li>
                           
                        </ul>            
                        
                        <p>STUDENT RESOURCES </p>
                        
                        <ul>
                           
                           <li><a href="http://net4.valenciacollege.edu/promos/internal/blackboard.cfm">BlackBoard Learn </a></li>
                           
                           <li><a href="http://valenciacollege.edu/schedule/">Credit Course Search</a></li>
                           
                           <li><a href="../../locations/catalog/index.html">College Catalog</a></li>
                           
                           <li><a href="../../locations/students/disputes/index.html">Conflict Resolution</a></li>
                           
                           <li><a href="http://faculty.valenciacollege.edu/">Faculty Websites</a></li>
                           
                           <li><a href="../../locations/internship/index.html">Internship and Workforce Services</a></li>
                           
                           <li><a href="../../locations/security/parking.html">Parking Passes </a></li>
                           
                           <li><a href="../../locations/service-learning/index.html">Service Learning </a></li>
                           
                           <li><a href="../../locations/student-development/default.html">Student Development</a></li>
                           
                           <li><a href="../../locations/international/studyabroad/students/index.html">Study Abroad &amp; Global Experiences            </a></li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/student-services.pcf">©</a>
      </div>
   </body>
</html>