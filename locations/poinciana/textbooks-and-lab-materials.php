<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/textbooks-and-lab-materials.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Textbooks and Lab Materials</h2>
                        
                        <ul>
                           
                           <li><a href="../../locations/poinciana/textbooks-and-lab-materials.html#credit">Textbooks  for Credit Programs </a></li>
                           
                           <li><a href="../../locations/poinciana/textbooks-and-lab-materials.html#DE">Textbooks  for Dual Enrollment Students </a></li>
                           
                           <li>
                              <a href="../../locations/poinciana/textbooks-and-lab-materials.html#lab">Lab  Materials</a><strong> </strong>
                              
                           </li>
                           
                           <li><a href="../../locations/poinciana/textbooks-and-lab-materials.html#classroom">Classroom  Supplies</a></li>
                           
                           <li><a href="../../locations/poinciana/textbooks-and-lab-materials.html#financial">Using  Financial Aid for Textbooks</a></li>
                           
                        </ul>
                        
                        <p><a name="credit" id="credit"></a></p>
                        
                        <div>
                           
                           <div>
                              
                              <h3>Textbooks  for Credit Programs</h3>
                              
                              <p>Students at the Poinciana Campus will place their textbook  order online at the Valencia
                                 campus store.   Click Textbooks, select the term and campus, and then the course prefix
                                 to search for books. You can choose to have the books shipped to your home  address
                                 or sent to the campus.  Books  will be delivered to the Poinciana Campus prior to
                                 the start of classes.  Please note the days of operations below  since there is not
                                 a permanent bookstore at the campus.  Pick up will be in <strong>building 1 room 112A</strong>, and you will need to bring your course schedule and a  photo ID. Credit cards, checks,
                                 and cash will be accepted.  Please allow 48-72 hours to process online  orders before
                                 picking up or shipping.
                              </p>
                              
                              <p>Bookstore Operations at Poinciana  Fall 2017: </p>
                              
                              <ul>
                                 
                                 <li>On site Purchases: August 21st  - September 8th</li>
                                 
                                 <li>Online Purchases:  <a href="http://www.valenciabookstores.com/valencc/">Campus  Store Website</a>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <p><a name="DE" id="DE"></a></p>
                              
                              <h3>Textbooks  for Dual Enrollment Students</h3>
                              
                              <p>Students must pick up their  textbooks at the Osceola County School District <a href="http://www.osceolaschools.net/cms/One.aspx?portalId=567190&amp;pageId=1721736">Dual Enrollment Bookstore</a> located at 803 Bill Beck Blvd., Kissimmee, FL, portable 803C to pick up books  and/or
                                 vouchers for books.  DO NOT GO TO  VALENCIA TO PICK UP YOUR BOOKS. For hours of operation
                                 and what to bring  to the SDOC DE bookroom visit the link above.
                              </p>
                              
                              <p><a name="lab" id="lab"></a></p>
                              
                              <h3> Lab  Materials </h3>
                              
                              <ul>
                                 
                                 <li>
                                    <strong>CULINARY - </strong>Students  enrolled in the culinary lab courses will need to purchase a knife set and
                                    a  chef uniform.
                                 </li>
                                 
                                 <li>
                                    <strong>MATH </strong> - Calculator for Math : 
                                 </li>
                                 
                                 <ul>
                                    
                                    <li><strong>TI-84 plus </strong></li>
                                    
                                    <li>For all College Algebra and College Math classes: <strong>MyMatlab access code</strong>
                                       
                                    </li>
                                    
                                    <li>For all Statistics courses: <strong>MyStatlab  access code</strong>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 <li>
                                    <strong>SCIENCE</strong> - Students  are required to wear closed-toe/closed-heel shoes and a lab coat that
                                    covers at  least to the knees.  Safety googles will be provided; however, students
                                    may choose to bring their own to the lab. Optional: scientific  calculator.
                                 </li>
                                 
                              </ul>
                              
                              <p><a name="classroom" id="classroom"></a></p>
                              
                              <h3>Classroom  Supplies</h3>
                              
                              <p>Classroom  supplies will be available in the temporary bookstore at the Poinciana
                                 Campus.  There will also be a classroom  supplies vending machine on campus located
                                 in The Pointe café in building 1 on  the first floor.
                              </p>
                              
                              <p><a name="financial" id="financial"></a></p>
                              
                              <h3> Using  Financial Aid for Textbooks</h3>
                              
                              <p>Students who would like to use  Federal Financial Aid for textbooks must submit an
                                 authorization through Atlas  prior to picking up your books at the campus.   Once
                                 submitted, you must wait for the financial aid update between Atlas  and the Campus
                                 Store.  Scheduled updates  are conducted every day at 6AM, 11AM, and 3PM.
                              </p>
                              
                              <ul>
                                 
                                 <ul>
                                    
                                    <li>Log into <a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=https%3A%2F%2Fptl5pd-prod.valenciacollege.edu%2Fc%2Fportal%2Flogin">Atlas</a>
                                       
                                    </li>
                                    
                                    <li>Click on the STUDENTS tab</li>
                                    
                                    <li>Click on FINANCIAL AID (under  STUDENT RESOURCES)</li>
                                    
                                    <li>Click on FEDERAL STUDENT AID PAYMENT  AUTHORIZATION</li>
                                    
                                 </ul>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" title="Apply Now"></a>
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-6054</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:poincianacampus@valenciacollege.edu">poincianacampus@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>3255 Pleasant Hill Road<br>Kissimmee, FL 34746
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        <a href="https://www.google.com/maps/place/Valencia+College,+Poinciana+Campus/@28.200584,-81.439852,17z/data=!4m5!3m4!1s0x0:0xd660c4e1fe2b010d!8m2!3d28.2005841!4d-81.439852?hl=en-US" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/poinciana.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/textbooks-and-lab-materials.pcf">©</a>
      </div>
   </body>
</html>