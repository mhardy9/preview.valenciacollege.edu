<ul>
	<li><a href="/locations/poinciana/">Poinciana</a></li>
	<li class="submenu"><a class="show-submenu" href="/locations/poinciana/about-poinciana-campus.php">About <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/locations/poinciana/about-administration.php">Administration</a></li>
			<li><a href="/locations/poinciana/locations-directory.php">Campus Directory</a></li>
			<li><a href="/locations/poinciana/locations-events.php">Campus Events</a></li>
		</ul>
		</li>
	<li class="submenu"><a class="show-submenu" href="/locations/poinciana/enroll.php">Enroll at Poinciana <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/locations/poinciana/enroll-first-time-students.php">First Time Students</a></li>
			<li><a href="/locations/poinciana/enroll-returning-students.php">Returning Students</a></li>
			<li><a href="/locations/poinciana/enroll-senior-citizen-students.php">Senior Citizen Students</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="javascript:void(0);">Student Info <span class="caret" aria-hidden="true"></span></a>
		<ul>
			
	<li><a href="/locations/poinciana/programs.php">Programs</a></li>
	<li><a href="/locations/poinciana/course-package-options.php">Course Packages</a></li>
	<li><a href="/locations/poinciana/scholarships.php">Scholarships</a></li>
	<li><a href="/locations/poinciana/student-development.php">Student Development</a></li>
	<li><a href="/locations/poinciana/student-services.php">Student Services</a></li>
	<li><a href="/locations/poinciana/textbooks-and-lab-materials.php">Textbooks and Lab Materials</a></li>
			</ul>
	</li>
	<li><a href="/locations/poinciana/faculty-and-staff.php">Faculty and Staff</a></li>
	<li><a href="/locations/poinciana/faculty-resources.php">Faculty Resources</a></li>
	<li><a href="http://net4.valenciacollege.edu/forms/poinciana/contact.php" target="_blank">Contact Us</a></li>


</ul>
