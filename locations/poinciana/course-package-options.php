<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/course-package-options.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Poinciana Course Package Options</h2>
                        
                        <p><strong>Course  Packages: Associate in Arts (A.A.)</strong></p>
                        
                        <ul>
                           
                           <li><a href="../../locations/poinciana/course-package-options.html#aa_full_time">Full  Time</a></li>
                           
                           <li><a href="../../locations/poinciana/course-package-options.html#aa_part_time">Part  Time</a></li>
                           
                        </ul>
                        
                        <p><strong>Course  Packages: Associate in Science (A.S.)</strong></p>
                        
                        <ul>
                           
                           <li><a href="../../locations/poinciana/course-package-options.html#as_full_time">Full  Time</a></li>
                           
                           <li><a href="../../locations/poinciana/course-package-options.html#as_part_time">Part  Time</a></li>
                           
                        </ul>
                        
                        <p>Students who are enrolling in college for the first time or dual enrollment students
                           can elect to choose from a variety of Associate in Arts and Associate in Science course
                           packages at the Poinciana Campus.  Course packages are offered in a variety of scheduling
                           options and they contain all the courses that students need to complete their degree
                           from start to finish at Valencia.
                        </p>
                        
                        
                        <p><a href="../../locations/poinciana/documents/Poinciana-Course-Package-Options-2017-18.pdf" target="_blank">Poinciana Course Package Options Fall 2017</a></p>
                        
                        <p>Students who enroll in a FULL TIME Course Package for the fall 2017 term will receive
                           a $500 scholarship.
                        </p>
                        
                        <p>To view the contents of a course package, select the degree area and then the time
                           of day when you would like to study from the lists below.  Please note that new students
                           should come to the New Student Orientation prepared to select and enroll in a course
                           package.  Dual enrollment students will need to contact an academic advisor to complete
                           their enrollment.
                        </p>
                        
                        
                        <h3>Associate in Arts (A.A.) Course Packages</h3>
                        <a name="aa_full_time" id="aa_full_time"></a>
                        
                        <h4>Full Time</h4>
                        
                        <ul>
                           
                           <li><a href="../../locations/poinciana/documents/Bio-Premed-FT-MWF-Afternoon.pdf" target="_blank"> Bio Premed  - Afternoon - MWF</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Bio-Premed-FT-MTR-Morning-Afternoon.pdf" target="_blank"> Bio Premed  - Morning and Afternoon - MTR</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/EAP-FT-EVENING-MTWR.pdf" target="_blank">General Studies (with EAP) - Afternoon - MTWR</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-FT-MTWR-Afternoon.pdf" target="_blank">General Studies - Afternoon - MTWR</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-FT-MTWRF-Afternoon.pdf" target="_blank">General Studies - Afternoon - MTWRF</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-FT-MTWRF-Morning.pdf" target="_blank">General Studies - Early Morning - MTWRF</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-FT-TR-Morning-Afternoon.pdf" target="_blank">General Studies - Morning and Afternoon - TR</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-FT-MWF-Morning-Afternoon.pdf" target="_blank">General Studies - Morning and Afternoon - MWF</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Health-Science-FT-MWF-Morning.pdf" target="_blank"> Health Science - Morning - MWF</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Health-Science-FT-TRF-Afternoon.pdf" target="_blank"> Health Science - Afternoon - TRF</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Math-Engineering-FT-MW-Afternoon-Evening.pdf" target="_blank">Math Engineering - Afternoon and Evening - MW</a></li>
                           
                        </ul>
                        <a name="aa_part_time" id="aa_part_time"></a>
                        
                        <h4>Part Time</h4>
                        
                        <ul>
                           
                           <li><a href="../../locations/poinciana/documents/Bio-Premed-PT-FS-Weekend.pdf" target="_blank">Bio Premed  - Weekend - FS</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-PT-MW-Afternoon.pdf" target="_blank">General Studies - Afternoon - MW</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-PT-TRF-Afternoon.pdf" target="_blank">General Studies - Afternoon - TRF</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-PT-MW-Evening.pdf" target="_blank">General Studies - Evening - MW</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-PT-TWR-Evening-prep.pdf" target="_blank">General Studies - Evening - TWR</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-PT-FS-Weekend.pdf" target="_blank">General Studies - Weekend - FS</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/General-Studies-PT-S-Weekend.pdf" target="_blank">General Studies - Weekend - S</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Health-Science-PT-TR-Afternoon-Prep.pdf" target="_blank">Health Science - Afternoon - TR</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Health-Science-PT-FS-Weekend-Prep.pdf" target="_blank">Health Science - Weekend - FS</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Health-Science-PT-MTR-Evening.pdf" target="_blank">Health Science - Evening - MTR</a></li>
                           
                        </ul>
                        
                        
                        <h3>Associate in Science (A.S.) Course Packages </h3>
                        <a name="as_full_time" id="as_full_time"></a>
                        
                        <h4>Full Time</h4>
                        
                        <ul>
                           
                           <li><a href="../../locations/poinciana/documents/Business-Admin-FT-TR-Afternoon.pdf" target="_blank">Business - Afternoon - TR</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Business-Admin-FT-MTR-Morning.pdf" target="_blank">Business - Morning - MTR</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Comp-Info-Tech-FT-M-Morning.pdf" target="_blank">Computer Info Tech - Morning - M+Online</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Crim-Justice-FT-TR-Evening.pdf" target="_blank">CriminalJust - Evening - TR+Online</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Crim-Justice-FT-MW-Morning.pdf" target="_blank">CriminalJust - Morning - MW+Online</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Culinary-FT-MT-Morning-Afternoon.pdf" target="_blank">Culinary - Morning and Afternoon - MT</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Hospitality-FT-TR-Morning-Prep.pdf" target="_blank">Hospitality - Morning - TR+Online</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Hospitality-FT-MTW-Evening.pdf" target="_blank">Hospitality - Evening - MTW+Online</a></li>
                           
                        </ul>
                        <a name="as_part_time" id="as_part_time"></a>
                        
                        <h4>Part Time</h4>
                        
                        <ul>
                           
                           <li><a href="../../locations/poinciana/documents/Business-Admin-PT-FS-Weekend.pdf" target="_blank">Business - Weekend - FS</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Comp-Info-Tech-PT-T-Evening.pdf" target="_blank">Computer Info Tech - Evening - T+Online</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Crim-Justice-PT-S-Weekend.pdf" target="_blank">Criminal Just - Weekend - S+Online</a></li>
                           
                           <li><a href="../../locations/poinciana/documents/Hospitality-PT-S-Weekend.pdf" target="_blank">Hospitality - Weekend - S+Online</a></li>
                           
                        </ul>
                        
                        <p>If you need more information about Course Packages, or if  you have any Poinciana
                           Campus related questions or concerns please email us at <a href="mailto:poincianacampus@valenciacollege.edu">poincianacampus@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" title="Apply Now"></a>
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-6054</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:poincianacampus@valenciacollege.edu">poincianacampus@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>3255 Pleasant Hill Road<br>Kissimmee, FL 34746
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        <a href="https://www.google.com/maps/place/Valencia+College,+Poinciana+Campus/@28.200584,-81.439852,17z/data=!4m5!3m4!1s0x0:0xd660c4e1fe2b010d!8m2!3d28.2005841!4d-81.439852?hl=en-US" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/poinciana.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/course-package-options.pcf">©</a>
      </div>
   </body>
</html>