<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College | Valencia College</title>
      <meta name="Description" content="A multi-campus college dedicated to the premise that educational opportunities are necessary to bring together the diverse forces in society"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li>Poinciana</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               
               		
               <div class="container margin-30" role="main">
                  			
                  <h2>You can get an education in your own neighborhood</h2>
                  			
                  <p>Valencia College is expanding our Osceola County presence with a new Poinciana Campus.
                     This state-of-the-art facility will house a robust selection of classrooms, amenities,
                     labs and more - designed to ensure that Poinciana residents have easy access to all
                     Valencia has to offer.
                  </p>
                  
                  			
                  <p>Proposed or Future Valencia College - Poinciana Campus Site (pending Substantive Change
                     report and approval by SACSCOC).
                  </p>
                  
                  			
                  <p>3255 Pleasant Hill Road<br>Kissimmee, FL 34746<br><a href="mailto:poincianacampus@valenciacollege.edu">poincianacampus@valenciacollege.edu</a></p>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <h3>Information</h3>     
                        					
                        <ul>
                           						
                           <li><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a></li>
                           						
                           <li><a href="about-poinciana-campus.php" title="About Poinciana Campus">About Poinciana Campus</a></li>
                           						
                           <li><a href="programs.php" title="Poinciana Programs">Poinciana Programs</a></li>
                           						
                           <li><a href="/locations/map/poinciana.php" title="Poinciana Driving Directions" target="_blank">Driving Directions</a></li>                  
                           					
                        </ul>
                        				
                     </div>
                     
                     				
                     <div class="col-md-4">
                        					
                        <h3> Valencia News</h3>
                        					
                        <p>Poinciana News Feed goes here</p>
                        				
                     </div>
                     
                     				
                     <div class="col-md-4">
                        
                        					
                        <h3>Upcoming Events</h3>
                        					
                        <p>Poinciana events goes here</p>    
                        				
                     </div>
                     			
                  </div>
                  
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/index.pcf">©</a>
      </div>
   </body>
</html>