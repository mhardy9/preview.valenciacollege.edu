<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/faculty-and-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Faculty and Staff</h2>
                        
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           <link href="../../locations/includes/jquery/css/colorbox_valencia.css" rel="stylesheet" type="text/css">
                           
                           
                           
                           
                           
                           
                           <h3>Staff</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                    <div>Name</div>
                                    
                                    <div>Department</div>
                                    
                                    <div>Position</div>
                                    
                                    <div>Phone</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg6001277121751532984.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=kplinske.html">Kathleen Plinske</a></div>
                                    
                                    <div>Campus President OSC</div>
                                    
                                    <div>Campus President, OSC/LNC/PNC</div>
                                    
                                    <div>(407) 582-4975</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-4175989704616040110.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mmcgowa1.html">Mary McGowan</a></div>
                                    
                                    <div>Dean of Students PNC</div>
                                    
                                    <div>Dir, Student Services, PNC</div>
                                    
                                    <div>(407) 582-4980</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg7945171577345162486.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=malicea5.html">Melissa Alicea</a></div>
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>Coord, Financial Aid Services</div>
                                    
                                    <div>(407) 582-6077</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-1214610082040350044.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=bbaralt.html">Brenda Baralt</a></div>
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>Executive Assistant</div>
                                    
                                    <div>(407) 582-4230</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-5425285850793601360.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jcordova3.html">Julix Cordova Rivera</a></div>
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>Student Services Advisor</div>
                                    
                                    <div>(407) 682-4140</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg7317739551666863435.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jfernandez46.html">Jenniffer Fernandez Toribio</a></div>
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>Student Services Advisor</div>
                                    
                                    <div>(407) 682-4095</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-2630446787289107090.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=aguzman54.html">Amaris Guzman</a></div>
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>Mgr, Community Outreach</div>
                                    
                                    <div>(407) 582-6070</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=ppalaciosrodrigue.html">Paz Palacios Rodriguez</a></div>
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>Student Svcs Representative</div>
                                    
                                    <div>(321) 682-4142</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg3407863494738540770.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jrobertson.html">Jennifer Robertson</a></div>
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>Executive Dean, PNC</div>
                                    
                                    <div>(407) 582-6050</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-8560132518851638660.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mrojas15.html">Melissa Rojas</a></div>
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>Student Services Advisor</div>
                                    
                                    <div>(321) 682-4943</div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Photo" border="0" height="40" src="../../locations/poinciana/_cfimg2672747413699743433.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jaustin27.html">Joshua Austin</a></div>
                                    
                                    <div>Learning Support Office PNC</div>
                                    
                                    <div>Mgr, Learning Support Services</div>
                                    
                                    <div>(407) 582-6155</div>
                                    
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Faculty</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                    <div>Name</div>
                                    
                                    <div>Department</div>
                                    
                                    <div>Website</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=avagle2.html">Angelica Vagle</a></div>
                                    
                                    
                                    <div>Chemistry EAC</div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=brapp2.html">Brian Rapp</a></div>
                                    
                                    
                                    <div>English PNC</div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-6241407400508311367.PNG" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jdenlinger.html">Jennifer Denlinger</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>
                                       
                                       
                                       <a href="http://frontdoor.valenciacollege.edu/?jdenlinger" target="_blank"> http://frontdoor.valenciacollege.edu/?jdenlinger</a> <br><a href="http://frontdoor.valenciacollege.edu?jdenlinger" target="_blank">http://frontdoor.valenciacollege.edu?jdenlinger</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=tgitto.html">Tammy Gitto</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg8825013011532811028.PNG" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=elaurent1.html">Eunice Laurent</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>
                                       
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?elaurent1" target="_blank">http://frontdoor.valenciacollege.edu?elaurent1</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg8542450835720802479.PNG" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=smurugan1.html">Sam Murugan</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>
                                       
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?smurugan1" target="_blank">http://frontdoor.valenciacollege.edu?smurugan1</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg3935797553413099717.PNG" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mrobbins9.html">Michael Robbins</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>
                                       
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?mrobbins9" target="_blank">http://frontdoor.valenciacollege.edu?mrobbins9</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg4832761825483667940.PNG" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jvalladares1.html">Jorge Valladares</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    <div>
                                       
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?jvalladares1" target="_blank">http://frontdoor.valenciacollege.edu?jvalladares1</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg7765334532445197630.PNG" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=kbest7.html">Karene Best</a></div>
                                    
                                    
                                    <div>Library Science PNC</div>
                                    
                                    <div>
                                       
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?kbest7" target="_blank">http://frontdoor.valenciacollege.edu?kbest7</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg1073897209362914671.PNG" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mobeidat.html">Mohammad Obeidat</a></div>
                                    
                                    
                                    <div>Mathematics PNC</div>
                                    
                                    <div>
                                       
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?mobeidat" target="_blank">http://frontdoor.valenciacollege.edu?mobeidat</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-5188187063116865808.PNG" width="40">
                                       
                                    </div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=gcuan.html">Gabrielle Cuan</a></div>
                                    
                                    
                                    <div>Speech PNC</div>
                                    
                                    <div>
                                       
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?gcuan" target="_blank">http://frontdoor.valenciacollege.edu?gcuan</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Adjunct (Part Time Faculty)</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                    <div>Name</div>
                                    
                                    <div>Department</div>
                                    
                                    <div>Website</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=dsirois.html">Debbie Sirois</a></div>
                                    
                                    
                                    <div>Accounting PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mpinho1.html">Marcos Pinho</a></div>
                                    
                                    
                                    <div>Biological Science WEC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?mpinho1" target="_blank">http://frontdoor.valenciacollege.edu?mpinho1</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jchesler.html">Jamie Chesler</a></div>
                                    
                                    
                                    <div>Business OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=rnascimento1.html">Ricardo Nascimento</a></div>
                                    
                                    
                                    <div>Business PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-4115865215312402995.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=dstendel.html">David Stendel</a></div>
                                    
                                    
                                    <div>Computer Program OSC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?dstendel" target="_blank">http://frontdoor.valenciacollege.edu?dstendel</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mhernandez5.html">Manny Hernandez</a></div>
                                    
                                    
                                    <div>Criminal Justice Institute SPS</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=emartinez5.html">Edward Martinez</a></div>
                                    
                                    
                                    <div>Criminal Justice Tech OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jwatson59.html">Jerry Watson</a></div>
                                    
                                    
                                    <div>Criminal Justice Tech WEC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?jwatson59" target="_blank">http://frontdoor.valenciacollege.edu?jwatson59</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=ssoto10.html">Sherly Soto Ortiz</a></div>
                                    
                                    
                                    <div>EAP PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=eerbe.html">Robert Erbe</a></div>
                                    
                                    
                                    <div>Economics OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jtimmerman5.html">John Timmerman</a></div>
                                    
                                    
                                    <div>Education OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=tbirol.html">Tom Birol</a></div>
                                    
                                    
                                    <div>English PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg790094617038486360.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=ptermine.html">Prudence Termine</a></div>
                                    
                                    
                                    <div>English PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jalamo8.html">Juan Alamo</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?jalamo8" target="_blank">http://frontdoor.valenciacollege.edu?jalamo8</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=barmstrong12.html">Brandon Armstrong</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=marroyoosorio.html">Maelyn Arroyo Osorio</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg539613449314936821.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=jblackburn11.html">Jennifer Blackburn</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=lbrooks30.html">Laurentia Brooks</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=cchalifoux.html">Carolyn Chalifoux</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=bcolon27.html">Brenda Colon</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?bcolon27" target="_blank">http://frontdoor.valenciacollege.edu?bcolon27</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=acook.html">Anthony Cook</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=acrawford26.html">Angel Crawford</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg4599956313516302380.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=cdavis194.html">Chad Davis</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-871868025812022871.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=gfigueroamartorell1.html">Griselle Figueroa Martorell</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=wkeynee.html">William Key-Nee</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=rkirk7.html">Regan Kirk</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=gmairhernandez.html">Gretchka Mair Hernandez</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mnolasco2.html">Miguel Nolasco</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg489036462565600209.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=apagan36.html">Angel Pagan</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?apagan36" target="_blank">http://frontdoor.valenciacollege.edu?apagan36</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg1143849473341389070.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mpuertariera.html">Maria Puerta Riera</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu/?mpuertariera" target="_blank">http://frontdoor.valenciacollege.edu/?mpuertariera</a> <a href="http://frontdoor.valenciacollege.edu?mpuertariera" target="_blank">http://frontdoor.valenciacollege.edu?mpuertariera</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=eramos71.html">Elia Ramos</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=lramosvoigt.html">Lisette Ramos-Voigt</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=gsapijaszko.html">Genevieve Sapijaszko</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=msherlock.html">Mark Sherlock</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mveach.html">Matthew Veach</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg1505585398612588815.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=wwebb8.html">Whitley Webb</a></div>
                                    
                                    
                                    <div>Exec Dean's Office PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=caleksic.html">Christina Aleksic</a></div>
                                    
                                    
                                    <div>Health Sciences WEC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=asoto39.html">Aisha Soto</a></div>
                                    
                                    
                                    <div>Humanities EAC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=mchristopher2.html">Mike Christopher</a></div>
                                    
                                    
                                    <div>Humanities WEC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?mchristopher2" target="_blank">http://frontdoor.valenciacollege.edu?mchristopher2</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=dmcbride.html">Dianna McBride</a></div>
                                    
                                    
                                    <div>Humanities WEC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?dmcbride" target="_blank">http://frontdoor.valenciacollege.edu?dmcbride</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=tblack21.html">Tom Black</a></div>
                                    
                                    
                                    <div>Math OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=yiwai1.html">Yamato Iwai</a></div>
                                    
                                    
                                    <div>Math OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=cmontoy1.html">Camilo Montoya</a></div>
                                    
                                    
                                    <div>Math OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=corozcomoret.html">Cirilo Orozco Moret</a></div>
                                    
                                    
                                    <div>Math OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=lmarrerozamora.html">Liza Pettett</a></div>
                                    
                                    
                                    <div>Math OSC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?lmarrerozamora" target="_blank">http://frontdoor.valenciacollege.edu?lmarrerozamora</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=nstarr1.html">Nina Starr</a></div>
                                    
                                    
                                    <div>Math OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=pstpreux.html">Pierre Saint-Preux</a></div>
                                    
                                    
                                    <div>Mathematics PNC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?pstpreux" target="_blank">http://frontdoor.valenciacollege.edu?pstpreux</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=sgreiter.html">Sue Greiter</a></div>
                                    
                                    
                                    <div>New Student Experience OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/_cfimg-6714570275727128194.PNG" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=tmarti14.html">Tiffany Lowther</a></div>
                                    
                                    
                                    <div>New Student Experience OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=tgallagher7.html">Thomas Gallagher</a></div>
                                    
                                    
                                    <div>Political Science OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=awilkerson1.html">Amanda Wilkerson</a></div>
                                    
                                    
                                    <div>Political Science OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=rziegler6.html">Ronald Ziegler</a></div>
                                    
                                    
                                    <div>Political Science OSC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=econtaste.html">Edlin Contaste</a></div>
                                    
                                    
                                    <div>Psychology OSC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?econtaste" target="_blank">http://frontdoor.valenciacollege.edu?econtaste</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=tjimenez4.html">Theresa Jimenez</a></div>
                                    
                                    
                                    <div>Psychology PNC</div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><img alt="Faculty Photo" border="0" height="40" src="../../locations/poinciana/default_profile.jpg" width="40"></div>
                                    
                                    <div><a href="../../locations/includes/UserInfo.cfm-username=pparadise.html">Phyllis Paradise</a></div>
                                    
                                    
                                    <div>Speech LNC</div>
                                    
                                    
                                    <div> 
                                       
                                       <a href="http://frontdoor.valenciacollege.edu?pparadise" target="_blank">http://frontdoor.valenciacollege.edu?pparadise</a>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/faculty-and-staff.pcf">©</a>
      </div>
   </body>
</html>