<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poinciana Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/poinciana/scholarships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/poinciana/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Poinciana Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/poinciana/">Poinciana</a></li>
               <li>Poinciana Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Scholarships</h2>
                        
                        <p>Valencia College is proud to announce its <strong>Poinciana Campus Inaugural Scholarship</strong> <strong>Award</strong>.&nbsp; All students who enroll in Fall 2017 in a full-time cohort program at the Poinciana
                           Campus will earn a $500 scholarship. To qualify for the scholarship, students must:
                           
                        </p>
                        
                        <ul>
                           
                           <li>Enroll in a full-time course package designed for new students </li>
                           
                           <li>Complete the FASFA (if eligible) </li>
                           
                           <li>Submit tuition payment by August 11, 2017 </li>
                           
                        </ul>
                        
                        <h4>For more information on this scholarship award, please send inquiries to <a href="mailto:poincianacampus@valenciacollege.edu" rel="noreferrer">poincianacampus@valenciacollege.edu</a>.
                        </h4>
                        
                        <p>Media inquiries should be directed to Carol Traynor at <a href="mailto:ctraynor@valenciacollege.edu" rel="noreferrer">ctraynor@valenciacollege.edu</a>.
                        </p>
                        
                        <h3>Scholarships</h3>
                        
                        <h4>Culinary Scholarship</h4> 
                        <strong>$1,500 for Full-time Culinary Students</strong> 
                        
                        <p>To be eligible, students must enroll in the Culinary full-time culinary course package
                           (which meets Mondays and Tuesdays) at the Poinciana Campus. Interested students should
                           call 407-582-4980.
                        </p>
                        
                        <p><a href="tel:407-582-4980">407-582-4980</a></p>
                        
                        
                        <h4>Poinciana Area Council Scholarship</h4>
                        
                        <p> Deadline February 28</p>
                        
                        <p><a href="http://business.kissimmeechamber.com/events/details/poinciana-area-council-scholarship-application-deadline-03-31-2016-803" target="_blank">Learn More</a></p>
                        
                        
                        <h4>Valencia Foundation Scholarships</h4>
                        
                        <p> Fall semester deadline - June 30<br>
                           Spring semester deadline - November 30<br>
                           Summer semester deadline - March 31 
                        </p>
                        
                        <p><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466" target="_blank">Learn More</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h5>Valencia College Poinciana Campus Culinary Scholarship</h5> 
                        <img alt="Poinciana Campus Culinary Scholarship. Interested students should call 407-582-4980." height="150" src="../../locations/poinciana/Culinary-Students018.jpg" width="245">
                        
                        
                        <div> 
                           
                           <p> 
                              <strong>$1,500 for Full-time Culinary Students</strong> 
                              
                           </p>
                           
                           <p>To be eligible, students must enroll in the Culinary full-time culinary course package
                              (which meets Mondays and Tuesdays) at the Poinciana Campus. 
                              <strong>Interested? Call <a href="tel:407-582-4980">407-582-4980</a></strong></p> 
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/poinciana/scholarships.pcf">©</a>
      </div>
   </body>
</html>