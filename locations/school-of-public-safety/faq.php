<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>School of Public Safety | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li>School of Public Safety</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../locations/school-of-public-safety/index.html"></a>
                        
                        <h2><strong>Frequently     Asked Questions</strong></h2>
                        
                        <p><strong>BASIC ACADEMY </strong></p>
                        
                        <p><strong>Q:     What programs do you offer? </strong></p>
                        
                        <p>A:     The Criminal Justice Institute (CJI) is a state certified Law Enforcement and
                           Corrections training academy. CJI offers the Law Enforcement and Corrections     Certification
                           Programs and Advanced/Specialized courses for certified, sworn     officers. 
                        </p>
                        
                        <p><strong>Q: Can the CJI provide me with a Valencia College Transcript? </strong></p>
                        
                        <p>A: Yes, we can provide both your credit and vocational transcripts. However, you can
                           only get  Criminal Justice Institute (Corrections or Law Enforcement) transcripts
                           from the Criminal Justice Institute. <strong>Credit and vocational coursework will print as two separate transcripts</strong> (each transcript requires a separate transcript request fee). 
                        </p>
                        
                        <p><a href="http://valenciacollege.edu/public-safety/documents/TranscriptRequestForm.docx"><img alt="transcript request link" border="0" height="97" src="../../locations/school-of-public-safety/Transcript-Request-Button.png" width="116"></a></p>
                        
                        
                        <p>You will need to print and complete the Transcript Request Form (<strong>any incomplete forms and forms without payment will not be processed</strong>.) The form must be submitted to the <u>Criminal Justice Institute</u> with  payment attached (cash, check or money order). Please make checks or money
                           orders payable to <em>Valencia College</em>. If paying with cash, you must provide the exact amount. The $3.00 per transcript
                           fee must be paid before your request will be processed.
                        </p>
                        
                        <p>This form can be submitted in person or mailed in with the transcript fee to:</p>
                        
                        <p>Valencia College</p>
                        
                        <p>School of Public Safety</p>
                        
                        <p>Attn: Transcript Request </p>
                        
                        <p>8600 Valencia College Lane</p>
                        
                        <p>Orlando, FL. 32825</p>
                        
                        
                        <p><em>Transcript requests are typically processed within 2 business days.</em></p>
                        
                        
                        <p><strong>Q:     Do I need to be a student at Valencia to apply to the Criminal Justice Institute?
                              </strong></p>
                        
                        <p>A:     Yes. You must apply to the college before you can enroll in the Criminal Justice
                           Institute. You can apply online at <a href="../../locations/students/admissions-records/index.html"><strong>http://valenciacollege.edu/admissions-records</strong></a></p>
                        
                        <p><strong>Q: What is the 
                              cost of tuition? </strong></p>
                        
                        <p>A:     Tuition for Florida residents is $2,700 for the Law Enforcement Academy and
                           $1,500 for the Corrections Academy. Out of state tuition is approximately $8000  
                           for the Law Enforcement Academy and $4000 for the Corrections Academy (cost subject
                           to change). 
                        </p>
                        
                        <p><strong>Q: How do I qualify 
                              for Florida residency? </strong></p>
                        
                        <p>When     completing your Valencia Admissions Application, you must prove Florida residency
                           by providing <u><strong><a href="../../locations/admissions-records/forms.html">supporting documents</a></strong></u> that are dated <strong>at least 12 consecutive months</strong>     prior to the term for which the residency is sought. 
                        </p>
                        
                        
                        <p><strong>Q:     Is Financial Aid available for the Criminal Justice Institute programs? </strong></p>
                        
                        <p>A:     The Law Enforcement Academy is the only Financial Aid eligible program that
                           we offer. Refer to Valencia 's <u><strong><a href="../../locations/finaid/gettingstarted/getting_started.html">Financial     Aid Department</a></strong></u> for more information on applying. Financial     Aid should not be applied for until
                           you have been accepted into the Law Enforcement     Academy . 
                        </p>
                        
                        <p><a href="http://valenciacollege.financialaidtv.com/">Valencia's Financial Aid TV </a></p>
                        
                        <p><strong>Q: What is the 
                              deadline to apply to the Criminal Justice Institute? </strong></p>
                        
                        <p>A: The Criminal Justice Institute programs allow year-round enrollment.    &nbsp; Once
                           a student meets all the eligibility requirements and obtains the required     score
                           of 79% for Law Enforcement and 72% for Corrections on the appropriate     version
                           of the <a href="../../locations/school-of-public-safety/admission.html#CJ"><strong>CJ-BAT</strong></a>, the student     can submit their application.&nbsp; Applications are date stamped upon
                           receipt and     placed in date order on the wait-list. 
                        </p>
                        
                        <p><strong>Q:     How long is the wait list? </strong></p>
                        
                        <p>A:     Currently, the estimated wait is 6 months to 1 year. The     length of time
                           that you will wait to get into an academy is based on the number     of agency employed/sponsored
                           applicants that we admit. 
                        </p>
                        
                        <p>Students     are admitted into the Academy in the following order of priority: </p>
                        
                        <ol>
                           
                           <li>Applicants employed by any agency which is a member of the CJI 
                              Advisory Board. 
                           </li>
                           
                           <li>Applicants employed by any other agency in the state. </li>
                           
                           <li>Applicants who are sponsored by any agency. </li>
                           
                           <li>Applicants who have received a CJ degree from Valencia Community 
                              College. 
                           </li>
                           
                           <li>Pre-service applicants that are on the waitlist. </li>
                           
                        </ol>
                        
                        <p><strong>Q: What can I 
                              do while I'm waiting to begin the program? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
                        
                        <p>A:     Many students complete their AS degree in Criminal Justice <em>. </em>You      must successfully complete a state mandated training academy program    
                           such as those offered by the Criminal Justice Institute in order to be a sworn   
                           law enforcement or corrections officer in the state of Florida, however a degree 
                           in Criminal Justice will make you a more competitive candidate for a law enforcement
                           position.
                        </p>
                        
                        <p><strong>Q: What is the process for requesting a Letter of Good Standing,  Enrollment, Verification
                              or a Block Request for another academy? </strong></p>
                        
                        <p>A: Print the <strong>Letter Request Form</strong> and submit it via fax, email, mail or in person  to the CJI administration. The Letter
                           Request Form <strong><em>may take up to 48 hours</em></strong> to be completed. Hand delivered requests will not be processed 'while you wait'.
                           <strong> Law Enforcement Letter Requests</strong>, should be emailed to <strong>eedwards9@valenciacollege.edu</strong> and <strong>Corrections Letter Requests </strong>should be emailed to <strong>cperez84@valenciacollege.edu</strong>, <span><em>FAX: (407) 582-8222</em></span></p>
                        
                        
                        <p><a href="http://valenciacollege.edu/public-safety/documents/LetterRequestForm.docx"><img alt="letter request link" border="0" height="97" src="../../locations/school-of-public-safety/Letter-Request.png" width="116"></a> 
                        </p>
                        
                        <p><strong>Q: What is the processfor requesting a block of training at The Criminal Justice Institute
                              in the Law Enforcement or Corrections Academies if I am a CJI student or a student
                              coming from another academy?</strong></p>
                        
                        <p>A; The first step is to complete the<strong> Block Request Form</strong> and submit it via fax, email, mail, or in person. Upon submission of the form, you
                           will be contacted regarding the remaining steps to complete the process. Block Request
                           Forms should be emailed to jruiz98@valenciacollege.edu, FAX: (407) 582-8222, Mailing
                           Address: Valencia College, 8600 Valencia College Lane, Attn: Block Request, Orlando,
                           FL 32825 
                        </p>
                        
                        <p><a href="http://valenciacollege.edu/public-safety/documents/BlockRequestForm.docx"><img alt="cjiblockrequestbutton" border="0" height="97" src="../../locations/school-of-public-safety/blockrequestbutton.jpg" width="116"></a></p>
                        
                        <p>SWORN OFFICER </p>
                        
                        <p><strong>Q: Do you offer Equivalency for Out-of-State/Federal Law Enforcement Officers?</strong></p>
                        
                        <p>A:&nbsp; No. Nearby Lake County Criminal Justice Training Center (EOT) provides this service.
                           Contact Ms. Donna Whiting, Coordinator 352.343.3791 
                           
                           
                           <a href="mailto:donna.whiting@lcso.org">donna.whiting@lcso.org</a> 
                        </p>
                        
                        <p>Lake Tech website: <a href="http://www.laketech.org/ips/lake-county-criminal-justice-selection-center-eot">http://www.laketech.org/ips/lake-county-criminal-justice-selection-center-eot</a> 
                        </p>
                        
                        <p> Click on the following link for other&nbsp;<a href="http://www.fdle.state.fl.us/Content/getdoc/fb5b7e26-8edc-4f18-a8c3-b95eb76362e9/SELECTION-CENTER-LISTING.aspx"><strong>Selection Centers</strong></a>&nbsp;&nbsp; - - - <a href="http://www.fdle.state.fl.us/Content/getdoc/4c487a42-8430-4b68-9f94-5cd0370ca200/Equivalency.aspx"><strong>Equivalency Requirements</strong></a></p>
                        
                        <p><strong>Q: Do you provide Mandatory Retraining (40 hours) for LEO or Corrections?</strong></p>
                        
                        <p>A: No.&nbsp; Much of the training is provided by FDLE online: <a href="http://www.fdle.state.fl.us/Content/getdoc/54058cbd-7b19-4062-a8bb-c660278b0815/Mandatory-Retraining.aspx">FDLE Online Mandatory Retraining</a> - - - - - - - - <a href="http://www.fdle.state.fl.us/Content/getdoc/08b74938-6e0c-4ce4-9bb0-13e8a57aa5fb/08RetrainingMatrix081308.aspx">Mandatory Retraining Requirements</a></p>
                        
                        <p><strong>Q: How does Salary Incentive work? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
                        
                        <p>A: <strong>FDLE Rule 11B-14.003 Authorized Salary Incentive Payments</strong> provides the guidance for salary incentive payments. Here is a brief summary of how
                           it works. Sworn officers recieve $20 each month for each successfully completed 80
                           hour unit of Advanced Courses. Only FDLE approved salary incentive classes count toward
                           these hours.  You can recieve up to a maximum of $120 a month for these career development
                           courses.
                        </p>
                        
                        <p> You can also receive $30 a month for completion of an accredited two year degree
                           and $80 for an accredited four-year degree. The maximum salary incentive money per
                           month for combined educational and career development courses is $130. 
                        </p>
                        
                        <p>Mandatory Only courses and Instructor Courses do not quality for salary incentive
                           payments. All courses are listed on our online website: <a href="https://secure.valenciacollege.edu/cjisignup2/index.cfm?fuseaction=control_course.viewList"><strong>CJI Online Schedule</strong></a> 
                        </p>
                        
                        <p><br>
                           
                        </p>
                        
                        <p> <a href="../../locations/school-of-public-safety/CJIRecruit.html">Administrator</a></p>
                        
                        <p><a href="../../locations/school-of-public-safety/faq.html#top">TOP</a></p>  
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/faq.pcf">©</a>
      </div>
   </body>
</html>