<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Basic Correctional | Criminal Justice | Valencia College</title>
      <meta name="Description" content="Basic Correctional | Criminal Justice">
      <meta name="Keywords" content="college, school, educational, basic, correctional, criminal, justice">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/criminal-justice-institute/basic_correctional.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li><a href="/locations/school-of-public-safety/criminal-justice-institute/">Criminal Justice Institute</a></li>
               <li>Basic Correctional Officer</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Basic Correctional Officer Academy</h2>
                        
                        <p>&nbsp;</p>
                        
                        <h3><span> Program Description:</span></h3>
                        
                        <p><span><br> </span>This program is designed to prepare individuals for entry level positions as correctional
                           officers.
                        </p>
                        
                        <p><span>Program Content:<br> </span>Course includes introduction to criminal justice, legal aspects, defensive tactics,
                           physical fitness training, weapons, first responder techniques, court system, communications,
                           interpersonal skills, correctional operations, emergency preparedness and state exam
                           review.
                        </p>
                        
                        <p><a href="/locations/school-of-public-safety/criminal-justice-institute/documents/ApplicantFitnessLetter.doc">Minimum Fitness Standards</a></p>
                        
                        <p><span>Practical Skills and Field Exercises:<br> </span>Practical skills and field exercises include courtroom demeanor and testifying, report
                           writing, identification, collection and preservation of evidence, radio communications,
                           patrol techniques, defensive tactics, and firearms training; chemical agent application,
                           first responder, and fingerprinting.<span><br> <br></span></p>
                        
                        <p><span class="head">Program Length: 420 hours</span></p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td colspan="2"><strong>DAY - 12 Weeks</strong>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td colspan="2" height="45">6:00AM to 5:00PM<br>Monday - Thursday&nbsp;
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>&nbsp;</p>
                        
                        <p>The Criminal Justice Institute at Valencia College is certified by the Florida Department
                           of Law Enforcement, Division of Criminal Justice Standards and Training.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <h3><span>Estimated Costs:</span></h3>
                        
                        <div>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>Tuition:</strong></td>
                                    
                                    <td>Florida Resident $1500<span>&nbsp;</span><br>(approximate; subject to change)
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>Materials Cost:</strong></td>
                                    
                                    <td>$500.00<br>(approximate) Includes books, uniforms and other equipment
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>CJI Application Fee:</strong></td>
                                    
                                    <td>$100.00 (non refundable)</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>Valencia College Application Fee:</strong></td>
                                    
                                    <td>
                                       
                                       <p>&nbsp;</p>
                                       
                                       <p>$35.00 (non refundable)</p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>CJ BAT fee:</strong></td>
                                    
                                    <td>$40.00</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>State exam fee:</strong></td>
                                    
                                    <td>$100.00 &amp; ($35 proctoring fee if exam taken at CJI)</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Program Courses</h3>
                        
                        <div>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0300</td>
                                    
                                    <td>Introduction to Corrections</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0305</td>
                                    
                                    <td>Communications</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0310</td>
                                    
                                    <td>Officer Safety</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0315</td>
                                    
                                    <td>Facility and Equipment</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>CJK 0320</td>
                                    
                                    <td>Intake and Release</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>CJK 0325</td>
                                    
                                    <td>Suervising in a Correctional Facility</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0330</td>
                                    
                                    <td>Supervising Special Populations</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0335</td>
                                    
                                    <td>Responding to Incidents and Emergencies</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0031</td>
                                    
                                    <td>CMS First Aid for Criminal Justice Officers</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>CJK 0040</td>
                                    
                                    <td>CMS Criminal Justice Firearms</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0051</td>
                                    
                                    <td>CMS Criminal Justice Defensive Tactics</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75" height="25">CJK 0340</td>
                                    
                                    <td>Officer Wellness and Physical Abilities</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="25">CJK 0939</td>
                                    
                                    <td>Criminal Justice Exam Review</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Admission Requirements</h3>
                        
                        <ul>
                           
                           <li>Applicant must be at least 19 years of age.</li>
                           
                           <li>Applicant must be a citizen of the United States.</li>
                           
                           <li>Applicant must have a high school diploma or equivalent.</li>
                           
                           <li>Applicant cannot have a dishonorable discharge from the Armed Forces of the United
                              States.
                           </li>
                           
                           <li>Applicant cannot have been convicted of any felony, or of a misdemeanor involving
                              perjury or false statement. Any person who, after July 1, 1981, pleads guilty or nolo
                              contendere to, or is found guilty of a felony, or of a misdemeanor involving perjury
                              or a false statement, shall not be eligible for employment or appointment as an officer,
                              not-withstanding suspension of sentence or withholding of adjudication.
                           </li>
                           
                           <li>Applicant must take the Criminal Justice Basic Abilities Test (CJ BAT) and achieve
                              required scores.
                           </li>
                           
                           <li>Applicant will be required to provide birth certificate, high school diploma or equivalent,
                              valid Florida Driver's License, Social Security Card, Military Form DD214 (if applicable),
                              college transcript or other proof of degree (if applicable), and proof of medical
                              insurance.
                           </li>
                           
                           <li>Applicant must have a physical examination prior to program entry. Forms for this
                              purpose are part of the application packet available in the Criminal justice Institute
                              Office.
                           </li>
                           
                           <li>Applicant must fill out a Valencia Student Application.</li>
                           
                           <li>Applicant must fill out a Criminal justice Institute Application.</li>
                           
                        </ul>
                        
                        <p><strong>Note: </strong>Most of the recruits attending Valencia's Corrections Academy are employeed by Orange
                           County Corrections Department (OCCD). For hired employees, tuition and salaries are
                           paid by OCCD during attendence at the academy. <a href="http://www.ocfl.net/JailInmateServices/CorrectionsCareers.aspx#.VorBKE_G9pU">Cick here for information to APPLY at Orange County Corrections</a>.
                        </p>
                        
                        <p><strong>Students will be admitted into the Academy in the following order of priority. </strong></p>
                        
                        <ul>
                           
                           <li>Applicants who are employed by any agency which is a member of the C.J.I. Advisory
                              Board.
                           </li>
                           
                           <li>Applicants who are employed by any other agency in the state.</li>
                           
                           <li>Applicants who are sponsored by any agency.</li>
                           
                           <li></li>
                           
                           <li>Valencia AA/AS students or US Military Veterans.</li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>All other qualified pre-service applicants.</li>
                           
                           <li>Upon completion of the Academy, each student will be required to take and pass a state
                              examination prior to being certified in the State of Florida. This exam consists of
                              five sections, which requires a passing score of 80%.
                           </li>
                           
                        </ul>
                        
                        <p><strong>For further information call (407) 582-8200</strong></p>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/criminal-justice-institute/basic_correctional.pcf">©</a>
      </div>
   </body>
</html>