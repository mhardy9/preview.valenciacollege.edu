<ul>
	<li><a href="/locations/school-of-public-safety/">School of Public Safety</a></li>


	<li class="megamenu submenu"> <a class="show-submenu-mega" href="javascript:void(0);"> Programs &amp; Degrees <i aria-hidden="true" class="far fa-angle-down"> </i> </a>
		<div class="menu-wrapper c1">
			<div class="col-md-12">

				<h3>
					<a href="/locations/school-of-public-safety/criminal-justice-institute/">Criminal Justice</a>
				</h3>
				<ul>
					<li><a href="/locations/school-of-public-safety/criminal-justice-institute/basic_law.php">Law Enforcement Officer</a></li>
					<li><a href="/locations/school-of-public-safety/criminal-justice-institute/basic_correctional.php">Correctional Officer</a></li>
				</ul>
				<h3>
					<a href="/locations/school-of-public-safety/fire-rescue-institute/">Fire Rescue</a>
				</h3>
				<ul>
					<li><a href="/locations/school-of-public-safety/fire-rescue-institute/academy-track.php">Fire Science Academy</a></li>
					<li><a href="/locations/school-of-public-safety/fire-rescue-institute/academy-fire-fighter-minimum-standards-course.php">Fire Fighter Minimum Standards</a></li>
					<li><a href="/locations/school-of-public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.php">FLorida Bureau of Fire Standards and Training</a></li>
				</ul>

				<h3>
					<a href="/locations/school-of-public-safety/safety-and-security-institute/">Safety and Security</a>
				</h3>
				<ul>
					<li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Criminal Justice Tech Specialist</a></li>
					<li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Homeland Security Law Enforcement Specialist</a></li>
				</ul>
			</div>
		</div>
	</li>


	<li><a href="/locations/school-of-public-safety/how-to-apply.php">How to Apply</a></li>
	<li><a href="/locations/school-of-public-safety/contact.php">Contact Us</a></li>
</ul>
