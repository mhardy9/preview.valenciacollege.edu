<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Leo Auxiliary | Criminal Justice | Valencia College</title>
      <meta name="Description" content="Leo Auxiliary | Criminal Justice">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/criminal-justice-institute/leo_auxiliary.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li><a href="/locations/school-of-public-safety/criminal-justice-institute/">Criminal Justice Institute</a></li>
               <li>Leo Auxiliary</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>LEO Auxiliary Academy</h2>
                        
                        <hr>
                        
                        <h3><span> Program Description</span></h3>
                        
                        <p>This program is designed to train applicants for employment or appointment by criminal
                           justice agencies, with or without compensation, to assist or aid full-time or part-time
                           officers.
                        </p>
                        
                        <p><span>Program Content:<br> </span> Courses include Vehicle Operations,First Aid, Firearms, Defensive Tactics, Patrol
                           andTraffic,Tactical Applications, Dart-Firing Stun Gun and State Exam Review. <span><br> <br></span></p>
                        
                        <h3 align="justify"><span class="head">Program Length: hours</span></h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td colspan="2"><strong>8 Weeks&nbsp; (319 hours in addition to 20 hour exam review)</strong>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td>
                                    
                                    <p><strong>Monday through Friday. (Or as scheduled)</strong></p>
                                    
                                    <p><strong>Hours: To be determined.</strong></p>
                                    
                                    <p><strong>Saturday: Hours vary</strong></p>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3><span>Estimated Costs:</span></h3>
                        
                        <div>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right" width="187"><strong>Tuition:</strong></td>
                                    
                                    <td width="289">Florida Resident $1,225.00<br>(approximate; subject to change)
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>Materials Cost:</strong></td>
                                    
                                    <td>$60.00<br>(approximate) includes books
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right" height="40"><strong>Valencia College Application Fee:</strong></td>
                                    
                                    <td>
                                       
                                       <p><br>$35.00
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>CJ BAT fee:</strong></td>
                                    
                                    <td>$40.00</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td align="right"><strong>State exam fee:</strong></td>
                                    
                                    <td>$100.00</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Program Courses</h3>
                        
                        <div>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="75" height="28">CJK 0240</td>
                                    
                                    <td width="404">Law Enforcement Auxiliary Introduction</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0241</td>
                                    
                                    <td>Law Enforcement Auxiliary Patrol and Traffic</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0242</td>
                                    
                                    <td>Law Enforcement Auxiliary Investigations</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0422</td>
                                    
                                    <td>Dart Firing Stun Gun</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>CJK 0031</td>
                                    
                                    <td>CMS First Aid for Criminal Justice Officers</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>CJK 0040</td>
                                    
                                    <td>CMS Criminal Justice Firearms</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0051</td>
                                    
                                    <td>CMS Criminal Justice Defensive Tactics</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="75">CJK 0020</td>
                                    
                                    <td>CMS Criminal Justice Vehicle Operations</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="28">CJD 0939</td>
                                    
                                    <td>State Exam Review (3 days)</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Admission Requirements</h3>
                        
                        <ul>
                           
                           <li>
                              
                              <div>Applicant must be at least 19 years of age.</div>
                              
                           </li>
                           
                           <li>
                              
                              <div>Applicant must be a citizen of the United States.</div>
                              
                           </li>
                           
                           <li>
                              
                              <div>Applicant must have a high school diploma or equivalent.</div>
                              
                           </li>
                           
                           <li>
                              
                              <div>Applicant cannot have a dishonorable discharge from the Armed Forces of the United
                                 States.
                              </div>
                              
                           </li>
                           
                           <li>
                              
                              <div>Applicant cannot have been convicted of any felony, or of a misdemeanor involving
                                 perjury or false statement. Any person who, after July 1, 1981, pleads guilty or nolo
                                 contendere to, or is found guilty of a felony, or of a misdemeanor involving perjury
                                 or a false statement, shall not be eligible for employment or appointment as an officer,
                                 not-withstanding suspension of sentence or withholding of adjudication.
                              </div>
                              
                           </li>
                           
                           <li>
                              
                              <div>Applicant must take the Criminal Justice Basic Abilities Test (CJ BAT) Law Enforcement
                                 Version and achieve required scores.
                              </div>
                              
                           </li>
                           
                           <li>
                              
                              <div>Applicant will be required to provide birth certificate, high school diploma or equivalent,
                                 valid Florida Driver's License, Social Security Card, Military Form DD214 (if applicable),
                                 college transcript or other proof of degree (if applicable), and proof of medical
                                 insurance.
                              </div>
                              
                           </li>
                           
                        </ul>
                        
                        <div>
                           
                           <ul>
                              
                              <li>Proof of completion of the FDLE Basic Correctional Academy and passed the State Correctional
                                 Officer Certificate Examination within the past 4 years of the start of the crossover
                                 academy or if an active certified officer currently employed by a correctional agency
                                 a copy of your current pay stub.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <div>
                           
                           <ul>
                              
                              <li>If <strong>NOT</strong> currently employed, a background check will be performed by CJI.
                              </li>
                              
                              <li>Applicant must fill out a Valencia Student Application.</li>
                              
                              <li>Applicant must fill out a Criminal justice Institute Application.</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr>
                        
                        <p><strong>For further information call </strong></p>
                        
                        <p><strong><span size="5" style="font-size: x-large;">(407) 582-8200</span></strong></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/criminal-justice-institute/leo_auxiliary.pcf">©</a>
      </div>
   </body>
</html>