<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Scholarships | Criminal Justice | Valencia College</title>
      <meta name="Description" content="Scholarships | Criminal Justice">
      <meta name="Keywords" content="college, school, educational, criminal, justice, scholarships">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/criminal-justice-institute/scholarships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li><a href="/locations/school-of-public-safety/criminal-justice-institute/">Criminal Justice Institute</a></li>
               <li>Scholarships</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <div style="text-align: left;"><span><strong><span size="5" style="font-size: x-large;">SCHOLARSHIP RESOURCES </span></strong></span><hr>
                        </div>
                        
                        <div style="text-align: left;">
                           
                           <p><strong><span size="5" style="font-size: x-large;"><a id="CPM" name="CPM"></a> </span></strong></p>
                           
                        </div>
                        
                        <p style="text-align: left;"><img style="display: block; margin-left: auto; margin-right: auto;" src="/locations/school-of-public-safety/criminal-justice-institute/images/ValenciaFoundationLogo_000.png" alt="Valencia Foundation Logo" width="228" height="71"></p>
                        
                        <h3 style="text-align: left;"><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466"><strong><img style="display: block; margin-left: auto; margin-right: auto;" src="/locations/school-of-public-safety/criminal-justice-institute/images/scholarshipClickNow.png" alt="Click to Apply" width="297" height="85" border="0"></strong></a></h3>
                        
                        <p style="text-align: left;"><strong>VALENCIA COLLEGE'S ONE STOP SHOP FOR SCHOLARSHIPS! </strong></p>
                        
                        <hr>
                        
                        <p style="text-align: left;">The two scholarships below are in honor of Orange County Sheriff's Deputies and&nbsp;Valencia
                           Criminal Justice Institute graduates who lost their lives in the line of duty.
                        </p>
                        
                        <hr>
                        
                        <p style="text-align: left;"><a href="http://www.michaelcallin.org/criteria.cfm"><strong>Michael Callin Scholarship</strong></a></p>
                        
                        <p><a href="http://www.michaelcallin.org/criteria.cfm"><img style="float: left; margin-right: 5px; margin-left: 5px;" src="/locations/school-of-public-safety/criminal-justice-institute/images/MichaelCallinScholarshipLogo_000.gif" alt="Michael Callin Scholarship Logo" width="200" height="194" border="0"></a></p>
                        
                        <ul>
                           
                           <li><strong>Criminal Justice Institute at Valencia Community College - Law Enforcement Academy
                                 Recruit.</strong></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              
                              <p>Acceptance Letter from the Criminal Justice Institute at Valencia College.</p>
                              
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <ul>
                              
                              <li>Complete Essay:
                                 
                                 <ul>
                                    
                                    <li>List the specific examples of how you exhibit these qualities</li>
                                    
                                    <li>Give examples of your service to the community</li>
                                    
                                    <li>The typewritten essay must be between 500 – 750 words</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                        </ul>
                        
                        <blockquote>&nbsp;</blockquote>
                        
                        <ul>
                           
                           <ul>
                              
                              <li>Transcripts from previous school showing at least a 2.5 grade point average.</li>
                              
                           </ul>
                           
                        </ul>
                        
                        <ul>
                           
                           <ul>
                              
                              <ul>
                                 
                                 <li>Two character references (non family).</li>
                                 
                                 <li></li>
                                 
                              </ul>
                              
                           </ul>
                           
                        </ul>
                        
                        <blockquote>&nbsp;</blockquote>
                        
                        <p>$1000.00 scholarships will be awarded to the recipients chosen by the Michael Callin
                           Memorial Scholarship Fund Board of Directors based on the listed qualifications.
                        </p>
                        
                        <hr>
                        
                        <p style="text-align: left;"><span><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">DFC Brandon Lee Coates Memorial Scholarship</a></span></p>
                        
                        <p style="text-align: left;">Apply with <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation</a> to automatically apply for this scholarship and any other eligible scholarships from
                           the foundation.
                        </p>
                        
                        <p><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466"><img style="float: left;" src="/locations/school-of-public-safety/criminal-justice-institute/images/BrandonLeeCoatesPhoto.jpg" alt="Brandon Lee Coates Photo" width="225" height="225" border="0"></a></p>
                        
                        <p>DFC Brandon Lee Coates Memorial Scholarship was created in memory of Deputy First
                           Class Brandon Lee Coates. This memorial scholarship is for academy students at Valencia's
                           Criminal Justice Institute. Students must attend the School of Public Safety and be
                           enrolled full time. Eligible students must complete a FAFSA and demonstrate financial
                           need. Deputy Coates attended basic recruit law enforcement training at CJI in 2006.
                           His wife, Orange County Deputy Sheriff Virginia Coates, is also a graduate of the
                           program. <br> <br> Created by the Basic Law Enforcement Class 11-02, donations are still being accepted
                           at <a href="http://www.VALENCIA.org">www.VALENCIA.org</a>. Click on &amp;gt;&amp;gt;Make A Donation and when asked to specify the designation please
                           select DFC Brandon Lee Coates Memorial Scholarship. Those who wish to donate may also
                           do so by mail to Valencia Foundation, 190 S. Orange Avenue, Orlando, FL 32801.
                        </p>
                        
                        <hr>
                        
                        <h3>SCHOLARSHIP FUND RAISERS</h3>
                        
                        <h3><img src="/locations/school-of-public-safety/criminal-justice-institute/images/CJI-Grads-2011-013-523x374.jpg" alt="2011 Night LEO Class Donation" width="349" height="250"></h3>
                        
                        <p>Nineteen students graduated from the Criminal Justice Institute (CJI) Basic Law Enforcement
                           Class on August 25, leaving behind more than just memories of defensive tactics and
                           first aid training. Through fundraisers, the class made a $1,250 donation to the Valencia
                           Foundation for the Deputy First Class <a href="/locations/school-of-public-safety/criminal-justice-institute/scholarship.php">Brandon Lee Coates Memorial Scholarship</a>.
                        </p>
                        
                        <p>Courtesy of Valencia College's <a href="http://thegrove.valenciacollege.edu/cji-graduates-give-back/">THE GROVE </a></p>
                        
                        <hr>
                        
                        <p>&nbsp; "I cannot teach anybody anything, I can only make them think."</p>
                        
                        <p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Socrates
                        </p>
                        
                        <hr>
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/criminal-justice-institute/scholarships.pcf">©</a>
      </div>
   </body>
</html>