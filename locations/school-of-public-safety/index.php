<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>School of Public Safety | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li>School Of Public Safety</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Education and&nbsp;Training&nbsp;for&nbsp;All Public Safety Professionals</h2>
                        
                        <p>Valencia College's new School of Public Safety provides comprehensive education, training
                           and resources for our communities' first responders—helping Central Florida maintain
                           the finest in public safety services to protect its booming visitor and residential
                           populations.
                        </p>
                        
                        <p>The&nbsp;School of Public Safety&nbsp;houses three major program areas: Criminal Justice, Fire&nbsp;Rescue
                           and Safety and Security.&nbsp;It will train the next generation of public safety professionals&nbsp;using
                           state-of-the-art technology and the latest in simulation training.&nbsp;By offering programs
                           for firefighters, law enforcement, corrections, security officers and homeland security
                           under one umbrella, Valencia will elevate and unify training standards across all
                           divisions of public safety. This interdisciplinary approach promotes cross-sector
                           training and collaboration between public safety agencies to help make our communities
                           safer—both in times of crisis and in maintaining the peace.
                        </p>
                        
                        <h3>One Education and Training Source.</h3>
                        
                        <p>Valencia envisions at least a two-year degree for all first responders in Central
                           Florida. Through its “Two for All” initiative, state-certified professionals can receive
                           college credits toward an Associate in Science (A.S.) degree. In addition to degrees,
                           the School of Public Safety provides education and training to public safety professionals
                           throughout all stages of their careers.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><a href="/locations/school-of-public-safety/criminal-justice-institute/"><img src="/locations/school-of-public-safety/images/2013Criminal-Justice-Inspection012.jpg" alt="criminal justice" width="300" height="213" border="0"></a></div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><strong>Criminal Justice</strong></p>
                                       
                                       <p><a href="/locations/school-of-public-safety/criminal-justice-institute/">Program Details </a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><a href="/locations/school-of-public-safety/fire-rescue-institute/"><img src="/locations/school-of-public-safety/images/sps-fire1237_1000px.jpg" alt="fire rescue" width="300" height="199" border="0"></a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><strong>Fire Rescue</strong></p>
                                       
                                       <p><a href="/locations/school-of-public-safety/fire-rescue-institute/"><strong>Program Details </strong></a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="/locations/school-of-public-safety/safety-and-security-institute/"><img src="/locations/school-of-public-safety/images/86512749_1000px.jpg" alt="safety and security" width="300" height="199" border="0"></a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Safety and Security</strong></p>
                                    
                                    <p><a href="/locations/school-of-public-safety/safety-and-security-institute/"><strong>Program Details </strong></a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h2>&nbsp;</h2>
                        <strong>Welcome to Public Safety's latest development</strong></div>
                     
                     <div class="col-md-9"><a href="/locations/school-of-public-safety/health-wellness-initiative.html">Public Safety's Health &amp; Wellness Initiative </a><hr class="styled_2">
                        
                        <h3>Helpful Links</h3>
                        
                        <ul>
                           
                           <li><a href="/locations/school-of-public-safety/how-to-apply.html"><strong>How to Apply</strong></a></li>
                           
                           <li><a href="/locations/school-of-public-safety/contact.html"><strong>Contact an Advisor</strong></a></li>
                           
                           <li><a href="/locations/school-of-public-safety/partner-with-us.html"><strong>Partner with Us</strong></a></li>
                           
                        </ul>
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=public+safety&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank"> <img src="/locations/school-of-public-safety/images/career-coach.png" alt="" width="250" height="140"> </a><hr class="styled_2">
                        
                        <h3>Locations</h3>
                        <strong>The School of Public Safety offers training and education at six convenient locations.</strong> <img title="School of Public Safety at Valencia College" src="/locations/school-of-public-safety/images/public-safety-locations-250x174.png" alt="School of Public Safety at Valencia College" width="250" height="174"> <a href="/locations/school-of-public-safety/documents/16SPS002-programs-and-campuses-flyer.pdf" target="_blank">Download Locations Map </a></div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/index.pcf">©</a>
      </div>
   </body>
</html>