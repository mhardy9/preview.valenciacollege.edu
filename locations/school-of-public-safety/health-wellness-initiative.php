<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Health and Wellness Initiative | School of Public Safety | Valencia College</title>
      <meta name="Description" content="Health and Wellness Initiative | School of Public Safety">
      <meta name="Keywords" content="college, school, educational, public, safety, health, wellness">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/health-wellness-initiative.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li>Health and Wellness Initiative</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Health &amp; Wellness Initiative</h2>
                        
                        <p>Valencia College's School of Public Safety is taking a leadership role in providing
                           training in health and wellness for our communities' first responders—helping Central
                           Florida maintain the heathiest public safety workforce. The Health &amp; Wellness Initiative
                           is the product of a collaborative group of public safety and private sector organizations
                           and individuals who have volunteered to develop and deliver training and seminars
                           to improve the quality of life for central Florida's public safety employees. This
                           group includes; <a href="http://centralfloridawellnesscouncil.org/index.html">Central Florida Wellness Council</a>, Executive Wellness Strategies, <a href="https://www.facebook.com/firefighterfitnesscollaborative">Florida Firefighter Fitness Collaborative</a>, Spectrum Sports Performance, wellness professionals from Orange &amp; Osceola County
                           government, and others.
                        </p>
                        
                        <hr>
                        
                        <h3><strong>Vision:</strong></h3>
                        
                        <p>Greatly increase the overall health and wellness of public safety professionals in
                           central Florida while greatly reducing health care costs for public safety organizations
                           and individuals.
                        </p>
                        
                        <hr>
                        
                        <h3><strong>Mission:</strong></h3>
                        
                        <p>&nbsp;<em>Integrated health and wellness</em> education and resources for central Florida public safety professionals and organizations:
                        </p>
                        
                        <ul>
                           
                           <li>Enhance quality of life</li>
                           
                           <li>Reduce stress</li>
                           
                           <li>Lower health care costs</li>
                           
                           <li>Improve productivity and morale</li>
                           
                        </ul>
                        
                        <p><strong>This program would benefit individuals and agencies by:</strong></p>
                        
                        <p>&nbsp;Building a foundation for future health and wellness education and resources that
                           will prepare public safety professionals for a healthy lifestyle while serving their
                           community, and into their retirement years.
                        </p>
                        
                        <p><strong>Delivery of Information:</strong></p>
                        
                        <p>Advertisements, Annual calendar of events, Boot camps, Electronic media (clearing
                           house of information), Health food events,&nbsp; Newsletter, Peer fitness trainer program
                           (IFF model), Promotion of information to public safety agencies, Public Safety Health
                           Fairs, Symposiums.
                        </p>
                        
                        <hr>
                        
                        <h3>NOTICES</h3>
                        
                        <p><strong>Health &amp; Wellness Seminar</strong></p>
                        
                        <p>To be scheduled soon!</p>
                        
                        <p><a href="/locations/school-of-public-safety/Veggie.html"><strong>Local Plant Based Dining Locations </strong></a></p>
                        
                        <p><strong>How healthy is your meal? </strong></p>
                        
                        <p><a href="https://chipotle.com/nutrition-calculator">Chipotle Lunch Builder</a> - Burrito or Salad? You compare!
                        </p>
                        
                        <p><a href="https://chipotle.com/nutrition-calculator"><u>https://chipotle.com/nutrition-calculator</u></a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/health-wellness-initiative.pcf">©</a>
      </div>
   </body>
</html>