<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>School of Public Safety | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/cjirecruit.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li>School of Public Safety</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../locations/school-of-public-safety/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Recruit Resources</h2>
                        
                        <p>This page will be used to share information and additional resources beneficial to
                           recruits enrolled in the Valencia Criminal Justice Institute academies. 
                        </p>
                        
                        <h3> <strong>Memo Template: </strong>
                           
                        </h3>
                        
                        <p><strong></strong></p>
                        
                        <p>All memorandums and correspondence are to be typed/computer-generated on the Criminal
                           Justice Institute<a href="http://valenciacollege.edu/public-safety/documents/RecruitMemoTemplateLEO-BCO.docx"><strong> Recruit Memorandum template</strong></a>. 
                        </p>
                        
                        
                        <h3><strong>Class Officer Duties &amp; Responsibilities:</strong></h3>
                        
                        <p><strong><a href="../../locations/school-of-public-safety/documents/ClassLeaderResponsibilities.pdf">Class Leader Responsibilities</a></strong></p>
                        
                        
                        <h3><strong>Equipment Lists:</strong></h3>
                        
                        <p><strong><a href="../../locations/school-of-public-safety/documents/HighLiabiltiyEquipmentList.pdf">High Liabiltiy Equipment List</a></strong></p>
                        
                        <p><strong><a href="../../locations/school-of-public-safety/documents/RecruitPT_DTEquipmentChecklist.pdf">PT/DT Equipment Checklist </a></strong></p>
                        
                        <p><strong><a href="../../locations/school-of-public-safety/documents/PT_DTTSHIRTSPECS.pdf">PT/DT T-Shirt Specifications</a> </strong></p>
                        
                        
                        <h3><strong>Miscellaneous:</strong></h3>
                        
                        <p><a href="../../locations/school-of-public-safety/documents/ParkingApplicationInstructions.pdf"><strong>Parking Application Instructions </strong></a></p>
                        
                        <p><a href="../../locations/school-of-public-safety/documents/MRSAInformationSheet.pdf"><strong>MRSA Information Sheet</strong></a> 
                        </p>
                        
                        
                        <p>Class Schedules</p>
                        
                        <p>Click the link for your class to download your class schedule. </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>Class</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div><strong>Class Coordinator</strong></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../locations/school-of-public-safety/documents/17-04-Schedule.pdf" target="_blank"><strong> LEO 2017-04</strong></a></div>
                                    </div>
                                    
                                    <div>
                                       <div>Officer Mount </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          <a href="../../locations/school-of-public-safety/documents/BLE17-05Schedule.xls" target="_blank"><strong>LEO 2017-05</strong></a> 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>Sergeant Lopez </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>&nbsp;</h3>
                        
                        
                        <p><a href="../../locations/school-of-public-safety/Veggie.html"></a></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <p><a href="../../locations/school-of-public-safety/how-to-apply.html"><strong>How to Apply</strong></a></p>
                        
                        <p><a href="../../locations/school-of-public-safety/contact.html"><strong>Contact an Advisor</strong></a></p>
                        
                        <p><a href="../../locations/school-of-public-safety/partner-with-us.html"><strong>Partner with Us</strong></a></p>
                        
                        
                        
                        
                        
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=public+safety&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank">
                           <img height="140" src="../../locations/school-of-public-safety/career-coach.png" width="250">            
                           </a>
                        
                        
                        
                        
                        <div>
                           
                           <h4>Locations</h4>
                           
                           <align><strong>The School of Public Safety offers training and education at six convenient locations.</strong>
                              <img alt="School of Public Safety at Valencia College" height="174" src="../../locations/school-of-public-safety/public-safety-locations-250x174.png" title="School of Public Safety at Valencia College" width="250">
                              <a href="../../locations/school-of-public-safety/documents/16SPS002-programs-and-campuses-flyer.pdf" target="_blank">Download Locations Map <i aria-hidden="true" title="Locations Map .pdf"></i></a>
                              
                              
                           </align>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/cjirecruit.pcf">©</a>
      </div>
   </body>
</html>