<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>School of Public Safety | Valencia College</title>
      <meta name="Description" content="School of Public Safety">
      <meta name="Keywords" content="college, school, educational, school, public, safety, apply">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/how-to-apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li>School of Public Safety</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12"><a id="content" name="content"></a>
                        				
                        <h2>How to Apply</h2>
                        				
                        <hr>
                        				
                        <h3>CRIMINAL JUSTICE ACADEMY PROGRAMS</h3>
                        				
                        <div>
                           					
                           <div>
                              						
                              <h4 style="text-align: left;" align="center">Admission Process for 2016/2017 Academies</h4>
                              						
                              <h4 style="text-align: left;" align="center">BASIC CORRECTIONS ACADEMIES</h4>
                              						
                              <div style="text-align: left;" align="center">
                                 							
                                 <table class="table ">
                                    								
                                    <tbody>
                                       									
                                       <tr>
                                          										
                                          <td>BCO 18-01</td>
                                          										
                                          <td>1/8/2018 - 3/28/2017</td>
                                          										
                                          <td>No longer Accepting Applications</td>
                                          									
                                       </tr>
                                       									
                                       <tr>
                                          										
                                          <td>BCO 18-02</td>
                                          										
                                          <td>06/25/2018 - 9/13/2018</td>
                                          										
                                          <td>Accepting Applications</td>
                                          									
                                       </tr>
                                       									
                                       <tr>
                                          										
                                          <td>BCO 18-03</td>
                                          										
                                          <td>09/17/2018 - 12/6/2018</td>
                                          										
                                          <td>Accepting Applications</td>
                                          									
                                       </tr>
                                       								
                                    </tbody>
                                    							
                                 </table>
                                 							
                                 <p><br><br></p>
                                 						
                              </div>
                              						
                              <h4 style="text-align: left;" align="center">BASIC LAW ENFORCEMENT ACADEMIES</h4>
                              						
                              <div style="text-align: left;" align="center">
                                 							
                                 <table class="table ">
                                    								
                                    <tbody>
                                       									
                                       <tr>
                                          										
                                          <td>2017-05</td>
                                          										
                                          <td>09/25/2017 - 02/15/2018</td>
                                          										
                                          <td>No Longer Accepting Applications</td>
                                          									
                                       </tr>
                                       									
                                       <tr>
                                          										
                                          <td>2018-01</td>
                                          										
                                          <td>01/08/2018 - 05/24/2018</td>
                                          										
                                          <td>Accepting Applications</td>
                                          									
                                       </tr>
                                       									
                                       <tr>
                                          										
                                          <td>2018-02</td>
                                          										
                                          <td>01/10/2018 - 08/30/2018</td>
                                          										
                                          <td>Accepting Applications (Part-Time Academy)</td>
                                          									
                                       </tr>
                                       									
                                       <tr>
                                          										
                                          <td>2018-03</td>
                                          										
                                          <td>03/26/2018 - 08/09/2018</td>
                                          										
                                          <td>Accepting Applications</td>
                                          									
                                       </tr>
                                       									
                                       <tr>
                                          										
                                          <td>2018-04</td>
                                          										
                                          <td>06/18/2018 - 11/01/2018</td>
                                          										
                                          <td>Accepting Applications</td>
                                          									
                                       </tr>
                                       									
                                       <tr>
                                          										
                                          <td>2018-05</td>
                                          										
                                          <td>09/24/2018 - 02/14/2019</td>
                                          										
                                          <td>Accepting Applications</td>
                                          									
                                       </tr>
                                       								
                                    </tbody>
                                    							
                                 </table>
                                 							
                                 <p>&nbsp;</p>
                                 						
                              </div>
                              						
                              <h4 style="text-align: left;" align="center">CORRECTIONAL OFFICER CROSSOVER TO FLORIDA LAW ENFORCEMENT *</h4>
                              						
                              <div style="text-align: left;" align="center">
                                 							
                                 <table class="table ">
                                    								
                                    <tbody>
                                       									
                                       <tr>
                                          										
                                          <td width="64">
                                             											
                                             <div align="center">2017</div>
                                             										
                                          </td>
                                          										
                                          <td width="153">
                                             											
                                             <p align="center">TBD</p>
                                             										
                                          </td>
                                          										
                                          <td width="210">
                                             											
                                             <div align="center">TBD</div>
                                             										
                                          </td>
                                          									
                                       </tr>
                                       								
                                    </tbody>
                                    							
                                 </table>
                                 						
                              </div>
                              						
                              <p style="text-align: left;"><strong>*For inquiries regarding the Crossover Academy please contact Crystal Perez at<span>&nbsp;</span><a href="mailto:cperez84@valenciacollege.edu">cperez84@valenciacollege.edu</a></strong></p>
                              						
                              <h4>Admission Requirements</h4>
                              						
                              <ul>
                                 							
                                 <li>Applicant must be at least 19 years of age.</li>
                                 							
                                 <li>Applicant must be a citizen of the United States.</li>
                                 							
                                 <li>Applicant must have a high school diploma or equivalent.</li>
                                 							
                                 <li>Applicant cannot have a dishonorable discharge from the Armed Forces of the United
                                    States.
                                 </li>
                                 							
                                 <li>
                                    								
                                    <p>Applicant canot have been convicted of any felony, or misdemeanor involving perjury
                                       or false statement, or a crime of moral turpitude.
                                    </p>
                                    							
                                 </li>
                                 							
                                 <li>Applicant must take the Criminal Justice Basic Abilities Test (CJ BAT) and achieve
                                    a passing score.
                                 </li>
                                 							
                                 <li>Applicant will be required to provide birth certificate, high school diploma or equivalent,
                                    valid Florida Driver's License, Social Security Card, Military Form DD214 (if applicable),
                                    college transcript or other proof of degree (if applicable), and proof of medical
                                    insurance.
                                 </li>
                                 							
                                 <li>Applicant must have a physical examination prior to program entry. Forms for this
                                    purpose are part of the application packet available in the Criminal Justice Institute
                                    Office.
                                 </li>
                                 							
                                 <li>Applicant must fill out a Valencia Student Application.</li>
                                 							
                                 <li>Applicant must fill out a Criminal Justice Institute Application.</li>
                                 						
                              </ul>
                              						
                              <p>Students will be admitted into the Academy in the following order of priority.</p>
                              						
                              <ul>
                                 							
                                 <li>Applicants who are employed by any agency which is a member of the CJI Advisory Board.</li>
                                 							
                                 <li>Applicants who are employed by any other agency in the state.</li>
                                 							
                                 <li>Applicants who are sponsored by any agency.</li>
                                 						
                              </ul>
                              						
                              <ul>
                                 							
                                 <li>Valencia College AA/AS students or US Military Veteran.</li>
                                 							
                                 <li>All other qualified pre-service applicants.<br> Upon completion of the Academy, each student will be required to take and pass a
                                    state examination prior to being certified in the State of Florida. This exam consists
                                    of five sections and requires a passing score of 80%.
                                 </li>
                                 						
                              </ul>
                              						
                              <p><strong>1. Attend Orientation</strong></p>
                              						
                              <ul>
                                 							
                                 <li>All candidates must attend the School of Public Safety Orientation before applying
                                    to the Basic Law Enforcement Academy or Corrections Academy.
                                 </li>
                                 							
                                 <li>Candidates arriving late, will not be admitted into the orientation after the scheduled
                                    start time.
                                 </li>
                                 							
                                 <li>Candidates will be refused entrance to orientation if inappropriately dressed. Please
                                    wear dress slacks or skirts, button-down shirts or blouse. No sneakers.
                                 </li>
                                 							
                                 <li>Location:&nbsp;School of Public Safety Auditorium, 8600 Valencia College Lane, Orlando,
                                    FL 32825
                                 </li>
                                 							
                                 <li>Time:&nbsp;Orientation sign-in is 8:15 a.m. - 8:45 a.m. If you arrive after 8:45 a.m.,&nbsp;you
                                    will not be admitted. Orientation begins promptly at 9:00 a.m.
                                 </li>
                                 						
                              </ul>
                              						
                              <h4>2017 Orientation Dates</h4>
                              						
                              <blockquote>
                                 							
                                 <div>
                                    								
                                    <div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <div><s>January 20 </s></div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div><s>February 17</s></div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div><s>March 10 </s></div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <div>April 7</div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>May 12</div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>June 9</div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <div>July 7</div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>August 11</div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>September 8</div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       									
                                       <div>
                                          										
                                          <div>
                                             											
                                             <p>October 13</p>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>November 10</div>
                                             										
                                          </div>
                                          										
                                          <div>
                                             											
                                             <div>December 8</div>
                                             										
                                          </div>
                                          									
                                       </div>
                                       								
                                    </div>
                                    							
                                 </div>
                                 						
                              </blockquote>
                              						
                              <p><strong>2. Submit a Valencia Application</strong></p>
                              						
                              <ul>
                                 							
                                 <li>Apply to Valencia via the&nbsp;<a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.P_DispLoginNon" target="_blank"><u>online admissions application</u></a>. Note: Please select&nbsp;Public Safety&nbsp;for the application type. There is a $35 application
                                    fee.
                                 </li>
                                 							
                                 <li>Please allow 5 -&nbsp;7 days for your application to be processed before attempting to
                                    take the CJBAT.
                                 </li>
                                 							
                                 <li>Be sure to complete the Statement of Florida Residency for Tuition Purposes.</li>
                                 						
                              </ul>
                              						
                              <p><strong>3. Take the Criminal Justice Basic Abilities Test (CJBAT)</strong></p>
                              						
                              <ul>
                                 							
                                 <li>The Criminal Justice Basic Abilities Test (CJ BAT) for law enforcement and correctional
                                    officers consists of 125 multiple-choice questions that will assess your language,
                                    visualization and reasoning skills. It is an FDLE required test and there are no exemptions.
                                    Students must pass the correct BAT for the discipline they are applying for;&nbsp;Corrections
                                    BAT&nbsp;for corrections or&nbsp;BAT Law Enforcement&nbsp;for law enforcement.
                                 </li>
                                 							
                                 <li>All paperwork for this test is completed in the Assessment Centers at the East, West
                                    or Osceola Campuses. There is a $40 fee for this test. To find out the times that
                                    the test is administered, visit the&nbsp;<a href="/locations/assessments/cjbat/index.html" target="_blank"><u>Assessment Center website.</u></a></li>
                                 							
                                 <li>The test is administered by computer. You will have two hours and thirty minutes to
                                    complete the CJ BAT.
                                 </li>
                                 							
                                 <li>Test Scores are available immediately after the test has been completed by the student.
                                    One copy of the results will be given to the student. It is the responsibility of
                                    the student to ensure that the Criminal Justice Institute receives a copy of the test
                                    results.
                                 </li>
                                 							
                                 <li>The&nbsp;CJ BAT is valid for 4 years.</li>
                                 						
                              </ul>
                              						
                              <p><strong>4.&nbsp;Submit a Criminal Justice Institute Academy Application</strong></p>
                              						
                              <ul>
                                 							
                                 <li>Once the above steps are completed successfully, applicants must turn in their CJ
                                    BAT score in order to receive the CJI application.
                                 </li>
                                 							
                                 <li>Applications will not be provided without a passing CJ BAT score. No exceptions.</li>
                                 							
                                 <li>Candidates&nbsp;with an arrest or three traffic citations in the last three years must&nbsp;also
                                    be interviewed&nbsp;by the director or designated academy coordinator. To schedule an interview,
                                    please call (407) 582-8200. Bring all supporting documentation to the interview.&nbsp;Candidates
                                    must be professionally dressed during the interview.&nbsp;Candidates not deemed to be dressed
                                    appropriately will not be interviewed.
                                 </li>
                                 							
                                 <li>When the Academy application is&nbsp;fully completed&nbsp;with physical and all required documents,
                                    return to the Criminal Justice Institute and pay the $100 application fee.
                                 </li>
                                 							
                                 <li>Your name will then be placed on the waiting list for the next available academy.</li>
                                 							
                                 <li>Office Location:&nbsp;<a href="/locations/map/public-safety.html" target="_blank"><u>Valencia College School of Public Safety, 8600 Valencia College Lane, Orlando, FL
                                          32825</u></a></li>
                                 						
                              </ul>
                              						
                              <p><strong>5. Receive Notification from CJI</strong></p>
                              						
                              <ul>
                                 							
                                 <li>You will receive a phone call and letter from the Criminal Justice Institute inviting
                                    you to participate in the Physical Assessment Test.
                                 </li>
                                 						
                              </ul>
                              						
                              <p><strong>6. Take the Physical Assessment Test</strong></p>
                              						
                              <ul>
                                 							
                                 <li>Physical Assessment Tests will be administered approximately one (1) month prior to
                                    the start of the academy. The assessment test is only good for the academy that is
                                    in the current registration process.
                                 </li>
                                 							
                                 <li>Applicants who pass the test will be offered a seat in the upcoming academy.</li>
                                 							
                                 <li>Applicants who fail the test will remain on the waiting list and may be invited to
                                    re-take the Physical Assessment for the following academy.
                                 </li>
                                 							
                                 <li>All applicants must provide proof of medical insurance prior to taking the Physical
                                    Assessment.
                                 </li>
                                 							
                                 <li>Familiarize&nbsp;yourself&nbsp;with the&nbsp;<a href="/locations/school-of-public-safety/documents/CJI-PhysicalAssessmentGuideFeb2015.pdf" target="_blank"><u>Physical Assessment Guide</u></a>&nbsp;prior to the assessment. Failure to comply with these instructions may result in
                                    disqualification for the current selection process.
                                 </li>
                                 						
                              </ul>
                              						
                              <p><strong>7. Register and Pay Tuition</strong></p>
                              						
                              <ul>
                                 							
                                 <li>Upon receipt of an acceptance letter into an Academy, you will register and pay the
                                    tuition for the&nbsp;Academy at the School of Public Safety:&nbsp;8600&nbsp;Valencia College Lane.
                                 </li>
                                 							
                                 <li>The tuition must be paid in full by cash, money order, cashiers check, credit card
                                    or personal check on the day you are registered for the Academy.
                                 </li>
                                 							
                                 <li>After registering, make sure to purchase your uniforms and books according to the
                                    academy guidelines that are listed in the front of the academy application.
                                 </li>
                                 						
                              </ul>
                              						
                              <p><strong>8. Go to Class</strong></p>
                              						
                              <hr>
                              						
                              <h3>FIRE SCIENCE ACADEMY TRACK</h3>
                              					
                           </div>
                           					
                           <div>
                              						
                              <div>
                                 							
                                 <p>The Fire Science Academy is a limited-access program and admission is competitive.
                                    The top candidates will be selected based on their application information.
                                 </p>
                                 							
                                 <p>1. <a href="/locations/school-of-public-safety/contact.html">Contact the Career Program Advisor</a> to learn about the program.
                                 </p>
                                 							
                                 <p>2. Attend a mandatory information session.</p>
                                 							
                                 <p>3.&nbsp;Apply to Valencia via the <a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.P_DispLoginNon">online admissions application</a>. There is a $35 application fee.
                                 </p>
                                 							
                                 <p>4.&nbsp;Follow the college's remaining <a href="/locations/admissions-records/steps.html">Steps to Enroll</a>.
                                 </p>
                                 							
                                 <p>5.&nbsp;Meet the following additional admission requirements.<u> </u></p>
                                 							
                                 <ul>
                                    								
                                    <li>Must be at least 18 years of age<u> </u></li>
                                    								
                                    <li>Possess a high school diploma or GED<u> </u></li>
                                    								
                                    <li>Have no convictions or significant misdemeanors or felonies<u> </u></li>
                                    								
                                    <li>Be of good moral character<u> </u></li>
                                    								
                                    <li>Be in good physical condition<u> </u></li>
                                    							
                                 </ul>
                                 							
                                 <p>6. Attend aspecial orientation session.You will also be required to complete a separate
                                    application ($50) that you will receive from the department when you attend the special
                                    orientation.
                                 </p>
                                 							
                                 <hr>
                                 							
                                 <h3>&nbsp;FIRE FIGHTER MINIMUM STANDARDS CLASS</h3>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <div>
                              						
                              <div>
                                 							
                                 <p>This is a&nbsp;limited-access program with special admissions requirements.&nbsp;Admission to
                                    Valencia College does not imply acceptance to the Fire Rescue&nbsp;Institute's&nbsp;Fire Fighter
                                    program. This program has a separate admission process.
                                 </p>
                                 							
                                 <p>The Fire Fighter Minimum Standards Class may&nbsp;not be offered every semester. Check
                                    our <a href="/locations/school-of-public-safety/fire-rescue-institute/default.html" target="_blank">events&nbsp;calendar</a> for availability.
                                 </p>
                                 							
                                 <p>1. Apply to Valencia via the&nbsp;<a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.P_DispLoginNon" target="_blank"><u>online admissions application</u></a>. There is a $35 application fee.
                                 </p>
                                 							
                                 <p>2. Meet the&nbsp;following additional admission requirements.</p>
                                 							
                                 <ul>
                                    								
                                    <li>Must be at least 18 years of age</li>
                                    								
                                    <li>Possess a high school diploma or GED</li>
                                    								
                                    <li>Be a Florida certified EMT - Basic or Paramedic</li>
                                    								
                                    <li>Have no convictions or significant misdemeanors or felonies</li>
                                    								
                                    <li>Be of good moral character</li>
                                    								
                                    <li>Be in good physical condition</li>
                                    							
                                 </ul>
                                 							
                                 <p>3.&nbsp;Attend a&nbsp;special orientation session.&nbsp;You will also be required to complete a separate
                                    application ($50) that you will receive from the department when you attend the special
                                    orientation.
                                 </p>
                                 							
                                 <p><em>The Fire Fighter program is a&nbsp;vocational credit program&nbsp;that is not part of the Fire
                                       Science A.S. degree. It is&nbsp;not eligible for financial aid.&nbsp;</em><a href="http://catalog.valenciacollege.edu/financialinformationfees/estimatedexpenses/centralfloridafireinstitute/" target="_blank"><u>Review estimated expenses.</u></a></p>
                                 							
                                 <hr>
                                 							
                                 <h3>&nbsp;FLORIDA BUREAU OF FIRE STANDARDS AND TRAINING CERTIFICATIONS</h3>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <div>
                              						
                              <div>
                                 							
                                 <p>For fire fighters desiring to earn certifications through the Florida Bureau of Fire
                                    Standards and Training, Valencia offers sets of courses that qualify students to test
                                    for certifications&nbsp;in&nbsp;Fire Officer I,&nbsp;Fire Officer II,&nbsp;Firesafety Inspector I,&nbsp;Firesafety
                                    Inspector II,&nbsp;Fire Instructor and&nbsp;Apparatus and Pumps Operator.
                                 </p>
                                 							
                                 <ol>
                                    								
                                    <li>Apply to Valencia via the&nbsp;<a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.P_DispLoginNon" target="_blank"><u>online admissions application</u></a>. There is a $35 application fee.
                                    </li>
                                    								
                                    <li>Take the&nbsp;set of courses&nbsp;outlined for your desired certification.</li>
                                    								
                                    <li>Once you've completed the course series, you will be qualified to sit for the state
                                       exam.
                                    </li>
                                    							
                                 </ol>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <hr>
                           					
                           <h3>ASSOCIATE IN SCIENCE DEGREES AND CERTIFICATES</h3>
                           					
                           <div>
                              						
                              <div>
                                 							
                                 <p>Valencia's two-year A.S. degrees provide a path to employment, a bachelor's degree
                                    and career enhancement. We offer several A.S. programs for public safety professionals,
                                    including&nbsp;<a href="http://valenciacollege.edu/asdegrees/fire-services/fire-science-technology.cfm" target="_blank"><u>Fire Science Technology</u></a>,&nbsp;<a href="http://valenciacollege.edu/asdegrees/health/emst.cfm" target="_blank"><u>Emergency Medical Services Technology</u></a> and&nbsp;<a href="/locations/school-of-public-safety/criminal-justice-institute/index.html" target="_blank"><u>Criminal Justice</u></a>.&nbsp;Certificates provide specialized training in as little as a year and apply toward
                                    a related A.S. degree.
                                 </p>
                                 							
                                 <ol>
                                    								
                                    <li>Apply to Valencia via the&nbsp;<a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.P_DispLoginNon" target="_blank"><u>online admissions application</u></a>.&nbsp;There is a $35 application fee.
                                    </li>
                                    								
                                    <li>Follow the college's&nbsp;remaining&nbsp;<a href="/locations/admissions-records/steps.html" target="_blank"><u>Steps to Enroll</u></a>.
                                    </li>
                                    							
                                 </ol>
                                 							
                                 <p><u></u>If you are already state-certified in your field, you&nbsp;may be able to receive credits
                                    toward the A.S. degree for your&nbsp;professional experience. For details, contact a <a href="/locations/school-of-public-safety/contact.html" target="_blank">Career Program Advisor</a>.
                                 </p>
                                 							
                                 <hr>
                                 							
                                 <h3>&nbsp;ADVANCED SPECIALIZED TRAINING</h3>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <div>
                              						
                              <div>
                                 							
                                 <p>These programs and courses are offered throughout the year and do not have a formal
                                    admissions process.&nbsp;
                                 </p>
                                 							
                                 <p><strong>Fire Services</strong></p>
                                 							
                                 <p>Advanced Specialized Training for fire services professionals is available through
                                    open registration online.
                                 </p>
                                 							
                                 <blockquote>
                                    								
                                    <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-services/" target="_blank"><u>View Offerings</u></a></p>
                                    							
                                 </blockquote>
                                 							
                                 <p>&nbsp;</p>
                                 							
                                 <p><strong>Law Enforcement and Corrections</strong></p>
                                 							
                                 <p>Courses are open to sworn Florida law enforcement, corrections or probation officers.
                                    Officers from agencies located in Orange County have priority enrollment over officers
                                    from other counties or state agencies.&nbsp;Only agency training coordinators can register
                                    officers for Advanced Specialized Training courses. Contact your agency training coordinator
                                    if you are interested in enrolling in a course or program.
                                 </p>
                                 							
                                 <blockquote>
                                    								
                                    <p><a href="https://secure.valenciacollege.edu/cjisignup2/index.cfm" target="_blank"><u>View Offerings</u></a></p>
                                    							
                                 </blockquote>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <hr>
                           					
                           <h3>CONTINUING EDUCATION</h3>
                           					
                           <div>
                              						
                              <div>
                                 							
                                 <p>These programs and courses are offered throughout the year and do not have a formal
                                    admissions process. You may register for upcoming offerings at any&nbsp;time online. Please
                                    note, that space is limited and courses fill up fast.
                                 </p>
                                 							
                                 <blockquote>
                                    								
                                    <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-services/" target="_blank"><u>Fire Services</u></a></p>
                                    								
                                    <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/criminal-justice/" target="_blank"><u>Law Enforcement, Security and Corrections</u></a></p>
                                    							
                                 </blockquote>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/how-to-apply.pcf">©</a>
      </div>
   </body>
</html>