<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Fire Fighter Minimum Standards Course | Valencia College</title>
      <meta name="Description" content="Fire Fighter Minimum Standards Course">
      <meta name="Keywords" content="college, school, educational, fire, fighter, standards">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/fire-rescue-institute/academy-fire-fighter-minimum-standards-course.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/fire-rescue-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>Fire Rescue</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li><a href="/locations/school-of-public-safety/fire-rescue-institute/">Fire Rescue Institute</a></li>
               <li>Fire Fighter Minimum Standards Course</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Fire Fighter Minimum Standards Course</h2>
                        
                        <h3>PROGRAM DESCRIPTION:</h3>
                        
                        <p>The Fire Fighter Minimum Standards program (MSC) includes both Fire Fighter I and
                           Fire Fighter II courses, and is designed to incorporate the orientation to fire service
                           with the necessary theory and applications needed to become a certified Fire Fighter.
                           This program meets the requirements for students to take the state certification exam
                           with the Bureau of Fire Standards and Training (which includes both written and practical
                           skills) to become a Certified Fire Fighter in the state of Florida. This is a vocational
                           credit, limited-access program. Once the state Certification is obtained, students
                           can apply to have 3 academic credits awarded toward an AS degree in Fire Science Technology.
                        </p>
                        
                        <p><strong>Note:</strong></p>
                        
                        <p>The practical training portions of Fire Fighter I &amp; II will be offered at the Fire
                           training facility located at John Young Parkway and Oak Ridge Road. Students must
                           furnish their own transportation.
                        </p>
                        
                        <p><strong>POTENTIAL CAREERS: </strong>Fire Fighter
                        </p>
                        
                        <h3>ADMISSION REQUIREMENTS:</h3>
                        
                        <p>Admission to this program is limited. Students must meet the following criteria:</p>
                        
                        <ul>
                           
                           <li>Must be at least 18 years of age</li>
                           
                           <li>Possess a high school diploma or a GED</li>
                           
                           <li>Must successfully complete a Florida EMT or Paramedic program</li>
                           
                           <li>Have no conviction of felonies or significant misdemeanors</li>
                           
                           <li>Cannot have been&nbsp;dishonorably discharged from military service</li>
                           
                           <li>Be of good moral character</li>
                           
                           <li>Must be in good physical condition</li>
                           
                        </ul>
                        
                        <h3>REQUIREMENTS FOR STATE CERTIFICATION:</h3>
                        
                        <ul>
                           
                           <li>Successfully complete required coursework for this program&nbsp;</li>
                           
                           <li>Receive Certificate of Competency as Firefighter I</li>
                           
                           <li>Successfully pass the State Exam for Fire Fighter II</li>
                           
                        </ul>
                        
                        <h3>REQUIRED COURSEWORK:</h3>
                        
                        <div>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top" bgcolor="#EBEBEB">
                                    
                                    <td width="23%"><strong>Course</strong></td>
                                    
                                    <td width="23%"><strong>Title</strong></td>
                                    
                                    <td width="23%"><strong>Contact Hours</strong></td>
                                    
                                    <td width="31%"><strong>Vocational Credit</strong></td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 0010</td>
                                    
                                    <td>Fire Fighter I</td>
                                    
                                    <td>206</td>
                                    
                                    <td>6.867</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 0020</td>
                                    
                                    <td>Fire Fighter II</td>
                                    
                                    <td>192</td>
                                    
                                    <td>6.4</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td colspan="2">
                                       
                                       <div align="right">Total Career Certificate Clock Hours</div>
                                       
                                    </td>
                                    
                                    <td>418</td>
                                    
                                    <td>13.934</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <p>&nbsp;</p>
                        
                        <p><strong>Fire Fighter I Course</strong></p>
                        
                        <p>The Fire Fighter I course provides both practical applications and an orientation
                           to the fire service. This course includes many functions required of a fire fighter
                           such as: fire service safety; fire behavior; building construction; protective clothing;
                           SCBA; portable extinguishers; ropes and knots; building search and victim removal;
                           forcible entry tools; constructions techniques; ground ladders; ventilation; water
                           supply; loading and rolling hose; laying, carrying and advancing hose lines; water
                           streams; vehicle and wildland fire control; sprinkler system fundamentals; salvage
                           and overhaul; protecting evidence of fire cause; fire department communication; equipment
                           and techniques; fire prevention and public fire education. The course also includes
                           Operations-Level Hazardous Materials Training. Upon completion of the program, the
                           student will receive a Certificate of Competency from the Bureau of Fire Standards
                           and Training as a Firefighter I which will qualify them to serve as a volunteer Fire
                           Fighter.
                        </p>
                        
                        <p><strong>Fire Fighter II&nbsp;Course</strong></p>
                        
                        <p>The student must successfully complete the requirements for Fire Fighter I to enter
                           Fire Fighter II. The Fire Fighter II course prepares the student to meet the requirements
                           to become a state certified fire fighter. Course content includes implementing the
                           incident management system; construction materials and building collapse; rescue and
                           extrication tools; vehicle extrication and special rescue; hydrant flow and hose operability;
                           tools and appliances; foam fire systems; ignitable liquid and gas fire control; fire
                           detection; alarm and suppression systems; fire cause and origin; radio communications;
                           incident reports; pre-incident survey; and wildland fire fighting. Students must successfully
                           complete the program to be eligible to test for certification with the Florida Bureau
                           of Fire Standards and Training for Fire Fighter II. This exam encompasses both written
                           and practical skills. Certification is required in the state of Florida to become
                           a career Fire Fighter.&nbsp;
                        </p>
                        
                        <p><strong>Program Outcomes</strong></p>
                        
                        <ul>
                           
                           <li>Demonstrate knowledge of fire department organization,&nbsp;procedures and responsibilities&nbsp;</li>
                           
                           <li>Demonstrate knowledge of personal protective equipment</li>
                           
                           <li>Demonstrate knowledge of ventilation practices</li>
                           
                           <li>Demonstrate the proper use of ropes, tools and equipment&nbsp;</li>
                           
                           <li>Demonstrate proper use of fire hose, nozzles and appliances&nbsp;</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/fire-rescue-institute/academy-fire-fighter-minimum-standards-course.pcf">©</a>
      </div>
   </body>
</html>