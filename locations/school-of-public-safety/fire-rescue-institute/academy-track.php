<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Fire Science Academy Track | Valencia College</title>
      <meta name="Description" content="Fire Science Academy Track">
      <meta name="Keywords" content="college, school, educational, fire, science, academy, track">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/school-of-public-safety/fire-rescue-institute/academy-track.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/school-of-public-safety/fire-rescue-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>Fire Rescue</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/school-of-public-safety/">School Of Public Safety</a></li>
               <li><a href="/locations/school-of-public-safety/fire-rescue-institute/">Fire Rescue Institute</a></li>
               <li>Fire Science Academy Track</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        	
                        <h2>Fire Science Academy Track</h2>
                        
                        <h3>PROGRAM DESCRIPTION:</h3>
                        
                        <p>The Fire Science Academy is a degree-granting program that provides students with
                           the skills, abilities and education for a successful fire service career. It provides
                           a strong educational foundation in fire science coupled with training and education
                           in emergency medical services and firefighting. Students will complete the two-year
                           academy as a cohort, learning to function as team members through instructional and
                           team-building exercises.
                        </p>
                        
                        <p><strong>One Program, Three Areas of Education</strong></p>
                        
                        <p>The Fire Science Academy is a fast-paced, intensive program. Participants will complete
                           three components of career development in just two years.
                        </p>
                        
                        <p>Upon completion, students will receive:</p>
                        
                        <ul>
                           
                           <li>A.S. degree in Fire Science Technology (60 college credits)</li>
                           
                           <li>EMT (Emergency Medical Technology) Technical Certificate (must pass a state exam)</li>
                           
                           <li>Minimum Standards Training – Firefighter I and II (must pass a state exam)</li>
                           
                        </ul>
                        
                        <h3>ADMISSION REQUIREMENTS:</h3>
                        
                        <p>The Fire Science Academy is a limited-access program and admission is competitive.
                           The top candidates will be selected based on their application information. Before
                           being considered, all candidates must attend a mandatory information session.
                        </p>
                        
                        <ul>
                           
                           <li>Must be at least 18 years of age<u> </u></li>
                           
                           <li>Possess a high school diploma or GED<u> </u></li>
                           
                           <li>Have no felony convictions or significant misdemeanors</li>
                           
                           <li>Be of good moral character<u> </u></li>
                           
                           <li>Be in good physical condition<u> </u></li>
                           
                        </ul>
                        
                        <h3>REQUIRED COURSEWORK:</h3>
                        
                        <div>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top" bgcolor="#EBEBEB">
                                    
                                    <td width="23%"><strong>Course</strong></td>
                                    
                                    <td width="23%"><strong>Title</strong></td>
                                    
                                    <td width="31%"><strong>Credits</strong></td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 1000</td>
                                    
                                    <td>Fire Science Fundamentals</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 1505</td>
                                    
                                    <td>Fire Prevention I</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 1612</td>
                                    
                                    <td>Fire Behavior and Combustion</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>ENC 1101</td>
                                    
                                    <td>English Composition I</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>MGF 1106</td>
                                    
                                    <td>College Mathematics</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 1540</td>
                                    
                                    <td>Private Fire Protection Systems I</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 2120</td>
                                    
                                    <td>Building Construction for the Fire Service</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>SPC 1608</td>
                                    
                                    <td>Fundamentals of Speech</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 2740</td>
                                    
                                    <td>Fire Service Course Delivery</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>POS 2112</td>
                                    
                                    <td>State and Local Government</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 1040</td>
                                    
                                    <td>Minimum Standards Training Classes: Firefighter I, Firefighter II, Emergency Vehicle
                                       Operator
                                    </td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 2810</td>
                                    
                                    <td>Firefighting Tactics and Strategy I</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 2510</td>
                                    
                                    <td>Fire Codes and Standards</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 2720</td>
                                    
                                    <td>Company Officer</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>FFP 2521</td>
                                    
                                    <td>Blueprint Reading and Plans Review</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>PHI 2600</td>
                                    
                                    <td>Ethics and Critical Thinking</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>EMS 1119</td>
                                    
                                    <td>Fundamentals of Emergency Medical Technology</td>
                                    
                                    <td>8</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>EMS 1119L</td>
                                    
                                    <td>Fundamentals of Emergency Medical Technology</td>
                                    
                                    <td>2</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>EMS 1431L</td>
                                    
                                    <td>Emergency Medical Technician Practicum</td>
                                    
                                    <td>2</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td colspan="2">
                                       
                                       <div align="right">Total Credits</div>
                                       
                                    </td>
                                    
                                    <td>60</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <h3>REQUIREMENTS FOR STATE CERTIFICATION:</h3>
                        
                        <ul>
                           
                           <li>After completing FFP 0020, students must pass the Florida Bureau of Fire Standards
                              test (FL FF II) and complete the three EMS courses in the spring of the second year
                              of the program to be certified as a Firefighter.
                           </li>
                           
                           <li>After completing FFP 2810 Firefighting Tactics and Strategy I, students will earn
                              FEMA ICS 300 certificate.
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/school-of-public-safety/fire-rescue-institute/academy-track.pcf">©</a>
      </div>
   </body>
</html>