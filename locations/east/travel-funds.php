<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus Academic Division Travel Funds - East Campus - Valencia College | Valencia College</title>
      <meta name="Description" content="A multi-campus college dedicated to the premise that educational opportunities are necessary to bring together the diverse forces in society"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/travel-funds.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li>East Campus Academic Division Travel Funds - East Campus - Valencia College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <h2>East Campus Academic Division Travel Funds</h2>
                        
                        					
                        <h3>Principles and Guidelines</h3>
                        
                        					
                        <h4>PRINCIPLES</h4>
                        					
                        <ul style="list_style_1">
                           						
                           <li>East Campus Academic Division (<strong>ECAD</strong>) Travel Funds are meant to promote employee performance and skill enhancement and
                              to provide funding for East Campus full-time faculty, and for East Campus staff--career
                              or professional--who work in an academic division that reports to the president of
                              East Campus.
                           </li>
                           						
                           <li>Each Campus receives a specific amount of money each year to be used by the campus<strong> to cover travel costs and related expenses for conference, workshops and related
                                 professional development.</strong> On East Campus, the campus president disperses this money through a committee of
                              representatives from administration, faculty, and career staff (the ECAD travel committee).
                           </li>
                           					
                        </ul>
                        
                        					
                        <h4>GUIDELINES</h4>
                        					
                        <p>Listed below are guidelines the ECAD travel committee uses to award travel money.
                           
                        </p>
                        					
                        <ul style="list_style_1">
                           						
                           <li>The convention, conference, workshop, etc., must relate directly to your assigned
                              duties at Valencia and you must have the approval of your supervisor.
                           </li>
                           						
                           <li>New requests are considered ahead of requests from applicants who received funding
                              during the previous two cycles. 
                           </li>
                           						
                           <li>Preference is given to higher levels of participation (speakers or conference chairs).
                              
                           </li>
                           						
                           <li>Preference is given to requests that the travel committee considers to have high impact
                              on personal, division, campus, and college goals.
                           </li>
                           						
                           <li>Preference is given to requests for in-state travel.</li>
                           						
                           <li>In cases where multiple applicants wish to attend the same event, the travel committee
                              will also consider:
                              							
                              <ul style="list_style_1">
                                 								
                                 <li>The additional impact gained by multiple people attending the event.</li>
                                 								
                                 <li>The description of how what is learned at the event will be shared with others at
                                    the college.
                                 </li>
                                 								
                                 <li>Recommendations by division administration.</li>
                                 								
                                 <li>Which applications to the same event were recieved first.</li>
                                 							
                              </ul>
                              						
                           </li>
                           						
                           <li>Applicants who didn't use their last travel reward are considered after all other
                              applicants in a given cycle. 
                           </li>
                           					
                        </ul>
                        
                        					
                        <h4>AWARD AMOUNTS</h4>
                        					
                        <p>Starting with the 2016 - 2017 cycle, the annual cap is $1100 with planned increases
                           of ~3% per year. The goal of the cap is to balance adequate funding for individual
                           trips and an equitable distribution of the travel money.
                        </p>
                        					
                        <p>The ECAD travel committee monitors and reacts to the amount of money that comes available
                           each year for travel. It also debates on a regular basis the merits of capping awards
                           versus fully funding trips, taking into account feedback from the East Campus faculty
                           senate, career and professional staff eligible for the awards, and the campus president.
                        </p>
                        
                        					
                        <h4>NOTIFICATION</h4>
                        					
                        <p>Award emails are typically sent out to the employee receiving the award and their
                           dean or immediate supervisor within a week of application deadlines.
                        </p>
                        
                        					
                        <h4>RETURNING / REALLOCATING YOUR AWARD</h4>
                        					
                        <p>Because the travel committee must sometimes reject worthy applications due to limited
                           budgets, it expects awarded money to be used. If an applicant doesn't use their award
                           money, this becomes a consideration for the committee the next time the applicant
                           asks for travel money.
                        </p>
                        					
                        <p>If you decide not to use your funding, you should inform your dean and the ECAD travel
                           committee chairs as soon as possible.
                        </p>
                        
                        					
                        <h4>SHARING YOUR INFORMATION</h4>
                        					
                        <p>Upon returning from your trip, you’re encouraged to share information as appropriate
                           with students and colleagues to increase the impact of your experience. Past awardees
                           have presented in division and committee meetings, shared in Blackboard course shells,
                           held skillshops or posted on social media.
                        </p>
                        					
                        <p>If you wish, you may also shared what you learned with the ECAD travel committee by
                           emailing the committee co-chair, Robert McCaffrey, at rmccaffrey@valenciacollege.edu.
                           The committee will maintain a record on this page for the benefit of the campus at
                           large.
                        </p>
                        
                        					
                        <h3>Cycles / Application Deadlines</h3>
                        
                        					
                        <h4>2017 - 2018</h4>
                        					
                        <p><strong>Note: </strong>After filling out the online form, print it, sign it, and send it to your dean or
                           supervisor for an approval signature. Complete applications are due to the campus
                           president's office by the end of the business day on the application deadline. 
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Cycle</th>
                                 								
                                 <th>If You Plan To <br> Travel During
                                 </th>
                                 								
                                 <th>Application Deadline </th>
                                 								
                                 <th>Committee Meeting </th>
                                 								
                                 <th>% Budget Awarded </th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <div>Cycle 1 </div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>7/1/17 - 10/31/17</div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div><strong>5/19/17 12 noon </strong></div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>Before 6/1/17</div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>20%</div>
                                    								
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <div>Cycle 2 </div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>11/1/16 - 2/28/17</div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div><strong>9/29/17 5PM </strong></div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>Before 10/1/17</div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>40%</div>
                                    								
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <div>Cycle 3 </div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>3/1/18 - 6/30/18</div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div><strong>1/12/18 5PM </strong></div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>Before 2/1/18</div>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <div>40%</div>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        
                        					
                        <h3>FAQ</h3>
                        
                        					
                        <h4>How much ECAD money can I be awarded?</h4>
                        					
                        <p>For the 2016 - 2017 year, awards will be capped annually at $1100.</p>
                        					
                        <p>As of Spring 2016, the ECAD travel committee plans to raise this amount by ~3% each
                           year. Estimated caps moving forward would be:
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <td><strong>2017 - 2018 </strong></td>
                                 								
                                 <td> $1133 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>2018 - 2019 </strong></td>
                                 								
                                 <td> $1167 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>2019 - 2020 </strong></td>
                                 								
                                 <td> $1202 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>2020 - 2021 </strong></td>
                                 								
                                 <td> $1238 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>2021 - 2022 </strong></td>
                                 								
                                 <td> $1275 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p>Prior to the 2016 - 2017 cycle, awards were capped annually at $1000.</p>
                        
                        					
                        <h4>Who is eligible for ECAD travel funds?</h4>
                        					
                        <ol>
                           						
                           <li>East Campus career staff who work in an academic division, that is to say, those who
                              report to the campus president rather than a college-wide administrator
                           </li>
                           						
                           <li>East Campus full-time faculty, 'full-time' in this case being defined by the Faculty
                              Association Constitution.
                           </li>
                           					
                        </ol>
                        
                        					
                        <h4>When is the next application deadline?</h4>
                        					
                        <p>Applications are <strong>due by the end of the business day </strong>on the days listed under the 'Cycles' link on this page. Typically, this means by
                           Friday, 12 noon for the May deadline and Friday, 5 PM for the September and January
                           deadlines. 
                        </p>
                        
                        					
                        <h4>My event bridges more than one cycle. Which should I apply for?</h4>
                        					
                        <p>If the dates of your event occur in any cycle, you can apply. As an example, if you
                           wanted to visit a confernece that occured from Thursday, October 30th, 2015 through
                           Monday, November 3rd, 2015, you could apply for funding in either cycle 1 or cycle
                           2. 
                        </p>
                        
                        					
                        <h4>What if I return my award money?</h4>
                        					
                        <p>The ECAD travel committee makes a one-time consideration on your next application,
                           looking at it only after funding other eligible applications for that cycle. The reason
                           we do this is to reduce the chance that people apply for these funds without a commitment
                           to use them should they be awarded. Because eligible applications sometimes go unfunded
                           due to limited resources, we want awards to be used.
                        </p>
                        
                        					
                        <h3>Past Awards</h3>
                        					
                        <p>Committee Minutes <a href="documents/201520.docx">201520 </a>/ <a href="documents/201510.docx">201510</a> 
                        </p>
                        
                        					
                        <h3>Peer Reports</h3>
                        					
                        <p><span>2012</span></p>
                        					
                        <p><a href="documents/EdieGaythwaite_RolePlay_Conflict.pdf">Gaythwaite G.I.F.T.S.</a> <a href="documents/GIFTS.zip">ZIP file</a> 
                        </p>
                        
                        					
                        <h3>Apply for Travel Funds</h3>
                        					
                        <p><a href="http://net4.valenciacollege.edu/forms/east/travel-request/travel-request.cfm" target="_blank">Click here to apply!</a></p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/travel-funds.pcf">©</a>
      </div>
   </body>
</html>