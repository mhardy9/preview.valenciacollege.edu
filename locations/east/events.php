<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li>East Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../locations/east/index.html"></a>
                        
                        
                        
                        
                        <h2>Campus Events</h2>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/east_campus_banned_books_week" target="_blank" title="East Campus will be celebrating the freedom to read during the annual Banned Books Week event in September. This year, a coalition of organizations sponsoring Banned Books...">
                              
                              
                              <div>
                                 <img src="../../locations/east/caadbd02accec567953ceadc196649fb390e76d5.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>East Campus Banned Books Week  at East Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>September 21, 2017</strong>
                                    
                                    
                                    </span>
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/international_peace_day_festival" target="_blank" title="Join us for our annual International Day of Peace Festival in the Mall Area of the East Campus on Thursday, September 21st! Festivities will include arts and crafts, face...">
                              
                              
                              <div>
                                 <img src="../../locations/east/ec5e3f43cbfefe5a2c20360c64a79b19e3c20227.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>International Peace Day Festival  at East Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>September 21, 2017</strong>
                                    
                                    at 11:00 AM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/a-maize-ing_harvest_fest" target="_blank" title="Harvest Fest will have booths for students to learn about resources and services available at the East Campus. Get to know our staff from the Academic Advising, Veteran...">
                              
                              
                              <div>
                                 <img src="../../locations/east/6ee2ce3ba542bbfa71361842e807a245312a64e3.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>A-Maize-ing Harvest Fest at East Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>September 26, 2017</strong>
                                    
                                    at 11:00 AM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/on-campus_recruitment_-_city_year_7234" target="_blank" title="I am recruiting for the AmeriCorps position. The duties of this role are listed below.       Academic Support: Planning tutoring sessions and providing one-on-one or small...">
                              
                              
                              <div>
                                 <img src="../../locations/east/6ee2ce3ba542bbfa71361842e807a245312a64e3.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>On-Campus Recruitment - City Year at East Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>September 28, 2017</strong>
                                    
                                    at 10:00 AM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/career_center_open_house_and_dessert_reception" target="_blank" title="Join us for our annual Dessert Reception! Meet advisors from the Career Center, Internship and Workforce Services, and UCF Direct Connect! Find out how to sign up for the...">
                              
                              
                              <div>
                                 <img src="../../locations/east/920dd58fafb6c975197496fce453c40634f68b38.jpg">
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <span>Career Center Open House and Dessert Reception! at East Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>September 28, 2017</strong>
                                    
                                    at 11:00 AM</span>
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        <a href="http://events.valenciacollege.edu/EastCampus#.US4OrTBwea8">View All</a>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-299-5000</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>701 N Econlockhatchee Trail<br>Orlando, FL 32825
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac7710c6c2e1edb7e&amp;msa=0&amp;ll=28.554002,-81.251235&amp;spn=0.009198,0.017027" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/east.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        <a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="east campus virtual tour" height="245" src="../../locations/east/youvisit-east-245px.jpg" width="245"></a>
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/events.pcf">©</a>
      </div>
   </body>
</html>