<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/campus-president/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/campus-president/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li>Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/provost/east/index.html"></a>
                        
                        
                        
                        <p>Dr. Stacey Johnson, East and Winter Park  Campus President</p>
                        
                        <p>Welcome to Valencia's East Campus where the faculty and staff are committed to creating
                           an environment that is academically challenging, offers flexibility and spells success!
                        </p>
                        
                        <p>Our student and academic support services promote students and support their educational,
                           career, and life goals. Our goal will be to continue our quest for excellence by developing
                           programs and services to support the learning needs of our diverse student community.
                           We're proud to offer centralized academic support services in the Academic Success
                           Center for students from prep to honors.
                        </p>
                        
                        <p>On East Campus you'll find many opportunities to participate in a variety of art exhibits,
                           theater productions, and music &amp; dance performances. These performances and exhibits
                           are open to the public and I encourage all of you to participate. A calendar of events
                           is available at <a href="../../../locations/calendar/index.html">valenciacollege.edu/calendar</a>.
                        </p>
                        
                        <p>To learn more about the many exciting opportunities available at Valencia, please
                           use the following web links for more information:
                        </p>
                        
                        <p>Visit:&nbsp; <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">http://preview.valenciacollege.edu/future-students/visit-valencia/</a><u> </u></p>
                        
                        <p>East Campus is a stimulating place to study and grow and we look forward to seeing
                           you on campus soon!
                        </p>
                        
                        
                        <p>Dr. Stacey Johnson<br>
                           Campus President for East and Winter Park Campuses
                           
                        </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/campus-president/index.pcf">©</a>
      </div>
   </body>
</html>