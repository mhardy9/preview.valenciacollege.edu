<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/campus-president/teampresentations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/campus-president/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li><a href="/locations/east/campus-president/">Campus President</a></li>
               <li>East Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/provost/east/index.html"></a>
                        
                        <h2> Team Presentations</h2>
                        
                        <ul>
                           
                           <li><a href="../../../locations/east/locations-president/michelleFoster.html">Michelle Foster</a></li>
                           
                        </ul>
                        
                        <h3>Enrollment Presentations</h3>
                        
                        <ul>
                           
                           <li><a href="../../../locations/east/locations-president/documents/EnrollmentandFacilitiesManagement2-17-10.pdf">Enrollment and Facilities Update Booklet, February 17, 2010</a></li>
                           
                           <li>
                              <a href="../../../locations/east/locations-president/documents/EastCampusEnrollmentManagementRetreatppt-2-17-10.ppt">Enrollment and Facilities Update PowerPoint, February 17, 2010</a> 
                           </li>
                           
                        </ul>
                        
                        <h3>League for Innovation (March 2010) </h3>
                        
                        <ul>
                           
                           <li><a href="../../../locations/east/locations-president/documents/LeagueofInnovation-March2010-PrecisionSchedulingI.ppt">Precision Scheduling I PowerPoint Presentation </a></li>
                           
                           <li><a href="../../../locations/east/locations-president/EnrollmentReports.html">Enrollment Reports </a></li>
                           
                        </ul>
                        
                        <h3>East  &amp; Winter Park &nbsp;Listening Tour Campus Convocation December 14, 2012</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="http://prezi.com/pgjoptmbae0r/final-east-campus-convocation/?auth_key=6f0385ac1e40399c8e6196df703d877fea8a6951&amp;kw=view-pgjoptmbae0r&amp;rc=ref-25248887" target="_blank">East  Campus Presentation </a><br>
                              <a href="http://prezi.com/pgjoptmbae0r/final-east-campus-convocation/?auth_key=6f0385ac1e40399c8e6196df703d877fea8a6951&amp;kw=view-pgjoptmbae0r&amp;rc=ref-25248887"></a>
                              
                           </li>
                           
                           <li><a href="http://prezi.com/9gpm_1mcm0qn/copy-of-final-winter-park-campus-convocation/?auth_key=d057845b05d3aaa9f5745e21ddb1d39b60ee95fe&amp;kw=view-9gpm_1mcm0qn&amp;rc=ref-25248887" target="_blank">Winter  Park Presentation </a></li>
                           
                           <li><a href="http://www.kaltura.com/tiny/uqh9q">East Campus Recorded Presentation</a></li>
                           
                           <li><a href="http://www.kaltura.com/tiny/z4kw6">Winter Park Campus  Recorded&nbsp;Presentation</a></li>
                           
                        </ul>
                        
                        <h3>East Campus Welcme Back </h3>
                        
                        <ul>
                           
                           <li><a href="http://valenciacollege.edu/east/locations-president/documents/WelcomeBack2013Final2.pptx" target="_blank">Fall 2013 Welcome Back</a></li>
                           
                           <li><a href="http://valenciacollege.edu/east/locations-president/documents/WelcomeBackJan2014PowerPoint-MRFBDWEditsFINAL1-2-2014.pptx">Spring 2014 Welcome Back</a></li>
                           
                        </ul>            
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/campus-president/teampresentations.pcf">©</a>
      </div>
   </body>
</html>