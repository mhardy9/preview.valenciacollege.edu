<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/campus-president/eastdeans.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/campus-president/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li><a href="/locations/east/campus-president/">Campus President</a></li>
               <li>East Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/provost/east/index.html"></a>
                        
                        <p><strong>Dr. Stacey Johnson, Campus President for East and Winter Park Campuses </strong><br>
                           <a href="mailto:mthornton12@valenciacollege.edu">Mary Beth Thornton</a>, Executive Assistant, Sr.<br>
                           Building 3, Room 108 <br>
                           407-582-2216 
                        </p>
                        
                        
                        <h2><span>East Deans/Directors</span></h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><span><strong>Academic Affairs <a href="../../../locations/library/index.html"><br>
                                                </a></strong></span><a href="mailto:mrfoster@valenciacollege.edu">Michelle Foster</a>, Dean <br>
                                       Samantha Pabon, Administrative Assistant <br>
                                       Building 3, Room 108 <br>
                                       407-582-2007
                                    </p>
                                    
                                    <p> Susan Howard, Coordinator, Academic Affairs<br>
                                       Building 3, Room 108<br>
                                       407-582-2057
                                    </p>
                                    
                                    <p> Kenneth Kuk, Part-time Coordinator, Academic Affairs <br>
                                       Building 3, Room 108<br>
                                       407-582-2320 
                                    </p>
                                    
                                    <p><span><strong><a href="../../../locations/arts-and-entertainment/default.html">Arts and Entertainment</a><br>
                                             </strong></span><a href="mailto:wgivoglu@valenciacollege.edu">Wendy Givoglu</a>, Dean
                                    </p>
                                    
                                    <p>Pauline Chang, Office Manager</p>
                                    
                                    <p> Jackie Harris, 
                                       Administrative Assistant
                                    </p>
                                    
                                    <p> Building 3, Room 106<br>
                                       (407) 582-2340 
                                    </p>
                                    
                                    <p><span><strong><a href="../../../locations/departments/east/business/default.html">Business, IT, and Public Services </a><br>
                                             </strong></span><a href="mailto:cgordon15@valenciacollege.edu">Carin Gordon</a>,  Dean
                                    </p>
                                    
                                    <p>Suzie Gant, Administrative Assistant</p>
                                    
                                    <p> Stephanie Hines, Administrative Assistant </p>
                                    
                                    <p> Building 8, Room 105<br>
                                       (407) 582-2551
                                    </p>
                                    
                                    <p><span><strong><a href="../../../academics/departments/communications-east/default.html">Communications</a><br>
                                             </strong></span><a href="mailto:lneal@valenciacollege.edu">Linda Neal</a>, Dean
                                    </p>
                                    
                                    <p>Arlette Rivera, Administrative Assistant</p>
                                    
                                    <p> Kareen Watson, Administrative Assistant </p>
                                    
                                    <p> Building 7, Room 163<br>
                                       (407) 582-2444
                                    </p>
                                    
                                    <p><span><strong><a href="../../../academics/departments/communications-east/default.html">Humanities and Foreign Language </a><br>
                                             </strong></span><a href="mailto:dsutton@valenciacollege.edu">David Sutton</a>, Dean
                                    </p>
                                    
                                    <p>Denise Bourne, Administrative Assistant </p>
                                    
                                    <p> Alebia Medina, Administrative Assistant </p>
                                    
                                    <p> Building 6, Room 118 <br>
                                       (407) 582-2746
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><span><strong>Learning Support <br>
                                             </strong></span><a href="mailto:lbass11@valenciacollege.edu">Leonard Bass</a>, Dean 
                                    </p>
                                    
                                    <p>Stacey Ramos, Administrative Assistant</p>
                                    
                                    <p> Building 4, Room 213 <br>
                                       (407) 582-2823
                                    </p>
                                    
                                    <p> <a href="../../../locations/library/index.html">Library</a><br>
                                       <a href="../../../locations/east/academic-success-east/index.html">Academic Success Center
                                          </a> 
                                    </p>
                                    
                                    <p><span><strong><a href="../../../academics/departments/math-east/default.html">Mathematics</a><br>
                                             </strong></span><a href="mailto:mllee@valenciacollege.edu">Maryke Lee</a>, Dean
                                    </p>
                                    
                                    <p>Guy Miller, Administrative Assistant</p>
                                    
                                    <p> Dawn Chesire, Administrative Assistant </p>
                                    
                                    <p> Building 7, Room 142<br>
                                       (407) 582-2366
                                    </p>
                                    
                                    <p><span><strong><a href="../../../academics/departments/science/default.html">Science</a><br>
                                             </strong></span><a href="mailto:jsnyder38@valenciacollege.edu">Jennifer Snyder</a>, Dean
                                    </p>
                                    
                                    <p>Tonaysha Askew, Administrative Assistant<br>
                                       (407) 582-2434 
                                    </p>
                                    
                                    <p>Kelsey Bryceson, Administrative Assistant</p>
                                    
                                    <p> Building 1, Room 216 <br>
                                       (407) 582-2583
                                    </p>
                                    
                                    <p><span><strong><a href="../../../locations/east/social-sciences/default.html">Social Sciences and PE </a><br>
                                             </strong></span><span class="headBlack"><strong>(407) 582-2628 <br>
                                             </strong></span><a href="mailto:nmaurer1@valenciacollege.edu">Nancy Maurer</a>, Interim Dean
                                    </p>
                                    
                                    <p> Maria Castro, Administrative Assistant</p>
                                    
                                    <p> Jon Taylor, Administrative Assistant </p>
                                    
                                    <p> Building 8, Room 105<br>
                                       (407) 582-2443
                                    </p>
                                    
                                    <p><span><strong>Manager, Campus Operations<br>
                                             </strong></span><a href="mailto:rcorriveau1@valenciacollege.edu">Roger Corriveau</a><br>
                                       Building 3, Room 108H<br>
                                       407-582-2398
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <blockquote>
                           
                           
                           
                        </blockquote>            
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/campus-president/eastdeans.pcf">©</a>
      </div>
   </body>
</html>