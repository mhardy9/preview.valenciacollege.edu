<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/campus-president/enrollmentreports.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/campus-president/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li><a href="/locations/east/campus-president/">Campus President</a></li>
               <li>East Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/provost/east/index.html"></a>
                        
                        <h2>Enrollment Reports (League for Innovation-March 2010) </h2>
                        
                        
                        <blockquote>
                           
                           <h4>Red/Green Reports (Fall 2009 and Spring 2010) </h4>
                           
                           <ul>
                              
                              <li><a href="../../../locations/provost/east/documents/arts-and-entertainmentFirst-LastFall201010Spring201020.xls.html">Arts and Entertainment</a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/BITPSFirst-LastFall201010Spring201020.xls.html">Business, IT, and Public Services</a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/CommunicationsFirst-LastFall201010Spring201020.xls.html">Communications</a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/HumanitiesFirst-LastFall201010Spring201020.xls.html">Humanities and Foreign Languages</a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/MathematicsFirst-LastFall201010Spring201020.xls.html">Mathematics</a></li>
                              
                              <li> <a href="../../../locations/provost/east/documents/ScienceFirst-LastFall201010Spring201020.xls.html">Science</a>
                                 
                              </li>
                              
                              <li>
                                 <a href="../../../locations/provost/east/documents/SocialScienceFirst-LastFall201010Spring201020.xls.html">Social Sciences and Physical Eduation</a> 
                              </li>
                              
                              <li>
                                 <a href="../../../locations/provost/east/documents/StudentSuccessFirst-LastFall201010Spring201020.xls.html">Student Success</a> 
                              </li>
                              
                           </ul>
                           
                           <h4>Red/White Report (Spring 2010) </h4>
                           
                           <ul>
                              
                              <li><a href="../../../locations/provost/east/documents/arts-and-entertainmentFirst-LastFall201010Spring201020.xls.html">Red/White Report (Spring 2010) </a></li>
                              
                           </ul>
                           
                           <h4>Instructional Capacity Reports </h4>
                           
                           <ul>
                              
                              <li><a href="../../../locations/provost/east/documents/Coursecap201010asof3-25-09FIRSTFORFALL.xls.html">Course Cap Fall 2010 (First Report) </a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/Coursecap201010asof9-3-09LASTFORFALL.xls.html">Course Cap Fall 2010 (Last Report) </a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/Coursecap201010DUAL.xls.html">Course Cap 2010 Dual Enrollment</a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/Coursecap201010Honors.xls.html">Course Cap 2010 Honors </a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/Coursecap201010Online.xls.html">Course Cap 2010 Online </a></li>
                              
                           </ul>
                           
                           <h4>Pink Bar Reports </h4>
                           
                           <ul>
                              
                              <li><a href="../../../locations/provost/east/documents/6percentEnrollmentProjectionReport-arts-and-entertainment201010.xls.html">Arts and Entertainment</a></li>
                              
                              <li>
                                 <a href="../../../locations/provost/east/documents/5percentEnrollmentProjectionReportBITPS201010.xls.html">Business, IT, and Public Services</a> 
                              </li>
                              
                              <li><a href="../../../locations/provost/east/documents/5percentEnrollmentProjectionReportComm201010.xls.html">Communications</a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/5percentEnrollmentProjectionReport-HumFL201010.xls.html">Humanities and Foreign Languages</a></li>
                              
                              <li><a href="../../../locations/provost/east/documents/5percentEnrollmentProjectionReportMathematics201010.xls.html">Mathematics</a></li>
                              
                              <li> <a href="../../../locations/provost/east/documents/5percentEnrollmentProjectionReportScience201010.xls.html">Science</a>
                                 
                              </li>
                              
                              <li>
                                 <a href="../../../locations/provost/east/documents/5percentEnrollmentReportSocialScience-PE201010.xls.html">Social Sciences and Physical Eduation</a> 
                              </li>
                              
                              <li>
                                 <a href="../../../locations/provost/east/documents/5percentEnrollmentReportStudentSuccess201010.xls.html">Student Success</a> 
                              </li>
                              
                           </ul>
                           
                           
                           
                        </blockquote>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/campus-president/enrollmentreports.pcf">©</a>
      </div>
   </body>
</html>