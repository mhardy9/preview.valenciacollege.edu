<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/computerlabs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li>East Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">
                        
                        					
                        <h2>Campus Labs</h2>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Labs open to all students</th>
                                 								
                                 <th>Labs open to students in specific disciplines</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <ul>
                                       										
                                       <li><a href="#communications">Atlas Access Lab</a></li>
                                       										
                                       <li><a href="#communications">Communications Center </a> 
                                       </li>
                                       										
                                       <li><a href="#infolab">Library Computer Lab </a></li>
                                       										
                                       <li><a href="#microcomputer">Student Computer Lab </a></li>
                                       									
                                    </ul>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <ul>
                                       										
                                       <li><a href="#business">Business Service Lab </a></li>
                                       										
                                       <li><a href="#foreign">Foreign Language Labs</a></li>
                                       										
                                       <li><a href="#foreign">Graphics Technology Lab </a></li>
                                       										
                                       <li><a href="#math">Math Center</a></li>
                                       									
                                    </ul>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <h3>Labs Open To All Students</h3>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Atlas Access Lab</th>
                                 								
                                 <th><a name="math"></a> Building 5, Room 213 
                                 </th>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p>Open Lab for any registered student to conduct educational/career related research
                           and planning. Open use includes Access to the Internet and Atlas account Staff on
                           hand to assist. Also supports <strong>SLS1122</strong></p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p><strong>Fall/Spring</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 8am-7pm
                                       										<br>
                                       										<strong>Friday</strong>: 9am-5pm
                                    </p>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p><strong>Summer</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 8am-7pm
                                       										<br>
                                       										<strong>Friday</strong>: 9am-12pm
                                    </p>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Communications Center </th>
                                 								
                                 <th><a name="math"></a> Building 4, Room 120 
                                 </th>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p><em>The Communications Center</em> provides writing support for all disciplines. Writing consultants work with students
                           on a walk-in basis and provide feedback about the strengths and weaknesses of the
                           students’ academic writing. Software for English for Academic Purposes (EAP) lab work
                           and for many reading and writing courses is also available, as are CPT reviews for
                           reading and sentence skills.
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p><strong>Fall/Spring</strong></p>
                                    									
                                    <p><strong>Monday - Thursday: </strong>8am-7pm
                                       										<br>
                                       										<strong>Friday</strong>: 8am-3pm
                                       										<br>
                                       										<strong>Saturday</strong>: 9am-3pm
                                    </p>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p><strong>Summer</strong></p>
                                    									
                                    <p><strong>Monday - Thursday: </strong>8am-7pm
                                       										<br>
                                       										<strong>Friday</strong>: 8am-12pm
                                       										<br>
                                       										<strong>Saturday</strong>: 9am-2pm
                                    </p>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th><a href="/students/library/">Library Computer Lab</a></th>
                                 								
                                 <th><a name="infolab"></a>Building 4, 2nd Floor
                                 </th>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p>Library Computer Lab is the library's academic research lab available for Valencia
                           students. Access to the Internet and specialized reference databases (EBSCOhost, FirstSearch,
                           NewsBank’s InfoWeb, LINCCweb). Workstations dedicated to check e-mail are available.
                           Research assistance and technical support available.
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p><strong>Fall/Spring</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 7am-9:45pm
                                       										<br>
                                       										<strong>Friday</strong>: 7am-7:45pm
                                       										<br>
                                       										<strong>Saturday</strong>: 8am-3:45pm
                                       										<br>
                                       										<strong>Sunday</strong>: 2pm-7:45pm
                                    </p>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p><strong>Summer</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 7am-10pm
                                       										<br>
                                       										<strong>Friday</strong>: 7am-11:45pm
                                       										<br>
                                       										<strong>Saturday</strong>: 8am-1:45pm
                                    </p>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th><a href="/students/academic-success-east/math/">Math Center</a></th>
                                 								
                                 <th><a name="math"></a> Building 4, Room 108
                                 </th>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p>
                                       										<br>
                                       										<strong>Fall/Spring</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 7am-9:45pm
                                       										<br>
                                       										<strong>Friday</strong>: 7am-7:45pm
                                       										<br>
                                       										<strong>Saturday</strong>: 8am-3:45pm
                                       										<br>
                                       										<strong>Sunday</strong>: 2pm-7:45pm
                                    </p>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p><strong><br>
                                          										Summer</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 7am-10pm
                                       										<br>
                                       										<strong>Friday</strong>: 7am-11:45pm
                                       										<br>
                                       										<strong>Saturday</strong>: 8am-1:45pm
                                    </p>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Student Computer Lab </th>
                                 								
                                 <th><a name="math"></a> Building 4, Room 101
                                 </th>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p>Open to all students. Open use/including access to Internet. Also helps students with
                           video transfers. 
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p><strong>Fall/Spring</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 7am-9:45pm
                                       										<br>
                                       										<strong>Friday</strong>: 7am-7:45pm
                                       										<br>
                                       										<strong>Saturday</strong>: 8am-3:45pm
                                       										<br>
                                       										<strong>Sunday</strong>: 2pm-7:45pm
                                    </p>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p><strong>Summer</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 7am-10pm
                                       										<br>
                                       										<strong>Friday</strong>: 7am-11:45pm
                                       										<br>
                                       										<strong>Saturday</strong>: 8am-1:45pm
                                    </p>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <h3>Labs Open To Students In Specific Disciplines</h3>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Business Service Lab</th>
                                 								
                                 <th><a name="math"></a> Building 4, Room 201 
                                 </th>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p> For Business Students. Students have access to all software used in all business,
                           computer office systems, and accounting classes taught on East Campus.
                        </p>
                        					
                        <div>
                           						
                           <p><strong>Fall &amp; Spring</strong> 
                           </p>
                           						
                           <p><strong>Monday - Thursday: </strong>8am - 9:45pm
                              							<br>
                              							<strong>Friday</strong>: 8am - 7pm
                              							<br>
                              							<strong>Saturday: </strong>9am - 5pm
                           </p>
                           						
                           <p><strong>Summer</strong></p>
                           						
                           <p><strong>Monday - Thursday: </strong>8am - 9:45pm
                              							<br>
                              							<strong>Friday</strong>: 8am - 12pm
                              							<br>
                              							<strong>Saturday: </strong>closed
                           </p>
                           					
                        </div>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Foreign Language Labs</th>
                                 								
                                 <th><a name="foreign"></a> Building 4, Room 104
                                 </th>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p>Open to any student enrolled in language courses. Audio programs, reference books,
                           Instructional assistance available. 
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p><strong>Fall/Spring</strong></p>
                                    									
                                    <p><strong>Monday</strong>: 8am-10pm
                                       										<br>
                                       										<strong>Tuesday/Wednesday/Thursday</strong>: 8am-9:30pm
                                       										<br>
                                       										<strong>Friday</strong>: 8am-8pm
                                       										<br>
                                       										<strong>Saturday</strong>: 9am-3pm
                                    </p>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p><strong>Summer</strong></p>
                                    									
                                    <p><strong>Monday - Thursday</strong>: 8am-10pm
                                       										<br>
                                       										<strong>Friday</strong>: 8am-12pm
                                       										<br>
                                       										<strong>Saturday</strong>: 9am-2pm
                                    </p>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Graphics Technology Lab</th>
                                 								
                                 <th> <a name="math"></a> Building 1, Room 213
                                 </th>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p> Designed for AS Degree students in the Graphics Technology Program. Students have
                           access to specialized software with lab assistance available.
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p><strong>Fall/Spring</strong></p>
                                    									
                                    <p><strong>Monday-Friday:</strong> 9am-9pm
                                       										<br>
                                       										<strong>Saturday</strong>: 9am-2pm
                                    </p>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p><strong>Summer</strong></p>
                                    									
                                    <p><strong>Monday/Wednesday/Thursday</strong>: 9am-9pm
                                       										<br>
                                       										<strong>Tuesday:</strong> 9am-6pm
                                       										<br>
                                       										<strong>Friday</strong>: 9am-12pm
                                       										<br>
                                       										<strong>Saturday</strong>: 9am-2pm
                                    </p>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        				
                     </div>
                     
                     				
                     <aside class="col-md-3">
                        
                        
                        
                        					<a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        					<a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        					
                        <div>
                           						
                           <div>CONTACT</div>
                           						<br>
                           
                           						
                           <div>
                              
                              							
                              <div>
                                 								
                                 <div>
                                    									
                                    <div>407-299-5000</div>
                                    								
                                 </div>
                                 
                                 								
                                 <div>
                                    									
                                    <div>701 N Econlockhatchee Trail<br>Orlando, FL 32825
                                    </div>
                                    								
                                 </div>
                                 
                                 							
                              </div>
                              						
                           </div>
                           
                           					
                        </div>
                        					<a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac7710c6c2e1edb7e&amp;msa=0&amp;ll=28.554002,-81.251235&amp;spn=0.009198,0.017027" target="_blank">
                           						Directions</a>
                        					<a href="/locations/map/east-map.php" target="_blank">
                           						Campus Map</a>
                        
                        					<a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="east campus virtual tour" height="245" src="/locations/east/youvisit-east-245px.jpg" width="245"></a>
                        				
                     </aside>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/computerlabs.pcf">©</a>
      </div>
   </body>
</html>