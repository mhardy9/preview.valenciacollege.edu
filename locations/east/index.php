<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li>East</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">
                        
                        					
                        <p>Valencia   College’s &nbsp;East Campus is located at 701 North Econlockhatchee Trail in
                           East Orange County.&nbsp; Just outside the city limits, the campus proudly  serves approximately
                           15,000 students annually.&nbsp; East Campus, known for  its friendly and caring faculty
                           and staff, has received recognition in  several academic areas.
                        </p>
                        					
                        <h2>East Campus Links</h2>
                        					
                        <h3>Academic Divisions</h3>
                        					
                        <ul>
                           						
                           <li><a href="/locations/arts-and-entertainment/">Arts &amp; Entertainment </a></li>
                           						
                           <li><a href="/locations/departments/east/business/">Business, IT &amp; Public Services</a></li>
                           						
                           <li><a href="/academics/departments/communications-east/">Communications</a></li>
                           						
                           <li><a href="/academics/departments/humanities/">Humanities and Foreign Language</a></li>
                           						
                           <li><a href="/students/library/">Library</a></li>
                           						
                           <li><a href="/academics/departments/math-east/">Mathematics</a></li>
                           						
                           <li><a href="/academics/departments/science/">Science</a></li>
                           						
                           <li>
                              							<a href="/locations/east/social-sciences/">Social Sciences &amp; Physical Education</a>
                              							
                              <ul>
                                 								
                                 <li><a href="http://www.youtube.com/embed/XZkuZ91Lu3I?autoplay=1" target="_blank">East Campus Fitness Center Video</a></li>
                                 							
                              </ul>
                              						
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>Campus Administration</h3>
                        					
                        <ul>
                           						
                           <li><a href="/locations/east/locations-president/">East Campus President </a></li>
                           						
                           <li><a href="/locations/east/locations-president/leadership-team-meeting-minutes.php">East/Winter Park Campus Leadership Team Meeting Minutes</a></li>
                           						
                           <li><a href="/locations/east/locations-president/EastDeans.php">East Campus Leadership Team </a></li>
                           						
                           <li>
                              							<a href="eastwpcampusplan.php">East-Winter Park Campus Plan</a>
                              							
                              <ul>
                                 								
                                 <li><a href="/locations/east/StrategicDivisionalPlanningBriefs.php">Strategic Divisional Planning Briefs</a></li>
                                 								
                                 <li><a href="campus-achievements.php">Campus Achievements</a></li>
                                 								
                                 <li><a href="/locations/east/documents/TheDawnofSystemLeadership.pdf" target="_blank">The Dawn of System Leadership</a></li>
                                 							
                              </ul>
                              						
                           </li>
                           						
                           <li><a href="http://site.valenciacollege.edu/faculty-association/east/">East Campus Faculty Senate</a></li>
                           						
                           <li><a href="https://mailvalenciacc-my.sharepoint.com/personal/bhopkins_mail_valenciacollege_edu/Documents/East-WP%20Campus%20President-Leadership%20Team%20Minutes">East-Winter Park Campus President-Leadership Team Minutes</a></li>
                           						
                           <li><a href="/locations/east/documents/CampusMapShowingVisitorParking.pdf" target="_blank">East Campus Map noting Visitor Parking </a></li>
                           					
                        </ul>
                        					
                        <p><br>
                           					
                        </p>
                        					
                        <h3>Faculty &amp; Staff Services</h3>
                        					
                        <ul>
                           						
                           <li><a href="/faculty/resources/east/">East Faculty Resources</a></li>
                           						
                           <li><a href="/employees/admin-assistants/">Administrative Assistants' Toolbox </a></li>
                           						
                           <li><a href="/employees/office-of-information-technology/locations-technology-services/">Campus Technology Services</a></li>
                           						
                           <li><a href="/locations/east/resources.php">Faculty/Staff Resources </a></li>
                           						
                           <li><a href="http://frontdoor.valenciacollege.edu/websites.cfm">Faculty Web Sites</a></li>
                           
                           <!-- 						<li><a href="http://valenciacollege.edu/facultystaff/room-request.cfm">Room Request Form </a></li> -->
                           						
                           <li><a href="/locations/east/documents/jan5thrujan11.pdf" target="_blank">Weekly Facilities Utilization Report </a></li>
                           						
                           <li><a href="/employees/office-of-information-technology/">OIT Helpdesk</a></li>
                           						
                           <li><a href="/locations/east/documents/UCF-Quick-Reference-Guide-for-VISITING-FACULTY-to-Valencia-College-East-Campus-as-of-2-21-2017.pdf" target="_blank">UCF Faculty Quick Reference Guide</a></li>
                           						
                           <li><a href="/employees/development/edge/">Valencia EDGE</a></li>
                           						
                           <li><a href="/employees/word-processing/">Word Processing</a></li>
                           					
                        </ul>
                        				
                     </div>
                     
                     				
                     <aside class="col-md-3">
                        
                        
                        
                        					<a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        					<a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        					
                        <div>
                           						
                           <div>CONTACT</div>
                           						<br>
                           
                           						
                           <div>
                              
                              							
                              <div>
                                 								
                                 <div>
                                    									
                                    <div>407-299-5000</div>
                                    								
                                 </div>
                                 
                                 								
                                 <div>
                                    									
                                    <div>701 N Econlockhatchee Trail<br>Orlando, FL 32825
                                    </div>
                                    								
                                 </div>
                                 
                                 							
                              </div>
                              						
                           </div>
                           
                           					
                        </div>
                        					<a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac7710c6c2e1edb7e&amp;msa=0&amp;ll=28.554002,-81.251235&amp;spn=0.009198,0.017027" target="_blank">
                           						Directions</a>
                        					<a href="/locations/map/east-campus.php" target="_blank">
                           						Campus Map</a>
                        
                        
                        
                        					<a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="east campus virtual tour" height="245" src="/locations/east/youvisit-east-245px.jpg" width="245"></a>
                        
                        
                        
                        
                        				
                     </aside>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/index.pcf">©</a>
      </div>
   </body>
</html>