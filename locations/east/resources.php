<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li>East Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <h2>Resources Available to Faculty and Staff</h2>
                        
                        <h3>Calendars</h3>
                        
                        <ul>
                           
                           <li><a href="/academics/calendar/">College Calendars, Withdrawal, and No Show Deadlines</a></li>
                           
                           <li><a href="/academics/calendar/">Final Exam Schedule</a> (Flex &amp; Summer finals exams given at the last class.)
                           </li>
                           
                           <li><a href="http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121">Arts Calendar </a></li>
                           
                        </ul>
                        
                        <h3>Collegewide Information</h3>
                        
                        <ul>
                           
                           <li><a href="/locations/catalog/">College Catalog</a></li>
                           
                           <li><a href="/about/budget/">College Budget</a></li>
                           
                           <li><a href="/about/support/directory.php">Phone Directory</a></li>
                           
                           <li><a href="/about/general-counsel/">Policy Manual </a></li>
                           
                           <li>
                              <a href="/admissions/refund/">Refund Policy</a> and <a href="/students/business-office/">Financial Services Appeal Form </a>
                              
                           </li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Planning</a></li>
                           
                           <li>
                              <a href="http://www.valencia.org/">Valencia Foundation</a></li>
                           
                        </ul>
                        
                        <h3>Faculty Information</h3>
                        
                        <ul>
                           
                           <li><a href="/faculty/forms/credentials">Faculty Credentials Manual</a></li>
                           
                           <li><a href="/employees/human-resources/compensation.php">Faculty (Full-Time) Compensation Plan</a></li>
                           
                           <li>
                              <a href="http://site.valenciacollege.edu/faculty-association/east/SitePages/Home.aspx">Faculty Senate - East</a> and <a href="/faculty/association/">Faculty Association - Collegewide</a>
                              
                           </li>
                           
                           <li><a href="/students/office-for-students-with-disabilities/">Office for Students with Disabilities</a></li>
                           
                        </ul>
                        
                        <h3>Faculty (Full-Time and Part-Time) Professional Development Information </h3>
                        
                        <ul>
                           
                           <li>
                              <a href="/faculty/resources/east/">Adjunct Faculty Resources</a> and <a href="/employees/human-resources/Full-TimeEmployees.php">Adjunct Faculty Pay Days</a>
                              
                           </li>
                           
                           <li><a href="/faculty/development/programs/adjunct/certificateProgram.php">Associate Faculty Program</a></li>
                           
                           <li>
                              <a href="/faculty/development/">Faculty Development</a> with <a href="/faculty/development/programs/adjunct/ScenariosCourses.php">Scenarios Online</a> 
                           </li>
                           
                           <!--               <li><a href="/locations/careerstaff/Leadership.php">Leadership Valencia</a></li> -->
                           
                           <li>
                              <a href="http://site.valenciacollege.edu/faculty-association/Archives/Prof%20Dev%20Compensation%20Plan%20Information/Documents/Old%20PD%20Website%20Snapshot.pdf">Professional Development Implementation and Oversight Team</a></li>
                           
                           <li>
                              <a href="/faculty/development/tla/Seminar/">Teaching and Learning Academy Seminar</a>: 5 Year
                           </li>
                           
                        </ul>
                        
                        <h3>Human Resources</h3>
                        
                        <ul>
                           
                           <li><a href="/employees/human-resources/benefits.php">Benefits</a></li>
                           
                           <li><a href="/employees/human-resources/compensation.php">Compensation</a></li>
                           
                           <li><a href="/employees/human-resources/jobs.php">Employment Opportunities</a></li>
                           
                           <li><a href="/employees/human-resources/">Human Resources Homepage</a></li>
                           
                        </ul>
                        
                        <h3> Forms</h3>
                        
                        <ul>
                           
                           <li>Change of Address and Emergency Contact Information (Now Found in Atlas - Employee
                              Tab - Personal Info)
                           </li>
                           
                           <li><a href="/locations/east/travel-funds.php">East Campus Academic Division Travel Funds</a></li>
                           
                           <li><a href="http://valenciacollege.edu/facultystaff/room-request.cfm">Room Request Form</a></li>
                           
                           <li><a href="http://goo.gl/forms/wmPZ7l2ax1">Student Data Request</a></li>                    
                           
                           <li><a href="/employees/word-processing/">Word Processing Forms</a></li>
                           
                        </ul>
                        
                        <h3>Handbooks</h3>
                        
                        <ul>
                           
                           <li><a href="/faculty/resources/east/">East Adjunct Faculty Handbook</a></li>
                           
                           <li><a href="/students/student-development/">Student Handbook</a></li>
                           
                        </ul>
                        
                        <h3>Library Resources </h3>
                        
                        <ul>
                           
                           <li> <a href="http://www.linccweb.org/linccsearchdirect?setting_key=29x02">East Campus Library Catalog</a> 
                           </li>
                           
                           <li> <a href="/students/library/mla-apa-chicago-guides/mla-documentation-electronic.php">Library Databases</a>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Plant Operations and Security </h3>
                        
                        <ul>
                           
                           <li>
                              <a href="/employees/plant-operations/">Plant Operations</a> (Location to pick up keys or request maintenance)
                           </li>
                           
                           <li>
                              <a href="/students/security/">Security</a> (Location to pick up parking decal and keycards)
                           </li>
                           
                        </ul>
                        
                        <h3>Miscellaneous Resources</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="/admissions/dual-enrollment/">Dual Enrollment Program</a> (College Classes for High School Students)
                           </li>
                           
                           <li><a href="/academics/programs/honors/">Honors Program</a></li>
                           
                           <li>
                              <a href="/employees/office-of-information-technology/tss/cts/tierclass/">Smart &amp; Bright Classroom</a> Information and Training
                           </li>
                           
                           <li>
                              <a href="/students/student-development/">Student Development</a> 
                           </li>
                           
                        </ul>
                        
                        <h3>Distance Education </h3>
                        
                        <ul>
                           
                           <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology and Alternative Delivery</a></li>
                           
                           <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-299-5000</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>701 N Econlockhatchee Trail<br>Orlando, FL 32825
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac7710c6c2e1edb7e&amp;msa=0&amp;ll=28.554002,-81.251235&amp;spn=0.009198,0.017027" target="_blank">
                           Directions</a>
                        <a href="/locations/map/east.php" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        <a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="east campus virtual tour" height="245" src="/locations/east/youvisit-east-245px.jpg" width="245"></a>
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/resources.pcf">©</a>
      </div>
   </body>
</html>