<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/east/student-services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/east/">East</a></li>
               <li>East Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">
                        
                        					
                        <h2>Student Services</h2>
                        					
                        <p>The Student Services office provides developmental advising which includes life, career,
                           and educational planning, interpretation of assessments, strategies to address academic
                           and personal challenges, programs to encourage student success skills, assistance
                           with understanding academic policies, preparation for university transfer, and financial
                           aid assistance.
                        </p>
                        
                        					
                        <ul class="list_style_1">
                           						
                           <li><a href="/locations/east/academic-success-east/">Academic Success Center </a></li>
                           						
                           <li><a href="/students/advising-counseling/">Advising Center</a></li>
                           						
                           <li><a href="https://learn.valenciacollege.edu//">Blackboard Learn</a></li>
                           
                           
                           						
                           <li><a href="/students/locations-store/">Bookstore</a></li>
                           						
                           <li><a href="/students/academic-success-east/">Communications Support</a></li>
                           						
                           <li><a href="computerlabs.php">Computer Labs</a></li>
                           						
                           <li><a href="/students/catalog/">College Catalog</a></li>
                           						
                           <li><a href="http://faculty.valenciacollege.edu/">Faculty Websites</a></li>
                           						
                           <li><a href="/admissions/finaid/">Financial Aid</a></li>
                           						
                           <li><a href="/students/library/">Library</a></li>
                           						
                           <li>
                              							<a href="/students/disputes/" target="_blank">Student Conflict Resolution</a>
                              						
                           </li>
                           						
                           <li><a href="students/student-development/">Student Development Center</a></li>
                           						
                           <li><a href="/students/testing/">Testing Center</a></li>
                           						
                           <li><a href="/students/academic-success-east/tutoring/">Tutoring</a></li>
                           					
                        </ul>
                        					<br>
                        				
                     </div>
                     				
                     <aside class="col-md-3">
                        					<a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        					<a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        					
                        <div>
                           						
                           <div>CONTACT</div>
                           						<br>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <div>
                                    									
                                    <div>407-299-5000</div>
                                    								
                                 </div>
                                 
                                 								
                                 <div>
                                    									
                                    <div>701 N Econlockhatchee Trail<br>Orlando, FL 32825
                                    </div>
                                    								
                                 </div>
                                 
                                 							
                              </div>
                              						
                           </div>
                           
                           					
                        </div>
                        					<a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac7710c6c2e1edb7e&amp;msa=0&amp;ll=28.554002,-81.251235&amp;spn=0.009198,0.017027" target="_blank">
                           						Directions</a>
                        					<a href="/locations/map/east-campus.php" target="_blank">
                           						Campus Map</a>
                        
                        
                        
                        					<a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="east campus virtual tour" height="245" src="../../locations/east/youvisit-east-245px.jpg" width="245"></a>
                        
                        				
                     </aside>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/east/student-services.pcf">©</a>
      </div>
   </body>
</html>