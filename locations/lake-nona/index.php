<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Campus  | Valencia College</title>
      <meta name="Description" content="Welcome to the Lake Nona campus! Our faculty and staff are excited to help our students achieve their goals. The Lake Nona Campus has been designed with a tremendous attention to detail, with unique features that we believe support a collaborative learning environment, including a beautiful outdoor space and an open design library and learning support area.">
      <meta name="Keywords" content="lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/lake-nona/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li>Lake Nona</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30" role="main">
                  			
                  <div class="row">
                     					
                     <div class="box_style_1">
                        						
                        <div>
                           							
                           <h3>Welcome to the Lake Nona Campus</h3>
                           						
                        </div>
                        						
                        <div>
                           							
                           <p>Welcome to the Lake Nona campus! Our faculty and staff are excited to help our students
                              achieve their
                              								goals. The Lake Nona Campus has been designed with a tremendous attention
                              to detail, with unique features
                              								that we believe support a collaborative learning environment, including a
                              beautiful outdoor space and an
                              								open design library and learning support area. 
                           </p>
                           							
                           <p>Valencia College is excited to be a partner in Lake Nona Medical City. The Lake Nona
                              campus contains 18
                              								classrooms, 2 computer labs, 6 science labs, Learning Support Services (including
                              tutoring, assessment
                              								services, and computer access), a campus store, a café, a library and an amazing
                              outdoor courtyard for
                              								student events.
                           </p>
                           							
                           <p>We look forward to seeing you in our facility and hope that you will reach out to
                              us if you need any
                              								assistance while you are on campus or while you are preparing to enroll.
                           </p>
                           						
                        </div>
                        						
                        <hr class="styled_2">
                        						
                        <div class="row">
                           						
                           <div class="col-md-3">
                              							
                              <h3 class="add_bottom_30">Information</h3>
                              							<a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button btn-block text-center">Apply Now</a> <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.109424450.67023854.1466708865" class="button btn-block text-center">Schedule a Tour</a><a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" class="button btn-block text-center">Directions</a> <a href="http://valenciacollege.edu/map/lake-nona.cfm" class="button btn-block text-center">Campus Map</a>
                              						
                           </div>
                           
                           						
                           <div class="col-md-3">
                              							
                              <h3 class="add_bottom_30">Contact</h3>
                              							
                              <p>Contact Info Coming Soon</p>
                              							
                              <p>campusID = 2<br>campus_fields = "phone, phone_2, address, email"
                              </p>
                              							
                              						
                           </div>
                           						
                           <div class="col-md-6">
                              							
                              <h3 class="add_bottom_30">Social Media</h3>
                              							<a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/"><i class="social_facebook"></i>&nbsp;Lake Nona
                                 								on Facebook</a>
                              							<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FValenciaCollegeLakeNonaCampus%2F&amp;tabs=timeline&amp;width=500&amp;height=500&amp;small_header=false&amp;adapt_container_width=true&amp;hide_cover=true&amp;show_facepile=false&amp;appId=203940866824764" width="500" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                              						
                           </div>
                           							
                        </div>
                        					
                     </div>
                     				
                  </div>
                  			
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/lake-nona/index.pcf">©</a>
      </div>
   </body>
</html>