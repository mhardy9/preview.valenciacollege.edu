<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Collegiate Academy  | Valencia College</title>
      <meta name="Description" content="The Collegiate Academy at Lake Nona High School, developed in partnership with Valencia College, is an accelerated academic program that provides Lake Nona High School students with new educational opportunities. At present, students can experience selected college-level courses. Future planning will include the curricular options and scheduling necessary to complete the Associate in Arts (A.A.) degree.">
      <meta name="Keywords" content="high, school, partnership, collegiate, academy, lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/lake-nona/collegiate-academy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/lake-nona/">Lake Nona</a></li>
               <li>Collegiate Academy </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Valencia's Collegiate Academy at Lake Nona High School</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The Collegiate Academy at Lake Nona High School, developed in partnership with Valencia
                              College, is an accelerated academic program that provides Lake Nona High School students
                              with new educational opportunities. At present, students can experience selected college-level
                              courses. Future planning will include the curricular options and scheduling necessary
                              to complete the Associate in Arts (A.A.) degree.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Our Program</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The Collegiate Academy concept is adaptable to multiple areas of academic and professional
                              interest. Through a combination of course work, Advanced Placement and Dual Enrollment
                              credit, students can build a highly competitive academic profile based on challenges
                              and opportunities that exceed the scope of traditional high school programs. In addition,
                              there are the financial benefits of no tuition, book costs or fees for participating
                              students.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Our Students</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Lake Nona High School students who complete the Collegiate Academy Application, the
                              Valencia College Dual Enrollment Application, and meet the following criteria are
                              eligible for enrollment:
                           </p>
                           
                           <ul>
                              
                              <li>Unweighted GPA of 3.0 in high school coursework</li>
                              
                              <li>A minimum PERT/CPT/SAT/ACT score determined by the program of study</li>
                              
                              <li>Written goal statement on file in the guidance office</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Our Commitment</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Collegiate Academy students are dually enrolled in Lake Nona High School and Valencia
                              College; our students will receive a high level of academic support and guidance from
                              both institutions. Valencia will also provide Collegiate Academy students with access
                              to the following college resources:
                           </p>
                           
                           <ul>
                              
                              <li>Dedicated faculty who are experts in their field of study</li>
                              
                              <li>Library resources including books, journals and online research tools</li>
                              
                              <li>Advising and tutoring services</li>
                              
                              <li>Opportunities to attend college-sponsored cultural and leadership events as well as
                                 access to campus clubs and organizations
                              </li>
                              
                              <li>Access to online resources through Valencia's interactive portal</li>
                              
                           </ul>
                           
                           <p>For more information, please <a href="mailto:mbosley@valenciacollege.edu">email Dr. Bosley</a>.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side"><a class="button btn-block text-center" href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Apply Now</a> <a class="button btn-block text-center" href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.109424450.67023854.1466708865">Schedule a Tour</a></div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Contact</h3>
                        
                        <p>campusID = 2 campus_fields = "phone, phone_2, address, email"</p>
                        <br> <a class="button btn-block text-center" href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17">Directions</a> <a class="button btn-block text-center" href="http://valenciacollege.edu/map/lake-nona.cfm">Campus Map</a></div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Social Media</h3>
                        
                     </div>
                     <a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/">&nbsp;Lake Nona on Facebook</a></aside>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/lake-nona/collegiate-academy.pcf">©</a>
      </div>
   </body>
</html>