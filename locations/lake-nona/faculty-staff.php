<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/lake-nona/faculty-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lake Nona Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/lake-nona/">Lake Nona</a></li>
               <li>Lake Nona Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        <h2><strong>Faculty &amp; Staff</strong></h2>
                        
                        <h3>Administration</h3>
                        
                        <ul class="list_staff">
                           
                           <li>
                              
                              <figure><img alt="Dr. Mike Bosley" height="105" src="/locations/lake-nona/images/MikeBosleyThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:mbosley@valenciacollege.edu">Dr. Mike Bosley</a><br>
                                 Executive Dean
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Jonathan Hernandez" height="109" src="/locations/lake-nona/images/JonThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:jhernandez71@valenciacollege.edu">Jonathan Hernandez</a><br>
                                 Campus Manager
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Jeanette Dos Santos" height="93" src="/locations/lake-nona/images/JeanetteThumbnail_001.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:jdossantos8@valenciacollege.edu">Jeanette Dos Santos</a><br>
                                 Executive  Assistant
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Helean Brown" height="96" src="/locations/lake-nona/images/HelenaBrown_Thumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:hbrown31@valenciacollege.edu">Helena Brown</a><br>
                                 Staff Assistant II
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="" height="95" src="/locations/lake-nona/images/DeAndraDixon_Thumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:ddixon25@valenciacollege.edu">DeAndra Dixon</a><br>
                                 Staff Assistant II
                              </h4>
                              						
                           </li>
                           
                        </ul>
                        
                        <h3>Faculty</h3>
                        
                        <ul class="list_staff">
                           
                           <li>
                              
                              <figure><img alt="Kenny Bingle Picture" height="95" src="/locations/lake-nona/images/KennyBingleThumbnail.jpg" width="73"></figure>
                              
                              <h4>
                                 <a href="mailto:kbingle@valenciacollege.edu">Kenny Bingle</a><br>
                                 Professor <br>
                                 Mathematics
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Misty Bozzacco" height="97" src="/locations/lake-nona/images/MistyThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:mbozzacco@valenciacollege.edu">Misty Bozzacco</a><br>
                                 Professor <br>
                                 Mathematics
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Maria Delis" src="/locations/lake-nona/images/MariaDelisThumbnail.jpg"></figure>
                              
                              <h4>
                                 <a href="mailto:mdelis@valenciacollege.edu">Maria Delis</a><br>
                                 Professor <br>
                                 English
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Dr. Flora Chisholm" height="97" src="/locations/lake-nona/images/FloraChisholmThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:fchisholm@valenciacollege.edu">Dr. Flora Chisholm</a><br>
                                 Professor <br>
                                 Biology
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Dr. Debra Hollister" height="101" src="/locations/lake-nona/images/HollisterThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:dhollister@valenciacollege.edu">Dr. Debra Hollister</a><br>
                                 Professor <br>
                                 Psychology
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Susan Ingersoll" height="84" src="/locations/lake-nona/images/SusanThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:singersoll@valenciacollege.edu">Dr. Susan Ingersoll</a><br>
                                 Professor <br>
                                 Biotechnology
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Jonathan Lair Picture" height="91" src="/locations/lake-nona/images/Lairthumbnail.jpg" width="74"></figure>
                              
                              <h4>
                                 <a href="mailto:jlair2@valenciacollege.edu">Jonathan Lair</a><br>
                                 Professor <br>
                                 Political Science
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Brian Macon" height="106" src="/locations/lake-nona/images/BrianMaconThumbnail_000.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:bmacon@valenciacollege.edu">Brian Macon</a><br>
                                 Professor <br>
                                 Mathematics 
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Dr. Nicole Marsh" height="106" src="/locations/lake-nona/images/NicoleMarshThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:nmarsh@valenciacollege.edu">Dr. Nicole Marsh</a><br>
                                 Professor <br>
                                 Speech 
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Dr. Andrew Piercy" height="101" src="/locations/lake-nona/images/AndrewThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:apiercy2@valenciacollege.edu">Dr. Andrew Piercy</a><br>
                                 Professor <br>
                                 Biology
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="April Raneri" height="106" src="/locations/lake-nona/images/AprilRaneriThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:araneri@valenciacollege.edu">April Raneri</a><br>
                                 
                              </h4>
                              
                              <p>Professor <br>
                                 Speech
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Dr. Vivian Ruiz" height="109" src="/locations/lake-nona/images/VivianRuiz.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:vruiz9@valenciacollege.edu">Dr. Vivian Ruiz</a><br>
                                 Professor <br>
                                 English &amp; Humanities
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Nichole Shorter" height="112" src="/locations/lake-nona/images/nicholeshorterthumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:nshorter1@valenciacollege.edu">Dr. Nichole Segarra</a><br>
                                 Professor <br>
                                 Mathematics
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Dr. Laura Sessions" height="105" src="/locations/lake-nona/images/LauraSessionsThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:lsessions@valenciacollege.edu">Dr. Laura Sessions</a><br>
                                 Professor <br>
                                 Chemistry
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Jackie Starren" height="105" src="/locations/lake-nona/images/JackieStarrenThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:jstarren@valenciacollege.edu">Jackie Starren</a><br>
                                 Professor <br>
                                 Humanities
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Dr. Lynta Thomas" height="90" src="/locations/lake-nona/images/LyntaThomasThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:lthomas87@valenciacollege.edu">Dr. Lynta Thomas</a><br>
                                 Professor <br>
                                 Chemistry
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="LaVonda Walker" height="100" src="/locations/lake-nona/images/LaVondaWalker.jpg" width="70"></figure>
                              
                              <h4>
                                 <a href="mailto:lwalker24@valenciacollege.edu">LaVonda Walker</a><br>
                                 Professor <br>
                                 New Student Experience
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Professor Michael Wheaton" height="100" src="/locations/lake-nona/images/MichaelWheatonThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:mwheaton1@valenciacollege.edu">Michael Wheaton</a><br>
                                 Professor <br>
                                 English
                              </h4>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Learning Support</h3>
                        
                        <ul class="list_staff">
                           
                           <li>
                              
                              <figure><img alt="Ada Amador" height="96" src="/locations/lake-nona/images/AdaThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:aamador1@valenciacollege.edu">Ada Amador</a><br>
                                 Assessment &amp; Testing Center Coordinator
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Kevin Beers" height="64" src="/locations/lake-nona/images/Kevin_Beers_Thumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:kbeers1@valenciacollege.edu">Kevin Beers</a><br>
                                 Instructional Lab <br>
                                 Supervisor - English
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Michael Blackburn" height="95" src="/locations/lake-nona/images/MichaelBlackburnThumbnail2.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:mblackburn4@valenciacollege.edu">Michael Blackburn</a><br>
                                 Manager, Learning Support Services
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Laura Browning" height="104" src="/locations/lake-nona/images/LauraBrowningThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:lbrowning2@valenciacollege.edu">Laura Browning</a><br>
                                 Library Services Supervisor
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Emilie Buckley " height="104" src="/locations/lake-nona/images/EmilieBuckleyThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:ebuckley3@valenciacollege.edu">Emilie Buckley </a><br>
                                 Librarian
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Rachael Dailey" height="74" src="/locations/lake-nona/images/RachaelThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:rdailey3@valenciacollege.edu">Rachael Dailey</a><br>
                                 Lab Supervisor - Biology
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Sothy Kien" height="94" src="/locations/lake-nona/images/SothyThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:skien@valenciacollege.edu">Sothy Kien</a><br>
                                 Instructional Lab <br>
                                 Supervisor - Biotechnology
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Theresa Koehler" height="72" src="/locations/lake-nona/images/Theresa.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:tkoehler@valenciacollege.edu">Theresa Koehler</a><br>
                                 Instructional Lab <br>
                                 Supervisor - Math
                              </h4>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Security</h3>
                        
                        <ul class="list_staff">
                           
                           
                           <li>
                              
                              <figure><img alt="Gil Santiago" height="88" src="/locations/lake-nona/images/GilSantiagoThumbnail.jpg" width="74"></figure>
                              
                              <h4> <a href="mailto:gsantiago1@valenciacollege.edu">Gil Santiago</a><br>
                                 Security Field Supervisor
                              </h4>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Student Services</h3>
                        
                        <ul class="list_staff">
                           
                           
                           <li>
                              
                              <figure><img alt="Christopher Alvarez" height="88" src="/locations/lake-nona/images/ChrisAlvarezthumbnail.jpg" width="74"></figure>
                              
                              <h4> <a href="mailto:calvarez37@valenciacollege.edu">Christopher Alvarez</a><br>
                                 Student Services Advisor
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Teresa Aplin" height="100" src="/locations/lake-nona/images/TeresaAplinThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:taplin1@valenciacollege.edu">Teresa Aplin</a><br>
                                 Staff Assistant,<br>
                                 Student Development
                              </h4>
                              
                           </li> 
                           
                           <li>
                              
                              <figure><img alt="Sarah Ashby" height="87" src="/locations/lake-nona/images/SarahAshbyThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:sashby4@valenciacollege.edu">Sarah Ashby</a><br>
                                 Program Advisor
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Brenda Castillo" height="91" src="/locations/lake-nona/images/BrendaCastilloThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:bcastillo8@valenciacollege.edu">Brenda Castillo</a><br>
                                 Staff Assistant II
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Gabriela Cerda" height="91" src="/locations/lake-nona/images/GabrielaCerdaThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:gcerda@valenciacollege.edu">Gabriela Cerda</a><br>
                                 Support Specialist II
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Allanah Freeman" height="84" src="/locations/lake-nona/images/allanahfreemanthumbnail2.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:afreeman14@valenciacollege.edu">Allanah Freeman</a><br>
                                 Support Specialist II
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Tiffany Kilpatrick" height="72" src="/locations/lake-nona/images/TiffanyKilpatrickThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:tkilpatrick1@valenciacollege.edu">Tiffany Kilpatrick</a><br>
                                 Coordinator, Student Services
                              </h4>
                              
                           </li>                    
                           
                           <li>
                              
                              <figure><img alt="Justine Monsalve " height="56" hspace="0" src="/locations/lake-nona/images/JustineMonsalveThumbnail.JPG" vspace="0" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:jmonsalve2@valenciacollege.edu">Justine Monsalve</a><br>
                                 Lab Manager
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Gwendolyn Noel" height="56" hspace="0" src="/locations/lake-nona/images/GwendolynThumbnail.JPG" vspace="0" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:gnoel1@valenciacollege.edu">Gwendolyn Noel</a><br>
                                 Academic Advisor
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="" height="82" src="/locations/lake-nona/images/HeidiOvallesThumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:hovalles@valenciacollege.edu">Heidi Ovalles</a><br>
                                 Student Services Advisor
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Rosa Silva" height="56" src="/locations/lake-nona/images/RosaThumbnail.JPG" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:rsilva13@valenciacollege.edu">Rosa Silva</a><br>
                                 Student Services Advisor
                              </h4>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img alt="Melinda Smith" height="82" src="/locations/lake-nona/images/mindyheadshot2thumbnail.jpg" width="75"></figure>
                              
                              <h4>
                                 <a href="mailto:msmith225@valenciacollege.edu">Melinda Smith</a><br>
                                 Director, Student Services
                              </h4>
                              
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/lake-nona/faculty-staff.pcf">©</a>
      </div>
   </body>
</html>