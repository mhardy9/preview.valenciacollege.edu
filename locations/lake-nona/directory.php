<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Directory  | Valencia College</title>
      <meta name="Description" content="Locations of services and offices within the Lake Nona campus.">
      <meta name="Keywords" content="directory, lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/lake-nona/directory.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/lake-nona/">Lake Nona</a></li>
               <li>Campus Directory </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Campus Directory</h2>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>First Floor</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li><strong>Adjunct Office: </strong> 120
                              </li>
                              
                              <li><strong>Answer Center: </strong> 149
                              </li>
                              
                              <li><strong>Atlas Access Lab: </strong> 147
                              </li>
                              
                              <li><strong>Cafe: </strong> 131
                              </li>
                              
                              <li><strong>Campus Store: </strong> 135
                              </li>
                              
                              <li><strong>Security: </strong> 150
                              </li>
                              
                              <li><strong>Student Development: </strong> 149A
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Second Floor</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li><strong>Assessment &amp; Testing:</strong> 206
                              </li>
                              
                              <li><strong>Computer Access Lab:</strong> 236
                              </li>
                              
                              <li><strong>Math Lab:</strong> 230
                              </li>
                              
                              <li><strong>Tutoring:</strong> 230
                              </li>
                              
                              <li><strong>Writing Center:</strong> 230
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Third Floor</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li><strong>Campus Administration:</strong> 302
                              </li>
                              
                              <li><strong>Conference Room:</strong> 301
                              </li>
                              
                              <li><strong>Library:</strong> 330
                              </li>
                              
                              <li><strong>OIT:</strong>&nbsp;345
                              </li>
                              
                              <li><strong>Teaching/Learning Innovation:</strong> 344
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side"><a class="button btn-block text-center" href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Apply Now</a> <a class="button btn-block text-center" href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.109424450.67023854.1466708865">Schedule a Tour</a></div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Contact</h3>
                        
                        <p>campusID = 2 campus_fields = "phone, phone_2, address, email"</p>
                        <br> <a class="button btn-block text-center" href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17">Directions</a> <a class="button btn-block text-center" href="http://valenciacollege.edu/map/lake-nona.cfm">Campus Map</a></div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Social Media</h3>
                        
                     </div>
                     <a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/">&nbsp;Lake Nona on Facebook</a></aside>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/lake-nona/directory.pcf">©</a>
      </div>
   </body>
</html>