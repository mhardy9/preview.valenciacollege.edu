<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/lake-nona/math-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lake Nona Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/lake-nona/">Lake Nona</a></li>
               <li>Lake Nona Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Math Resources </h2>
                        
                        <p><strong>Here are some resources we have found to be helpful:</strong></p>
                        
                        <h3>Graphing Devices</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="../../locations/east/academic-success/math/calculators.html" target="_blank">TI-83/TI-84 Hints</a>                
                           </li>
                           
                           <li><a href="https://www.desmos.com/calculator" target="_blank">Desmos</a></li>
                           
                           <li><a href="https://www.geogebra.org/" target="_blank">GeoGebra</a></li>
                           
                           <li><a href="http://web.monroecc.edu/manila/webfiles/pseeburger/CalcPlot3D/" target="_blank">CalcPlot3D</a></li>
                           
                           <li><a href="https://technology.cpm.org/general/3dgraph/" target="_blank">CPM 3D Plotter</a></li>
                           
                        </ul>
                        
                        <h3>Developmental Math</h3>
                        
                        <ul>
                           
                           <li><a href="../../locations/math/liveScribe.html">Valencia College Tutorials</a></li>
                           
                           <li>
                              <a href="http://patrickjmt.com/" target="_blank">Patrick JMT</a>                
                           </li>
                           
                           <li><a href="https://www.mathsisfun.com/numbers/index.html" target="_blank">Math is Fun</a></li>
                           
                           <li><a href="https://www.khanacademy.org/math/pre-algebra" target="_blank">Khan Academy (Pre-Algebra)</a></li>
                           
                           <li><a href="https://openstax.org/details/books/elementary-algebra" target="_blank">OpenStax  (Elementary Algebra)</a></li>
                           
                        </ul>
                        
                        <h3>College Level Math</h3>
                        
                        <ul>
                           
                           <li><a href="../../locations/math/liveScribe.html" target="_blank">Valencia College Tutorials</a></li>
                           
                           <li>
                              <a href="http://patrickjmt.com/" target="_blank">Patrick JMT</a>                
                           </li>
                           
                           <li><a href="http://mathandstatshelp.com/" target="_blank">Math and Stats Help</a></li>
                           
                           <li><a href="http://tutorial.math.lamar.edu/" target="_blank">Paul's Online Math Notes</a></li>
                           
                           <li><a href="https://www.mathsisfun.com/numbers/index.html" target="_blank">Math is Fun</a></li>
                           
                           <li><a href="https://www.khanacademy.org/math" target="_blank">Khan Academy (Algebra)</a></li>
                           
                           <li><a href="https://www.youtube.com/user/professorleonard57/playlists" target="_blank">Professor  Leonard</a></li>
                           
                           <li><a href="https://openstax.org/subjects/math" target="_blank">OpenStax  (Upper Level Math)</a></li>
                           
                        </ul>            
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Lake Nona Campus</strong><br>407-582-7100
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Student Services</strong><br>407-582-1507
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:jhernandez71@valenciacollege.edu">jhernandez71@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>12350 Narcoossee Rd<br>Orlando, FL 32832
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/lake-nona.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/lake-nona/math-resources.pcf">©</a>
      </div>
   </body>
</html>