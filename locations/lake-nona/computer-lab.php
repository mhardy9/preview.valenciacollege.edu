<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Computer Lab  | Valencia College</title>
      <meta name="Description" content="Need a place to do homework, research, or surf the web? We have 37 computers with scanning, printing and copying available. This is a great place for students to work on their Blackboard, Course Compass, or other class assignments.">
      <meta name="Keywords" content="computer, lab, lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/lake-nona/computer-lab.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/lake-nona/">Lake Nona</a></li>
               <li>Computer Lab </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Open Access Computer Lab</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Need a place to do homework, research, or surf the web? We have 37 computers with
                              scanning, printing and copying available. This is a great place for students to work
                              on their Blackboard, Course Compass, or other class assignments.
                           </p>
                           
                           <p>The Lab is located in Room 236.</p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Hours of Operation</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Fall &amp; Spring</h4>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li><strong>Monday - Thursday:</strong> 8:00am - 10:00pm
                                    </li>
                                    
                                    <li><strong>Friday:</strong> 8:00am - 5:00pm
                                    </li>
                                    
                                    <li><strong>Saturday: </strong>8:00am - 3:00pm
                                    </li>
                                    
                                    <li><strong>Sunday: </strong>CLOSED
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Summer</h4>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li><strong>Monday - Thursday:</strong> 8:00am - 10:00pm
                                    </li>
                                    
                                    <li><strong>Friday:</strong> 8:00am - 12:00pm
                                    </li>
                                    
                                    <li><strong>Saturday: </strong>8:00am - 2:00pm
                                    </li>
                                    
                                    <li><strong>Sunday:</strong> CLOSED
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>For Faculty</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The Lake Nona Campus has 4 mobile laptop carts (30 per cart) and 1 computer lab (located
                              in Room 121) available for reservation. Please submit a request using the link below
                              to reserve a cart or lab. Please call Campus Technology Services at ext. 7555 or <a href="mailto:lnccts@valenciacollege.edu">e-mail</a> for additional questions or concerns.
                           </p>
                           
                           <p><a class="button" href="http://net4.valenciacollege.edu/forms/oit/locations-technology-services/equipment-request.cfm">Computer Reservation Request Form</a></p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side"><a class="button btn-block text-center" href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Apply Now</a> <a class="button btn-block text-center" href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.109424450.67023854.1466708865">Schedule a Tour</a></div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Contact</h3>
                        
                        <p>campusID = 2 campus_fields = "phone, phone_2, address, email"</p>
                        <br> <a class="button btn-block text-center" href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17">Directions</a> <a class="button btn-block text-center" href="http://valenciacollege.edu/map/lake-nona.cfm">Campus Map</a></div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Social Media</h3>
                        
                     </div>
                     <a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/">&nbsp;Lake Nona on Facebook</a></aside>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/lake-nona/computer-lab.pcf">©</a>
      </div>
   </body>
</html>