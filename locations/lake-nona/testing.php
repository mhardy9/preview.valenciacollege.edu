<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment &amp; Testing Center  | Valencia College</title>
      <meta name="Description" content="Information on the operating hours of the Assessment &amp;amp; Testing Center, its rules and procedures, and assessments offered.">
      <meta name="Keywords" content="assessment, testing, center, lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/lake-nona/testing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/lake-nona/">Lake Nona</a></li>
               <li>Assessment &amp; Testing Center </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        
                        <h2>Assessment &amp; Testing Center</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Important Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list-unstyled">
                              
                              <li>05/29/17 — <strong>Memorial Day</strong>, College Closed
                              </li>
                              
                              <li>07/04/17 — <strong>Independence Day</strong>, College Closed
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Testing Center Hours &amp; Policies</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The Testing Center proctors course exams on behalf of instructors.</p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>A <strong>current Valencia photo ID</strong> is required to sit for a test at the Testing Center.
                              </li>
                              
                              <li>Please note: Testing Center hours of operation vary between campuses.</li>
                              
                              <li>We <strong>STOP</strong> proctoring tests <strong>one hour prior to closing time</strong>. You must
                                 arrive at least one hour prior to closing to begin an exam.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Summer 2017</h4>
                           
                           <ul class="list-unstyled">
                              
                              <li>
                                 <strong>Monday - Thursday:</strong> 8:00am - 8:00pm — Closing Time: 9:00pm
                                 
                              </li>
                              
                              <li>
                                 <strong>Friday:</strong> 8:00am - 11:00am — Closing Time: 12:00pm
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Assessment Center Hours &amp; Policies</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The Assessment Center provides placement tests for prospective students taking PERT,
                              CPT, COMPASS, CJBAT
                              or TEAS.
                           </p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>You must have applied to Valencia College and have paid the $35 application fee 5
                                 days prior to
                                 testing.
                                 
                              </li>
                              
                              <li>
                                 <strong>A government issued ID or Valencia College ID is required</strong> to take assessments!
                              </li>
                              
                              <li>There is a mandatory one-day waiting period between PERT retakes.</li>
                              
                           </ul>
                           
                           
                           <h4>PERT/CPT/COMPASS Availability</h4>
                           
                           <ul class="list-unstyled">
                              
                              <li>
                                 <strong>Monday - Thursday:</strong> 8:00am - 5:00pm — Closing Time: 9:00pm
                              </li>
                              
                              <li>
                                 <strong>Friday:</strong> 9:00am - 11:00am — Closing Time: 12:00pm
                              </li>
                              
                           </ul>
                           
                           <p><strong>Test Fees:</strong> Valencia Students get the first attempt for free; retakes cost $10 per
                              subtest. Non-Valencia students pay a flat fee of $25 for all attempts. (Exact cash
                              or check payment only.)
                              
                           </p>
                           
                           
                           <h4>CJBAT Availability</h4>
                           
                           <ul class="list-unstyled">
                              
                              <li>
                                 <strong>Monday - Thursday:</strong> By Appointment Only
                              </li>
                              
                           </ul>
                           
                           <p><strong>Test Fees:</strong> $40 for Valencia Students; $50 for Non-Valencia Students. (Exact cash or
                              check payment only.)
                           </p>
                           
                           
                           <h4>TEAS Availability</h4>
                           
                           <ul class="list-unstyled">
                              
                              <li>
                                 <strong>Monday - Thursday:</strong> By Appointment Only
                              </li>
                              
                           </ul>
                           
                           <p><strong>Test Fees:</strong> $65 for Valencia Students; $85 for Non-Valencia Students. (Exact cash or
                              check payment only.)
                           </p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Faculty Exam Referral</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>To submit exams to the Testing Center, log into your ATLAS account. Choose the Faculty
                              Services tab.
                              Select the Testing Center Referral link located in the left column under Faculty Tools.
                              Complete the
                              Referral Form, attach your exam, class roster(s) and submit. <br><strong>PLEASE NOTE:</strong> class
                              rosters must be submitted with referrals.
                           </p>
                           
                           <p>We may be unable to answer the phone during peak testing periods; for a prompt response,
                              please <a href="mailto:lnc_testingcenter@valenciacollege.edu">contact Testing Center staff by e-mail</a>.
                           </p>
                           
                           <p>Referral Forms may be picked up and filled out in the Testing Center and submitted
                              in person if
                              desired.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Assessment Center Staff</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li>
                                       <a href="mailto:aamador1@valenciacollege.edu">
                                          
                                          <figure><img src="/_resources/img/teacher_1_thumb.jpg" alt="Valencia image description" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>Ada Amador</h5>
                                          
                                          <p>Assessment Coordinator</p>
                                          <i class="pe-7s-angle-right-circle"></i> </a>
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="mailto:sbonilla6@valenciacollege.edu">
                                          
                                          <figure><img src="/_resources/img/teacher_1_thumb.jpg" alt="Valencia image description" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>Steven Bonilla</h5>
                                          
                                          <p>Assessment Specialist</p>
                                          <i class="pe-7s-angle-right-circle"></i> </a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li>
                                       <a href="mailto:aramos98@valenciacollege.edu">
                                          
                                          <figure><img src="/_resources/img/teacher_1_thumb.jpg" alt="Valencia image description" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>Alexis Ramos</h5>
                                          
                                          <p>Assessment Specialist</p>
                                          <i class="pe-7s-angle-right-circle"></i> </a>
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="mailto:vsilva3@valenciacollege.edu">
                                          
                                          <figure><img src="/_resources/img/teacher_1_thumb.jpg" alt="Valencia image description" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>Victoria Silva</h5>
                                          
                                          <p>Assessment Specialist</p>
                                          <i class="pe-7s-angle-right-circle"></i> </a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button btn-block text-center">Apply Now</a> <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.109424450.67023854.1466708865" class="button btn-block text-center">Schedule a Tour</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Contact</h3>
                        
                        
                        
                        <p>campusID = 2 campus_fields = "phone, phone_2, address, email"</p>
                        
                        <br>
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" class="button btn-block text-center">Directions</a> <a href="http://valenciacollege.edu/map/lake-nona.cfm" class="button btn-block text-center">Campus Map</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Social Media</h3>
                        
                        
                        
                     </div>
                     <a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/"><i class="social_facebook"></i>&nbsp;Lake Nona
                        on Facebook</a>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/lake-nona/testing.pcf">©</a>
      </div>
   </body>
</html>