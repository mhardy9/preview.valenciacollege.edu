<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/lake-nona/testing-center.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lake Nona Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/lake-nona/">Lake Nona</a></li>
               <li>Lake Nona Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Assessment/Testing Center</h2>
                        
                        <h3>Important Information </h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p>9/4/17</p>
                                 </div>
                                 
                                 <div>
                                    <p>College Closed</p>
                                 </div>
                                 
                                 <div>
                                    <p>Labor Day</p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>10/17/17</p>
                                 </div>
                                 
                                 <div>
                                    <p>College Closed</p>
                                 </div>
                                 
                                 <div>
                                    <p>College Day</p>
                                 </div>
                                 
                              </div> 
                              
                              <div>
                                 
                                 <div>
                                    <p>11/13/17</p>
                                 </div>
                                 
                                 <div>
                                    <p>Limited Hours</p>
                                 </div>
                                 
                                 <div>
                                    <p>10:00am - 4:00pm</p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>11/22/17 - 11/26/17</p>
                                 </div>
                                 
                                 <div>
                                    <p>College Closed</p>
                                 </div>
                                 
                                 <div>
                                    <p>Thanksgiving Break</p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>12/21/17 - 1/1/17</p>
                                 </div>
                                 
                                 <div>
                                    <p>College Closed</p>
                                 </div>
                                 
                                 <div>
                                    <p>Winter Break</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3> Hours for Lake Nona Assessment/Testing Center</h3>
                        
                        <p><strong>Location:</strong> 1-206
                           
                        </p>
                        
                        <h3>Testing Center  Hours</h3> 
                        (Test for college course)
                        
                        <ul>
                           
                           <li>A current <strong>Valencia photo ID</strong> is required to take a test in the Testing Center
                           </li>
                           
                           <li><strong>PLEASE NOTE: Testing Center Hours vary per Campus</strong></li>
                           
                           <li>We <span><strong>STOP</strong></span> proctoring tests <u><strong>one hour prior to closing time</strong></u> 
                           </li>
                           
                        </ul>            
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>***Arrive between the hours below*** </div>
                                 
                                 <div>Closing Time </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Monday - Thursday </div>
                                 
                                 <div>8:00am - 8:00pm </div>
                                 
                                 <div>9:00pm</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Friday</div>
                                 
                                 <div>9:00am - 4:00pm </div>
                                 
                                 <div>5:00pm</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Saturday</div>
                                 
                                 <div>8:00am - 12:00pm </div>
                                 
                                 <div>1:00pm</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        <br>
                        <br>
                        
                        <h3>Assessment Center Hours</h3> 
                        (Placement test for prospective students taking PERT, CPT, LOEP, CJBAT or TEAS) 
                        
                        
                        <ul>
                           
                           <li>Must have applied to Valencia and paid the $35 application fee 5 days prior to testing
                              
                           </li>
                           
                           <li>A<strong> government issued ID or Valencia ID is required</strong> to take the PERT / CPT / LOEP / CJBAT / TEAS
                           </li>
                           
                           <li>There is a 1 day wait period between PERT retakes <br>
                              
                           </li>
                           
                        </ul>            
                        
                        <h4>PERT/CPT/COMPASS</h4>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>***Arrive between the hours below*** </div>
                                 
                                 <div>Closing Time </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Monday - Thursday</div>
                                 
                                 <div>8:00am - 5:00pm </div>
                                 
                                 <div>9:00pm</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Friday</div>
                                 
                                 <div>9:00am - 4:00pm </div>
                                 
                                 <div>5:00pm</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Test Fee: Frist Time - FREE for Valencia Students </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Test Fee: Retakes for Valencia Students - $10 per subtest for (Exact Cash or Check
                                    Only) 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Test Fee: First Time &amp; Retakes for Non-Valencia Students - $25 flat fee (Exact Cash
                                    or Check Only) 
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <h4>CJBAT <span>(By Appointment Only)</span>
                           
                        </h4>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Monday - Thursday </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Test Fee: $40.00 for Valencia Students (Exact Cash or Check Only) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Test Fee: $50.00 for Non-Valencia Students (Exact Cash or Check Only) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h4>TEAS <span>(By Appointment Only)</span>
                           
                        </h4>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Monday - Thursday</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Test Fee: $65.00 for Valencia Students (Exact Cash  or Check Only) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Test Fee: $85.00 for Non-Valencia Students (Exact Cash or Check Only) </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <p>For more information about Assessment Testing, please visit the Assessment Services
                           Website: <a href="../../locations/assessments/index.html">http://valenciacollege.edu/assessments/</a> 
                        </p>
                        
                        <h3>Faculty Information </h3>
                        
                        <ul>
                           
                           <li>
                              <strong>Exam Referral: </strong>To submit exams to the Testing Center, log into your ATLAS account. Choose the Faculty
                              Services tab. Select the Testing Center Referral link located in the left column under
                              Faculty Tools. Complete the Referral Form, attach your exam, class roster(s) and submit.
                              <strong>PLEASE NOTE: class rosters must be submitted with referrals.</strong>
                              
                           </li>
                           
                           <li>We may be unable to answer the phone during peak testing periods, for prompter response
                              please contact Testing Center staff at <a href="mailto:lnc_testingcenter@valenciacollege.edu">lnc_testingcenter@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>Referral Forms may be picked up and filled out in the Testing Center and submitted
                              in person.
                           </li>
                           
                        </ul>            
                        
                        <h3>Contact Information </h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <a href="mailto:aamador1@valenciacollege.edu">Ada Amador </a> - Assessment Coordinator (x7414) 
                                 </div>
                                 
                                 <div>
                                    <a href="mailto:vsilva3@valenciacollege.edu">Victoria Silva</a> - Assessment Specialist (x7104)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <a href="mailto:sbonilla6@valenciacollege.edu">Steven Bonilla</a> - Assessment Specialist (x7104) 
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>  
                        
                        
                        
                        
                        <p><a href="../../locations/lakenona/testingcenter.html#top">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Lake Nona Campus</strong><br>407-582-7100
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Student Services</strong><br>407-582-1507
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:jhernandez71@valenciacollege.edu">jhernandez71@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>12350 Narcoossee Rd<br>Orlando, FL 32832
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/lake-nona.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/lake-nona/testing-center.pcf">©</a>
      </div>
   </body>
</html>