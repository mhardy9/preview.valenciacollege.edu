<ul>
	<li><a href="/locations/lake-nona/">Lake Nona</a></li>
	<li><a href="/locations/lake-nona/collegiate-academy.php">Collegiate Academy</a></li>
	<li class="submenu">
		<a href="/locations/lake-nona/services.php" class="show-submenu">Services<i class="far fa-angle-down" aria-hidden="true"></i></a>
		<ul>
			<li><a href="/locations/lake-nona/testing.php">Assessment &amp; Testing</a></li>
			<li><a href="/locations/lake-nona/computer-lab.php">Computer Lab</a></li>
			<li><a href="/locations/lake-nona/directory.php">Directory</a></li>
			<li><a href="/locations/lake-nona/events.php">Events</a></li>
	<li><a href="/students/learning-support/lake-nona/">Learning Support Services</a></li>
			<li><a href="/locations/lake-nona/student-services.php" class="Group">Student Services</a></li>
		</ul>
	</li>
	<li><a href="/locations/lake-nona/faculty-staff.php" class="Group">Faculty &amp; Staff</a></li>
	<li><a href="http://net4.valenciacollege.edu/forms/lakenona/contact.cfm">Contact Us</a></li>
</ul>