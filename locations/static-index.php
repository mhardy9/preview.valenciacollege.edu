<?pcf-stylesheet path="/_resources/xsl/home.xsl" title="Locations Home Page" extension="php"?>
<!DOCTYPE HTML>
<html lang="en-US">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="keywords" content=", college, school, educational" />
		<meta name="description" content="Academics | Valencia College" />
		<meta name="author" content="Valencia College" />
		<title>Locations | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_staging/headcode.inc"); ?>
		<script>
			var page_url="https://preview.valenciacollege.edu/admissions/static_index.php";
		</script>
		<script>
			FontAwesomeConfig = { searchPseudoElements: true };
		</script>
		<link rel="stylesheet" type="text/css" href="_resources/css/bootstrap/bootstrap.min.css" />
		<style>
			.check ul {
				list-style: none;
			}

			.check ul li:before {
				content: '✓\00a0\00a0';
			
			}
				.center {text-align:center;}
			.card-footer-light {
    padding: 2.5rem 1.25rem;
    background-color: transparent;
    border-top: none;
}
			.card-light {
				border: none;
			}
			@media screen and (min-width: 1024px){
				.card-deck {
					-webkit-box-orient: horizontal;
					-webkit-box-direction: normal;
					-ms-flex-flow: row wrap;
					flex-flow: row wrap;
					margin-right: -30px!important;
					margin-left: -30px!important;
				}
			}
		</style>
	</head>
	<body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
		<div id="preloader">
			<div class="pulse"></div>
		</div>
		<!-- Pulse Preloader --> <!-- Return to Top --><a id="return-to-top" href="/javascript:" aria-label="Return to top"> </a> <!-- <header class="valencia-alert"><iframe></header> --> 
		<!-- ChromeNav Header ================================================== -->
           <style>
            @media screen and (min-width: 960px){
              .menu-site{margin-top: 80px; position: absolute; z-index: 1;}
              .menu-site a, .menu-college a {color: white!important;}
              .menu-college {background-color:#BF311A;}
            }
          </style>
    <nav class="navbar navbar-expand-lg menu-college">
      <div class="container"> 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
<div class="menu-college">          
          <ul class="navbar-nav mr-auto">
              <a class="navbar-brand" href="#">LOGO HERE</a>
<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Students
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li class="nav-item"><a class="dropdown-item" href="#">Continuing Education</a></li>
                <li class="nav-item"><a class="dropdown-item" href="#">Current Students</a></li>
                <li class="nav-item"><a class="dropdown-item" href="#">Future Students</a></li>
                <a class="dropdown-item" href="#">Internation Students</a>
                <li class="nav-item"><a class="dropdown-item" href="#">Military &amp; Veterans</a></li>

              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Faculty</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">Employees</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="#">Alumni &amp; Foundation</a>
            </li>          
			  <li><form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form></li>
          </ul>

			</div>
          <style>
            @media screen and (min-width: 960px){
              .menu-site{margin-top: 80px; position: absolute; z-index: 1;}
              .menu-site a {color: white!important;}
            }
          </style>
          <div class="menu-site">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" href="#">Academics</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Admissions</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Programs &amp; Degrees</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

		<!-- SiteMenu Header ================================================== -->

		<div class="sub-header bg bg-locations">
			<div id="intro-txt"><h1>
				Locations
				</h1></div>
		</div>
		<div class="container-fluid bg-grey">

			<div class="container margin-30">
				<section>
<div class="card-deck">
  <div class="card card-light">
    <img class="card-img-top" src="/_resources/img/prestige/art-locations-amtc.jpg" alt="Card image cap">
    <div class="card-body">
      <p class="card-text" style="font-family:montserrat_light; font-size:14px;">Valencia College offers its Advanced Manufacturing programs in two locations, serving both Osceola and Orange counties. Classroom instruction is available at both locations and hands-on training is provided at the Advanced Manufacturing Training Center in Kissimmee, or may be completed at one of Valencia’s advanced manufacturing partners’ facilities in Orange County.</p>
		<p>
			  1099 Shady Lane<br/>
   Kissimmee, FL 34744
<br/>
 407-582-8227
		</p>
    </div>
    <div class="card-footer card-footer-light center">
<button type="button" class="btn btn-sm btn-outline-primary px-3">LEARN MORE</button>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="/_resources/img/prestige/art-locations-dc.jpg" alt="Card image cap">
    <div class="card-body">
      <p class="card-text">Set to open in fall 2019, Valencia’s Downtown Campus will give students an opportunity to live and learn in downtown Orlando. The only Valencia location to offer on-campus housing, this campus is a great fit for students who want study in a centrally located setting, and have a variety of options for activities on nights and weekends. </p>
    </div>
    <div class="card-footer">
      <small class="text-muted">LEARN MORE</small>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="/_resources/img/prestige/art-locations-ec.jpg" alt="Card image cap">
    <div class="card-body">
      <p class="card-text">East meets art at the campus that is home to our Performing Arts Center, School of Arts and Entertainment – Film Production, Sound Technology building, as well as our dance and music programs. The campus is on the banks of the Little Econlockhatchee River and only a few minutes from UCF.</p>
    </div>
    <div class="card-footer">
      <small class="text-muted">Last updated 3 mins ago</small>
    </div>
  </div>
</div>
				</section>
			</div>
		</div>




		<?php/* include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/homepage-featured-stories.inc"); */?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_staging/footcode.inc"); ?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
		<div id="hidden" style="display: none;"><a id="de" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/index.pcf" rel="nofollow">©</a></div><a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/static-index.php">©</a></body>
</html>
