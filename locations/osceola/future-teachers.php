<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Future Teachers Academy of Osceola County | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/future-teachers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Future Teachers Academy of Osceola County</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li>Future Teachers Academy of Osceola County</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <p>The Future Teachers Academy is a special program for students interested in becoming
                           elementary school teachers in Osceola County. Through the program, students will complete
                           an Associate in Arts from Valencia and a Bachelor's of Science in Elementary Education
                           from the University of Central Florida. Upon successful completion of the program
                           and upon meeting eligibility requirements, graduates from the program will be offered
                           a teaching position by the School District of Osceola County.
                        </p>
                        
                        <h3 dir="auto">Who is eligible to apply?</h3>
                        
                        <p dir="auto">Currently, applications are being accepted from current high school seniors in Osceola
                           County.
                        </p>
                        
                        <h3 dir="auto">How do I apply?</h3>
                        
                        <p dir="auto">The priority application deadline for current high school seniors is April 15, 2017.
                           To apply, complete this online application (<a href="http://valenciacc.ut1.qualtrics.com/jfe/form/SV_cUwg47BgSOy2r3f" rel="nofollow noreferrer" target="_blank">http://valenciacc.ut1.qualtrics.com/jfe/form/SV_cUwg47BgSOy2r3f</a>) . (Please note that the application has two required essays that you may want to
                           prepare in advance before accessing the online application: 1) "In 300-500 words,
                           please describe your favorite teacher and why this teacher is so memorable." and 2)
                           "In 300-500 words, please explain why you want to be a elementary school teacher in
                           Osceola County.”)
                        </p>
                        
                        <h3 dir="auto">What are the program expectations?</h3>
                        
                        <p dir="auto">To participate in the program, students will enroll full-time at Valencia College
                           beginning in Fall 2017. Participants will register for a cohort of classes that meet
                           on Tuesdays, Wednesdays, and Thursdays during the Fall, Spring, and Summer terms.
                           Upon completion of an Associate's degree, students will enroll full-time at UCF beginning
                           in Fall 2019, and will complete student teaching and internship experiences in an
                           Osceola County school. Students must maintain at least a 2.5 grade point average throughout
                           the program, and must maintain Satisfactory Academic Progress standards (<a href="../../../locations/finaid/satisfactory-progress.html" rel="nofollow noreferrer" target="_blank">http://valenciacollege.edu/finaid/satisfactory_progress.cfm</a>) .
                        </p>
                        
                        <h3 dir="auto">Is there a scholarship?</h3>
                        
                        <p dir="auto">While at Valencia College, upon successful completion of each term, students will
                           earn a $200 scholarship for the subsequent term. For example, upon successful completion
                           of the Fall 2017 term, students will earn a $200 scholarship toward the Spring 2018
                           term. While at UCF, students will earn a $2,500 stipend for the successful completion
                           of the junior year internship, and a $2,500 stipend for the successful completion
                           of the senior year internship.
                        </p>
                        
                        <h3 dir="auto">Who can I contact with questions?</h3>
                        
                        <p dir="auto">If you have questions about the Future Teachers Academy, please contact Dr. Thomas
                           Takayama, Dean of Humanities and Social Sciences at Valencia's Osceola Campus, at
                           <a href="mailto:ttakayama@valenciacollege.edu" rel="noreferrer">ttakayama@valenciacollege.edu</a></p>
                        
                        <p><a href="../../../locations/osceola/future-teachers/index.html#top">TOP</a></p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           
                           <p><strong>Call an Admissions Coach<br>
                                 <i>Llama a tu consejero personal</i></strong></p>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-1507</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>1800 Denn John Lane<br>Kissimmee, FL 34744
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                           Directions</a>
                        <a href="../../../locations/map/osceola.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        <a href="http://www.youvisit.com/tour/valencia/osceola?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="osceola campus virtual tour" height="245" src="../../../locations/osceola/future-teachers/youvisit-osceola-245px.jpg" width="245"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/future-teachers.pcf">©</a>
      </div>
   </body>
</html>