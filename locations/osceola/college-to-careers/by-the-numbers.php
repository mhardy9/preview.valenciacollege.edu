<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/by-the-numbers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>
                           
                           <h2>About Us -  By the Numbers </h2>
                           
                           
                           <p><img alt="Graph of Percent of Population" height="110" src="../../../locations/osceola/college-to-careers/graph1.png" width="350"></p>
                           
                           <h3>&nbsp;</h3>
                           
                           <h3>&nbsp;</h3>
                           
                           <h3>&nbsp;</h3>
                           
                           <h3>&nbsp;</h3>
                           
                           <h3>Valencia College</h3>
                           
                           <p>With a total college enrollment of 68,351, Valencia College is fortunate to draw students
                              from a variety of backgrounds. That said, almost 33% of the total college enrollment
                              is Latino. At the Osceola Campus approximately 50% of the 19,595 students are Latino,
                              earning the Osceola Campus a U.S. Department of Education designation as a Hispanic
                              Serving Institution (HSI). 
                           </p>
                           
                           
                           
                           <h3>Osceola County </h3>
                           
                           <p>Osceola County is in a unique position in Central Florida because of the size of our
                              Latino population. In fact, almost 50% of Osceola County is Latino. That's compared
                              to just 23% in the state of Florida and 16% nationwide. Those numbers, alone, prove
                              why specific information geared toward these residents is necessary in Osceola County.
                              
                           </p>
                           
                           
                           
                           
                           <p><img alt="Graph of Holders of Bachelor's Degrees" height="110" src="../../../locations/osceola/college-to-careers/graph2.png" width="350"></p>
                           
                           <p>Improving the lives of people living in Osceola County is a major goal of the Title
                              V grant at the Valencia College Osceola Campus. We plan to help make these improvements
                              happen by offering affordable education in career-driven fields. The numbers speak
                              for themselves. When you consider that just 13% of Osceola residents over the age
                              of 25 hold Bachelor's Degrees, compared to 17% in Florida and 18% nationwide, the
                              need to create new opportunities for Osceola County residents becomes real. By providing
                              cost-effective ways for people in Osceola County to complete the first two years of
                              college, we create an opportunity for them to pursue the last two years of a Bachelor's
                              Degree without breaking the bank.
                           </p>
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>For more info about the College to Careers</strong><br>
                                    <span>
                                       <strong>Title V Osceola campus grant, contact:</strong><br>         
                                       </span>
                                    </span></p>
                              
                              <p>
                                 <span>
                                    <strong>Dr. Jennifer Keefe, Grant Director</strong><br>
                                    <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                    <i></i> (407) 582-4823</span></p>
                              
                              
                              <p>
                                 <span>
                                    <strong>Dr. James McDonald, Osceola campus Dean of Career &amp; Technical Programs</strong><br>
                                    <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                    <i></i> (407) 582-4401<br>
                                    </span></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                              Directions</a>
                           <a href="../../../locations/map/osceola.html" target="_blank">
                              Campus Map</a>
                           
                           <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/by-the-numbers.pcf">©</a>
      </div>
   </body>
</html>