<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/goals.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>     
                           
                           
                           <h2>Goals</h2>
                           
                           <p>The College to Careers program at the Valencia College Osceola campus aligns with
                              two major facets of the college's overall strategic goals: <a href="../../../locations/osceola/college-to-careers/building-pathways.html" target="_self">Build Pathways</a> and <a href="../../../locations/osceola/college-to-careers/partners.html" target="_self">Partner with the Community</a>. 
                           </p>
                           
                           <p><strong>There are three main goals within the grant:</strong></p>
                           
                           
                           <p> <strong>First</strong>, through the use of Title V funds from the U.S. Department of Education, the Valencia
                              College Osceola campus will work to find new and innovative pathways to make education
                              more accessible for students, especially the county's largely Hispanic population.
                              This work includes: 
                              
                           </p>
                           
                           <ul>
                              
                              <li>translation of marketing materials into Spanish.</li>
                              
                              
                              <li>new and improved means of financial aid access.</li>
                              
                              
                              <li>new and innovative methods of course delivery. 
                                 
                                 
                              </li>
                              
                              <li>new and improved methods of maintaining contact with prospective and new students.</li>
                              
                              
                              <li>improved methods of class scheduling to better utilize campus facilities and to increase
                                 course offerings.
                              </li>
                              
                              
                           </ul>
                           
                           
                           <p><strong>Second</strong>, through the use of Title V Grant funds from the U.S. Department of Education, the
                              Valencia Osceola campus will increase opportunities for our students to gain the skills
                              needed to become contributors to the  economic stability of Central Florida, and of
                              Osceola County in particular. We are actively working with community and local business
                              partners to develop the  direction two new career and technical programs at the Osceola
                              campus will take.<br> <br><em>To learn more about Valencia's current Career and Technical Education offerings <a href="../../../locations/osceola/college-to-careers/career-technical-offerings.html">click here.</a></em></p>
                           
                           
                           <p>
                              Tied to the first two goals is a <strong>third</strong> set of goals: increasing the number of students who complete our career and technical
                              certificate programs by 10% by September 2020 and increasing the number of students
                              who apply previously earned credits toward Valencia A.S. degree programs by 10% by
                              September 2020. We anticipate many of these students will come from our partner institutions,
                              such as the School District of Osceola County's TECO facility.    
                           </p>
                           
                           
                           
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>For more info about the College to Careers</strong><br>
                                    <span>
                                       <strong>Title V Osceola campus grant, contact:</strong><br>         
                                       </span>
                                    </span></p>
                              
                              <p>
                                 <span>
                                    <strong>Dr. Jennifer Keefe, Grant Director</strong><br>
                                    <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                    <i></i> (407) 582-4823</span></p>
                              
                              
                              <p>
                                 <span>
                                    <strong>Dr. James McDonald, Osceola campus Dean of Career &amp; Technical Programs</strong><br>
                                    <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                    <i></i> (407) 582-4401<br>
                                    </span></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                              Directions</a>
                           <a href="../../../locations/map/osceola.html" target="_blank">
                              Campus Map</a>
                           
                           <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/goals.pcf">©</a>
      </div>
   </body>
</html>