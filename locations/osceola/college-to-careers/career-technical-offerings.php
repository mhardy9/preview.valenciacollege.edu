<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/career-technical-offerings.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>
                           
                           <h2>Career and Technical Offerings</h2>
                           
                           
                           <p><strong>Several of Valencia's A.S. Degree programs can be completed at the Osceola Campus.
                                 Those programs include:</strong>
                              
                           </p>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#programrequirementstext">A.S. Degree in Business Administration</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#programrequirementstext">Articulated A.S. to B.A./B.S. in General Business Administration</a></li> 
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/industrialmanagementtechnology/#programrequirementstext">A.S. Degree in Supervision and Management for Industry</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/accountingtechnology/#programrequirementstext">A.S. Degree in Accounting Technology*</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerinformationtechnology/#programrequirementstext">A.S. in Computer Information Technology**</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/#programrequirementstext">A.S. in Hospitality &amp; Tourism Management*</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/graphicandinteractivedesign/#programrequirementstext">A.S. in Graphic &amp; Interactive Design*</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/">A.S. in Medical Office Administration</a></li>
                           
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#programrequirementstext">A.S. in Office Administration</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#programrequirementstext">A.S. in Criminal Justice</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/paralegalstudies/#programrequirementstext">A.S. in Paralegal Studies*</a></li>
                           <br>
                           
                           <strong>Students can also complete certificates at the Osceola Campus in:</strong> 
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Business Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Customer Service Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Real Estate Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Human Resources Management* </a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">International Business Specialist*</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Entrepreneurship*</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerinformationtechnology/#certificatestext">Computer Information Technology Analyst**</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerinformationtechnology/#certificatestext">Computer Information Technology Specialist**</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerprogramminganalysis/#certificatestext">Computer Programming**</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerprogramminganalysis/#certificatestext">Computer Programming Specialist**</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/#certificatestext">Event Planning Management*</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/#certificatestext">Restaurant &amp; Food Service Management*</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/#certificatestext">Guest Service Specialist* </a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/#certificatestext">Hospitality - Rooms Division Management*</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/#certificatestext">Medical Office Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/#certificatestext">Medical Office Management</a></li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#certificatestext">Office Management</a> 
                           </li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#certificatestext">Office Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#certificatestext">Office Support</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Homeland Security Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Homeland Security Law Enforcement Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Criminal Justice Technology Specialist </a></li>
                           <br>
                           
                           
                           <p>*students may have to travel to a different campus for some classes
                              **students may be required to take some classes online
                              Valencia's Certificate programs can put you on the fast-track to reaching your career
                              goals. They are shorter in length and most can be completed in one year or less. They
                              are designed to equip you with a specialized skill set for entry-level employment,
                              or to upgrade your skills for job advancement.
                           </p>
                           
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>For more info about the College to Careers</strong><br>
                                    <span>
                                       <strong>Title V Osceola campus grant, contact:</strong><br>         
                                       </span>
                                    </span></p>
                              
                              <p>
                                 <span>
                                    <strong>Dr. Jennifer Keefe, Grant Director</strong><br>
                                    <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                    <i></i> (407) 582-4823</span></p>
                              
                              
                              <p>
                                 <span>
                                    <strong>Dr. James McDonald, Osceola campus Dean of Career &amp; Technical Programs</strong><br>
                                    <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                    <i></i> (407) 582-4401<br>
                                    </span></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                              Directions</a>
                           <a href="../../../locations/map/osceola.html" target="_blank">
                              Campus Map</a>
                           
                           <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/career-technical-offerings.pcf">©</a>
      </div>
   </body>
</html>