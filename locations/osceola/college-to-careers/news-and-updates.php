<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/news-and-updates.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>     
                           
                           
                           <h2>News and Updates</h2>
                           
                           <h3>Residential Property Management Program Prepares for  Students*</h3>
                           
                           <p><img alt="stock image" height="327" src="../../../locations/osceola/college-to-careers/iStock-546201678.jpg" width="490"></p>
                           
                           <p>Valencia College's Residential Property Management AS Degree  program, the development
                              of which was funded through the U.S. Department of  Education's Title V Osceola Grant,
                              has taken several steps forward to prepare  to welcome students for the first time
                              this fall. 
                           </p>
                           
                           
                           <ul>
                              
                              <li>
                                 <img alt="Eldon Warfield" height="159" src="../../../locations/osceola/college-to-careers/Eldon.png" width="213">Veteran of the industry Eldon Warfield will  serve as a faculty member and Program
                                 Chair for the new Residential Property  Management AS Degree program beginning this
                                 fall. Eldon is the President of  IREM's West Coast Florida Chapter and has over 30
                                 years of experience at all  levels of the Residential Property Management industry.
                                 He has been working,  through funding from the Title V Osceola Grant, this summer
                                 to build the  program's Advisory Council and to market the program to potential students.
                                 
                              </li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>The Advisory Council for the program, which is  made up of experts from across the
                                 industry, including our partners at the  National Apartment Association, Florida Apartment
                                 Association, the Apartment  Association of Greater Orlando, and a number of management
                                 companies from  across Central Florida, met for the first time on July 10th. The 
                                 group's goal will be to guide the program to ensure it remains current with the  industry.
                              </li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>Throughout July, Valencia staff and our new  Residential Property Management faculty
                                 member, Eldon Warfield, teamed up with  Title V Osceola Grant staff to host two webinars
                                 for incumbent workers in  Residential Property Management to explain the new degree
                                 program and to answer  questions before potential students apply to Valencia. A meeting
                                 for current  Valencia students is scheduled for July 25th. 
                              </li>
                              
                           </ul>
                           
                           <p>*pending approval from SACSCOC</p>
                           
                           
                           <h3>Hospitality Partners Help Guide Program Growth</h3>
                           
                           <p><img alt="Hospitality" height="370" src="../../../locations/osceola/college-to-careers/Hospitality55-for-Career-Book.jpg" width="250"></p>
                           
                           <p>As we search for new ways to expand our Hospitality program  at Valencia College,
                              the Osceola Campus reached out to area partners in the  Hospitality industry to look
                              for new ways to reach out to potential students.  On July 10, 2017, partners from
                              SeaWorld, Reunion Resort, Marriott, Holiday Inn  Vacation Clubs, and more gathered
                              with Hospitality program staff from our  Osceola and West campuses to discuss new
                              ideas for course scheduling and  marketing the program to their incumbent workers.
                              The new elements will take  time to develop, but check back on our News and Events
                              page for updates.                     
                           </p>
                           
                           <p><strong>Ad Astra Configuration Underway</strong></p>
                           
                           <p>A project is underway to implement software that will help  the Osceola Campus, and
                              ultimately all Valencia campuses, to increase capacity  for students through better
                              space utilization. The project aligns with Goal 3  of the Title V Osceola Grant; and
                              through U.S. Department of Education funding,  the Osceola Campus was able to purchase
                              the Ad <img alt="image" height="149" src="../../../locations/osceola/college-to-careers/IMG_3280.JPG" width="184">Astra Scheduler software to  complete this task. The software was purchased earlier
                              this year and  configuration began in July. On July 19th and 20th, Ad  Astra representatives
                              visited the Osceola Campus to meet with the college wide  configuration team to make
                              key decisions about how the software will be set up.  Implementation tasks will continue
                              into the fall and we hope to be scheduling  classes in the Scheduler by the Summer
                              of 2018.
                           </p>
                           
                           
                           <h3></h3>
                           
                           
                           <h3><strong>Next Stop: Transportation, Logistics, and Supply Chain</strong></h3>
                           
                           <h4>Valencia Moves Forward With Exploring Transportation, Logistics, and Distribution
                              Programs
                           </h4>
                           
                           <p> On June 14, 2017, experts in the fields of Transportation, Logistics, Supply Chain,
                              and Distribution met at the Valencia College Osceola Campus to discuss the needs of
                              the industry and how Valencia College might develop a program to train students to
                              meet those needs. Representatives from Disney, Sea World, Rooms to Go, FreshPoint,
                              HNM Global Logistics, Saddle Creek Logistics, and FedEx joined representatives from
                              Osceola County, Port Canaveral, and the Florida Department of Transportation to talk
                              about the job and educational skills needed to keep Osceola County competitive in
                              the 21st century marketplace when it comes to moving goods from their manufacturer
                              all the way to the consumer.
                           </p>
                           
                           <p>                      The next step in this process will be for the team at Valencia
                              to work together to determine what kind of program we will create. Fortunately, many
                              of our participants expressed an interest in continuing to be a part of that process.
                              Stay tuned for more information as we move forward in this process.
                           </p>
                           
                           
                           <h3>
                              <strong>New Property Management A.S. Degree Finds a Home in Osceola County</strong> 
                           </h3>
                           
                           <p> Valencia College's new A.S. Degree program in Residential Property Management will
                              soon be ready to start accepting students for the Fall 2017 semester. The 60-hour
                              degree program will include classes in all aspects of residential property management
                              with an aim toward preparing graduates for careers in areas such as apartment complex
                              management. Students will also be prepared, through the program, to sit for the Certified
                              Apartment Manager (CAM) license. Look for details on the program and a link to the
                              program website in the coming months. The work to develop the project is being managed
                              through the Title V Osceola grant office, including curriculum development and program
                              marketing. 
                           </p>
                           
                           
                           <h3> <strong>Future Teachers Academy Creates Community Partnership</strong> 
                           </h3>
                           
                           <p><img alt="Valencia College" height="153" hspace="0" src="../../../locations/osceola/college-to-careers/ValenciaCollege.png" vspace="0" width="153"> <img alt="School District of Osceola County" height="126" hspace="0" src="../../../locations/osceola/college-to-careers/SchoolDistrictOfOsceolaCounty.png" vspace="0" width="126"> <img alt="University of Central Florida" height="226" hspace="0" src="../../../locations/osceola/college-to-careers/University-of-Central-Florida-logo.png" vspace="0" width="144"></p>
                           
                           <p> Building partnerships across our community is an important part of the Title V Osceola
                              grant and one new program is doing just that in a big way. The Future Teachers Academy
                              began accepting a pilot group of students at the Osceola Campus in the fall of 2016
                              and will admit a new set of students in the fall of 2017. The students are joining
                              a cohort that will go through their Associate of Arts (AA) degree program at Valencia
                              together and then apply for admission to the University of Central Florida's Elementary
                              Education program. Their UCF classes will be taught at the UCF regional campus building
                              on Valencia's Osceola Campus. The School District of Osceola County has also gotten
                              involved in the project, providing substitute teaching opportunities to students on
                              Mondays and Fridays, while their classes are offered Tuesdays, Wednesdays, and Thursdays.
                              The students will also have first access to jobs in the Osceola County School system.
                              
                           </p>
                           
                           
                           <h3>
                              <strong>Articulation Opportunities Abound for Osceola Students</strong> 
                           </h3>
                           
                           <p><img alt="Valencia College" height="153" hspace="0" src="../../../locations/osceola/college-to-careers/ValenciaCollege_000.png" vspace="0" width="153"><img alt="TECO" border="1" height="160" hspace="0" src="../../../locations/osceola/college-to-careers/TECO.png" vspace="0" width="160"></p>
                           
                           <p>Through the work of the Title V Osceola grant, we are pleased to announce the creation
                              of several new articulation agreements with our community partner, TECO, the Technical
                              Education Center Osceola. These agreements will help educate students so they can
                              meet community employment needs. 
                           </p>
                           
                           <ul>
                              
                              <li>Students in TECO's Medical Laboratory Assisting program can now earn up to 4 credits
                                 in Valencia's Biomedical Technology A.S. Degree program.
                              </li>
                              
                              <li>Students in TECO's Cosmetology and Pharmacy Technician programs can now earn 24-27
                                 credits in Valencia's Supervision for Management &amp; Industry A.S. Degree program. 
                              </li>
                              
                              <li>Beginning in 2018, students in TECO's Medical Coder Biller program who earn the Certified
                                 Coder Associate (CCA) certification from AHIMA or Certified Professional Coder (CPC)
                                 from AAPC will be able to earn 25 credits in Valencia's Health Information Technology
                                 A.S. Degree program.
                              </li>
                              
                           </ul>
                           
                           <p>We look forward to the creation of new pathways for TECO's students in the future.
                              
                           </p>
                           
                           <p>A new articulation agreement is also now in place with the Academy of Construction
                              Technologies (A.C.T.). A.C.T. works with high school juniors and seniors to train
                              them in construction-related trades. Students completing the A.C.T. program can now
                              earn 2-4 internship credits in Valencia's Building Construction Technology A.S. Degree
                              program.
                              
                           </p>
                           
                           
                           <h3> <strong>TECO Students Gain New Student Experience</strong> 
                           </h3>
                           
                           <p><img alt="Tanisha Castor" border="1" height="214" hspace="0" src="../../../locations/osceola/college-to-careers/Tanisha_OfficePic.jpg" vspace="0" width="215"></p>
                           
                           <p>The partnership between Valencia College and TECO is growing even stronger as TECO
                              students engage in Valencia's New Student Experience (NSE) class. Valencia NSE Professor
                              Tanisha Castor is teaching Valencia's coordinated experience for all first-year students
                              on the TECO campus. The class is required for almost all of the Valencia A.S. Degree
                              programs that TECO students can bring their industry certificates into for college
                              credits through our articulation agreements. 
                           </p>
                           
                           <p>In the fall of 2016, 22 Dual Enrollment (DE) students from the TECO Paths High School
                              program completed the class, and an additional 10 DE students and 5 adult students
                              in TECO certificate programs completed the class in the spring of 2017. The adult
                              students were granted tuition waivers and were only required to purchase their books
                              for the class. A new class is already forming for fall of 2017.
                           </p>
                           
                           <p>The goal of the NSE class is to teach study skills, acclimate the students to the
                              college experience, and to provide an avenue for career exploration. The 3-credit
                              class also makes their TECO certificates immediately eligible for credit awards once
                              they enroll for a new semester at Valencia. The class will help fulfill the goal of
                              the Title V Osceola grant to make more students aware of the certificate articulation
                              opportunities and to help them to take advantage of those opportunities. 
                           </p>
                           
                           
                           <h3> <strong>New Classroom Equipment Provides Opportunities</strong> 
                           </h3>
                           
                           <p><img alt="Synchronized Learning Classroom" border="1" height="250" hspace="0" src="../../../locations/osceola/college-to-careers/EastStudent_DisplayPanel.jpg" vspace="0" width="400"></p>
                           
                           <p>In addition to the Blackboard Collaborate project, the Title V Osceola grant has partnered
                              with Valencia's Office of Instructional Technology (OIT) and our East Campus to pilot
                              a new way to help Osceola Campus students gain access to A.S. Degree programs on other
                              campuses. Over $111,000 in grant funds was used to help purchase equipment to outfit
                              a classroom at Osceola Campus, and one at the East Campus, with touchscreen monitors,
                              cameras and microphones. We are calling the experience Synchronized Learning because
                              the two classrooms are in synch, in the same class, at the same time.
                           </p>
                           
                           <p>The pilot class, Introduction to Paralegal Studies, was held in the Spring of 2017.
                              Instructor Wendy Toscano was able to see, to hear, and to fully interact with the
                              students at Osceola Campus from her classroom at East Campus. The students at Osceola
                              Campus told Title V Grant Director Dr. Jennifer Keefe after the class ended that they
                              felt connected to the instructor and appreciated the chance to take their class without
                              having to drive to the East Campus.&nbsp; 
                           </p>
                           
                           <p>A second pilot of Introduction to Paralegal is scheduled for this summer and there
                              are plans to add new class offerings over the Synchronized Learning System in the
                              fall. 
                           </p>
                           
                           
                           <h3><strong>Changes to Supervision A.S. Degree Will Help Students</strong></h3>
                           
                           <p>Valencia College is making it easier for students to transfer credits from certificate
                              programs into our Supervision for Management and Industry A.S. Degree program. In
                              the past, students transferring credit through articulation from TECO would only be
                              awarded a lump-sum of credits that did not equate to actual classes in the program.
                              Now, those credits apply to actual program coursework.
                           </p>
                           
                           <p>In the process of making the change, we also updated the name of the degree, which
                              used to be called Industrial Management Supervision. The new name reflects the more
                              broad scope of the degree. We also added the required New Student Experience class
                              to the A.S. Degree to help students get better acclimated to college. The change impacts
                              TECO students in the Industrial-related programs, such as Air Conditioning, Refrigeration,
                              and Heating, Automotive Service Technology, Cosmetology, Pharmacy Technician, and
                              Electricity. They can earn 24-27 credit hours for earning any of those certificates
                              from TECO and then coming to Valencia College. 
                           </p>
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>For more info about the College to Careers</strong><br>
                                    <span>
                                       <strong>Title V Osceola campus grant, contact:</strong><br>         
                                       </span>
                                    </span></p>
                              
                              <p>
                                 <span>
                                    <strong>Dr. Jennifer Keefe, Grant Director</strong><br>
                                    <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                    <i></i> (407) 582-4823</span></p>
                              
                              
                              <p>
                                 <span>
                                    <strong>Dr. James McDonald, Osceola campus Dean of Career &amp; Technical Programs</strong><br>
                                    <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                    <i></i> (407) 582-4401<br>
                                    </span></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                              Directions</a>
                           <a href="../../../locations/map/osceola.html" target="_blank">
                              Campus Map</a>
                           
                           <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/news-and-updates.pcf">©</a>
      </div>
   </body>
</html>