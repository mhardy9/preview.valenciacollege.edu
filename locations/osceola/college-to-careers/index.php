<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Title V - College to Careers - Valencia College | Valencia College</title>
      <meta name="Description" content="A multi-campus college dedicated to the premise that educational opportunities are necessary to bring together the diverse forces in society"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li>College To Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div id="containerHeaderAlert">
                  
                  
                  
                  
                  
                  
                  
                  
               </div>
               
               
               <div id="containerTop">
                  
                  
                  <header><a name="top" id="top"></a>
                     
                     <div id="accessibility">
                        <a href="../../../locations/osceola/college-to-careers/index.html#navigate" class="trkHeader">Skip to Local Navigation</a> | <a href="../../../locations/osceola/college-to-careers/index.html#content" class="trkHeader">Skip to Content</a>
                        
                     </div>
                     
                     <div id="outContainerTopBar">
                        
                        <div id="topBar">
                           
                           <div id="logoandlogin">
                              
                              <div id="logo">
                                 
                                 
                                 <a href="../../../locations/index.html" class="trkHeader" aria-label="Valencia College"><img src="https://valenciacollege.edu/images/logos/valencia-college-logo.svg" alt="Valencia College" width="275" height="42" role="img"></a>
                                 
                                 
                              </div>
                              
                              <div class="AtlasLoginContainer"> 
                                 <a href="https://atlas.valenciacollege.edu/" title="Atlas Login" style="text-decoration:none;" class="trkHeader"><span class="AtlasIcon"></span> <span class="AtlasText"><span class="kern-2">A</span>T<span class="kern-1">LAS</span>&nbsp;<span class="kern-2">L</span><span class="kern-1">OGIN</span></span></a>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div id="navContainer" role="navigation" aria-label="Sitewide Navigation">
                           
                           <div id="nav">
                              
                              <ul class="navMenu">
                                 
                                 <li class="navLinks"><a href="http://preview.valenciacollege.edu/future-students/" id="nav-future" class="trkHeader">Future Students</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../../../locations/students/index.html" id="nav-current" class="trkHeader">Current Students</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../../../locations/faculty.html" id="nav-faculty" class="trkHeader">Faculty &amp; Staff</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../../../locations/visitors.html" id="nav-visitors" class="trkHeader">Visitors &amp; Friends</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="blank">
                                    
                                    <div class="util">
                                       
                                       <ul class="x_level1">
                                          
                                          <li class="x_quicklinks">
                                             <a href="../../../locations/includes/quicklinks.html" style="text-decoration:none;" class="trkHeader">Quick Links</a>
                                             
                                             <ul class="x_level2">
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/calendar/index.html" class="trkHeader">Academic Calendar</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/students/admissions-records/index.html" class="trkHeader">Admissions</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://net4.valenciacollege.edu/promos/internal/blackboard.cfm" class="trkHeader">Blackboard Learn</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://www.valenciabookstores.com/valencc/" class="trkHeader">Bookstore</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/business-office/index.html" class="trkHeader">Business Office</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/calendar/index.html" class="trkHeader">Calendars</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/map/index.html" class="trkHeader">Campuses</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/catalog/index.html" class="trkHeader">Catalog</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/schedule/" class="trkHeader">Class Schedule</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/continuing-education/" class="trkHeader">Continuing Education</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/programs/index.html" class="trkHeader">Degree &amp; Career Programs</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/departments/index.html" class="trkHeader">Departments</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/economicdevelopment/" class="trkHeader">Economic Development</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/human-resources/index.html" class="trkHeader">Employment</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/finaid/index.html" class="trkHeader">Financial Aid Services</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/international/index.html" class="trkHeader">International</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/library/index.html" class="trkHeader">Libraries</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/map/index.html" class="trkHeader">Locations</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/contact/directory.html" class="trkHeader">Phone Directory</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/students/admissions-records/index.html" class="trkHeader">Records/Transcripts</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/security/index.html" class="trkHeader">Security/Parking</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../../locations/support/index.html" class="trkHeader">Support</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/events/" class="trkHeader">Valencia Events</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="blank">
                                    
                                    <div class="util">
                                       
                                       <div class="x_search" id="Search">
                                          
                                          <form label="search" class="relative" action="https://search.valenciacollege.edu/search" name="seek" id="seek" method="get">
                                             <label for="q"><span style="display:none;">Search</span></label>
                                             <input type="text" name="q" id="q" alt="Search" value="" aria-label="Search">
                                             <button style="position:absolute; top:0; right:12;" type="submit" name="submitMe" value="seek" onmouseover="clearTextSubmit(document.seek.q)" onfocus="clearTextSubmit(document.seek.q)" onmouseout="clearTextSubmitOut(document.seek.q)" onblur="clearTextSubmitOut(document.seek.q)">go</button>
                                             <input type="hidden" name="site" value="default_collection">
                                             <input type="hidden" name="client" value="default_frontend">
                                             <input type="hidden" name="proxystylesheet" value="default_frontend">
                                             <input type="hidden" name="output" value="xml_no_dtd">
                                             
                                          </form>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </li>
                                 
                              </ul>
                              
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </header>
                  
               </div>
               
               <div id="containerMain">
                  
                  
                  <div id="containerContent" role="main">
                     
                     <div id="wrapper">
                        
                        <div id="crumb">
                           <a href="../../../locations/default.html">Home</a>
                           <img src="http://valenciacollege.edu/images/arrow.gif" width="8" height="8" alt=""> <a href="../../../locations/osceola/index.html">Osceola Campus</a>
                           <img src="http://valenciacollege.edu/images/arrow.gif" width="8" height="8" alt=""> <a href="../../../locations/osceola/college-to-careers/index.html">Osceola Title V</a>
                           <img src="http://valenciacollege.edu/images/arrow.gif" width="8" height="8" alt=""> <a href="../../../locations/osceola/college-to-careers/es/index.html">
                              En español</a>
                           
                        </div>
                        
                        
                        
                        <div class="container_12">
                           
                           
                           
                           <div class="grid_12">
                              
                              
                              <div id="SPslideshow">
                                 
                                 <div id="slideNav" style="z-index: 1000; position: absolute; display: none;">
                                    
                                    
                                 </div> 
                                 
                                 
                                 <div id="SPmainImage"> 
                                    
                                    <div class="SPmainImage">
                                       <img src="http://valenciacollege.edu/osceola/college-to-careers/img/home5.png" width="930" height="314" alt="Title V Osceola Home Image" border="0">
                                       
                                       
                                       
                                       <div style="display:none;" class="SPmainImage">
                                          <img src="http://valenciacollege.edu/osceola/college-to-careers/img/home4.png" width="930" height="314" alt="Title V Osceola Home Image" border="0">
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    <div style="display:none;" class="SPmainImage">
                                       <img src="http://valenciacollege.edu/osceola/college-to-careers/img/home3.png" width="930" height="314" alt="Title V Osceola Home Image" border="0">
                                       
                                       
                                    </div>
                                    
                                    
                                    <div style="display:none;" class="SPmainImage">
                                       <img src="http://valenciacollege.edu/osceola/college-to-careers/img/home2.png" width="930" height="314" alt="Title V Osceola Home Image" border="0">
                                       
                                       
                                    </div>
                                    
                                    
                                    <div style="display:none;" class="SPmainImage">
                                       <img src="http://valenciacollege.edu/osceola/college-to-careers/img/home1.png" width="930" height="314" alt="Title V Osceola Home Image" border="0">
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              
                              
                           </div>
                           
                           <div class="clear">&nbsp;</div> 
                           
                           
                           <div class="grid_12">&nbsp;</div>
                           
                           <div class="clear">&nbsp;</div> 
                           
                           
                           
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:350px;">
                                 
                                 <div align="left">  
                                    <h2>  
                                       <div>
                                          <h1 class="fontcolor_7" id="headMain">Title V Osceola</h1>
                                       </div>
                                       
                                    </h2>
                                    
                                    <div>
                                       
                                       <div align="left">
                                          
                                          <p>Simply put, the College to Careers program means better access to higher education.
                                             The College to Careers program, created under the Title V Osceola Grant, allows Valencia
                                             College to make specific infrastructure and program changes that will provide members
                                             of this historically underserved community increased access to, and opportunities
                                             for, Career and Technical Education (CTE) programs and certifications which lead directly
                                             to jobs.
                                          </p>
                                          
                                          
                                          
                                          <p><a class="button color1" href="../../../locations/osceola/college-to-careers/es/index.html" target="_self" alt="En español" title="En español">En español</a></p>
                                          
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:350px;">
                                 
                                 <div>  
                                    <h2>Building a Better Community</h2>
                                    
                                    <div>Valencia College has made a commitment to serve the unique needs of our Osceola County
                                       community. <a href="../../../locations/osceola/college-to-careers/welcome.html">Read the Welcome from the Osceola campus President...</a>
                                       
                                    </div>
                                    <br><div>
                                       <center> <iframe id="kaltura_player" src="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=0_ku6bwkf6&amp;flashvars%5BstreamerType%5D=auto&amp;flashvars%5BlocalizationCode%5D=en&amp;&amp;wid=1_2j4athyw" width="270" height="200" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" frameborder="0"></iframe>
                                          
                                       </center>
                                    </div> 
                                    <div>
                                       
                                       
                                    </div>
                                    
                                 </div> 
                              </div>
                              
                              
                           </div>
                           
                           
                           
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:350px;">
                                 
                                 <a class="button color1 narrow" href="../../../locations/osceola/college-to-careers/about.html" target="_self" alt="About Us" title="About Us">About Us</a>
                                 <a class="button color1 narrow" href="../../../locations/osceola/college-to-careers/goals.html" target="_self" alt="Goals" title="Goals">Goals</a>
                                 <a class="button color1 narrow" href="../../../locations/osceola/college-to-careers/career-technical-offerings.html" target="_self" alt="Career Technical Offerings" title="Career Technical Offerings">Career Technical Offerings</a>
                                 <a class="button color1 narrow" href="../../../locations/osceola/college-to-careers/technology.html" target="_self" alt="Technology" title="Technology">Technology</a>
                                 <a class="button color1 narrow" href="../../../locations/osceola/college-to-careers/building-pathways.html" target="_self" alt="Building Pathways" title="Building Pathways">Building Pathways</a>
                                 <a class="button color1 narrow" href="../../../locations/osceola/college-to-careers/partners.html" target="_self" alt="Partners" title="Partners">Partners</a>
                                 <a class="button color1 narrow" href="../../../locations/osceola/college-to-careers/news-and-updates.html" target="_self" alt="News and Updates" title="News and Updates">News and Updates</a>
                                 <a class="button color1 narrow" href="http://net4.valenciacollege.edu/forms/osceola/college-to-careers/join-us.cfm" target="_blank" alt="Join Us" title="Join Us">Join Us</a> <br>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div> 
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div> 
               
               
               
               
               
               <div class="clear">&nbsp;</div> 
               
               
               <div class="grid_12">&nbsp;</div>
               
               <div class="clear">&nbsp;</div> 
               
               
               
               
               
               
               
               
               
               <div id="containerBtm">
                  
                  
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/index.pcf">©</a>
      </div>
   </body>
</html>