<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/technology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>     
                           
                           
                           <h2>Technology</h2>
                           
                           <p>Technology is a significant part of how the Title V Osceola Grant will better enable
                              Valencia College's Osceola campus to serve Hispanic students. Through the College
                              to Careers technology upgrades, students will be offered an improved academic experience
                              even before they walk through Valencia's doors as well as once they are here. 
                           </p>
                           
                           <blockquote>
                              
                              
                              <div> <i aria-hidden="true"></i>
                                 
                              </div>
                              <strong>Constituent Relationship Management (CRM) system</strong><br>
                              Valencia students already become quite familiar with Atlas, our student communication
                              portal, as their source of information about registration, plans of study, and other
                              aspects of college life.  An addition to that program will allow prospective students
                              to become part of the college community even faster. The new Constituent Relationship
                              Management (CRM) system will allow the college to better track prospective students
                              and to guide them through our admissions and advising processes. Once they become
                              Valencia students the CRM and Atlas will help them stay on track toward graduation.
                              
                           </blockquote>
                           
                           
                           <blockquote>
                              
                              <div><i aria-hidden="true"></i></div>
                              <strong>Online collaborative learning solution</strong><br>
                              Imagine being able to interact with students on other campuses in a class that's happening
                              in real-time in both locations.
                              
                              
                              <p>New Synchronized Learning Classrooms will allow the Osceola campus to expand its course
                                 offerings by making it possible to share instructors across campuses in real-time,
                                 interactive, learning environments. The addition of new equipment to connect classrooms,
                                 made possible through the Title V Osceola Grant, will mean fewer class cancellations
                                 at the Osceola campus due to low enrollment. It will mean less travel and a greater
                                 chance of completion for Osceola campus students who might have previously been able
                                 to start a career and technical program at that campus but couldn't finish it there
                                 because of a lack of course offerings.
                                 
                              </p> 
                           </blockquote>
                           
                           
                           <blockquote>
                              
                              <div><i aria-hidden="true"></i></div>
                              <strong>Ad Astra - Scheduling software system</strong><br>Creating a more efficient campus is the goal of the final piece of technology made
                              possible by the College to Careers projects. Through Title V Osceola Grant funding,
                              the Osceola campus will be implementing use of the Ad Astra scheduling software system.
                              The software will allow campus staff to keep better track of technology and seating
                              capacity of rooms. This information will allow us to make better decisions related
                              to class scheduling and meeting space availability, ultimately enhancing the student
                              experience by allowing us to offer more classes at convenient times for students.
                              
                              
                           </blockquote>
                           
                           
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>For more info about the College to Careers</strong><br>
                                    <span>
                                       <strong>Title V Osceola campus grant, contact:</strong><br>         
                                       </span>
                                    </span></p>
                              
                              <p>
                                 <span>
                                    <strong>Dr. Jennifer Keefe, Grant Director</strong><br>
                                    <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                    <i></i> (407) 582-4823</span></p>
                              
                              
                              <p>
                                 <span>
                                    <strong>Dr. James McDonald, Osceola campus Dean of Career &amp; Technical Programs</strong><br>
                                    <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                    <i></i> (407) 582-4401<br>
                                    </span></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                              Directions</a>
                           <a href="../../../locations/map/osceola.html" target="_blank">
                              Campus Map</a>
                           
                           <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/technology.pcf">©</a>
      </div>
   </body>
</html>