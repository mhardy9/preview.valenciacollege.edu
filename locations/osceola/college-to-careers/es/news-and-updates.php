<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/es/news-and-updates.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li><a href="/locations/osceola/college-to-careers/es/">Es</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>     
                           
                           
                           <h2>Noticias y actualizaciones</h2>
                           
                           <h3>El programa de Residential  Property Management se Prepara para los Estudiantes*</h3>
                           
                           <p><img alt="stock image" height="327" src="../../../../locations/osceola/college-to-careers/es/iStock-546201678.jpg" width="490"></p>
                           
                           <p><br>
                              El programa de Residential Property Management AS Degree de Valencia College, el desarrollo
                              en la que fue financiado por el Departamento de Educación de Estados Unidos de la
                              beca del Título V de Osceola, ha tomado varios pasos hacia adelante para preparar
                              la bienvenida de estudiantes en otoño por primera vez.
                           </p>
                           
                           <ul>
                              
                              <li>
                                 <img alt="Eldon Warfield" height="159" src="../../../../locations/osceola/college-to-careers/es/Eldon.png" width="213">Veterano de la industria Eldon Warfield servirá como miembro de la facultad y Program
                                 Chair para el nuevo programa de Residential Property Management AS Degree que empezará
                                 este otoño. Eldon es el Presidente de IREM's West Coast Florida Chapter y tiene sobre
                                 30 años de experiencia de todos los niveles de la industria de Residential Property
                                 Management. Él ha trabajado, con el financiamiento de la beca del Título V, este verano
                                 para crear el Advisory Council del programa y para mercadear el programa a futuros
                                 estudiantes. 
                              </li>
                              
                              <li>El Avisory Council del programa, en la cual esta creado por expertos de la industria,
                                 incluyendo nuestros socios de National Apartment Association, Florida Apartment Association,
                                 Apartment Association of Great Orlando y un número de compañías administrativas de
                                 la Florida Central, se reunieron por primera vez el 10 de julio. El objetivo del grupo
                                 será guiar al programa a que la industria sea actual.
                              </li>
                              
                              <li>Durante julio, el personal de Valencia y nuestro nuevo miembro de facultad de Residential
                                 Property Management, Eldon Warfield, unirse con el equipo del Título V de Osceola
                                 para presentar dos webinars para trabajadores en Residential Property Management para
                                 explicar el nuevo programa y contestar preguntas antes que los futuros estudiantes
                                 apliquen en Valencia. Una reunión para estudiantes actuales de Valencia está programada
                                 para el 25 de julio.
                              </li>
                              
                           </ul>
                           
                           <p>*aprobación pendiente para SACSCOC&nbsp; </p>
                           
                           
                           <h3>Los Socios  de Hospitality Ayudan a Guiar al Programa a Crecer</h3>
                           
                           <p>                       <img alt="Hospitality" height="370" src="../../../../locations/osceola/college-to-careers/es/Hospitality55-for-Career-Book.jpg" width="250">Hemos buscado nuevos caminos para expandir nuestro programa de Hospitality en Valencia
                              College. El Campus de Osceola contactó a socios en las áreas de la industria de Hospitality
                              para buscar nuevos caminos para los futuros estudiantes. En el 10 de julio de 2017,
                              socios de Sea World, Reunion Resort, Marriott, Holiday Inn Vacation Clubs y más se
                              reunieron con el personal del programa de Hospitality del Campus de Osceola y West
                              para discutir nuevas ideas para el curso planificación y mercadeo del programa para
                              sus trabajadores. Los nuevos elementos van a tomar más tiempo en desarrollar, pero
                              revisen luego en nuestra página de Novedades y Actualizaciones para más información.
                              
                           </p>
                           
                           
                           <h3>La Configuración de Ad Astra está en Camino</h3>
                           
                           <p> Un proyecto está en camino para implementar un software que ayudará al Campus de
                              Osceola y al resto de los campus de Valencia para aumentar la capacidad de estudiantes
                              en la utilización de espacio. El proyecto se alinea con el Objetivo 3 de la beca del
                              Título V de Osceola y el financiamiento del Departamento de Educación de Estados Unidos.
                              El Campus de Osceola pudo comprar el Ad Astra Scheduler software para completar esta
                              tarea. El software fue comprado al principio de este año y la configuración empezó
                              en julio. En el 19 de julio y 20 de julio, los representantes de Ad Astra visitaron
                              el Campus de Osceola para conocer el equipo de configuración de la institución y para
                              hacer decisiones sobre como el software va a ser establecido. La tarea de implementación
                              continuará en el otoño y esperemos scheduling classes en el Scheduler para el verano
                              de 2018. 
                           </p>
                           
                           
                           <h3><strong>Próxima Parada: Transportation, Logistics y Supply Chain</strong></h3>
                           
                           <h4>Valencia se Mueve Hacia Adelante con la Exploración de los Programas de Transportación,
                              Logística y Distribución
                           </h4>
                           
                           
                           <p>El 14 de junio de 2017, expertos en los campos de Transportación, Logística, Cadena
                              de Suministro y Distribución se reunieron en Valencia College en el Campus de Osceola
                              para discutir lo que es necesario en la industria. Posiblemente Valencia College construya
                              un programa para entrenar a estudiantes y que reciban requisitos de la misma. Representantes
                              de Disney, Sea World, Rooms to Go, FreshPoint, HNM Global Logistics, Saddle Creek
                              Logistics y FedEx se reunieron con los representantes del Condado de Osceola, Port
                              Canaveral y el Departamento de Transportación de Florida. Ellos conversaron sobre
                              las destrezas necesaria en trabajos y educación para mantener la competencia en el
                              Condado de Florida en el mercado del siglo XXI para la transportación de bienes de
                              sus fabricantes a sus consumidores.
                           </p>
                           
                           
                           <p>El próximo paso será que el equipo de Valencia trabaje junto en determinar qué tipo
                              de programa será creado. Afortunadamente, muchos de los participantes expresan interés
                              en continuar ser parte del proceso. Quédese en sintonía para más información mientras
                              nos movemos hacia adelante en este proceso.
                           </p>
                           
                           
                           
                           
                           <h3>
                              <strong>El Nuevo Asociado en Ciencias en Property Management Encontró Hogar en el Condado
                                 de Osceola</strong> 
                           </h3>
                           
                           <p>El nuevo programa de Residential Property Management del Grado Asociado en Ciencias
                              de Valencia College pronto estará listo para aceptar estudiantes en el semestre de
                              otoño de 2017. Las 60 horas del programa incluirá clases en todos los aspectos en
                              Residential Property Management con el objetivo de preparar los graduados para una
                              carrera en áreas como apartment complex management. Los estudiantes se estarán preparando
                              durante el programa para presentarse al Certified Apartment Manager (CAM). Busque
                              los detalles y el enlace al sitio web del programa en los próximos meses. El desarrollo
                              del proyecto es manejado por la oficina de beca del Título V de Osceola incluyendo
                              el plan de estudio y mercadotecnia. 
                           </p>
                           
                           
                           <h3> <strong>Future Teacher Academy Crea Asociación Para la Comunidad</strong> 
                           </h3>
                           
                           <p><img alt="Valencia College" height="153" hspace="0" src="../../../../locations/osceola/college-to-careers/es/ValenciaCollege.png" vspace="0" width="153"> <img alt="School District of Osceola County" height="126" hspace="0" src="../../../../locations/osceola/college-to-careers/es/SchoolDistrictOfOsceolaCounty.png" vspace="0" width="126"> <img alt="University of Central Florida" height="226" hspace="0" src="../../../../locations/osceola/college-to-careers/es/University-of-Central-Florida-logo.png" vspace="0" width="144"></p>
                           
                           <p> Creando asociaciones a través de nuestra comunidad es importante para la beca del
                              Título V de Osceola y un nuevo programa está haciendo eso. Future Teacher Academy
                              empezó aceptando un piloto de un grupo de estudiantes en el Campus de Osceola en el
                              otoño de 2016 e ingresará nuevos estudiantes en el otoño de 2017. Los estudiantes
                              están uniéndose en camino al Grado de Asociado de Artes (AA) en Valencia y luego aplicarán
                              en admisiones para el programa de Educación Elementar de la Universidad de Florida
                              Central (University of Central Florida o UCF). Las clases de UCF van a ser ofrecidas
                              en el edificio de UCF localizado en Valencia Osceola Campus. El Distrito Escolar del
                              Condado de Osceola también se ha envuelto en el proyecto ofreciendo sustitución de
                              enseñanza para los estudiantes los lunes y viernes, mientras sus clases son ofrecidas
                              martes, miércoles y jueves. Los estudiantes tendrán acceso a trabajos en el sistema
                              de las Escuelas del Condado de Osceola. 
                           </p>
                           
                           
                           <h3>
                              <strong>Abundancia de Oportunidades en Articulación para Estudiantes de Osceola</strong> 
                           </h3>
                           
                           <p><img alt="Valencia College" height="153" hspace="0" src="../../../../locations/osceola/college-to-careers/es/ValenciaCollege_000.png" vspace="0" width="153"><img alt="TECO" border="1" height="160" hspace="0" src="../../../../locations/osceola/college-to-careers/es/TECO.png" vspace="0" width="160"></p>
                           
                           <p>Estamos satisfechos en anunciar la creación de nuevos contratos de articulación con
                              nuestros socios de la comunidad Technical Education Center Osceola (TECO) durante
                              el trabajo de la beca de Título V de Osceola. Estos contratos ayudan a educar los
                              estudiantes para obtener los requisitos de empleo en la comunidad. 
                           </p>
                           
                           <ul>
                              
                              <li>Estudiantes en el programa de Medical Laboratory Assisting en TECO pueden obtener
                                 4 créditos en el Grado Asociado de Ciencias del programa Biomedical Technology en
                                 Valencia.
                              </li>
                              
                              <li>Estudiantes en el programa de Cosmetology and Pharmacy Technician en TECO pueden obtener
                                 24-27 créditos en el Grado Asociado de Ciencias del programa Supervision for Management
                                 &amp; Industry en Valencia.
                              </li>
                              
                              <li>Al principio del 2018, estudiantes del programa de Medical Coder Biller en TECO que
                                 han obtenido la certificación de Certified Coder Associate (CCA) en AHIMA o Certified
                                 Professional Coder (CPC) en AAPC obtendrá 25 creditos en el Grado Asociado de Ciencias
                                 del programa Health Information Technology en Valencia.
                              </li>
                              
                           </ul>
                           
                           <p>Esperamos con astucia la creación del nuevo camino para los futuros estudiante de
                              TECO.
                              Un nuevo contrato de articulación para Academy of Construction Technologies (A.C.T.)
                              está en marcha. A.C.T. está trabajando con los estudiantes de la escuela superior
                              para entrenarlos al oficio relacionado a construcción. Estudiantes que completen el
                              programa de A.C.T. pueden obtener un internado de 2-4 créditos en el Grado Asociado
                              de Ciencias en el programa Building Construction Technology.
                           </p>
                           
                           
                           
                           <h3> <strong>Estudiantes de TECO Adquirieron New Student Experience</strong> 
                           </h3>
                           
                           <p><img alt="Tanisha Castor" border="1" height="214" hspace="0" src="../../../../locations/osceola/college-to-careers/es/Tanisha_OfficePic.jpg" vspace="0" width="215"></p>
                           
                           <p>La asociación de Valencia College y TECO está creciendo mientras que los estudiantes
                              de TECO ocupan la clase de New Student Experience (NSE) en Valencia. La profesora
                              Tanisha Castor de NSE está enseñando a Valencia a coordinar student experience el
                              primer año en TECO. La clase es requerida para casi todos los programas del Grado
                              Asociado de Ciencias en Valencia y los estudiantes de TECO pueden llevar su certificado
                              de industria a los créditos de la institución por nuestro contrato de articulación.
                              En el otoño de 2016, 22 estudiantes del Dual Enrollment (DE) del programa de TECO
                              Paths High School completaron la clase. __ estudiantes adicionales de (DE) y __ estudiantes
                              adultos de programas del certificado de TECO completaron las clases en la primavera
                              de 2017. Los estudiantes adultos le ofrecieron una extensión de matrícula y solamente
                              fueron requeridos a comprar los libros para las clases. Una nueva clase está formándose
                              para el otoño de 2017.
                              
                           </p>
                           
                           <p>La meta de la clase de NSE es enseñar técnicas para estudiar, aplicar la experiencia
                              de la institución a los estudiantes y proveer un camino de exploración para carreras.
                              La clase de 3 créditos hace que los certificados de TECO sean elegibles inmediatamente
                              cuando sean matriculados al nuevo semestre en Valencia. La clase ayudará a completar
                              la meta de la beca del Título V de Osceola para que más estudiantes sean conscientes
                              de las oportunidades del certificado de articulación y los ayuden a adquirir beneficios
                              de esas oportunidades.
                           </p>
                           
                           
                           
                           <h3> <strong>Nuevo Equipo Para Clases Proveerá Oportunidades</strong> 
                           </h3>
                           
                           <p><img alt="Synchronized Learning Classroom" border="1" height="250" hspace="0" src="../../../../locations/osceola/college-to-careers/es/EastStudent_DisplayPanel.jpg" vspace="0" width="400"></p>
                           
                           <p>En adición al proyecto de Blackboard Collaborate, la beca del Título V de Osceola
                              ha colaborado con Office of Instructional Technology (OIT) de Valencia y nuestro East
                              Campus para ayudar a pilotear a los estudiantes del Osceola Campus para adquirir acceso
                              a los programas de Grado Asociado de Ciencias en otros campus. Más de $111,000 en
                              fondos para la beca fue usado para ayudar a comprar equipos para el salón de clase
                              en Osceola Campus y East Campus con monitores pantalla táctil, cámaras y micrófonos.
                              Llamamos esta experiencia Synchronized Learning porque los dos salones son sincronizados
                              en la misma clase y hora. 
                           </p>
                           
                           <p>La clase piloto, Introduction to Paralegal Studies, fue ofrecida en la primavera de
                              2017. La instructora Wendy Toscano pudo ver, escuchar e interactuar con los estudiantes
                              de Osceola Campus desde su salón de clase en East Campus. Los estudiantes de Osceola
                              Campus le informaron a la Directora de la beca del Título V, Dra. Jennifer Keefe,
                              después del final de la clase que se sintieron conectados con la instructora y aprecian
                              la oportunidad de tomar su clase sin tener que ir a East Campus.
                           </p>
                           
                           <p>El segundo piloto para Introduction to Paralegal será este verano y hay planes para
                              incluir una nueva clase para el Synchronized Learning System en el otoño.
                           </p>
                           
                           
                           <h3>
                              <strong>Cambios en Supervisión del Grado Asociado de Ciencias Para Ayudar a Estudiantes </strong> 
                           </h3>
                           
                           <p>Valencia College está ayudando a los estudiantes para que puedan transferir créditos
                              a los programas certificados de nuestro Grado Asociado de Ciencias en Supervision
                              for Management and Industry. En el pasado, estudiantes que han transferido créditos
                              a través de la articulación de TECO solamente podrían ofrecerles una cantidad de créditos
                              que no se identificaba con las clases actuales del programa. Actualmente, esos créditos
                              aplican a los programas
                           </p>
                           
                           <p>En el proceso hemos cambiado el nombre del grado que antes se llamaba Industrial Management
                              Supervision. El nuevo nombre refleja el extenso campo del grado. Hemos añadido las
                              clases requeridas para el Grado Asociado de Ciencias de New Student Experience para
                              que los estudiantes adquieran en la institución. Los cambios impactan a los estudiantes
                              de TECO en los programas relacionados con la industria como Air Conditioning, Refrigeration
                              y Heating, Automotive Services Technology, Cosmetology, Prarmacy Technician y Electricity.
                              Ellos obtendrán 24-27 horas de crédito por obtener certificados de TECO y entrar a
                              Valencia College. 
                           </p>
                           
                           <p><a href="../../../../locations/osceola/college-to-careers/es/news-and-updates.html#top">TOP</a></p>
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>For more info about the College to Careers</strong><br>
                                    <span>
                                       <strong>Title V Osceola campus grant, contact:</strong><br>         
                                       </span>
                                    </span></p>
                              
                              <p>
                                 <span>
                                    <strong>Dr. Jennifer Keefe, Grant Director</strong><br>
                                    <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                    <i></i> (407) 582-4823</span></p>
                              
                              
                              <p>
                                 <span>
                                    <strong>Dr. James McDonald, Osceola campus Dean of Career &amp; Technical Programs</strong><br>
                                    <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                    <i></i> (407) 582-4401<br>
                                    </span></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                              Directions</a>
                           <a href="../../../../locations/map/osceola.html" target="_blank">
                              Campus Map</a>
                           
                           <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/es/news-and-updates.pcf">©</a>
      </div>
   </body>
</html>