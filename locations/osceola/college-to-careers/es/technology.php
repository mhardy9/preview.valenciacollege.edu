<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/es/technology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li><a href="/locations/osceola/college-to-careers/es/">Es</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>     
                           
                           
                           <h2>Tecnología</h2>
                           
                           <h3>College to Careers y la tecnología </h3>
                           
                           <p>La tecnología es una parte significativa de la manera en que la Beca del Título V
                              de Osceola le permitirá al campus de Osceola de Valencia College prestar un mejor
                              servicio a los estudiantes hispanos. A través de las actualizaciones tecnológicas
                              del programa College to Careers, se les ofrecerá a los alumnos una mejor experiencia
                              académica incluso antes de atravesar las puertas de Valencia, y también una vez que
                              estén aquí. 
                              
                           </p>
                           
                           <blockquote>
                              
                              
                              <div> <i aria-hidden="true"></i>
                                 
                              </div>
                              <strong>Ellucian Recruit - Sistema de gestión de relaciones con los miembros de la comunidad
                                 (Constituent Relationship Management - CRM)</strong><br>
                              Los estudiantes de Valencia ya están bastante familiarizados con Atlas, nuestro portal
                              de comunicación para estudiantes, como fuente de información sobre inscripciones,
                              planes de estudio y otros aspectos de la vida universitaria.  Una nueva incorporación
                              a ese programa les permitirá a los futuros estudiantes formar parte de la comunidad
                              de la institución aún más rápido. El nuevo sistema de gestión de relaciones con los
                              miembros de la comunidad (CRM, por sus siglas en inglés) le permitirá a la institución
                              llevar un mejor control de los futuros estudiantes y actuar como guía en cada paso
                              de nuestro proceso de admisión y asesoría. Una vez que se conviertan en estudiantes
                              de Valencia, el CRM y Atlas les ayudarán a mantenerse encaminados hacia su graduación.
                              
                           </blockquote>
                           
                           
                           <blockquote>
                              
                              <div><i aria-hidden="true"></i></div>
                              <strong>Solución de aprendizaje colaborativo en línea</strong><br>
                              Imagine poder interactuar con estudiantes de otros campus en una clase que tiene lugar
                              en tiempo real en ambos establecimientos. 
                              
                              
                              
                              <p>La herramienta le permitirá al campus de Osceola ampliar su oferta de cursos, haciendo
                                 posible el intercambio de instructores entre campus en entornos de aprendizaje sincronizados.
                                 La incorporación del software fue posible gracias a la Beca Tïtulo V de Osceola, significará
                                 un menor número de cancelaciones de clases en el campus de Osceola debido a bajos
                                 niveles de inscripción. Significará menor cantidad de viajes y una mayor oportunidad
                                 de completar los cursos para aquellos estudiantes del campus de Osceola que quizás
                                 anteriormente hayan podido comenzar un programa laboral y técnico en dicho campus,
                                 pero no pudieron terminarlo debido a la falta de ofertas de cursos.  
                                 
                              </p> 
                           </blockquote>
                           
                           
                           <blockquote>
                              
                              <div><i aria-hidden="true"></i></div>
                              <strong>Ad Astra - Sistema de software de programación de clases</strong><br>Crear un campus más eficiente es el objetivo de la más reciente incorporación tecnológica
                              que ha sido posibilitada por los proyectos del programa College to Careers. Mediante
                              el financiamiento de la Beca del Título V de Osceola, el campus de Osceola implementará
                              el uso del sistema de software de programación de clases Ad Astra. Este software le
                              permitirá al personal del campus llevar un mejor control de la capacidad tecnológica
                              y los asientos disponibles en los salones. Esta información nos permitirá tomar mejores
                              decisiones en relación con la programación de clases y la disponibilidad de espacio
                              para reuniones, mejorando en definitiva la experiencia del estudiante al permitirnos
                              ofrecer más clases en horarios convenientes para los estudiantes. 
                              
                              
                           </blockquote>
                           
                           
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>Para mas información College to Careers:</strong><br><strong> La Beca del Title V del campus de Osceola, contacte: 
                                       <strong></strong></strong></span>
                                 
                              </p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dra. Jennifer Keefe, Directora de la Beca del Título V<br>
                                             <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                             <i></i> (407) 582-4823</strong></span></strong></strong></p>
                              
                              
                              
                              <p>
                                 <strong><strong>
                                       </strong></strong></p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dr. James McDonald, Decano del Programa de Carrera y  Técnicos del campus de Osceola
                                             <br>
                                             <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                             <i></i> (407) 582-4401<br>
                                             </strong> </span></strong></strong></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <strong><strong><a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                                    Directions</a>
                                 <a href="../../../../locations/map/osceola.html" target="_blank">
                                    Campus Map</a>
                                 
                                 <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                                 
                                 </strong></strong>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/es/technology.pcf">©</a>
      </div>
   </body>
</html>