<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/es/welcome.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li><a href="/locations/osceola/college-to-careers/es/">Es</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>
                           
                           <h2>Bienvenido</h2>
                           
                           
                           
                           
                           <p><em>Queridos Miembros de la Comunidad</em>,
                              
                              
                           </p>
                           
                           <p>Como parte de la comunidad de la Florida Central, Valencia College ha hecho una promesa
                              de servir a las necesidades individuales de nuestra comunidad de Osceola County.
                           </p>
                           
                           
                           <p>Los $2.65 millones de la beca de Título V recibida por el Departamento de Educación
                              de los Estados Unidos nos dará la oportunidad de aumentar nuestros programas de Carreras
                              y Educación Técnica que ofrecen y proveen a los residentes de Osceola nuevas oportunidades
                              para completar una carrera universitaria o certificado de industria que conduzcan
                              a trabajos de alta paga en nuestra comunidad. También nos proveerá la capacidad de
                              tomar decisiones basadas en la exploración de datos que nos ayudarán a definir nuestros
                              siguientes pasos a seguir como institución.
                              Nos esforzamos para hacer lo mejor por nuestra región, pero no lo podemos hacer solos.
                           </p>
                           
                           
                           <p>Por favor acompáñenos en nuestra misión de convertir a el Condado de Osceola en un
                              mejor lugar para vivir y trabajar. Nos gustaría que mantuvieran informados a lo largo
                              de la duración de la beca y que compartan sus conocimientos de nuestra comunidad con
                              nosotros mediante nuestra página Únase a nosotros. Juntos, podemos crear cambios positivos.
                              
                           </p>
                           
                           Cordiales saludos,<br>
                           <strong>Dr. Kathleen Plinske</strong>
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>Para mas información College to Careers:</strong><br><strong> La Beca del Title V del campus de Osceola, contacte: 
                                       <strong></strong></strong></span>
                                 
                              </p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dra. Jennifer Keefe, Directora de la Beca del Título V<br>
                                             <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                             <i></i> (407) 582-4823</strong></span></strong></strong></p>
                              
                              
                              
                              <p>
                                 <strong><strong>
                                       </strong></strong></p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dr. James McDonald, Decano del Programa de Carrera y  Técnicos del campus de Osceola
                                             <br>
                                             <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                             <i></i> (407) 582-4401<br>
                                             </strong> </span></strong></strong></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <strong><strong><a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                                    Directions</a>
                                 <a href="../../../../locations/map/osceola.html" target="_blank">
                                    Campus Map</a>
                                 
                                 <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                                 
                                 </strong></strong>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/es/welcome.pcf">©</a>
      </div>
   </body>
</html>