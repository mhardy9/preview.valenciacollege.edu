<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/es/about.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li><a href="/locations/osceola/college-to-careers/es/">Es</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>
                           
                           <h2>Acerca de nosotros </h2>
                           
                           
                           <h3>Programa College to Careers</h3>
                           
                           <p>En palabras simples, el programa College to Careers significa mejor acceso a la educación
                              superior. Casi la mitad (49.7%*) de los residentes del Condado de Osceola son de ascendencia
                              hispana. El programa College to Careers, creado por la Beca  del Título V de Osceola,
                              le permite a Valencia College realizar cambios en la infraestructura y en los programas.
                              Estos cambios son para brindar a los miembros de esta comunidad, que han sido históricamente
                              menos atendidos, acceso y oportunidades hacia los programas y certificaciones de la
                              Carrera y Educación Técnica (Career and Technical Education, o CTE). En la cual, conducirán
                              directamente a un empleo. 
                           </p>
                           
                           
                           
                           
                           <h3>Acerca de nosotros</h3>
                           
                           <p>El título V es una disposición de la Ley Federal de Educación superior que ofrece
                              protección y promoción a las oportunidades educativas de los estudiantes hispanos
                              de todo el país. Con esta beca, Valencia College puede hacer uso de las más recientes
                              tecnologías de enseñanza y tendencias de la industria en nuestra región. Esto garantiza
                              que los graduados de nuestros programas de certificación y títulos intermedios tengan
                              las habilidades necesarias para embarcarse en una carrera, y estén preparados para
                              contribuir de manera positiva con la comunidad que todos compartimos.
                           </p>
                           
                           
                           
                           <h3>Declaración de misión</h3>
                           
                           <p>El programa College to Careers de la Beca del Título V del Campus de Osceola de Valencia
                              College participará estratégicamente en el desarrollo y crecimiento y el crecimiento
                              económico de la Florida Central. Este programa generará una fuerza laboral calificada
                              y educada para nuestra región a través del diseño y la implementación. El programa
                              también producirá nuevas y mejoradas para que los estudiantes menos atendidos obtengan
                              acceso a entrenamiento y educación.
                           </p>
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>Para mas información College to Careers:</strong><br><strong> La Beca del Title V del campus de Osceola, contacte: 
                                       <strong></strong></strong></span>
                                 
                              </p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dra. Jennifer Keefe, Directora de la Beca del Título V<br>
                                             <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                             <i></i> (407) 582-4823</strong></span></strong></strong></p>
                              
                              
                              
                              <p>
                                 <strong><strong>
                                       </strong></strong></p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dr. James McDonald, Decano del Programa de Carrera y  Técnicos del campus de Osceola
                                             <br>
                                             <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                             <i></i> (407) 582-4401<br>
                                             </strong> </span></strong></strong></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <strong><strong><a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                                    Directions</a>
                                 <a href="../../../../locations/map/osceola.html" target="_blank">
                                    Campus Map</a>
                                 
                                 <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                                 
                                 </strong></strong>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/es/about.pcf">©</a>
      </div>
   </body>
</html>