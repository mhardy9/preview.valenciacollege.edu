<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/es/by-the-numbers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li><a href="/locations/osceola/college-to-careers/es/">Es</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>
                           
                           <h2>En los números </h2>
                           
                           
                           <p><img alt="Poseedores de títulos de licenciatura" height="110" src="../../../../locations/osceola/college-to-careers/es/graph2-es.png" width="350"></p>
                           
                           <h3>&nbsp;</h3>
                           
                           <h3>&nbsp;</h3>
                           
                           <h3>&nbsp;</h3>
                           
                           <h3>&nbsp;</h3>
                           
                           <h3>Valencia College</h3>
                           
                           <p>Con un total de 68,351 inscripciones de la institución, Valencia College tiene la
                              fortuna de atraer estudiantes provenientes de diferentes entornos. Dicho esto, casi
                              el 33% del total de inscripciones de la institución es de origen latino. En el Campus
                              de Osceola, aproximadamente el 50% de los 19,595 estudiantes es de origen latino,
                              lo cual le ha otorgado al Campus de Osceola la designación como Institución al Servicio
                              de los Hispanos (Hispanic Serving Institution, o HSI) por parte del Departamento de
                              Educación de los Estados Unidos. 
                           </p>
                           
                           
                           
                           <h3>Condado de Osceola</h3>
                           
                           <p>El Condado de Osceola se encuentra en una posición única en la Florida Central debido
                              al tamaño de nuestra población latina. De hecho, casi el 50% del Condado de Osceola
                              es de origen latino. Esto se compara con tan solo un 23% del estado de Florida y un
                              16% a nivel nacional. Esos números en forma individual demuestran por qué en el Condado
                              de Osceola es necesario contar con información orientada hacia dichos residentes.
                           </p>
                           
                           
                           
                           
                           <p><img alt="Porcentaje de la población de 25 años o más de edad" height="110" src="../../../../locations/osceola/college-to-careers/es/graph1-es.png" width="350"></p>
                           
                           <p>Mejorar la vida de las personas que viven en el Condado de Osceola es uno de los principales
                              objetivos de la beca del Título V en el Campus de Osceola de Valencia College. Planeamos
                              ayudar a hacer realidad estas mejoras ofreciendo educación accesible en campos con
                              orientación laboral. Los números hablan por sí mismos. Cuando consideramos que tan
                              solo el 13% de los residentes de Osceola mayores de 25 años de edad poseen títulos
                              de licenciatura, en comparación con el 17% en Florida y el 18% a nivel nacional, se
                              hace muy real la necesidad de generar nuevas oportunidades para los residentes del
                              Condado de Osceola. Al proporcionar opciones para que las personas del Condado de
                              Osceola. Al proporcionar opciones para que las personas del Condado de Osceola puedan
                              completar los primeros dos años de una educación universitaria con una buena relación
                              de costo-efectivo, estamos generando la oportunidad de que puedan cursar los últimos
                              dos años de un título de licenciatura sin que eso les cueste muchísimo dinero.
                           </p>
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>Para mas información College to Careers:</strong><br><strong> La Beca del Title V del campus de Osceola, contacte: 
                                       <strong></strong></strong></span>
                                 
                              </p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dra. Jennifer Keefe, Directora de la Beca del Título V<br>
                                             <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                             <i></i> (407) 582-4823</strong></span></strong></strong></p>
                              
                              
                              
                              <p>
                                 <strong><strong>
                                       </strong></strong></p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dr. James McDonald, Decano del Programa de Carrera y  Técnicos del campus de Osceola
                                             <br>
                                             <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                             <i></i> (407) 582-4401<br>
                                             </strong> </span></strong></strong></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <strong><strong><a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                                    Directions</a>
                                 <a href="../../../../locations/map/osceola.html" target="_blank">
                                    Campus Map</a>
                                 
                                 <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                                 
                                 </strong></strong>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/es/by-the-numbers.pcf">©</a>
      </div>
   </body>
</html>