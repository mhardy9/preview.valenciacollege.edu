<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/es/building-pathways.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li><a href="/locations/osceola/college-to-careers/es/">Es</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>     
                           
                           
                           <h2>Construyendo trayectorias </h2>
                           
                           <p>La Beca del Título V de Osceola nos permitirá trabajar hacia el desarrollo de trayectorias
                              para los estudiantes, uno de los principales objetivos establecidos en el plan estratégico
                              de Valencia College. Ya que una de nuestras metas es generar conexiones para elevar
                              las aspiraciones personales de los estudiantes y permitirles alcanzar sus objetivos,
                              siempre estamos buscando maneras de eliminar los obstáculos entre los estudiantes
                              y la educación superior. La Beca del Título V de Osceola nos ayudará a:
                           </p>
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <p><i>filter_1</i> <em><strong>generar nuevas trayectorias de ayuda financiera para los estudiantes que ingresan
                                          a nuestros programas de carrera y técnica.</strong></em>    
                                    
                                 </p>
                                 
                                 <p><i>filter_3</i> <strong><em>encontrar maneras de aumentar el número de estudiantes que obtienen certificaciones
                                          de carreras y técnicas de Valencia en un 10% para septiembre del año 2020.</em></strong></p>
                                 
                                 
                                 <p><i>filter_5</i><strong><em>agregar dos nuevos programas de educación laboral y técnica (Career and Technical
                                          Education, o CTE).</em></strong></p>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <p><i>filter_2</i> <strong><em>rediseñar tres trayectorias existentes para que los estudiantes puedan obtener créditos/certificaciones
                                          a través de nuestros programas de CTE.</em></strong> 
                                 </p>
                                 
                                 <p><i>filter_4</i> <strong><em>encontrar maneras de aumentar el número de estudiantes que transfieren créditos a
                                          los programas de título intermedio de Valencia en un 10% para septiembre del año 2020.
                                          </em></strong></p>
                                 
                                 <p><i>filter_6</i> <strong><em>producir folletos y materiales de mercadeo en español.</em></strong></p>
                                 
                                 
                              </div>
                              
                           </div>  
                           
                           
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>Para mas información College to Careers:</strong><br><strong> La Beca del Title V del campus de Osceola, contacte: 
                                       <strong></strong></strong></span>
                                 
                              </p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dra. Jennifer Keefe, Directora de la Beca del Título V<br>
                                             <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                             <i></i> (407) 582-4823</strong></span></strong></strong></p>
                              
                              
                              
                              <p>
                                 <strong><strong>
                                       </strong></strong></p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dr. James McDonald, Decano del Programa de Carrera y  Técnicos del campus de Osceola
                                             <br>
                                             <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                             <i></i> (407) 582-4401<br>
                                             </strong> </span></strong></strong></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <strong><strong><a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                                    Directions</a>
                                 <a href="../../../../locations/map/osceola.html" target="_blank">
                                    Campus Map</a>
                                 
                                 <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                                 
                                 </strong></strong>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/es/building-pathways.pcf">©</a>
      </div>
   </body>
</html>