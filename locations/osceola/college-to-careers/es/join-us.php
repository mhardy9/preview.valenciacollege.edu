<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Title V Osceola - College to Careers | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/college-to-careers/es/join-us.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/college-to-careers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title V Osceola - College to Careers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/college-to-careers/">College To Careers</a></li>
               <li><a href="/locations/osceola/college-to-careers/es/">Es</a></li>
               <li>Title V Osceola - College to Careers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>
                           
                           <h2>Contact Us</h2>
                           
                           
                           
                           <p>The Title V Osceola Grant presents great opportunities for our community. As a partner,
                              you can join us to strengthen the local economy by building a job-ready workforce
                              armed with relevant, cutting edge skills. As a student, you can achieve a better future
                              for you and your family by completing a program that will put you on an immediate
                              career path and increase your earning potential.
                           </p>
                           
                           <p>Join us. Together, we'll redefine the ways Osceola County succeeds.<br><br> <strong><em>Please provide your information below so you can be contacted by the right area of
                                    the college to meet your needs.</em></strong></p>
                           
                           
                           
                           
                           
                           <form action="../../../../locations/includes/mailer2.html" enctype="multipart/form-data" method="post">
                              
                              
                              
                              <h3>Contact Information</h3> 
                              <input name="encrypted_mail" type="hidden" value="true">
                              <input name="sendmailto" type="hidden" value="PTYgOjpAWk5ATDpCTEk0VVYoTzgwUk8qLiI5MTU0VSpUVjk2PjQoKCAK">
                              
                              <input name="encrypted_copy" type="hidden" value="true">
                              <input name="sendcopyto" type="hidden" value="">
                              
                              
                              
                              
                              
                              <input name="copysubmitter" type="hidden" value="false">
                              <input name="subject" type="hidden" value="Join us Title V">
                              <input name="redirect" type="hidden" value="/osceola/college-to-careers/es/join-us.cfm">
                              <input name="boomerang_fields" type="hidden" value="">
                              <input name="processing_message" type="hidden" value="">
                              <input name="confirmation_email" type="hidden" value="">
                              <input name="file_fields" type="hidden" value="">
                              <input name="bonceOut" type="text" value="">
                              <input name="csvlocation" type="hidden" value="">
                              
                              
                              
                              <label for="First_Name">First Name</label>
                              <input name="First_Name" required="" type="text" value="">
                              
                              <label for="Last_Name">Last Name</label>
                              <input name="Last_Name" required="" type="text" value="">
                              
                              <label for="Email">Email</label>
                              <input name="Email" required="" type="email" value="">
                              
                              <input name="csvlocation" type="hidden" value="frame/sample.csv">
                              
                              <label for="interested">I'm interested in </label>
                              <select name="interested">
                                 
                                 <option value="learning">Learning more about Valencia/becoming a student</option>
                                 
                                 <option value="community">Becoming a community partner</option>
                                 
                                 <option value="focusgroup">Participating in an industry focus group</option>
                                 
                                 <option value="impact">Learning more about how College to Careers will impact my community</option></select>
                              
                              <label for="Comments">Comments</label>
                              <textarea name="Comments"></textarea>
                              
                              
                              <input type="submit" value="Submit">
                              
                           </form>    
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div title="Osceola  Title V  Image - Osceola  Title V  Image">
                           
                           <p><em>       Serving the Community</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div itemscope="" itemtype="place">
                              
                              <p>
                                 <span>
                                    <strong>Para mas información College to Careers:</strong><br><strong> La Beca del Title V del campus de Osceola, contacte: 
                                       <strong></strong></strong></span>
                                 
                              </p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dra. Jennifer Keefe, Directora de la Beca del Título V<br>
                                             <i></i> <a href="mailto:Jkeefe1@valenciacollege.edu">Jkeefe1@valenciacollege.edu</a><br>
                                             <i></i> (407) 582-4823</strong></span></strong></strong></p>
                              
                              
                              
                              <p>
                                 <strong><strong>
                                       </strong></strong></p>
                              
                              <p>
                                 <strong><strong><span>
                                          <strong>Dr. James McDonald, Decano del Programa de Carrera y  Técnicos del campus de Osceola
                                             <br>
                                             <i></i> <a href="mailto:Jmcdonald4@valenciacollege.edu">Jmcdonald4@valenciacollege.edu</a><br> 
                                             <i></i> (407) 582-4401<br>
                                             </strong> </span></strong></strong></p>            
                              
                           </div>
                           
                        </div>
                        
                        
                        <div> <strong><strong><a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                                    Directions</a>
                                 <a href="../../../../locations/map/osceola.html" target="_blank">
                                    Campus Map</a>
                                 
                                 <a data-image-height="200" data-image-width="245" data-inst="60307" data-link-type="image" data-platform="v" href="http://www.youvisit.com" title="Virtual Reality, Virtual Tour">Virtual Tour</a>
                                 
                                 </strong></strong>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/college-to-careers/es/join-us.pcf">©</a>
      </div>
   </body>
</html>