
<ul>
	<li class="submenu"><a class="show-submenu" href="/locations/osceola/">Osceola Home <span class="caret"></span></a>
		<ul>
			<li><a href="/locations/osceola/announcements.php">Announcements</a></li>
			<li><a href="/locations/osceola/locations-directory.php">Directory </a></li>
			<li><a href="/locations/osceola/events.php">Events</a></li>
			<li><a href="/locations/osceola/student-services.php">Student Services</a></li>
		</ul>
	</li>

	<li><a href="/locations/osceola/locations-plan/">Campus Plan</a></li>
	<li class="submenu"><a class="show-submenu" href="/locations/osceola/locations-president/">Campus President <span class="caret"></span></a>
		<ul>
			<li><a href="/locations/osceola/locations-president/OLT/OLTNotes.php">OLT Minutes</a></li>
				<li><a href="/locations/osceola/locations-president/minutes.php">Faculty &amp; Staff Meetings</a></li>
				<li><a href="/locations/osceola/locations-president/osceola_reflections.php">Osceola Campus Reflections</a></li>
				<li><a href="/locations/osceola/locations-president/TheLearningCenter.php">The Learning Center</a></li>
			</ul></li>
	<li><a href="/locations/osceola/future-teachers.php">Future Teachers Academy</a></li>
	<li><a href="/locations/osceola/student-services.php">Student Services</a></li>
	<li class="submenu"><a class="show-submenu" href="/locations/osceola/college-to-careers/">Title V Osceola <span class="caret"></span></a>
		<ul>
			<li><a href="/locations/osceola/college-to-careers/welcome.php">Welcome</a></li>
				 <li><a href="/locations/osceola/college-to-careers/about.php">About</a></li>
				 <li><a href="/locations/osceola/college-to-careers/by-the-numbers.php">By the Numbers</a></li>
				 <li><a href="/locations/osceola/college-to-careers/goals.php">Goals</a></li>
				 <li><a href="/locations/osceola/college-to-careers/career-technical-offerings.php">Career Technical Offerings</a></li>
				 <li><a href="/locations/osceola/college-to-careers/technology.php">Technology</a></li>
				<li> <a href="/locations/osceola/college-to-careers/building-pathways.php">Building Pathways </a></li>
			     <li><a href="/locations/osceola/college-to-careers/partners.php">Partners</a></li>
				 <li><a href="/locations/osceola/college-to-careers/news-and-updates.php">News and Updates</a></li>
				 <li><a href="http://net4.valenciacollege.edu/forms/osceola/college-to-careers/join-us.cfm" target="_blank">Join Us</a></li>
			</ul></li>
	<li><a href="/locations/osceola/ucf-at-osc.php">UCF at Osceola</a></li>
</ul>

