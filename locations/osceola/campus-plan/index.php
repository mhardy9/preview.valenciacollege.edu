<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Plan | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/campus-plan/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Plan</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li>Campus Plan</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/locations-plan/index.html"></a>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <span><strong>Osceola •&nbsp;Lake Nona  •&nbsp;Poinciana Campuses</strong><br>
                                       <strong>2016-2020 Campus Plan</strong></span>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <p><strong>Introduction</strong></p>
                        The Osceola, Lake Nona, and Poinciana Campuses are vibrant communities of learning
                        characterized by a welcoming culture that embraces the diversity of all students,
                        faculty, and staff.  We strive to make all students, faculty, staff, and community
                        members feel at home on campus, regardless of ethnicity, sexual orientation, religious
                        affiliation, native language, or immigration status, and we intend to create a culture
                        such that our campuses feel like ï¿½community campusesï¿½ rather than ï¿½commuter
                        campuses.ï¿½
                        <p>
                           
                           We believe that an environment characterized by inclusive excellence creates the strongest
                           conditions to support student learning, including developing more resilient learners
                           who persevere to overcome challenges.  We have identified a number of significant
                           obstacles that face students from our communities, and we will develop strategies
                           to increase their success while maintaining our high academic standards and rigorous
                           curriculum.  In addition, we have determined a need for additional career pathways
                           to create educational and economic opportunities for residents in the areas served
                           by the Osceola, Lake Nona, and Poinciana Campuses.  Finally, we commit to continuing
                           to work with our communities to raise awareness of and aspirations for higher education,
                           including helping to eliminate barriers that may prevent students from attending college.
                        </p>
                        
                        <p>
                           
                           Developed during the 2015-16 academic year with input from more than 300 faculty,
                           staff, and students, our 2016-2020 Campus Plan does not attempt to describe all of
                           our important day-to-day operational efforts over the next three to five years; rather,
                           it identifies areas of strategic focus for the Osceola, Lake Nona, and Poinciana Campuses
                           based on our studentsï¿½ and communitiesï¿½ unique needs at this point in time.  A
                           multidisciplinary work team that will define a working theory and develop a work plan
                           to lead us to successful achievement of the objective will be established for each
                           of the objectives identified in our Campus Plan.  The Lake Nona and Poinciana Campuses,
                           as well as each of the divisions at the Osceola Campus, will create area campus and
                           division goals that support the objectives of our Campus Plan.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <span><strong>Goal #1: Elevate Student Success</strong><br>
                                       The Osceola, Lake Nona, and Poinciana Campuses will work rigorously to identify, create,
                                       assess, and improve the conditions that best support student learning.</span>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-1-1.html">Objective 1.1</a>: By 2018, the Osceola, Lake Nona, and Poinciana Campuses will identify and implement
                           strategies to support students who are second-language speakers of English and improve
                           their success as measured by retention, course completion, and degree completion.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-1-2.html">Objective 1.2</a>: By 2018, the classroom technology and facilities at the Osceola, Lake Nona, and
                           Poinciana Campuses will be consistently reliable and dependable so that every class
                           minute is a learning minute.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-1-3.html">Objective 1.3</a>: By 2019, the Osceola, Lake Nona, and Poinciana Campuses will develop and offer thoughtfully
                           scheduled ï¿½course packagesï¿½ that allow students to complete degree programs with
                           a guaranteed schedule, allowing them to effectively plan for work obligations, childcare
                           needs, and public transportation.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-1-4.html">Objective 1.4</a>: By 2020, the Osceola and Lake Nona Campuses will rigorously evaluate the effectiveness
                           of learning support strategies (including tutoring, supplemental learning, and cohort
                           programs) and expand the availability of those that most effectively support student
                           success, resulting in a 10% increase in the proportion of students who successfully
                           complete the 20 highest-enrolled courses.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <span><strong>Goal #2: Enhance Program Pathways</strong><br>
                                       The Osceola, Lake Nona, and Poinciana Campuses will offer a robust mix of degree,
                                       certificate, and continuing education programs that are relevant and responsive to
                                       our communitiesï¿½ needs.</span>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-2-1.html">Objective 2.1</a>: By 2018, the Osceola, Lake Nona, and Poinciana Campuses will support the growth
                           of the Intensive English Program so that opportunities to learn English are made available
                           to a larger audience in our communities, enrolling more than 2,000 students per year.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-2-2.html">Objective 2.2</a>: By 2018, the Osceola, Lake Nona, and Poinciana Campuses will implement technology
                           that makes it possible for students at the three campuses to complete a wider variety
                           of programs without the need to travel to other campus locations.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-2-3.html">Objective 2.3</a>: By 2020, the Osceola, Lake Nona, and Poinciana Campuses will offer at least five
                           Career and Technical Education (CTE) pathways leading to industry certifications,
                           technical certificates, or degrees that were not previously available at the campuses,
                           as well as redesign at least three CTE program pathways.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-2-4.html">Objective 2.4</a>: By 2020, the Osceola Campus will design and offer programming that is responsive
                           to adultsï¿½ career development and professional advancement needs, resulting in a
                           10% increase in the number of students enrolled over the age of 25.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <span><strong>Goal #3: Expand Access</strong><br>
                                       The Osceola, Lake Nona, and Poinciana Campuses will make focused and concentrated
                                       efforts to reach populations of potential students who are currently underserved or
                                       not yet served by Valencia College so that all members of our communities who aspire
                                       to a higher level of education know they are welcome at Valencia.</span>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-3-1.html">Objective 3.1</a>: By 2017, the Osceola, Lake Nona, and Poinciana Campuses will make information about
                           all of the programs that can be completed at the three campuses easily accessible
                           to the public, in English and Spanish, including admissions requirements, average
                           program cost, and recommended schedule to completion.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-3-2.html">Objective 3.2</a>: By 2018, the Osceola, Lake Nona, and Poinciana Campuses will provide all first-time-at-Valencia
                           students a personalized welcome that includes at least one invitation to a campus
                           activity related to their meta-major before or during their first semester of enrollment.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-3-3.html">Objective 3.3</a>: By 2019, the Osceola, Lake Nona, and Poinciana Campuses will offer community outreach
                           events, on- and off-campus, that will be attended by more than 1,000 community members
                           (potential and current students and their families) each year that provide information
                           about and personalized assistance with admissions, enrollment, scholarships, financial
                           aid, and academic programs.
                        </p>
                        
                        <p>
                           
                           <a href="../../../locations/osceola/locations-plan/objective-3-4.html">Objective 3.4</a>: By 2020, Osceola Countyï¿½s college-going rate will exceed 50%, and by 2021, the
                           college-going rate of Poinciana high schools will exceed 53%.
                           
                           
                           
                        </p>
                        
                        <h2>&nbsp;</h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/campus-plan/index.pcf">©</a>
      </div>
   </body>
</html>