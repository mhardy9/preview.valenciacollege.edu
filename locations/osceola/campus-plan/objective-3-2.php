<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Plan | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/campus-plan/objective-3-2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Plan</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/campus-plan/">Campus Plan</a></li>
               <li>Campus Plan</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/locations-plan/index.html"></a>
                        
                        
                        
                        <h2>Objective 3.2 - <em>By 2018, the Osceola, Lake Nona, and Poinciana Campuses will provide all first-time-at-Valencia
                              students a personalized welcome that includes at least one invitation to a campus
                              activity related to their meta-major before or during their first semester of enrollment.
                              </em>
                           
                        </h2>
                        
                        <h3></h3>
                        
                        <h3><strong>Team Sponsor</strong></h3>
                        
                        <p>Liz Suarez</p>
                        
                        <h3></h3>
                        
                        <h3>Team Members</h3>
                        
                        <p>AJ Kirby</p>
                        
                        <p>Laura Browning&nbsp;</p>
                        
                        <p>Marlene&nbsp;Temes&nbsp;</p>
                        
                        <p>Rick Dexter</p>
                        
                        <p>Maritza Gaviria</p>
                        
                        <p>Chris Klinger&nbsp; </p>
                        
                        <p>Andrea Foley</p>
                        
                        
                        <h3>Work Plan</h3>
                        
                        <p><u>Charge and Work Products </u><br>
                           <br>
                           The working team should produce a general plan and ideas to accomplish the aforementioned
                           objective. The general plan should include ideas, recommendations, and a framework
                           to be implemented by the spring term 2018. <br>
                           <br>
                           <u>Known Constraints, Criteria, or Design Principles </u><br>
                           <br>
                           Budget and logistical constraints are foreseeable with the implementation of this
                           objective, which will be discussed after the sponsor reviews the initial draft created
                           by the work team. It is beneficial to look at current programs, services, and other
                           implemented past/cancelled programs serving this objective that could be taken into
                           scale or revised to minimize budget and logistics impact. <br>
                           <br>
                           <u>Relevant Strategic Goals </u><br>
                           <br>
                           The Valencia’s strategic goals served by this work are:<br>
                           a. Build Pathways <br>
                           b. Invest in Each Other <br>
                           
                        </p>
                        
                        <h3>Updates</h3>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/campus-plan/objective-3-2.pcf">©</a>
      </div>
   </body>
</html>