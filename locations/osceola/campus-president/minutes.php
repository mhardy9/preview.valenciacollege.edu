<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/campus-president/minutes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/campus-president/">Campus President</a></li>
               <li>Osceola Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href=""></a>
                        
                        
                        
                        
                        
                        
                        <h2>Faculty and Staff Meetings </h2>
                        
                        <h2>&nbsp; 
                           
                        </h2>
                        
                        <h2>Meeting 
                           Minutes 
                        </h2>
                        
                        <ul>
                           
                           <li><a href="../../../locations/osceola/locations-president/Documents/Faculty_Staff_1_13_09_Minutes.doc">01-13-09</a></li>
                           
                           <li><a href="../../../locations/osceola/locations-president/Documents/F_S_2_10_09_Minutes.doc">02-10-09</a></li>
                           
                           <li>
                              <a href="../../../locations/osceola/locations-president/Documents/FS_5_12_09_Minutes.pdf">02-12-09</a> | <a href="../../../locations/osceola/locations-president/Documents/FreshAttAddendum09.pdf">Freshman Addendum 09</a> | <a href="../../../locations/osceola/locations-president/Documents/FreshmanAttitudes09.pdf">Freshman Attitudes 09</a> | <a href="../../../locations/osceola/locations-president/Documents/StudentAcademicDisputePolicy.pdf">Student Academic Disupute Policy</a> 
                           </li>
                           
                           <li>
                              <a href="../../../locations/osceola/locations-president/Documents/OsceolaCampusFacultyStaff10-13-09Minutes.doc">10-13-09</a> | <a href="http://valenciacollege.edu/osceola/locations-president/Documents/NewStudentDataSlides-CLC.pptx">New Student Data Slides - CLC</a> |
                           </li>
                           
                           <li><a href="../../../locations/osceola/locations-president/Documents/OsceolaCampusFacultyStaff04-13-10Minutes.doc">04-13-10</a></li>
                           
                           <li>
                              <a href="../../../locations/osceola/locations-president/Documents/OsceolaCampusFacultyStaff09_21_2010_Minutes.doc">09-21-10</a> | <a href="../../../locations/osceola/locations-president/Documents/SpeakerSeries.pdf">Leadership Speaker Series</a>
                              
                           </li>
                           
                           <li><a href="../../../locations/osceola/locations-president/Documents/OsceolaCampusFacultyStaff10-19-2010Minutes.doc">10-19-10</a></li>
                           
                           <li><a href="../../../locations/osceola/locations-president/Documents/OsceolaCampusFacultyStaff11-16-2010Minutes.doc">11-16-10</a></li>
                           
                           <li>
                              <a href="../../../locations/osceola/locations-president/Documents/OsceolaCampusFacultyStaff1-18-2011Minutes.doc">01-18-11</a> | <a href="../../../locations/osceola/locations-president/Documents/SGA_student_survey.pdf">SGA Student Survey</a> | <a href="../../../locations/osceola/locations-president/Documents/Speaker_Series.pdf">Student Development Speaker Series</a> | <a href="../../../locations/osceola/locations-president/Documents/Remembering_MLK_JR.pdf">Remembering MLK Jr. </a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/campus-president/minutes.pcf">©</a>
      </div>
   </body>
</html>