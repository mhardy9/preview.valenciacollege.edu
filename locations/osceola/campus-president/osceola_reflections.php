<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/campus-president/osceola_reflections.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/campus-president/">Campus President</a></li>
               <li>Osceola Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href=""></a>
                        
                        
                        
                        <p><font size="1"><i>Dated:&nbsp;
                                 November 21, 2003</i></font></p>
                        
                        <h2>Our Principles:
                           
                        </h2>
                        
                        <ul>
                           
                           <li>
                              
                              <p>To
                                 focus on the next five years
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>To
                                 be realistic and to present short range goals.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>To
                                 be honest
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>To
                                 plan and/or reflect on how to make students successful.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                        </ul>
                        
                        <h2>Our Legacy:</h2>
                        
                        <ul>
                           
                           <li>
                              
                              <p>Formed
                                 a partnership with UCF: The CORE Program.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Brought
                                 the Nursing Program to Osceola Campus.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Offered
                                 more variety and number of courses.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Expanded
                                 facilities and resources: LRC, Smart Classroom, Building Two, Ground
                                 Breaking of Building Three, Clock Tower, Entrance to 192, 
                                 <place>
                                    
                                    <placename>
                                       Fitness
                                    </placename>
                                    
                                    <placetype>
                                       Center
                                    </placetype>
                                    
                                 </place>
                                 , etc.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Great
                                 faculty involvement: Faculty to Faculty, Destinations, Focus on the
                                 Workplace.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>ATLAS,
                                 Banner and GroupWise successfully implemented. 
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Greater
                                 community involvement through boards and other programs.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Developed
                                 a good image in the community. 
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Membership
                                 in more college wide committees and councils.
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <h2>Our Mission:</h2>
                        
                        <p>We
                           need to be explicit that our main job is to fulfill 
                           <country-region>
                              
                              <place>
                                 Valencia
                              </place>
                              
                           </country-region>
                           's overall mission but it is also clear that we have a special role in
                           this community.&nbsp; Aspects of
                           that role include:
                        </p>
                        <p>
                           
                        </p>
                        
                        
                        <ul>
                           
                           <li>
                              
                              <p>To
                                 create broader and better educational opportunities
                              </p>
                              <p>
                                 &nbsp;
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>To
                                 help create a better economic environment
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>To
                                 serve as an important cultural resource for the community
                                 
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p>In
                           terms of our distinctive role within the college as a whole, Osceola
                           Campus has an
                        </p>
                        <p>
                           
                        </p>
                        
                        
                        <p>opportunity
                           to serve as:
                        </p>
                        <p>
                           
                        </p>
                        
                        
                        <ul>
                           
                           <li>
                              
                              <p>A
                                 campus which provides diverse learning opportunities to a diverse
                                 population.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>A
                                 laboratory for testing new ideas and practices.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>A campus that measures its success by how well its
                                 students learn.
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <h2>Our Goals and Objectives:</h2>
                        
                        <ul>
                           
                           <li>
                              
                              <p>
                                 <place>Open
                                    
                                    <placetype>
                                       Building
                                    </placetype>
                                    
                                 </place>
                                 3
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Design, plan and assist in finding the funding
                                 for our future LRC building.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Do a better forecasting of student enrollment at
                                 the campus.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Explore and enhance our partnerships with higher
                                 education entities.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Expand the A.S. degree programs.</p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Expand and enhance the quality of our science
                                 labs.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Expand the Honors Program at the Osceola Campus.</p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Expand resources available to faculty, staff and
                                 students in the LRC.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Provide more specific and effective academic
                                 leadership at the campus (hire a Dean).
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Provide users with the necessary staff
                                 development to maximize the utilization of the technology available at
                                 the campus.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Offer to the community at least one cultural
                                 event in the next year.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Asses the Math exit exam as a predictor of
                                 success in the next level of Math.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Create a better assessment instrument(s) for ENC
                                 1101.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Develop and assess (direct student learning)
                                 hybrid learning program.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Bring courses to make accessible baccalaureate
                                 degrees in Osceola County/Campus.
                              </p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Expand our offerings within the AA degree.</p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Offer faculty development opportunities (F2F).</p>
                              <p>
                                 
                              </p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p>Create articulated and accessible programs
                                 leading to degrees beyond the AA.
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <h2>Osceola Campus Members Present:</h2>
                        
                        <ul>
                           
                           <li>
                              
                              <p>Marie Brady</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Stephanie Freuler</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Tim Grogan</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Donna Haskins</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Chris Klinger</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Jenni Campbell </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Linda Swaine</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Silvia Zapico</p>
                              
                           </li>
                           
                        </ul>
                        
                        <p></p>
                        <p>
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/campus-president/osceola_reflections.pcf">©</a>
      </div>
   </body>
</html>