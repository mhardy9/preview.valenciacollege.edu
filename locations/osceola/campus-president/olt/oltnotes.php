<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/campus-president/olt/oltnotes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/campus-president/">Campus President</a></li>
               <li><a href="/locations/osceola/campus-president/olt/">Olt</a></li>
               <li>Osceola Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           <a name="content" id="content"></a>
                           <a href=""></a>
                           
                        </div>
                        
                        
                        
                        
                        <h2> OLT Minutes</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_March_22_06.pdf" target="_blank">March
                                             22, 2006</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_July_26_06.pdf" target="_blank">July
                                             26, 2006</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_Aug_9_06.pdf" target="_blank">August
                                             9, 2006</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_Sept_13_06.pdf" target="_blank">September
                                             13, 2006</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_Oct_04_06.pdf">October 4, 2006</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_Jan_17_07.pdf">January 17, 2007</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>
                                          <a href="../../../../locations/osceola/locations-president/olt/documents/OLT_Jan_31_07.pdf">January 31, 2007</a>
                                          
                                          <ul>
                                             
                                             <li><a href="../../../../locations/osceola/locations-president/olt/OLT1312007Preso.htm">Assessment Office Presentation</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_March_8_07.pdf">March 8, 2007 (smoking
                                             policy)</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_April_18_07.pdf">April 18, 2007</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_May_16_07.pdf">May 16, 2007</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_July_25_07.pdf">July 25, 2007</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLT_Nov_14_07.pdf">November
                                             14, 2007</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OLTAgenda22708.pdf">February 27, 2008 </a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><a href="../../../../locations/osceola/locations-president/olt/documents/OsceolaLeadershipTeamMeetingMinutes.doc">January 28, 2009 </a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/campus-president/olt/oltnotes.pcf">©</a>
      </div>
   </body>
</html>