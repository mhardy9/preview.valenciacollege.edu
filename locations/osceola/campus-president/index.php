<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/campus-president/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li>Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           <a name="content" id="content"></a>
                           <a href=""></a>
                           
                           <p><img alt="plinske" height="420" src="../../../locations/osceola/locations-president/plinske.jpg" width="300">
                              
                              
                              
                           </p>
                           
                        </div>
                        
                        <h2>
                           <strong>Kathleen A. Plinske, Ed.D., </strong>Campus President, Osceola and Lake Nona Campuses
                        </h2>
                        
                        
                        <p>Dr. Kathleen A. Plinske serves as the Campus President of the Osceola and Lake Nona
                           Campuses at Valencia College. Actively involved in the community, Plinske serves as
                           Chair of the Education Foundation of Osceola County, Past President of the Rotary
                           Club of Lake Nona, Chair of the Osceola Poverty Elimination Network, and member of
                           the Board of the Osceola Center for the Arts, Junior Achievement of Osceola County,
                           Community Vision, the Kissimmee/Osceola Chamber of Commerce, the Lake Nona Education
                           Council, and the Mexican Consulate's Comité de Educación del Centro y Norte de Florida.
                        </p>
                        
                        
                        <p>A graduate of the Illinois Mathematics and Science Academy, Plinske attended Indiana
                           University-Bloomington as a Herman B Wells Scholar, earning a Bachelor of Arts in
                           Spanish and Physics with highest distinction and honors. A member of Phi Beta Kappa,
                           she completed a Master of Arts in Spanish from Roosevelt University, a Doctorate in
                           Educational Technology from Pepperdine University, and a Master of Business Administration
                           from the University of Florida. Plinske has continued her studies of Spanish language
                           and culture at Instituto Tecnológico y de Estudios Superiores de Monterrey, México,
                           at the Don Quijote Escuela de Español in Granada, España, at Middlebury Language Schools
                           in Middlebury, Vermont, and through her travels to Ecuador, México, Perú, Puerto Rico,
                           and the Dominican Republic.
                        </p>
                        
                        
                        <p>Plinske was recognized as one of 24 emerging leaders in the world by Phi Delta Kappa
                           International in 2010, was named 2012 Woman of the Year by the Orlando Business Journal
                           in their 40 Under 40 competition, was named as the 2012 Outstanding Young Alumnus
                           by Indiana University, and was selected for the 2013 Distinguished Alumni Leadership
                           Award by the Illinois Mathematics and Science Academy.
                        </p>
                        
                        <p><br>
                           
                        </p>         
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/campus-president/index.pcf">©</a>
      </div>
   </body>
</html>