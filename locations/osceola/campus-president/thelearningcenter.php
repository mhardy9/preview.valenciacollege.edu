<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus President | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/campus-president/thelearningcenter.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus President</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li><a href="/locations/osceola/campus-president/">Campus President</a></li>
               <li>Osceola Campus President</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href=""></a>
                        
                        
                        
                        <font size="1"><i>Dated:&nbsp; February 2004</i></font>
                        
                        
                        <p>In the early part of February, several members of the Osceola campus
                           met to discuss plans for the 
                           <place>
                              
                              <placename>
                                 Learning
                              </placename>
                              
                              <placetype>
                                 Center
                              </placetype>
                              
                           </place>
                           to be housed in building three.The
                           stakeholders present represented a wide range of backgrounds including
                           deans, the LRC Director, student services, several instructional
                           assistants, and the Faculty Association President.The goal of the meeting was to create
                           a vision for what the
                           learning center would contain.We
                           did not intend to "design" the learning center.That is, we only brainstormed how we
                           wanted the center to serve the
                           learning community.For that
                           reason, we began all statements with "A place where students will."This was only the
                           first step in creating a dynamic learning center.
                           
                        </p>
                        
                        <h2>The 
                           <place>
                              
                              <placename>
                                 Learning
                              </placename>
                              
                              <placetype>
                                 Center
                              </placetype>
                              
                           </place>
                           will be:
                        </h2>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h4>A
                                       place where students will learn skills such as:
                                    </h4>          
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Burn CDs&nbsp;</li>
                                       
                                       <li>Use electronic portfolios&nbsp;</li>
                                       
                                       <li>Open, Close, Save files&nbsp;</li>
                                       
                                       <li>Use a scanner&nbsp;</li>
                                       
                                       <li>Basic Wordprocessing&nbsp;</li>
                                       
                                       <li>Design a website</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Create a PowerPoint presentation&nbsp;</li>
                                       
                                       <li>Use the internet (Basic internet research)&nbsp;</li>
                                       
                                       <li>Use&nbsp; Smartboard, elmo, digital tablet&nbsp;</li>
                                       
                                       <li>Use Atlas account&nbsp;</li>
                                       
                                       <li>Use WebCT</li>
                                       
                                       <li>Using LifeMap Tools</li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h4>A
                                       place where students will get assistance with course content tools
                                       such as:
                                    </h4>          
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Peer learning groups</li>
                                       
                                       <li>Research skills</li>
                                       
                                       <li>Career software tools</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Student tutors or other instructional
                                          assistance<br>
                                          (drop in content help)
                                       </li>
                                       
                                       <li>
                                          Possible faculty office hours
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h4>A
                                       place where students will create a LifeMap including:
                                    </h4>          
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Career development</li>
                                       
                                       <li>Study skills</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Personality/Values, etc regarding careers (i.e.MBTI)
                                          
                                       </li>
                                       
                                       <li>
                                          Life skills
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h4>A
                                       place where...
                                    </h4>          
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Students will complete assignments for their classes (includes printing)</li>
                                       
                                       <li>Students can choose to learn individually, in pairs, in groups, etc
                                          
                                       </li>
                                       
                                       <li>
                                          Students will have access to specialized software and equipment
                                          
                                       </li>
                                       
                                       <li>
                                          Faculty can bring their classes for specialized instruction
                                          
                                       </li>
                                       
                                       <li>Students can learn life skills</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/campus-president/thelearningcenter.pcf">©</a>
      </div>
   </body>
</html>