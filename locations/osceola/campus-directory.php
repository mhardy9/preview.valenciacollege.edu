<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/locations/osceola/campus-directory.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/locations/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Campus</a></li>
               <li><a href="/locations/osceola/">Osceola</a></li>
               <li>Osceola Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../locations/osceola/index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Campus Directory</h2>
                        
                        <p> 1800 Denn John Lane<br>
                           Kissimmee, FL 34744
                        </p>
                        
                        <p>
                           Telephone:  407-299-5000<br>
                           
                        </p>
                        
                        <p>
                           <a href="http://maps.yahoo.com/py/ddResults.py?Pyt=Tmap&amp;ed=w9FuE.V.winMkTjw5HNdhwfBAgFkJd9cVtommoH3b_pPIEG4p2C4GiaLDSzDhoPDZnb0_IGS5DTdgw--&amp;newcsz=&amp;newcountry=us&amp;newtcsz=Kissimmee,+FL+34744-3714&amp;newtcountry=us" target="_blank">Driving Directions </a></p>
                        
                        
                        <p><a href="../../locations/map/osceola.html">Campus Map</a></p>
                        
                        <p>We are here to help! </p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <a name="b1" id="b1"></a>Building 1 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Academic Department Offices for Communication &amp; Languages              </div>
                                 
                                 <div>Rm 141</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Auditorium</div>
                                 
                                 <div>Rm 101</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>EMT/Paramedic Program</div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Fine and Applied Arts</div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Graphics Lab </div>
                                 
                                 <div>Rm 246</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Academic Systems (Math) Labs </div>
                                 
                                 <div>Rm 142, 144</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <a name="b2" id="b2"></a>Building 2 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Admissions &amp; Records </div>
                                 
                                 <div>Rm 104 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Administration </div>
                                 
                                 <div>Rm 252</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Advising Center </div>
                                 
                                 <div>Rm 140 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Answer Center</div>
                                 
                                 <div>Rm 105</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Atlas Lab</div>
                                 
                                 <div>Rm 105G </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Business Office</div>
                                 
                                 <div>Rm 110</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Career Center </div>
                                 
                                 <div>Rm 125 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Financial Aid</div>
                                 
                                 <div>Rm 140</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Internship &amp; Workforce Services </div>
                                 
                                 <div>Rm 125</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>New Student Orientation </div>
                                 
                                 <div>Rm 171 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Security</div>
                                 
                                 <div>Rm 109 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Student Development</div>
                                 
                                 <div>Rm 150</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Student Services (including international students and veterans) </div>
                                 
                                 <div>Rm 140</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Students with Disabilities</div>
                                 
                                 <div>Rm 102 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Transitions Planning &amp; Bridges </div>
                                 
                                 <div>Rm 103 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <a name="b3" id="b3"></a>Building 3
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Academic Department Offices for Career &amp; Technical Programs and Humanities &amp; Social
                                    Sciences
                                 </div>
                                 
                                 <div>Rm 319</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Career Program Advisor</div>
                                 
                                 <div>Rm 319</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Information Technology Labs</div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Nursing</div>
                                 
                                 <div>Rm 200 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>The Language Lab </div>
                                 
                                 <div>Rm 100</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>The Learning Center</div>
                                 
                                 <div>Rm 100</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>The Writing Center </div>
                                 
                                 <div>Rm 100 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Building 4 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Academic Department Offices for Math &amp; Science</div>
                                 
                                 <div>Rm 231</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Campus Store </div>
                                 
                                 <div>Rm 103</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>The Depot (math and science tutoring) </div>
                                 
                                 <div>Rm 121 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Library</div>
                                 
                                 <div>Rm 202 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Testing / Assessment Center </div>
                                 
                                 <div>Rm 248 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>University of Central Florida</div>
                                 
                                 <div>Rm 244 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           
                           <p><strong>Call an Admissions Coach<br>
                                 <i>Llama a tu consejero personal</i></strong></p>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-1507</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>1800 Denn John Lane<br>Kissimmee, FL 34744
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/osceola.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        <a href="http://www.youvisit.com/tour/valencia/osceola?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="osceola campus virtual tour" height="245" src="../../locations/osceola/youvisit-osceola-245px.jpg" width="245"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/osceola/campus-directory.pcf">©</a>
      </div>
   </body>
</html>